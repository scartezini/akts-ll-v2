#!bin/sh


sudo apt-get update 2>&1 >/dev/null
sudo apt-get -y remove gcc-4.9
sudo apt-get -y remove g++-4.9
sudo apt-get -y install gcc-4.8 g++-4.8 gcc-4.8-plugin-dev

sudo apt-get -y install libsqlite3-0 libsqlite3-dev
sudo apt-get -y install libcutl-1.8 libcutl-dev

sudo apt-get -y install libcppnetlib-dev

sudo ln -s /usr/bin/gcc-4.8 /usr/bin/gcc
sudo ln -s /usr/bin/g++-4.8 /usr/bin/g++

mkdir -p odb-file
cd odb-file

if [ ! -f ./odb-2.4.0.zip ]; then
	wget http://www.codesynthesis.com/download/odb/2.4/odb-2.4.0.zip    
	unzip odb-2.4.0.zip
fi

cd odb-2.4.0
./configure
make
sudo make install
cd ..


if [ ! -f ./libodb-2.4.0.zip ]; then
	wget http://www.codesynthesis.com/download/odb/2.4/libodb-2.4.0.zip
	unzip libodb-2.4.0.zip
fi

cd libodb-2.4.0
./configure
make
sudo make install
cd ..

if [ ! -f ./libodb-sqlite-2.4.0.zip ]; then
	wget http://www.codesynthesis.com/download/odb/2.4/libodb-sqlite-2.4.0.zip
	unzip libodb-sqlite-2.4.0.zip
fi

cd libodb-sqlite-2.4.0
./configure
make
sudo make install
cd ..

cd ..