#include "../../headers/models/entities_models.hpp"
////////////////////////////////////////////////////////////////////////////////
// User
////////////////////////////////////////////////////////////////////////////////
void User::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;

	if (reader.parse(str, parsed)) {
		if (parsed["low_id"] != Json::nullValue)
			id = parsed["low_id"].asInt();
		if (parsed["name"] != Json::nullValue)
			name = parsed["name"].asString();
		if (parsed["cpf"] != Json::nullValue)
			cpf = parsed["cpf"].asString();
		if (parsed["rg"] != Json::nullValue)
			rg = parsed["rg"].asString();
		if (parsed["destiny"] != Json::nullValue)
			destiny = parsed["destiny"].asString();
		if (parsed["email"] != Json::nullValue)
			email = parsed["email"].asString();
		if (parsed["password"] != Json::nullValue)
			password = parsed["password"].asString();
		if (parsed["phone"] != Json::nullValue)
			phone = parsed["phone"].asString();

	} else
		throw std::invalid_argument("[entities_models] user.fromJson");
}

// std::string User::json2high() {
// 	Json::Value json;
// 	Json::FastWriter writer;
//
// 	json["name"] = name;
// 	json["cpf"] = cpf;
// 	json["rg"] = rg;
// 	json["email"] = email;
// 	json["phone"] = phone;
// 	json["destiny"] = destiny;
//
//
// 	std::vector<char> inter = this->value.databaseFormat();
//
// 	json["value"] = Json::Value(Json::arrayValue);
// 	for (uint i = 0; i < inter.size(); i++)
// 		json["value"][i] = (uint)((uint8_t)inter[i]);
// }

std::string User::toJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["low_id"] = id;
	json["name"] = name;
	json["cpf"] = cpf;
	json["rg"] = rg;
	json["destiny"] = destiny;
	json["email"] = email;
	json["password"] = password;
	json["phone"] = phone;

	return writer.write(json);
}

std::string User::toShortJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["low_id"] = id;
	if (name.size() > 50)
		name = name.substr(0, 49);

	json["name"] = name;

	return writer.write(json);
}

bool User::operator==(const User& other) {
	if (!(this->id == other.id && this->name.compare(other.name) == 0 &&
		  this->rg.compare(other.rg) == 0 &&
		  this->destiny.compare(other.destiny) == 0 &&
		  this->email.compare(other.email) == 0 &&
		  this->password.compare(other.password) == 0 &&
		  this->phone.compare(other.phone) == 0 &&
		  this->activated == other.activated))
		return false;
	return true;
}

bool User::operator!=(const User& other) {
	return !(*this == other);
}

////////////////////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////////////////////
void Access::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;

	if (reader.parse(str, parsed)) {
		if (parsed["low_id"] != Json::nullValue)
			id = parsed["low_id"].asInt();

		if (parsed["direction"] != Json::nullValue)
			direction = (Access::DIRECTION)parsed["direction"].asInt();

		if (parsed["time"] != Json::nullValue)
			hora.setAll(parsed["time"].asString());

		if (parsed["denied"] != Json::nullValue)
			denied = parsed["denied"].asInt();

		if (parsed["user_id"] != Json::nullValue) {
			User user(parsed["user_id"].asInt());
			this->setUser(user);
		}

		if (parsed["data_id"] != Json::nullValue) {
			Data data(parsed["data_id"].asInt());
			this->setData(data);
		}

		if (parsed["stuff_id"] != Json::nullValue) {
			Stuff stuff(parsed["stuff_id"].asInt());
			this->setStuff(stuff);
		}

		if (parsed["control_id"] != Json::nullValue) {
			AccessPoint access_point(parsed["control_id"].asInt());
			this->setAccessPoint(access_point);
		}

	} else
		throw std::invalid_argument("[entities_models] access.fromJson");
}

std::string Access::toJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["low_id"] = id;
	json["direction"] = (uint)direction;
	json["timestamp"] = hora.stringFormat();
	json["denied"] = denied;

	if (user != NULL)
		json["lperson"] = user->getId();

	if (data != NULL)
		json["ldata"] = data->getId();

	if (accessPoint != NULL)
		json["laccess_point"] = accessPoint->getId();

	if (stuff != NULL)
		json["lstuff"] = stuff->getId();

	return writer.write(json);
}

bool Access::operator==(const Access& other) {
	if (!(this->id == other.id && this->direction == other.direction &&
		  this->hora == other.hora && this->activated == other.activated))
		return false;

	if (this->user != NULL && other.user != NULL)
		if (this->user != other.user)
			return false;

	if (this->data != NULL && other.data != NULL)
		if (this->data != other.data)
			return false;

	if (this->stuff != NULL && other.stuff != NULL)
		if (this->stuff != other.stuff)
			return false;

	if (this->accessPoint != NULL && other.accessPoint != NULL)
		if (this->accessPoint != other.accessPoint)
			return false;

	return true;
}

bool Access::operator!=(const Access& other) {
	return !(*this == other);
}

////////////////////////////////////////////////////////////////////////////////
// Access Point
////////////////////////////////////////////////////////////////////////////////
AccessPoint::AccessPoint() {
	this->port = 10000;
}

void AccessPoint::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;

	if (reader.parse(str, parsed)) {
		if (parsed["low_id"] != Json::nullValue)
			id = parsed["low_id"].asInt();

		if (parsed["name"] != Json::nullValue)
			name = parsed["name"].asString();

		if (parsed["type"] != Json::nullValue)
			type = this->lowTypeConverter(parsed["type"].asString());

		if (parsed["ipAddress"] != Json::nullValue)
			ipAddress.setIp(parsed["ipAddress"].asString());

		if (parsed["port"] != Json::nullValue)
			port = parsed["port"].asInt();
	} else
		throw std::invalid_argument("[entities_models] accessPoint.fromJson");
}

AccessPoint::ACCESS_POINT_TYPE AccessPoint::lowTypeConverter(std::string type) {
	if (type.compare("door_front_back") == 0) {
		return DOOR_FRONT_BACK;
	}

	else if (type.compare("ticket_gate") == 0) {
		return TICKET_GATE;
	}

	else if (type.compare("door_back") == 0) {
		return DOOR_BACK;
	}

	else if (type.compare("door_front") == 0) {
		return DOOR_FRONT;
	}

	return DOOR_FRONT_BACK;
}


std::string AccessPoint::highTypeConverter(AccessPoint::ACCESS_POINT_TYPE type) {
	if (type == DOOR_FRONT_BACK) {
		return std::string("door_front_back");
	} else if (type == TICKET_GATE) {
		return std::string("ticket_gate");
	} else if (type == DOOR_BACK) {
		return std::string("door_back");
	} else if (type == DOOR_FRONT) {
		return std::string("door_front");
	}

	return std::string("door_front_back");
}

std::string AccessPoint::toJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["low_id"] = id;
	json["name"] = name;
	json["actuator_type"] = type;
	json["activated"] = activated;
	json["ip"] = ipAddress.databaseFormat();
	json["port"] = this->port;

	return writer.write(json);
}


bool AccessPoint::operator==(const AccessPoint& other) {
	if (!(this->id == other.id && this->name.compare(other.name) == 0 &&
		  this->type == other.type && this->ipAddress == other.ipAddress &&
		  this->port == other.port && this->activated == other.activated))
		return false;
	return true;
}

bool AccessPoint::operator!=(const AccessPoint& other) {
	return !(*this == other);
}

////////////////////////////////////////////////////////////////////////////////
// Data Type
////////////////////////////////////////////////////////////////////////////////
DataType::SENSOR_TYPE DataType::lowSensorTypeConverter(std::string sensor_type) {
	if (sensor_type.compare("nfc") == 0) {
		return NFC;
	}

	else if (sensor_type.compare("rfid") == 0) {
		return RFID;
	}

	else if (sensor_type.compare("biometric") == 0) {
		return BIOMETRIC;
	}

	return NFC;
}

void DataType::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;
	;

	if (reader.parse(str, parsed)) {
		if (parsed["id"] != Json::nullValue)
			id = parsed["id"].asInt();
		if (parsed["name"] != Json::nullValue)
			name = parsed["name"].asString();
		if (parsed["sensor"] != Json::nullValue)
			sensor = this->lowSensorTypeConverter(parsed["sensor"].asString());
		if (parsed["model"] != Json::nullValue)
			model = parsed["model"].asString();
	} else
		throw std::invalid_argument("[entities_models] dataType.fromJson");
}
std::string DataType::toJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["id"] = id;
	json["name"] = name;
	json["sensor"] = sensor;
	json["model"] = model;

	return writer.write(json);
}

bool DataType::operator==(const DataType& other) {
	if (!(this->id == other.id && this->name.compare(other.name) == 0 &&
		  this->sensor == other.sensor &&
		  this->model.compare(other.name) == 0 &&
		  this->activated == other.activated))
		return false;
	return true;
}

bool DataType::operator!=(const DataType& other) {
	return !(*this == other);
}

////////////////////////////////////////////////////////////////////////////////
// Data
////////////////////////////////////////////////////////////////////////////////
void Data::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;

	if (reader.parse(str, parsed)) {
		if (parsed["low_id"] != Json::nullValue)
			id = parsed["low_id"].asInt();

		if (parsed["value"] != Json::nullValue) {
			const Json::Value values = parsed["value"];
			std::vector<char> inter;
			for (uint i = 0; i < values.size(); ++i) {
				inter.push_back((char)values[i].asInt());
			}
			Blob value;
			value.setValue(inter, inter.size());
			this->setValue(value);
		}

		if (parsed["user_id"] != Json::nullValue) {
			User user(parsed["user_id"].asInt());
			this->setUser(user);
		}

		if (parsed["type"] != Json::nullValue) {
			DataType dataType(parsed["type"].asInt() + 1);
			this->setDataType(dataType);
		}

		if (parsed["visitor"] != Json::nullValue) {
			visitor = parsed["visitor"].asInt();
		}

	} else
		throw std::invalid_argument("[entities_models] data.fromJson");
}
std::string Data::toJson() {
	Json::Value json;
	Json::FastWriter writer;
	json["low_id"] = id;
	json["visitor"] = visitor;

	if (dataType != NULL)
		json["type"] = this->dataType->getId() - 1;
	if (user != NULL)
		json["user_id"] = this->user->getId();

	std::vector<char> inter = this->value.databaseFormat();

	json["value"] = Json::Value(Json::arrayValue);
	for (uint i = 0; i < inter.size(); i++)
		json["value"][i] = (uint)((uint8_t)inter[i]);

	return writer.write(json);
}

std::string Data::toShortJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["id"] = id;

	std::vector<char> inter = this->value.databaseFormat();
	json["value"] = Json::Value(Json::arrayValue);
	for (uint i = 0; i < inter.size(); i++)
		json["value"][i] = (uint)((uint8_t)inter[i]);

	return writer.write(json);
}

bool Data::operator==(const Data& other) {
	if (this->id == other.id && this->value == other.value &&
		this->activated == other.activated)
		return false;
	return true;
}

bool Data::operator!=(const Data& other) {
	return !(*this == other);
}

////////////////////////////////////////////////////////////////////////////////
// Stuff
////////////////////////////////////////////////////////////////////////////////
void Stuff::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;


	if (reader.parse(str, parsed)) {
		if (parsed["low_id"] != Json::nullValue)
			id = parsed["low_id"].asInt();
		if (parsed["name"] != Json::nullValue)
			name = parsed["name"].asString();
		if (parsed["description"] != Json::nullValue)
			description = parsed["description"].asString();
		if (parsed["serial"] != Json::nullValue)
			serial = parsed["serial"].asString();

		if (parsed["user_id"] != Json::nullValue) {
			User user(parsed["user_id"].asInt());
			this->setUser(user);
		}

	} else
		throw std::invalid_argument("[entities_models] stuff.fromJson");
}

std::string Stuff::toJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["low_id"] = id;
	json["name"] = name;
	json["description"] = description;
	json["serial"] = serial;

	if (user != NULL)
		json["fk_user"] = this->user->getId();

	return writer.write(json);
}

bool Stuff::operator==(const Stuff& other) {
	if (!(this->id == other.id && this->name.compare(other.name) == 0 &&
		  this->description.compare(other.description) == 0 &&
		  this->serial.compare(other.serial) == 0 &&
		  this->activated == other.activated))
		return false;

	if (this->user != NULL && other.user != NULL)
		if (this->user != other.user)
			return false;
	return true;
}

bool Stuff::operator!=(const Stuff& other) {
	return !(*this == other);
}

////////////////////////////////////////////////////////////////////////////////
// Access Point
////////////////////////////////////////////////////////////////////////////////

void Relation_Data_AccessPoint::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;

	if (reader.parse(str, parsed)) {
		if (parsed["id"] != Json::nullValue)
			id = parsed["id"].asInt();
		if (parsed["translated_id"] != Json::nullValue)
			translated_id = parsed["translated_id"].asInt();

		if (parsed["data"] != Json::nullValue) {
			Data data(parsed["data"].asInt());
			this->setData(data);
		}
		if (parsed["access_point"] != Json::nullValue) {
			AccessPoint accessPoint(parsed["accessPoint"].asInt());
			this->setAccessPoint(accessPoint);
		}

	} else
		throw std::invalid_argument("[entities_models] "
									"relation_data_accessPoint.fromJson");
}
std::string Relation_Data_AccessPoint::toJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["id"] = id;
	json["translated_id"] = translated_id;

	if (data != NULL)
		json["fk_data"] = this->data->getId();

	if (accessPoint != NULL)
		json["fk_access_point"] = this->accessPoint->getId();

	return writer.write(json);
}

bool Relation_Data_AccessPoint::operator==(
	const Relation_Data_AccessPoint& other) {
	if (!(this->id == other.id && this->translated_id == other.translated_id))
		return false;

	if (this->data != NULL && other.data != NULL)
		if (this->data != other.data)
			return false;

	if (this->accessPoint != NULL && other.accessPoint != NULL)
		if (this->accessPoint != other.accessPoint)
			return false;
	return true;
}

bool Relation_Data_AccessPoint::operator!=(
	const Relation_Data_AccessPoint& other) {
	return !(*this == other);
}
////////////////////////////////////////////////////////////////////////////////
// Relation_User_AccessPoint
////////////////////////////////////////////////////////////////////////////////
void Relation_User_AccessPoint::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;

	if (reader.parse(str, parsed)) {
		if (parsed["id"] != Json::nullValue)
			id = parsed["id"].asInt();
		if (parsed["period"] != Json::nullValue)
			period = parsed["period"].asInt();
		if (parsed["start"] != Json::nullValue)
			start.setAll(parsed["start"].asString());

		if (parsed["user"] != Json::nullValue) {
			User user(parsed["user"].asInt());
			this->setUser(user);
		}

		if (parsed["access_point"] != Json::nullValue) {
			AccessPoint ap(parsed["access_point"].asInt());
			this->setAccessPoint(ap);
		}

		// std::vector<char> inter;
		// if (parsed["sunday"] != Json::nullValue) {
		// 	const Json::Value values = parsed["sunday"];
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = (char)values[i].asInt();
		// 	}
		// } else
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = '1';
		// 	}
		// this->setSundey(inter);
		//
		// if (parsed["monday"] != Json::nullValue) {
		// 	const Json::Value values = parsed["monday"];
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = (char)values[i].asInt();
		// 	}
		// } else
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = '1';
		// 	}
		// this->setMonday(inter);
		//
		// if (parsed["tuesday"] != Json::nullValue) {
		// 	const Json::Value values = parsed["tuesday"];
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = (char)values[i].asInt();
		// 	}
		// } else
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = '1';
		// 	}
		// this->setTuesday(inter);
		//
		// if (parsed["wednesday"] != Json::nullValue) {
		// 	const Json::Value values = parsed["wednesday"];
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = (char)values[i].asInt();
		// 	}
		// } else
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = '1';
		// 	}
		// this->setWednesday(inter);
		//
		// if (parsed["thursday"] != Json::nullValue) {
		// 	const Json::Value values = parsed["thursday"];
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = (char)values[i].asInt();
		// 	}
		// } else
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = '1';
		// 	}
		// this->setThursday(inter);
		//
		// if (parsed["friday"] != Json::nullValue) {
		// 	const Json::Value values = parsed["friday"];
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = (char)values[i].asInt();
		// 	}
		// } else
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = '1';
		// 	}
		// this->setFriday(inter);
		//
		// if (parsed["saturday"] != Json::nullValue) {
		// 	const Json::Value values = parsed["saturday"];
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = (char)values[i].asInt();
		// 	}
		// } else
		// 	for (uint i = 0; i < 24; ++i) {
		// 		inter[i] = '1';
		// 	}
		// this->setSaturday(inter);

	} else
		throw std::invalid_argument("[entities_models] "
									"relation_user_accessPoint.fromJson");
}

std::string Relation_User_AccessPoint::toJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["low_id"] = id;
	json["period"] = period;
	json["start"] = start.databaseFormat();
	json["user"] = user->toJson();
	json["accessPoint"] = accessPoint->toJson();

	json["sunday"] = Json::Value(Json::arrayValue);
	json["monday"] = Json::Value(Json::arrayValue);
	json["tuesday"] = Json::Value(Json::arrayValue);
	json["wednesday"] = Json::Value(Json::arrayValue);
	json["thursday"] = Json::Value(Json::arrayValue);
	json["friday"] = Json::Value(Json::arrayValue);
	json["saturday"] = Json::Value(Json::arrayValue);

	for (uint i = 0; i < 24; i++) {
		json["sunday"][i] = (uint)((uint8_t)this->sunday[i]);
		json["monday"][i] = (uint)((uint8_t)this->monday[i]);
		json["tuesday"][i] = (uint)((uint8_t)this->tuesday[i]);
		json["wednesday"][i] = (uint)((uint8_t)this->wednesday[i]);
		json["thursday"][i] = (uint)((uint8_t)this->thursday[i]);
		json["friday"][i] = (uint)((uint8_t)this->friday[i]);
		json["saturday"][i] = (uint)((uint8_t)this->saturday[i]);
	}

	return writer.write(json);
}

bool Relation_User_AccessPoint::operator==(
	const Relation_User_AccessPoint& other) {
	if (!(this->id == other.id && this->period == other.period &&
		  this->start == other.start))
		return false;

	if (this->user != NULL && other.user != NULL)
		if (this->user != other.user)
			return false;

	if (this->accessPoint != NULL && other.accessPoint != NULL)
		if (this->accessPoint != other.accessPoint)
			return false;
	return true;
}

bool Relation_User_AccessPoint::operator!=(
	const Relation_User_AccessPoint& other) {
	return !(*this == other);
}


////////////////////////////////////////////////////////////////////////////////
// RegisterKit
////////////////////////////////////////////////////////////////////////////////
RegisterKit::RegisterKit() {
	this->port = 10000;
}

void RegisterKit::fromJson(std::string str) {
	Json::Value parsed;
	Json::Reader reader;

	if (reader.parse(str, parsed)) {
		if (parsed["low_id"] != Json::nullValue)
			id = parsed["low_id"].asInt();

		if (parsed["name"] != Json::nullValue)
			name = parsed["name"].asString();

		if (parsed["ipAddress"] != Json::nullValue)
			ipAddress.setIp(parsed["ipAddress"].asString());

		if (parsed["port"] != Json::nullValue)
			port = parsed["port"].asInt();
	} else
		throw std::invalid_argument("[entities_models] accessPoint.fromJson");
}


std::string RegisterKit::toJson() {
	Json::Value json;
	Json::FastWriter writer;

	json["low_id"] = id;
	json["name"] = name;
	json["activated"] = activated;
	json["ip"] = ipAddress.databaseFormat();
	json["port"] = this->port;

	return writer.write(json);
}

bool RegisterKit::operator==(const RegisterKit& other) {
	if (!(this->id == other.id && this->name.compare(other.name) == 0 &&
		  this->ipAddress == other.ipAddress && this->port == other.port &&
		  this->activated == other.activated))
		return false;
	return true;
}

bool RegisterKit::operator!=(const RegisterKit& other) {
	return !(*this == other);
}
