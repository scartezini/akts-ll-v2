#include "../../headers/models/base_models.hpp"

void BaseModel::saveConfigs() {
	io::man.out_log(Msg()) << "Saving the config, on database";
	Json::Value json = io::configs.json;
	// open the text
	Json::StyledWriter writer;
	string json_s = writer.write(json);

	this->config.json = json_s;

	if (this->config.id != 1) {
		this->config.id = 1;
		io::man.out_log(Msg()) << "Is a new configurarion";
		database_monkey::persist(this->config);
	} else {
		io::man.out_log(Msg()) << "Is a config to update";
		database_monkey::update(this->config);
	}
}

string BaseModel::createConfigFile() {
	bool start;
	string body = "{}";

	typedef odb::query<Config> query;

	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction trans(db->begin());

	try {
		for (auto &e : db->query<Config>(query::id == 1)) {
			body = e.json;
			this->config = e;
			this->config.id = 1;
		}
	} catch (...) {
	}

	trans.commit();
	db.release();

	return body;
}
