#include "../../headers/models/system_models.hpp"
#include "../models/base_models.hpp"

Action::Action() {
	this->exec = 0;
	this->success = false;

	Timestamp now;
	now.now();

	this->exec_time = now.transformInSeconds();

	this->table = Action::OTHER;
	this->type = Action::POST;
	this->dest = Action::LOCAL;
}

void Action::setSuccess() {
	this->success = true;
}

void Action::setExecuted() {
	std::unique_ptr<odb::database> db(database_monkey::open_database());
	const uint max_retries = 5;

	if (this->success) {
		for (uint retry_count = 0;; retry_count++) {
			try {
				transaction t(db->begin());
				db->erase(*this);
				t.commit();
				break;
			} catch (odb::recoverable &e) {
				if (retry_count > max_retries)
					std::cout << "ERRO SET EXECUTED" << std::endl;
				else
					continue;
			}
		}
	} else {
		this->running = false;

		for (uint retry_count = 0;; retry_count++) {
			try {
				transaction t(db->begin());
				db->update(*this);
				t.commit();
				break;
			} catch (odb::recoverable &e) {
				if (retry_count > max_retries)
					std::cout << "ERRO SET EXECUTED" << std::endl;
				else
					continue;
			}
		}
	}
	db.release();
}

Log::Log(std::string line) {
	this->line = line;
	std::string inter_line = line;

	while (inter_line.length() > 0) {
		std::size_t found = inter_line.rfind(";");
		// se existe split e add

		// se nao
		this->args.push_back(inter_line);
		inter_line = "";
	}
}

std::string Log::parse() {
	std::stringstream ss;
	try {
		ss << this->decodeLog(args[1]);
	} catch (std::invalid_argument error) {
		ss.str();
		ss << "log invalido, error: " << error.what();
		ss << " valor = " << this->line;
	}
	std::stringstream final;
	final << "hora:" << args[0] << " " << ss.str();
	return final.str();
}

std::string Log::decodeLog(std::string arg) {
	return "logado";
}

Config::Config() {
	this->json = "{}";
	this->id = 20;
}

Config::Config(std::string json) {
	this->json = json;
	this->id = 20;
}
