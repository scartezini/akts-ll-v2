#include "../../headers/io_core/Sensor.hpp"

void Sensor::startAll() {
	io::man.out_log(Msg()) << "Starting Sensors";
	for (uint i = 0; i < io::configs.sensors_types.size(); i++) {
		Sensor::TYPE type = (Sensor::TYPE)io::configs.sensors_types[i];
		uint port = io::configs.sensors_ports[i];
		Sensor::add_sensor(type, port);
	}
}

void Sensor::add_sensor(TYPE type, uint port) {
	Sensor *sensor;
	switch (type) {
		case NFC:
			sensor = (Sensor *)new SensorNfc(port);
			io::sensors.push_back(sensor);
			break;
		default:
			// TODO finalizar tudo e sair dando o error de sensor errado
			break;
	}
}

void Sensor::endAll() {
	for (uint i = 0; i < io::sensors.size(); i++) {
		delete io::sensors[i];
	}
}

SensorNfc::SensorNfc(uint) {
}

void SensorNfc::lock() {
}
void SensorNfc::unlock() {
}

bool SensorNfc::allOK() {
	return true;
}

bool SensorNfc::hasData() {
	return true;
}

Data SensorNfc::getData() {
	Data data;
	string json = "{\"id\":0,\"value\":[188,196,175,213],\"user_id\":0,"
				  "\"type\":1}";

	data.fromJson(json);
	return data;
}

void SensorNfc::restart() {
}
void SensorNfc::waitRemove() {
}
