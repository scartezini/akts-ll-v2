#include "../../headers/controllers/system_controller.hpp"

string HttpRoutes::decodeRoute(string ori_route, uint num_params,
							   vector<string> &params) {
	std::size_t found = ori_route.rfind("/");
	if (found == ori_route.size() - 1)
		ori_route = ori_route.substr(0, ori_route.size() - 1);

	string acc_route, param;
	for (uint i = 0; i < num_params; i++) {
		std::size_t found = ori_route.rfind("/");
		if (found == std::string::npos) {
			throw std::invalid_argument("");
		}

		acc_route = ori_route.substr(0, found);
		param = ori_route.substr(found + 1);

		params.push_back(param);
		ori_route = acc_route;
	}

	std::reverse(params.begin(), params.end());
	return ori_route;
}

HttpServer::response HttpRoutes::errorResponse(uint error_value) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)error_value;
	string body = decodeError(error_value);
	return HttpServer::response::stock_reply(status, body);
}

string HttpRoutes::decodeError(uint error_value) {
	switch (error_value) {
		case 400:
			return HttpError::textBadRequest();
		case 401:
			return HttpError::textUnauthorized();
		case 404:
			return HttpError::textNotFound();
		case 405:
			return HttpError::textMethodNotAllowed();

		case 500:
			return HttpError::textInternalError();
		case 501:
			return HttpError::textNotImplemented();
		case 503:
			return HttpError::textServiceUnavailable();
		case 505:
			return HttpError::textHTTPVersionNotSupported();
	}

	return HttpError::textInternalError();
}

#ifndef DEBUGER
HttpServer::response OtherRoutes::dropAll(vector<string> &params,
										  HttpServer::request const &req) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;

	Json::Value parsed;
	Json::Reader reader;

	stringstream file;
	if (reader.parse(req.body, parsed)) {
		if (parsed["database"] != Json::nullValue)
			;
		file << parsed["database"].asString();
	}

	std::remove(io::configs.database_name.c_str());

	stringstream msg;
	msg << "deletando database" << io::configs.database_name.c_str();
	io::output.printMsgBox("main_messages", msg.str());
	msg.clear();

	database_monkey::create_database();
	return HttpServer::response::stock_reply(status, "{\"Drop\":true}");
}
#endif

HttpServer::response OtherRoutes::ping(vector<string> &,
									   HttpServer::request const &) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;
	return HttpServer::response::stock_reply(status, "{\"mid\":true}");
}

HttpServer::response OtherRoutes::timeget(vector<string> &,
										  HttpServer::request const &) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;
	Timestamp time_local;
	time_local.now();

	stringstream ss;
	ss << "{" << endl;
	ss << "\t\"time\":\"" << time_local.stringFormat() << "\"" << endl;
	ss << "}" << endl;
	return HttpServer::response::stock_reply(status, ss.str());
}


void OtherRoutes::actionPanic(bool on) {
	Action action;
	action.type = Action::POST;
	action.table = Action::ACCESS_POINT;
	action.dest = Action::LOW;

	action.id_1 = on;

	queues::actions.push(action);
}


HttpServer::response OtherRoutes::panicOn(vector<string> &params,
										  HttpServer::request const &req) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;

	Timestamp time_local;
	time_local.now();

	OtherRoutes::actionPanic(true);

	stringstream ss;
	ss << "{" << endl;
	ss << "\t\"time\":\"" << time_local.stringFormat() << "\"" << endl;
	ss << "}" << endl;
	return HttpServer::response::stock_reply(status, ss.str());
}

HttpServer::response OtherRoutes::panicOff(vector<string> &params,
										   HttpServer::request const &req) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;
	Timestamp time_local;
	time_local.now();

	OtherRoutes::actionPanic(false);

	stringstream ss;
	ss << "{" << endl;
	ss << "\t\"time\":\"" << time_local.stringFormat() << "\"" << endl;
	ss << "}" << endl;
	return HttpServer::response::stock_reply(status, ss.str());
}

void OtherRoutes::actionSyncLow(uint id, std::string local_route) {
	Action action;
	action.table = Action::ROUTES;
	action.type = Action::SYNC;
	action.dest = Action::LOW;
	action.route = local_route;
	action.id_1 = id;

	queues::actions.push(action);
}

void OtherRoutes::actionSyncHigh() {
	Action action;
	action.table = Action::ROUTES;
	action.type = Action::SYNC;
	action.dest = Action::HIGH;

	queues::actions.push(action);
}

HttpServer::response OtherRoutes::synchronize(std::vector<string> &,
											  HttpServer::request const &) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;

	Timestamp time_local;
	time_local.now();


	// low pedir accessos - ja postar no high
	// OtherRoutes::actionSyncHigh();
	//
	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());

	stringstream ss;

	for (auto &e : db->query<AccessPoint>(true)) {
		ss << "http://" << e.getIpAddress()->databaseFormat() << ":10000/";
		OtherRoutes::actionSyncLow(e.getId(), ss.str());
		ss.str("");
	}

	t.commit();
	db.release();

	ss << "{" << endl;
	ss << "\t\"time\":\"" << time_local.stringFormat() << "\"" << endl;
	ss << "}" << endl;
	return HttpServer::response::stock_reply(status, ss.str());
}

void OtherRoutes::actionLoginHigh() {
	Action action;
	action.table = Action::ROUTES;
	action.type = Action::LOGIN;
	action.dest = Action::HIGH;

	queues::actions.push(action);
}
