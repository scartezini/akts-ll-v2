#include "../../headers/controllers/system_actors_controller.hpp"

void UserActor::mdelete(Action& action) {
	uint _id = action.id_1;

	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	stringstream ss;
	for (auto& e : db->query<Relation_User_AccessPoint>(
			 odb::query<Relation_User_AccessPoint>::user == _id)) {
		ss << "http://" << e.getAccessPoint().getIpAddress()->databaseFormat()
		   << ":10000";
		ss << "/user/" << _id;

		io::output.printMsgBox("main_messages", ss.str());

		BaseActor::actionDelete(Action::LOW, ss.str());
		ss.str("");
	}

	action.setSuccess();

	t.commit();
	db.release();
}

void DataActor::propagate(Action& action) {
	uint _id = action.id_1;

	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	unique_ptr<Data> data(db->query_one<Data>(odb::query<Data>::id == _id));
	std::string body;
	body = data->toJson();

	stringstream msg;
	msg << body;
	io::output.printMsgBox("main_messages", msg.str());
	msg.str("");

	for (auto& e : db->query<Relation_User_AccessPoint>(
			 odb::query<Relation_User_AccessPoint>::user ==
			 data->getUser().getId())) {
		stringstream ss;
		ss << "http://" << e.getAccessPoint().getIpAddress()->databaseFormat()
		   << ":" << e.getAccessPoint().port << "/data";
		io::output.printMsgBox("main_messages", ss.str());

		BaseActor::actionPost(Action::LOW, ss.str(), body);

		ss.str("");
	}

	action.setSuccess();
	t.commit();
	db.release();
}

void DataActor::mdelete(Action& action) {
	uint _id = action.id_1;

	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	stringstream ss;

	unique_ptr<Data> data(db->query_one<Data>(odb::query<Data>::id == _id));

	for (auto& e : db->query<Relation_Data_AccessPoint>(
			 odb::query<Relation_Data_AccessPoint>::data == _id)) {
		ss << "http://" << e.getAccessPoint().getIpAddress()->databaseFormat()
		   << ":10000";
		ss << "/data/" << data->getId() << "/"
		   << data->getDataType().getId() - 1;

		io::output.printMsgBox("main_messages", ss.str());
		BaseActor::actionDelete(Action::LOW, ss.str());

		ss.str("");
	}

	action.setSuccess();


	t.commit();
	db.release();
}


void BaseActor::actionPost(Action::DEST dest, std::string local_route,
						   std::string body) {
	Action action;
	action.table = Action::ROUTES;
	action.type = Action::POST;
	action.dest = dest;
	action.route = local_route;
	action.body = body;

	queues::actions.push(action);
}

void BaseActor::actionDelete(Action::DEST dest, std::string local_route) {
	Action action;
	action.table = Action::ROUTES;
	action.type = Action::DELETE;
	action.dest = dest;
	action.route = local_route;

	queues::actions.push(action);
}


void BaseActor::actionPut(Action::DEST dest, std::string local_route,
						  std::string body) {
	Action action;
	action.table = Action::ROUTES;
	action.type = Action::PUT;
	action.dest = dest;
	action.route = local_route;
	action.body = body;

	queues::actions.push(action);
}


template <typename E = Entity>
void BaseActor::post(Action& action, std::string route) {
	bool start = true;

	int _id = action.id_1;
	typedef odb::query<E> query;
	string body = "";

	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	odb::result<E> r(db->query<E>(odb::query<E>::id == _id));

	if (!r.empty()) {
		E entiny;
		auto i(r.begin());
		i.load(entiny);

		body = entiny.toJson();
		start = false;
	}

	t.commit();
	db.release();

	if (start == true)
		return;

	stringstream ss;
	ss << "http://" << io::configs.high_ip << ":1200" << route;
	io::output.printMsgBox("main_messages", ss.str());

	BaseActor::actionPost(action.dest, ss.str(), body);

	action.setSuccess();
}

template <typename E = Entity>
void BaseActor::put(Action& action, std::string route) {
	bool start = true;

	int _id = action.id_1;
	typedef odb::query<E> query;
	string body;

	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	for (auto& e : db->query<E>(query::id == _id)) {
		start = false;
		body = e.toJson();
	}

	t.commit();
	db.release();

	if (start == true)
		return;

	stringstream ss;
	ss << "http://" << io::configs.high_ip << ":1200" << route;
	io::output.printMsgBox("main_messages", ss.str());

	BaseActor::actionPut(action.dest, ss.str(), body);

	action.setSuccess();
}

void BaseActor::put_http(Action& action) {
	std::string local_route = action.route;
	std::string body = action.body;

	http::client client;
	http::client::request request(local_route);

	if (action.dest == Action::HIGH) {
		// Adicionar o cookie no header
		request << boost::network::header("Cookie", io::configs.cookie);

		client::options options;
		options.follow_redirects(true)
			.cache_resolved(true)
			.io_service(boost::make_shared<boost::asio::io_service>())
			.openssl_certificate("/tmp/my-cert")
			.openssl_verify_path("/tmp/ca-certs");
		client = http::client(options);
	}

	stringstream ss;
	ss << "application/json";
	http::client::response response = client.put(request, body, ss.str());

	if (response.status() == (HttpServer::response::status_type)200)
		action.setSuccess();
	else if (action.dest == Action::HIGH &&
			 response.status() == (HttpServer::response::status_type)401) {
		OtherRoutes::actionLoginHigh();
	}
}


void BaseActor::post_http(Action& action) {
	std::string local_route = action.route;
	std::string body = action.body;

	http::client client;
	http::client::request request(local_route);

	if (action.dest == Action::HIGH) {
		// Adicionar o cookie no header
		request << boost::network::header("Cookie", io::configs.cookie);

		client::options options;
		options.follow_redirects(true)
			.cache_resolved(true)
			.io_service(boost::make_shared<boost::asio::io_service>())
			.openssl_certificate("/tmp/my-cert")
			.openssl_verify_path("/tmp/ca-certs");
		client = http::client(options);
	}


	stringstream ss;
	ss << "application/json";
	http::client::response response = client.post(request, body, ss.str());

	if (response.status() == (HttpServer::response::status_type)200)
		action.setSuccess();
	else if (action.dest == Action::HIGH &&
			 response.status() == (HttpServer::response::status_type)401) {
		OtherRoutes::actionLoginHigh();
	}
}

void BaseActor::mdelete_http(Action& action) {
	http::client client;

	http::client::request request(action.route);
	if (action.dest == Action::HIGH) {
		// Adicionar o cookie no header
		request << boost::network::header("Cookie", io::configs.cookie);

		client::options options;
		options.follow_redirects(true)
			.cache_resolved(true)
			.io_service(boost::make_shared<boost::asio::io_service>())
			.openssl_certificate("/tmp/my-cert")
			.openssl_verify_path("/tmp/ca-certs");
		client = http::client(options);
	}

	io::output.printMsgBox("main_messages", "DELETE: " + action.route);

	http::client::response response = client.delete_(request);

	if (response.status() == (HttpServer::response::status_type)200)
		action.setSuccess();
	else if (action.dest == Action::HIGH &&
			 response.status() == (HttpServer::response::status_type)401) {
		OtherRoutes::actionLoginHigh();
	}
}

std::string BaseActor::get_http(Action::DEST dest, std::string route) {
	http::client client;
	http::client::request request(route);

	if (dest == Action::HIGH) {
		// Adicionar o cookie no header
		request << boost::network::header("Cookie", io::configs.cookie);

		client::options options;
		options.follow_redirects(true)
			.cache_resolved(true)
			.io_service(boost::make_shared<boost::asio::io_service>())
			.openssl_certificate("/tmp/my-cert")
			.openssl_verify_path("/tmp/ca-certs");
		client = http::client(options);
	}

	http::client::response response = client.get(request);

	if (dest == Action::HIGH &&
		response.status() == (HttpServer::response::status_type)401) {
		OtherRoutes::actionLoginHigh();
	}

	return body(response);
}

/////////////////
// AccessPoint
/////////////////
void AccessPointActor::post(Action& action) {
	BaseActor::post<AccessPoint>(action, "/api/access_point");
}

void AccessPointActor::put(Action& action) {
	bool start = true;

	AccessPoint accessPoint;
	std::string body;

	int _id = action.id_1;
	typedef odb::query<AccessPoint> query;

	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	for (auto& e : db->query<AccessPoint>(query::id == _id)) {
		start = false;
		accessPoint = e;
	}

	t.commit();
	db.release();

	if (start == true)
		return;

	Json::Value json;
	Json::FastWriter writer;
	json["my_name"] = accessPoint.getName();
	json["my_type"] = accessPoint.highTypeConverter(accessPoint.getType());
	body = writer.write(json);

	stringstream ss;
	ss << "http://" << accessPoint.getIpAddress()->databaseFormat() << ":"
	   << accessPoint.port << "/config";

	BaseActor::actionPut(action.dest, ss.str(), body);
	action.setSuccess();
}
////////////////
// RegisterKit
///////////////
void RegisterKitActor::put(Action& action) {
	bool start = true;

	RegisterKit registerKit;
	std::string body;

	int _id = action.id_1;
	typedef odb::query<RegisterKit> query;

	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	for (auto& e : db->query<RegisterKit>(query::id == _id)) {
		start = false;
		registerKit = e;
	}

	t.commit();
	db.release();

	if (start == true)
		return;

	Json::Value json;
	Json::FastWriter writer;
	json["my_name"] = registerKit.getName();
	body = writer.write(json);

	stringstream ss;
	ss << "http://" << registerKit.getIpAddress()->databaseFormat() << ":"
	   << registerKit.port << "/config";

	BaseActor::actionPut(action.dest, ss.str(), body);
	action.setSuccess();
}

void RegisterKitActor::post(Action& action) {
	BaseActor::post<RegisterKit>(action, "/api/kit");
}

///////////////
// Access
//////////////
void AccessActor::post(Action& action) {
	BaseActor::post<Access>(action, "/api/access");
}

void AccessActor::get(std::string route) {
	stringstream ss;
	ss << route;
	ss << "access/last";

	std::string response;
	while (!((response = BaseActor::get_http(Action::LOW, ss.str())).empty())) {
		Access access = Access();
		access.fromJson(response);

		uint id = database_monkey::persist(access);
		access.setId(id);

		AccessLowRoutes::saveAccessOnHigh(id);
		response.clear();
	}
}

void RelationAccessPointUserActor::getIds(uint& _id1, uint& _id2, uint _id) {
	bool start = true;

	typedef odb::query<Relation_User_AccessPoint> query;
	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	for (auto& e : db->query<Relation_User_AccessPoint>(query::id == _id)) {
		start = false;
		_id1 = e.getUser().getId();
		_id2 = e.getAccessPoint().getId();
	}

	t.commit();
	db.release();

	if (start)
		throw std::invalid_argument("Error id nao encontrado");
}

AccessPoint RelationAccessPointUserActor::getIp(uint _id) {
	AccessPoint accessPoint;
	bool start = true;
	string ip;

	typedef odb::query<AccessPoint> query;
	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	odb::result<AccessPoint> r(db->query<AccessPoint>(query::id == _id));

	if (!r.empty()) {
		auto i(r.begin());
		i.load(accessPoint);

		start = false;
	}

	t.commit();
	db.release();

	if (start)
		throw std::invalid_argument("Error id nao encontrado");

	return accessPoint;
}

void RelationAccessPointUserActor::saveDatas(
	uint user_id, AccessPoint& ap) throw(odb::exception) {
	string body;
	stringstream ss;
	ss << "http://" << ap.getIpAddress()->databaseFormat() << ":" << ap.port
	   << "/data";
	io::output.printMsgBox("main_messages", ss.str());


	typedef odb::query<Data> query;
	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());

	for (auto& e : db->query<Data>(query::user == user_id)) {
		body = e.toJson();

		stringstream msg;
		msg << body;
		io::output.printMsgBox("main_messages", msg.str());
		msg.str("");
		BaseActor::actionPost(Action::LOW, ss.str(), body);

		Relation_Data_AccessPoint rdata_ap = Relation_Data_AccessPoint();
		rdata_ap.setData(e);
		rdata_ap.setAccessPoint(ap);

		db->persist(rdata_ap);
	}

	t.commit();
	db.release();
}

void RelationAccessPointUserActor::saveUser(uint _id, AccessPoint& ap) {
	string body = "";
	bool start = true;

	typedef odb::query<User> query;
	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());
	odb::result<User> r(db->query<User>(query::id == _id));

	if (!r.empty()) {
		User user;
		auto i(r.begin());
		i.load(user);
		body = user.toShortJson();
		start = false;
	}

	t.commit();
	db.release();

	if (start)
		throw std::invalid_argument("Error id nao encontrado");

	stringstream ss;
	ss << "http://" << ap.getIpAddress()->databaseFormat() << ":" << ap.port
	   << "/user";

	io::output.printMsgBox("main_messages", ss.str());
	BaseActor::actionPost(Action::LOW, ss.str(), body);
	ss.str("");
}

void RelationAccessPointUserActor::post(Action& action) {
	uint _id = action.id_1;
	uint _id1 = 0, _id2 = 0;

	RelationAccessPointUserActor::getIds(_id1, _id2, _id);
	AccessPoint ap = RelationAccessPointUserActor::getIp(_id2);
	RelationAccessPointUserActor::saveUser(_id1, ap);
	RelationAccessPointUserActor::saveDatas(_id1, ap);

	action.setSuccess();
}

void RelationAccessPointUserActor::mdelete(Action& action) {
	uint _id = action.id_1;

	unique_ptr<odb::database> db(database_monkey::open_database());
	odb::transaction t(db->begin());


	typedef odb::query<Relation_User_AccessPoint> query;

	for (auto& ruap : db->query<Relation_User_AccessPoint>(query::id == _id)) {
		ruap.activated = false;
		db->update(ruap);

		User user = ruap.getUser();

		stringstream ss;
		ss << "http://"
		   << ruap.getAccessPoint().getIpAddress()->databaseFormat()
		   << ":10000";
		ss << "/user/" << user.getId();

		io::output.printMsgBox("main_messages", ss.str());
		BaseActor::actionDelete(Action::LOW, ss.str());
		ss.str("");

		for (auto& e : db->query<Data>(odb::query<Data>::user == user.getId() &&
									   odb::query<Data>::activated == true)) {
			ss << "http://"
			   << ruap.getAccessPoint().getIpAddress()->databaseFormat()
			   << ":10000";
			ss << "/data/" << e.getId() << "/" << e.getDataType().getId() - 1;

			io::output.printMsgBox("main_messages", ss.str());
			BaseActor::actionDelete(Action::LOW, ss.str());
			ss.str("");
		}

		action.setSuccess();
	}


	t.commit();
	db.release();
}

void OtherActor::setOnline(Action& action) {
	stringstream msg;
	msg << "chegou aqui";
	io::output.printMsgBox("main_messages", msg.str());
	msg.str("");

	msg << "{\"mid\":true}";
	stringstream ss;
	ss << "http://" << io::configs.high_ip << ":1200"
	   << "/api/ping";
	io::output.printMsgBox("main_messages", ss.str());

	BaseActor::actionPost(Action::HIGH, ss.str(), msg.str());
	action.setSuccess();
}

void OtherActor::syncLow(Action& action) {
	std::string route = action.route;
	uint ap_id = action.id_1;

	AccessActor::get(route);

	stringstream ss;
	ss << route;
	ss << "dropall";

	BaseActor::actionDelete(Action::LOW, ss.str());

	ss.str("");

	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());

	for (auto& e : db->query<Relation_User_AccessPoint>(
			 odb::query<Relation_User_AccessPoint>::accessPoint == ap_id)) {
		// ja ta salvando as datas do user
		RelationAccessPointUserHighRoutes::saveRelationAccessPointUserOnLow(
			e.getId());
	}

	t.commit();
	db.release();

	action.setSuccess();
}

Json::Value OtherActor::decodeJsonOfSync(std::string body) {
	Json::Value parsed;
	Json::Reader reader;

	Json::Value values;
	if (reader.parse(body, parsed)) {
		if (parsed["entities"] != Json::nullValue) {
			values = parsed["entities"];
		}
	}

	return values;
}

/////////////////////////////////////
/// SYNC HIGH
////////////////////////////////////
void OtherActor::syncDiffUserHigh(Json::Value& json) {
	Json::FastWriter writer;

	User atual;
	User user;
	user.fromJson(writer.write(json["user"]));

	uint id_user = json["low_id"].asInt();

	typedef odb::query<User> query;
	typedef odb::result<User> result;

	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());

	result r(db->query<User>(query::id == id_user && query::activated == true));

	std::string high_id = json["high_id"].asString();
	stringstream ss;
	ss << "/api/person/" << high_id;

	if (r.empty()) {
		user.setId(db->persist(user));
		Action action;
		action.id_1 = user.getId();
		BaseActor::put<User>(action, ss.str());
	} else {
		result::iterator i(r.begin());
		i.load(atual);

		// se o usuario do high for diferente do que eu tenho atualmente
		if (user != atual) {
			Action action;
			action.id_1 = atual.getId();
			BaseActor::put<User>(action, ss.str());
		}
		ss.str("");
	}

	t.commit();
	db.release();
}

void OtherActor::syncDiffDataHigh(Json::Value& json) {
	Json::FastWriter writer;
	stringstream ss;
	stringstream route;
	route << "/api/data/";
	uint id_user = json["low_id"].asInt();

	std::vector<Data> datas;
	EntityFunc::vectorFromJson<Data>("datas", writer.write(json), datas);

	typedef odb::query<Data> query;
	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());

	odb::result<Data> r(
		db->query<Data>(query::user == id_user && query::activated == true));

	bool saved;
	if (r.empty())
		for (Data data : datas) {
			ss << route << data.getId();	// id que esta no high
			data.setId(db->persist(data));  // id do low
			Action action;					// atualizando o high com novo id
			action.id_1 = data.getId();
			BaseActor::put<Data>(action, ss.str());
			ss.str("");
		}
	else {
		Data atual;
		for (Data data : datas) {
			saved = false;
			for (auto i(r.begin()); i != r.end(); ++i) {
				i.load(atual);
				if (data.getId() == atual.getId()) {
					saved = true;
					// se nao tiver coerente o bd do mid comanda
					if (data != atual) {
						ss << route << atual.getId();
						Action action;
						action.id_1 = atual.getId();
						BaseActor::put<Data>(action, ss.str());
						ss.str("");
					}
				}
			}

			if (saved == false) {				// salva data que nao tenho
				ss << route << data.getId();	// id que esta no high
				data.setId(db->persist(data));  // id do low
				Action action;  // atualizando o high com novo id
				action.id_1 = data.getId();
				BaseActor::put<Data>(action, ss.str());
				ss.str("");
			}
		}
	}

	t.commit();
	db.release();
	route.str("");
}

void OtherActor::syncDiffStuffHigh(Json::Value& json) {
	Json::FastWriter writer;
	stringstream ss;
	stringstream route;
	route << "/api/stuff/";
	uint id_user = json["low_id"].asInt();

	std::vector<Stuff> stuffs;
	EntityFunc::vectorFromJson<Stuff>("stuffs", writer.write(json), stuffs);

	typedef odb::query<Stuff> query;
	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());

	odb::result<Stuff> r(
		db->query<Stuff>(query::user == id_user && query::activated == true));

	bool saved;
	if (r.empty()) {  // salvo todas as stuffs
		for (Stuff stuff : stuffs) {
			ss << route << stuff.getId();
			stuff.setId(db->persist(stuff));  // id do low
			Action action;					  // atualizando o high com novo id
			action.id_1 = stuff.getId();
			BaseActor::put<Stuff>(action, ss.str());
			ss.str("");
		}
	} else {
		Stuff atual;
		for (Stuff stuff : stuffs) {
			saved = false;
			for (auto i(r.begin()); i != r.end(); ++i) {
				i.load(atual);
				if (stuff.getId() == atual.getId()) {
					saved = true;
					// se nao tiver coerente o bd do mid comanda
					if (stuff != atual) {
						ss << route << atual.getId();
						Action action;
						action.id_1 = atual.getId();
						BaseActor::put<Stuff>(action, ss.str());
						ss.str("");
					}
				}
			}

			if (saved == false) {				  // salva stuff que nao tenho
				ss << route << stuff.getId();	 // id que esta no high
				stuff.setId(db->persist(stuff));  // id do low
				Action action;  // atualizando o high com novo id
				action.id_1 = stuff.getId();
				BaseActor::put<Stuff>(action, ss.str());
				ss.str("");
			}
		}
	}

	t.commit();
	db.release();
	route.str("");
}


void OtherActor::syncDiffRelationUserApHigh(Json::Value& json) {
	Json::FastWriter writer;

	std::vector<Relation_User_AccessPoint> relations;
	EntityFunc::vectorFromJson<Relation_User_AccessPoint>(
		"relation_access_point_user", writer.write(json), relations);

	uint id_user = json["low_id"].asInt();

	typedef odb::query<Relation_User_AccessPoint> query;
	typedef odb::result<Relation_User_AccessPoint> result;
	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());

	for (Relation_User_AccessPoint relation : relations) {
		result r(db->query<Relation_User_AccessPoint>(
			query::accessPoint == relation.getAccessPoint().getId() &&
			query::user == id_user && query::activated == true));

		if (r.empty()) {
			User u(id_user);
			relation.setUser(u);
			relation.activated = true;
			db->persist(relation);
		}
	}

	t.commit();
	db.release();
}


void OtherActor::syncDiffHigh(Json::Value& values) {
	stringstream route;
	route << "/api/person";

	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());
	odb::result<User> r(db->query<User>(odb::query<User>::activated == true));

	bool flag = false;
	for (User user : r) {
		for (uint i = 0; i < values.size(); ++i)
			if (user.getId() == (values[i]["low_id"]).asInt()) {
				flag = true;
				break;
			}

		if (flag == false) {  // se nao tiver o user no high upar
			Action action;
			action.id_1 = user.getId();
			BaseActor::post<User>(action, route.str());  // postando no high
		}
		flag = false;
	}

	t.commit();
	db.release();


	for (uint i = 0; i < values.size(); ++i) {
		uint id_user = (values[i]["low_id"]).asInt();

		OtherActor::syncDiffUserHigh(values[i]);
		OtherActor::syncDiffDataHigh(values[i]);
		OtherActor::syncDiffStuffHigh(values[i]);
		OtherActor::syncDiffRelationUserApHigh(values[i]);
	}
}

void OtherActor::syncHigh(Action& action) {
	stringstream local_route;
	local_route << "http://" << io::configs.high_ip << ":1200/";

	stringstream ss;
	ss << local_route.str();
	ss << "system/mid_sync";

	std::string response;
	response = BaseActor::get_http(Action::HIGH, ss.str());
	ss.str("");

	if (response.empty()) {
		return;
	}

	Json::Value values;
	values = OtherActor::decodeJsonOfSync(response);
	OtherActor::syncDiffHigh(values);

	action.setSuccess();
}

void OtherActor::panic(Action& action) {
	std::string type;
	stringstream ss;
	std::string body = "";

	if (action.id_1 == 0)
		type = "off";
	else
		type = "on";

	typedef odb::query<AccessPoint> query;

	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());

	for (auto& e : db->query<AccessPoint>(true)) {
		ss << "http://" << e.getIpAddress()->databaseFormat();
		ss << ":10000/panic/";
		ss << type;

		BaseActor::actionPut(Action::LOW, ss.str(), body);
		ss.str("");
	}

	t.commit();
	db.release();

	action.setSuccess();
}

void OtherActor::loginHigh(Action& action) {
	std::string login = io::configs.login;
	std::string password = io::configs.password;

	stringstream route;
	route << "http://" << io::configs.high_ip << ":1200/"
		  << "api/login";

	stringstream ss;
	ss << "application/json";

	// TODO verificar onde vai o login e o password
	stringstream body;
	body << "{\"username\" : "
		 << "\"" << login << "\""
		 << ",";
	body << "\"password\" :"
		 << "\"" << password << "\""
		 << "}";

	http::client client;
	http::client::request request(route.str());
	http::client::response response =
		client.post(request, body.str(), ss.str());

	// salvando o cookie
	// https://groups.google.com/forum/#!topic/cpp-netlib/QwalfmBbe2w
	typedef boost::network::headers_range<http::client::response>::type
		headers_range_type;
	headers_range_type cookie_range = headers(response)["Set-Cookie"];

	BOOST_FOREACH (http::client::response::header_type const& header,
				   cookie_range) {
		io::configs.cookie = header.second;
		io::output.printMsgBox("main_messages", header.second);
	}

	if (response.status() == (HttpServer::response::status_type)200)
		action.setSuccess();
	else
		io::output.printMsgBox("main_messages", "login high error");
}
