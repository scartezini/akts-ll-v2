#include "../../headers/controllers/entities_controllers.hpp"

template <typename E = Entity>
HttpServer::response BaseRoutes::get(vector<string> &params,
									 HttpServer::request const &req) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;
	bool start = true;

	if (params.size() == 1) {
		int _id;
		try {
			_id = std::stoi(params[0], nullptr);
		} catch (invalid_argument) {
			return HttpRoutes::errorResponse(404);
		}

		try {
			typedef odb::query<E> query;
			unique_ptr<odb::database> db(open_database());
			transaction trans(db->begin());
			string body;

			for (auto &e : db->query<E>(query::id == _id)) {
				if (!e.activated)
					continue;
				start = false;


				body = e.toJson();
			}
			trans.commit();
			db.release();

			if (start)
				return HttpRoutes::errorResponse(404);

			return HttpServer::response::stock_reply(status, body);

		} catch (odb::exception &e) {
			return HttpRoutes::errorResponse(404);
		}
	} else {
		try {
			unique_ptr<odb::database> db(open_database());
			transaction trans(db->begin());

			string body = "{\"list\":[";

			for (auto &e : db->query<E>(true)) {
				if (!start)
					body += ",";

				start = false;
				body += e.toJson();
			}

			trans.commit();
			db.release();

			body += "]}";
			return HttpServer::response::stock_reply(status, body.c_str());
		} catch (odb::exception &e) {
			return HttpRoutes::errorResponse(404);
		}
	}

	return HttpRoutes::errorResponse(404);
}

template <typename E = Entity>
HttpServer::response BaseRoutes::post(vector<string> &params,
									  HttpServer::request const &req,
									  vector<actionfunc> actions) {
	HttpServer::response::status_type status =
		(HttpServer::response::status_type)201;

	stringstream msg;
	msg << "Body: " << req.body;
	io::output.printMsgBox("main_messages", msg.str());
	msg.str("");

	E entiny;
	try {
		entiny.fromJson(req.body);
	} catch (std::invalid_argument error) {
		msg << "Error no parse do json, json invalido: error " << error.what();
		io::output.printMsgBox("main_messages", msg.str());
		msg.str("");

		return HttpRoutes::errorResponse(400);
	}

	uint id = 0;
	try {
		id = database_monkey::persist(entiny);
	} catch (odb::exception &e) {
		return HttpRoutes::errorResponse(404);
	}

	stringstream body;
	body << "{" << endl;
	body << "\"id\":" << id << endl;
	body << "}" << endl;

	for (uint i = 0; i < actions.size(); i++) {
		actions[i](id);
	}

	return HttpServer::response::stock_reply(status, body.str());
}

template <typename E = Entity>
HttpServer::response BaseRoutes::put(
	vector<string> &params, HttpServer::request const &req,
	vector<actionfunc> actions) throw(std::invalid_argument) {
	if (params.size() < 1) {
		return HttpRoutes::errorResponse(404);
	}

	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;

	uint _id;
	E entiny;

	if (params.size() != 1) {
		return HttpRoutes::errorResponse(400);
	}

	try {
		_id = std::stoi(params[0], nullptr);
		entiny.fromJson(req.body);
		entiny.setId(_id);
	} catch (invalid_argument) {
		return HttpRoutes::errorResponse(404);
	}


	try {
		database_monkey::update(entiny);
	} catch (const odb::exception &e) {
		return HttpRoutes::errorResponse(404);
	}

	stringstream body;
	body << "{" << endl;
	body << "\"id\":" << _id << endl;
	body << "}" << endl;

	for (uint i = 0; i < actions.size(); i++) {
		actions[i](_id);
	}

	return HttpServer::response::stock_reply(status, body.str());
}


template <typename E = Entity>
HttpServer::response BaseRoutes::mdelete(
	vector<string> &params, HttpServer::request const &req,
	vector<actionfunc> actions) throw(std::invalid_argument) {
	bool start = true;

	if (params.size() < 1) {
		return HttpRoutes::errorResponse(400);
	}

	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;

	uint _id;
	try {
		_id = std::stoi(params[0], nullptr);
	} catch (invalid_argument) {
		return HttpRoutes::errorResponse(404);
	}

	try {
		unique_ptr<odb::database> db(database_monkey::open_database());
		transaction trans(db->begin());

		odb::result<E> r(db->query<E>(odb::query<E>::id == _id));

		E entiny;
		if (!r.empty()) {
			auto i(r.begin());
			i.load(entiny);

			start = false;
		}

		trans.commit();
		db.release();

		if (start)
			return HttpRoutes::errorResponse(404);


		if (entiny.activated) {
			database_monkey::remove(entiny);
		} else {
			return HttpRoutes::errorResponse(404);
		}

	} catch (const odb::exception &e) {
		return HttpRoutes::errorResponse(404);
	}

	stringstream body;
	body << "{" << endl;
	body << "\"id\":" << _id << endl;
	body << ",\"activated\":\"false\"" << endl;
	body << "}" << endl;

	for (uint i = 0; i < actions.size(); i++) {
		actions[i](_id);
	}

	return HttpServer::response::stock_reply(status, body.str());
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////

HttpServer::response DataTypeRoutes::get(vector<string> &params,
										 HttpServer::request const &req) {
	return BaseRoutes::get<DataType>(params, req);
}
HttpServer::response DataTypeRoutes::post(vector<string> &params,
										  HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	return BaseRoutes::post<DataType>(params, req, actions);
}
HttpServer::response DataTypeRoutes::put(vector<string> &params,
										 HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	return BaseRoutes::put<DataType>(params, req, actions);
}
HttpServer::response DataTypeRoutes::mdelete(vector<string> &params,
											 HttpServer::request const &req) {
	std::vector<actionfunc> actions;

	try {
		HttpServer::response response =
			BaseRoutes::mdelete<DataType>(params, req, actions);

		uint _id;

		if (params.size() != 1) {
			return HttpRoutes::errorResponse(400);
		}

		try {
			_id = std::stoi(params[0], nullptr);
		} catch (invalid_argument) {
			return HttpRoutes::errorResponse(404);
		}

		try {
			unique_ptr<odb::database> db(database_monkey::open_database());
			transaction t(db->begin());

			for (auto &e : db->query<Data>(odb::query<Data>::dataType == _id)) {
				e.activated = false;
				db->update(e);
				DataHighRoutes::deleteDataOnLow(e.getId());
			}

			t.commit();
			db.release();
		} catch (odb::exception &e) {
			return HttpRoutes::errorResponse(404);
		}

		return response;
	} catch (std::invalid_argument &e) {
		return HttpRoutes::errorResponse(404);
	}
}

//###########################################################################
//#############################	HIGH ########################################
//###########################################################################


///////////////////////////////////////////////////////////////////////////////
/// Access
///////////////////////////////////////////////////////////////////////////////
HttpServer::response AccessHighRoutes::get(vector<string> &params,
										   HttpServer::request const &req) {
	return BaseRoutes::get<Access>(params, req);
}


///////////////////////////////////////////////////////////////////////////////
/// Access Point
///////////////////////////////////////////////////////////////////////////////
void AccessPointHighRoutes::propagatePutOnLow(uint id) {
	Action action;
	action.table = Action::ACCESS_POINT;
	action.type = Action::PUT;
	action.dest = Action::LOW;

	action.id_1 = id;

	queues::actions.push(action);
}

HttpServer::response AccessPointHighRoutes::put(
	vector<string> &params, HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(AccessPointHighRoutes::propagatePutOnLow);
	return BaseRoutes::put<AccessPoint>(params, req, actions);
}

HttpServer::response AccessPointHighRoutes::get(
	vector<string> &params, HttpServer::request const &req) {
	return BaseRoutes::get<AccessPoint>(params, req);
}

HttpServer::response AccessPointHighRoutes::mdelete(
	vector<string> &params, HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	try {
		HttpServer::response response =
			BaseRoutes::mdelete<AccessPoint>(params, req, actions);

		uint _id;

		if (params.size() != 1) {
			return HttpRoutes::errorResponse(400);
		}

		try {
			_id = std::stoi(params[0], nullptr);
		} catch (invalid_argument) {
			return HttpRoutes::errorResponse(404);
		}

		try {
			unique_ptr<odb::database> db(database_monkey::open_database());
			transaction trans(db->begin());

			for (auto &e : db->query<Relation_User_AccessPoint>(
					 odb::query<Relation_User_AccessPoint>::accessPoint ==
					 _id)) {
				e.activated = false;
				db->update(e);
				// propagando o delete para o low
				RelationAccessPointUserHighRoutes::
					mdeleteRelationAccessPointUserOnLow(e.getId());
			}
			for (auto &e : db->query<Relation_Data_AccessPoint>(
					 odb::query<Relation_Data_AccessPoint>::accessPoint ==
					 _id)) {
				e.activated = false;
				db->update(e);
			}

			trans.commit();
			db.release();

		} catch (odb::exception &e) {
			return HttpRoutes::errorResponse(404);
		}

		return response;

	} catch (std::invalid_argument &e) {
		return HttpRoutes::errorResponse(404);
	}
}

///////////////////////////////////////////////////////////////////////////////
/// Register Kit
///////////////////////////////////////////////////////////////////////////////
void RegisterKitHighRoutes::propagatePutOnLow(uint id) {
	Action action;
	action.table = Action::REGISTER_KIT;
	action.type = Action::PUT;
	action.dest = Action::LOW;

	action.id_1 = id;

	queues::actions.push(action);
}

HttpServer::response RegisterKitHighRoutes::get(
	vector<string> &params, HttpServer::request const &req) {
	return BaseRoutes::get<AccessPoint>(params, req);
}

HttpServer::response RegisterKitHighRoutes::put(
	vector<string> &params, HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(RegisterKitHighRoutes::propagatePutOnLow);
	return BaseRoutes::put<RegisterKit>(params, req, actions);
}

HttpServer::response RegisterKitHighRoutes::mdelete(
	vector<string> &params, HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	return BaseRoutes::mdelete<RegisterKit>(params, req, actions);
}
///////////////////////////////////////////////////////////////////////////////
/// Data
///////////////////////////////////////////////////////////////////////////////
void DataHighRoutes::deleteDataOnLow(uint id) {
	Action action;
	action.type = Action::DELETE;
	action.table = Action::DATA;
	action.dest = Action::LOW;

	action.id_1 = id;

	queues::actions.push(action);
}

void DataHighRoutes::propagateDataOnLow(uint id) {
	Action action;
	action.type = Action::POST;
	action.table = Action::DATA;
	action.dest = Action::LOW;

	action.id_1 = id;
	queues::actions.push(action);
}

HttpServer::response DataHighRoutes::get(vector<string> &params,
										 HttpServer::request const &req) {
	return BaseRoutes::get<Data>(params, req);
}

HttpServer::response DataHighRoutes::post(vector<string> &params,
										  HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(DataHighRoutes::propagateDataOnLow);
	return BaseRoutes::post<Data>(params, req, actions);
}

HttpServer::response DataHighRoutes::mdelete(vector<string> &params,
											 HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(DataHighRoutes::deleteDataOnLow);

	try {
		HttpServer::response response =
			BaseRoutes::mdelete<Data>(params, req, actions);

		uint _id;

		if (params.size() != 1) {
			return HttpRoutes::errorResponse(400);
		}

		try {
			_id = std::stoi(params[0], nullptr);
		} catch (invalid_argument) {
			return HttpRoutes::errorResponse(404);
		}

		try {
			unique_ptr<odb::database> db(database_monkey::open_database());
			transaction trans(db->begin());


			odb::result<Data> r(db->query<Data>(odb::query<Data>::id == _id));

			Data data;
			if (r.empty()) {
				return HttpRoutes::errorResponse(404);
			} else {
				auto i(r.begin());
				i.load(data);
			}

			uint cont = 0;
			for (auto &e :
				 db->query<Data>(query<Data>::user == data.getUser().getId())) {
				if (e.activated)
					cont++;
			}

			if (cont == 1) {
				data.getUser().activated = false;
				db->update(data.getUser());
				UserHighRoutes::deleteUserOnLow(data.getUser().getId());
			}

			for (auto &e : db->query<Relation_Data_AccessPoint>(
					 odb::query<Relation_Data_AccessPoint>::data == _id)) {
				e.activated = false;
				db->update(e);
			}

			db.release();
			trans.commit();
		} catch (odb::exception &error) {
			return HttpRoutes::errorResponse(404);
		}

		return response;
	} catch (std::invalid_argument &e) {
		return HttpRoutes::errorResponse(404);
	}
}


///////////////////////////////////////////////////////////////////////////////
/// Stuff
///////////////////////////////////////////////////////////////////////////////
HttpServer::response StuffHighRoutes::get(vector<string> &params,
										  HttpServer::request const &req) {
	return BaseRoutes::get<Stuff>(params, req);
}

HttpServer::response StuffHighRoutes::post(vector<string> &params,
										   HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	return BaseRoutes::post<Stuff>(params, req, actions);
}

HttpServer::response StuffHighRoutes::put(vector<string> &params,
										  HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	return BaseRoutes::put<Stuff>(params, req, actions);
}

HttpServer::response StuffHighRoutes::mdelete(vector<string> &params,
											  HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	return BaseRoutes::mdelete<Stuff>(params, req, actions);
}

///////////////////////////////////////////////////////////////////////////////
/// User
///////////////////////////////////////////////////////////////////////////////

void UserHighRoutes::deleteUserOnLow(uint _id) {
	Action action;
	action.type = Action::DELETE;
	action.table = Action::USER;
	action.dest = Action::LOW;

	action.id_1 = _id;

	queues::actions.push(action);
}

HttpServer::response UserHighRoutes::get(vector<string> &params,
										 HttpServer::request const &req) {
	return BaseRoutes::get<User>(params, req);
}

HttpServer::response UserHighRoutes::post(vector<string> &params,
										  HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	return BaseRoutes::post<User>(params, req, actions);
}
HttpServer::response UserHighRoutes::put(vector<string> &params,
										 HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	return BaseRoutes::put<User>(params, req, actions);
}

HttpServer::response UserHighRoutes::mdelete(vector<string> &params,
											 HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(UserHighRoutes::deleteUserOnLow);

	try {
		HttpServer::response response =
			BaseRoutes::mdelete<User>(params, req, actions);

		uint _id;

		if (params.size() != 1) {
			return HttpRoutes::errorResponse(400);
		}

		try {
			_id = std::stoi(params[0], nullptr);
		} catch (invalid_argument) {
			return HttpRoutes::errorResponse(404);
		}

		try {
			unique_ptr<odb::database> db(database_monkey::open_database());
			transaction trans(db->begin());

			for (auto &e : db->query<Stuff>(odb::query<Stuff>::user == _id)) {
				e.activated = false;
				db->update(e);
			}
			for (auto &e : db->query<Data>(odb::query<Data>::user == _id)) {
				e.activated = false;
				db->update(e);
				DataHighRoutes::deleteDataOnLow(e.getId());
			}
			for (auto &e : db->query<Relation_User_AccessPoint>(
					 odb::query<Relation_User_AccessPoint>::user == _id)) {
				e.activated = false;
				db->update(e);
			}

			trans.commit();
			db.release();

		} catch (odb::exception &e) {
			return HttpRoutes::errorResponse(404);
		}

		return response;

	} catch (std::invalid_argument &e) {
		return HttpRoutes::errorResponse(404);
	}
}


///////////////////////////////////////////////////////////////////////////////
/// Rekation AccessPoint User
///////////////////////////////////////////////////////////////////////////////


void RelationAccessPointUserHighRoutes::saveRelationAccessPointUserOnLow(
	uint id) {
	Action action;
	action.type = Action::POST;
	action.table = Action::RELATION_ACCESSPOINT_USER;
	action.dest = Action::LOW;

	action.id_1 = id;
	queues::actions.push(action);
}

void RelationAccessPointUserHighRoutes::mdeleteRelationAccessPointUserOnLow(
	uint id) {
	Action action;
	action.type = Action::DELETE;
	action.table = Action::RELATION_ACCESSPOINT_USER;
	action.dest = Action::LOW;

	action.id_1 = id;
	queues::actions.push(action);
}

HttpServer::response RelationAccessPointUserHighRoutes::post(
	vector<string> &params, HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(
		RelationAccessPointUserHighRoutes::saveRelationAccessPointUserOnLow);

	return BaseRoutes::post<Relation_User_AccessPoint>(params, req, actions);
}

HttpServer::response RelationAccessPointUserHighRoutes::mdelete(
	vector<string> &params, HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(
		RelationAccessPointUserHighRoutes::mdeleteRelationAccessPointUserOnLow);

	if (params.size() != 2) {
		return HttpRoutes::errorResponse(400);
	}

	try {
		uint accessPoint = std::stoi(params[0], nullptr);
		uint user = std::stoi(params[1], nullptr);

		typedef odb::query<Relation_User_AccessPoint> odb_query;
		unique_ptr<odb::database> db(database_monkey::open_database());
		transaction t(db->begin());

		std::vector<std::string> array;

		unique_ptr<Relation_User_AccessPoint> ruap(
			db->query_one<Relation_User_AccessPoint>(odb_query::accessPoint ==
														 accessPoint &&
													 odb_query::user == user));


		if (ruap.get() != 0) {
			stringstream ss;
			ss << ruap->getId();
			array.push_back(std::string(ss.str().c_str()));
		} else {
			t.commit();
			db.release();
			return HttpRoutes::errorResponse(404);
		}

		t.commit();
		db.release();
		ruap.release();

		return BaseRoutes::mdelete<Relation_User_AccessPoint>(array, req,
															  actions);
	} catch (odb::exception &e) {
		return HttpRoutes::errorResponse(404);

	} catch (std::invalid_argument &e) {
		return HttpRoutes::errorResponse(404);
	}
}

//###########################################################################
//#############################	LOW #########################################
//###########################################################################


//////////////////////////////////////////////////////////////////////////////
/// Access
///////////////////////////////////////////////////////////////////////////////

void AccessLowRoutes::saveAccessOnHigh(uint id) {
	Action action;
	action.type = Action::POST;
	action.table = Action::ACCESS;
	action.dest = Action::HIGH;

	action.id_1 = id;

	queues::actions.push(action);
}

HttpServer::response AccessLowRoutes::post(vector<string> &params,
										   HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(AccessLowRoutes::saveAccessOnHigh);
	return BaseRoutes::post<Access>(params, req, actions);
}

//////////////////////////////////////////////////////////////////////////////
/// Access Point
///////////////////////////////////////////////////////////////////////////////

void AccessPointLowRoutes::saveAccessPointOnHigh(uint id) {
	Action action;
	action.type = Action::POST;
	action.table = Action::ACCESS_POINT;
	action.dest = Action::HIGH;

	action.id_1 = id;

	queues::actions.push(action);
}

HttpServer::response AccessPointLowRoutes::post(
	vector<string> &params, HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(AccessPointLowRoutes::saveAccessPointOnHigh);
	return BaseRoutes::post<AccessPoint>(params, req, actions);
}

//////////////////////////////////////////////////////////////////////////////
/// Register Kit
//////////////////////////////////////////////////////////////////////////////

void RegisterKitLowRoutes::saveRegisterKitOnHigh(uint id) {
	Action action;
	action.type = Action::POST;
	action.table = Action::REGISTER_KIT;
	action.dest = Action::HIGH;

	action.id_1 = id;

	queues::actions.push(action);
}

HttpServer::response RegisterKitLowRoutes::post(
	vector<string> &params, HttpServer::request const &req) {
	std::vector<actionfunc> actions;
	actions.push_back(RegisterKitLowRoutes::saveRegisterKitOnHigh);
	return BaseRoutes::post<RegisterKit>(params, req, actions);
}

//////////////////////////////////////////////////////////////////////////////
/// Denied Access
//////////////////////////////////////////////////////////////////////////////

void DeniedAccessLowRoutes::propagateOnHigh(std::string &body) {
	Action action;
	action.type = Action::POST;
	action.table = Action::ROUTES;
	action.dest = Action::HIGH;

	stringstream route;
	route << "http://" << io::configs.high_ip << ":1200";
	route << "/api/access";
	action.body = body;

	queues::actions.push(action);
}

HttpServer::response DeniedAccessLowRoutes::post(
	vector<string> &params, HttpServer::request const &req) {
	DeniedAccessLowRoutes::propagateOnHigh(req.body);

	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;
	return HttpServer::response::stock_reply(status, "");
}
