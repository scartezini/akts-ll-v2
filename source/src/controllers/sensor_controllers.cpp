#include "../../headers/controllers/sensor_controllers.hpp"

HttpServer::response KitSensorRoutes::get(vector<string> &,
										  HttpServer::request const &) {
	stringstream msg;

	HttpServer::response::status_type status =
		(HttpServer::response::status_type)200;

	uint kit = io::configs.kit_sensor_id;
	uint max_trys = io::configs.sensor_start_max_trys;

	uint kit_act = io::configs.kit_led_id;

	uint i = 0;

	for (i = 0; i < max_trys; i++) {
		bool ini = io::sensors[kit]->allOK();
		if (ini)
			break;
		if (!ini)
			io::sensors[kit]->restart();
	}

	if (i == max_trys)
		return HttpRoutes::errorResponse(500);

	io::sensors[kit]->lock();

	// colocar o led para acender aqui

	for (i = 0; i < io::configs.kit_max_trys; i++) {
		if (!io::sensors[kit]->hasData()) {
			sleep(1000);
			continue;
		}

		Data data = io::sensors[kit]->getData();
		string body = data.toJson();

		io::sensors[kit]->waitRemove();
		io::sensors[kit]->unlock();

		return HttpServer::response::stock_reply(status, body);
	}

	io::sensors[kit]->unlock();
	return HttpRoutes::errorResponse(500);
}
