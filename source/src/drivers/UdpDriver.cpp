#include "../../headers/drivers/UdpDriver.hpp"

int UdpDriver::getConnection(uint port) {
	sockaddr_in si_me;
	int socket_con = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (socket_con == -1)
		throw runtime_error("#connection_error");

	memset(&si_me, 0, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(port);
	si_me.sin_addr.s_addr = INADDR_ANY;

	int res = ::bind(socket_con, (sockaddr *)&si_me, sizeof(sockaddr));
	if (res == -1)
		throw runtime_error("#connection_error");

	return socket_con;
}

uint UdpDriver::getMessage(char *message, uint maxsize, int address,
						   sockaddr_in &from) {
	unsigned slen = sizeof(sockaddr);
	uint size =
		recvfrom(address, message, maxsize - 1, 0, (sockaddr *)&from, &slen);
	// TODO fazer verificaçoes de erro aqui
	return size;
}

void UdpDriver::sendMessage(char *message, uint size, int address,
							sockaddr_in &to) {
	socklen_t slen = sizeof(to);
	uint error = sendto(address, message, size, 0, (sockaddr *)&to, slen);
	// TODO fazer verificaçao de error aqui
}
