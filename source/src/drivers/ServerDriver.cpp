#include "../../headers/drivers/ServerDriver.hpp"

IpAddress ServerDriver::getMyIp() {
	struct ifaddrs *ifAddrStruct = NULL;
	struct ifaddrs *ifa = NULL;
	void *tmpAddrPtr = NULL;

	IpAddress ip;

	getifaddrs(&ifAddrStruct);

	for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
		if (!ifa->ifa_addr) {
			continue;
		}
		if (ifa->ifa_addr->sa_family == AF_INET) {  // check it is IP4
			// is a valid IP4 Address
			tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
			char addressBuffer[INET_ADDRSTRLEN];
			inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
			int interfaceComp =
				strcmp(ifa->ifa_name, io::configs.server_interface.c_str());
			if (interfaceComp == 0)
				ip = IpAddress(addressBuffer);
		}
	}

	if (ifAddrStruct != NULL)
		freeifaddrs(ifAddrStruct);
	return ip;
}

void ServerDriver::killProcessOnPort(uint port) {
	stringstream command;
	command << "fuser -n tcp " << port;

	uint error = system(command.str().c_str());
	// TODO verificar o retorno aqui
}

void ServerDriver::killUdpProcessOnPort(uint port) {
	stringstream command;
	command << "fuser -n udp -k " << port << "";

	uint error = system(command.str().c_str());
	// TODO verificar o retorno aqui
}
