#include "../../headers/mananger/RouteMananger.hpp"

void HttpRouter::makeRoutes() {
	io::man.out_log(Msg()) << "Start to make the routes of the server";


	this->addRoute("restapi/data_type", "get", DataTypeRoutes::get, 1);
	this->addRoute("highapi/data_type", "post", DataTypeRoutes::post, 0);
	this->addRoute("highapi/data_type", "put", DataTypeRoutes::put, 1);
	this->addRoute("highapi/data_type", "delete", DataTypeRoutes::mdelete, 1);


	//########################################################################//
	//################################# high #################################//
	//########################################################################//

	//////////////
	/// Access ///
	//////////////

	/**
	*
	* @apiGroup Access
	* @apiName GetAccess
	* @api {Get} /high_api/v0/access Get All
	*
	* @apiSuccess (Response Body){Object[]} lista de access
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){Number} type tipo do accesso
	* @apiSuccess (Response Body){Number} person id do user que fez o accesso
	* @apiSuccess (Response Body){Number} data id da data que fez o accesso
	* @apiSuccess (Response Body){Number} [stuff] id da stuff que tinha no
	* accesso
	* @apiSuccess (Response Body){Number} accessPoint id da porta que teve o
	* accesso
	* @apiSuccess (Response Body){String} timestamp hora do accesso
	* @apiSuccess (Response Body){Bool} denied se o accesso foi negado
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* [{
	*	"low_id": 1,
	*	"type": 0,
	* 	"person": 2,
	*	"data" : 1,
	*	"stuff": 3,
	*	"denied":0,
	*	"accessPoint": 2
	* }, ...]
	*
	* @apiUse errors
	*/
	//==========================================================================
	/**
	*
	* @apiGroup Access
	* @apiName FindById
	* @api {Get} /high_api/v0/access/:id Find By Id
	*
	* @apiSuccess (Response Body){Object} access
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){Number} type tipo do accesso
	* @apiSuccess (Response Body){Number} person id do user que fez o accesso
	* @apiSuccess (Response Body){Number} data id da data que fez o accesso
	* @apiSuccess (Response Body){Number} [stuff] id da stuff que tinha no
		* accesso
	* @apiSuccess (Response Body){Number} accessPoint id da porta que teve o
	* accesso
	* @apiSuccess (Response Body){String} timestamp hora do accesso
	* @apiSuccess (Response Body){Bool} denied se o accesso foi negado
	*
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* {
	*	"low_id": 1,
	*	"type": 0,
	* 	"person": 2,
	*	"data" : 1,
	*	"stuff": 3,
	*	"accessPoint": 2,
	*	"denied":0,
	*	"timestamp","..."
	* };
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/access", "get", AccessHighRoutes::get, 1);

	/////////////////////
	/// Access Point ////
	/////////////////////

	/**
	*
	* @apiGroup Access Point
	* @apiName Get Access Point
	* @api {get} /high_api/v0/access_point Get All
	*
	* @apiSuccess (Response Body){Object[]} lista de access point
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){String} name Nome do Access Point
	* @apiSuccess (Response Body){Number} type tipo do Access Point
	* @apiSuccess (Response Body){String} ipAddress ip do Access Point
	* @apiSuccess (Response Body){Number} port porta usada para o Access Point
	* server
	* @apiSuccess (Response Body){Number} activated se a porta esta ativada
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* [{
	*	"low_id": 1,
	* 	"name": "Akuntsu Tec",
	*	"type": "1",
	*	"ipAddress" : "10.190.60.52",
	*	"port": "10000",
	*	"accessPoint": 2,
	*	"activated":1
	* }, ...]
	*
	* @apiUse errors
	*/
	//==========================================================================
	/**
	*
	* @apiGroup Access Point
	* @apiName FindById
	* @api {Get} /high_api/v0/access_point/:id Find By Id
	*
	* @apiSuccess (Response Body){Object} access point
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){String} name Nome do Access Point
	* @apiSuccess (Response Body){String} ipAddress ip do Access Point
	* @apiSuccess (Response Body){Number} port porta usada para o Access Point
	* server
	* @apiSuccess (Response Body){Number} type tipo do Access Point
	* @apiSuccess (Response Body){Number} activated se a porta esta ativada
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* {
	*	"low_id": 1,
	* 	"name": "Akuntsu Tec",
	*	"type": "1",
	*	"ipAddress" : "10.190.60.52",
	*	"port": "10000",
	*	"accessPoint": 2,
	*	"activated":1
	* };
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/access_point", "get",
				   AccessPointHighRoutes::get, 1);


	/**
	*
	* @apiGroup Access Point
	* @apiName put Access Point
	* @api{put} /high_api/v0/access_point/:id
	*
	* @apiParam {Number} [low_id] id no mid server
	* @apiParam {String} name
	* @apiParam {String} type
	* @apiParam {String} ipAddress
	* @apiParam {Number} port
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/access_point", "put",
				   AccessPointHighRoutes::put, 1);

	/**
	* @apiGroup Access Point
	* @apiName Delete access point
	* @api{Delete} /high_api/v0/access_point/
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/access_point", "delete",
				   AccessPointHighRoutes::mdelete, 1);

	////////////////////
	/// Register Kit ///
	////////////////////

	/**
	*
	* @apiGroup Register Kit
	* @apiName Get Register Kit
	* @api {get} /high_api/v0/access_point Get All
	*
	* @apiSuccess (Response Body){Object[]} lista de register kit
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){String} name Nome do register kit
	* @apiSuccess (Response Body){String} ipAddress ip do register kit
	* @apiSuccess (Response Body){Number} port porta usada para o register kit
	* server
	* @apiSuccess (Response Body){Number} activated se a porta esta ativada
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* [{
	*	"low_id": 1,
	* 	"name": "guiche 3",
	*	"ipAddress" : "10.190.60.52",
	*	"port": "10000",
	*	"activated":1
	* }, ...]
	*
	* @apiUse errors
	*/
	//==========================================================================
	/**
	*
	* @apiGroup Register Kit
	* @apiName FindById
	* @api {Get} /high_api/v0/access_point/:id Find By Id
	*
	* @apiSuccess (Response Body){Object} register kit
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){String} name Nome do register kit
	* @apiSuccess (Response Body){String} ipAddress ip do register kit
	* @apiSuccess (Response Body){Number} port porta usada para o register kit
	* server
	* @apiSuccess (Response Body){Number} activated se a porta esta ativada
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* {
	*	"low_id": 1,
	* 	"name": "guiche 3",
	*	"ipAddress" : "10.190.60.52",
	*	"port": "10000",
	*	"activated":1
	* };
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/register_kit", "get",
				   RegisterKitHighRoutes::get, 1);

	/**
	*
	* @apiGroup Register Kit
	* @apiName put Register Kit
	* @api{put} /high_api/v0/register_kit/:id
	*
	* @apiParam {Number} [low_id] id no mid server
	* @apiParam {String} name
	* @apiParam {String} ipAddress
	* @apiParam {Number} port
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/register_kit", "put",
				   RegisterKitHighRoutes::put, 1);

	/**
	*
	* @apiGroup Register Kit
	* @apiName Delete Register Kit
	* @api{Delete} /high_api/v0/register_kit/:id
	*
	*/
	this->addRoute("high_api/v0/register_kit", "delete",
				   RegisterKitHighRoutes::mdelete, 1);

	////////////
	/// Data ///
	////////////
	/**
	*
	* @apiGroup Data
	* @apiName Get Data
	* @api{Get} /high_api/v0/data Get All
	*
	* @apiSuccess (Response Body){Object[]} lista de Data (Cartão/Celular/...)
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){Number} type tipo da data
	* @apiSuccess (Response Body){Number} user_id do usuario pertencente a data
	* @apiSuccess (Response Body){Number} visitor se a data é de uso unico
	* @apiSuccess (Response Body){object[]} value vetor de dados da data
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* [{
	*	"low_id": 1,
	* 	"type": "3",
	*	"ipAddress" : "10.190.60.52",
	*	"port": "10000",
	*	"activated":1
	* },...];
	*/
	//==========================================================================
	/**
	*
	* @apiGroup Data
	* @apiName Find By Id
	* @api{Get} /high_api/v0/data/:id Find By Id
	*
	* @apiSuccess (Response Body){Object[]}  Data (Cartão/Celular/...)
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){Number} type tipo da data
	* @apiSuccess (Response Body){Number} user_id  do usuario pertencente a data
	* @apiSuccess (Response Body){Number} visitor se a data é de uso unico
	* @apiSuccess (Response Body){object[]} value vetor de dados da data
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* {
	*	"low_id": 1,
	* 	"type": "3",
	*	"ipAddress" : "10.190.60.52",
	*	"port": "10000",
	*	"activated":1
	* };
	*/
	this->addRoute("high_api/v0/data", "get", DataHighRoutes::get, 1);

	/**
	*
	* @apiGroup Data
	* @apiName post Data
	* @api{Post} /high_api/v0/data post data
	*
	* @apiParam {Number} [low_id] id no mid server
	* @apiParam {Object[]} value vetor com o valor da data
	* @apiParam {Number} user_id id do usuario que associado a data
	* @apiParam {Number} type tipo da data { NFC = 0, RFID = 1, BIOMETRIC = 2 }
	* @apiParam {Number} visitor se a data é de accesso unico
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/data", "post", DataHighRoutes::post, 0);

	/**
	* @apiGroup Data
	* @apiName Delete data
	* @api{Delete} /high_api/v0/data/:id Delete
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/data", "delete", DataHighRoutes::mdelete, 1);


	/////////////
	/// stuff ///
	/////////////

	/**
	*
	* @apiGroup Stuff
	* @apiName Get Stuff
	* @api{Get} /high_api/v0/stuff Get All
	*
	* @apiSuccess (Response Body){Object[]} lista de stuffs
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){Number} name nome do objeto
	* @apiSuccess (Response Body){String} description descrição que
	*indentifique o objeto (cor, adesivo, marcas,..)
	* @apiSuccess (Response Body){String} serial numero de serie
	* @apiSuccess (Response Body){Number} fk_user id do usuario a quem pertence
	*o objeto
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* [{
	*	"low_id": 1,
	* 	"name": "Notebook Dell",
	*	"description" : "Notebook da dell, cor aço escovado com amassado na
	*quina e adesivo da razer",
	*	"serial": "9027936038",
	*	"fk_user":2
	* },...];
	*/
	//==========================================================================
	/**
	*
	* @apiGroup Stuff
	* @apiName Find By id
	* @api{Get} /high_api/v0/stuff/:id FindById
	*
	* @apiSuccess (Response Body){Number} low_id id no mid server
	* @apiSuccess (Response Body){String} name nome do objeto
	* @apiSuccess (Response Body){String} description descrição que
	*indentifique o objeto (cor, adesivo, marcas,..)
	* @apiSuccess (Response Body){String} serial numero de serie
	* @apiSuccess (Response Body){Number} fk_user id do usuario a quem pertence
	*o objeto
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	* {
	*	"low_id": 1,
	* 	"name": "Notebook Dell",
	*	"description" : "Notebook da dell, cor aço escovado com amassado na
	*quina e adesivo da razer",
	*	"serial": "9027936038",
	*	"fk_user":2
	* }
	*/
	this->addRoute("high_api/v0/stuff", "get", StuffHighRoutes::get, 1);

	/**
	*
	* @apiGroup Stuff
	* @apiName post Stuff
	* @api{Post} /high_api/v0/stuff post Stuff
	*
	* @apiParam {Number} [low_id] id no mid server
	* @apiParam {String} name
	* @apiParam {String} description
	* @apiParam {String} serial
	* @apiParam {Number} user_id id do usuario a quem pertence
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/stuff", "post", StuffHighRoutes::post, 0);

	/**
	*
	* @apiGroup Stuff
	* @apiName put Stuff
	* @api{put} /high_api/v0/stuff/:id put stuff
	*
	* @apiParam {Number} [low_id] id no mid server
	* @apiParam {String} name
	* @apiParam {String} description
	* @apiParam {String} serial
	* @apiParam {Number} user_id id do usuario a quem pertence
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/stuff", "put", StuffHighRoutes::put, 1);

	/**
	* @apiGroup Stuff
	* @apiName Delete stuff
	* @api{Delete} /high_api/v0/stuff/:id Delete
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/stuff", "delete", StuffHighRoutes::mdelete, 1);


	////////////
	/// user ///
	////////////

	/**
	*
	* @apiGroup User
	* @apiName Get User
	* @api{Get} /high_api/v0/user Get All
	*
	* @apiSuccess (Response Body){Number} low_id
	* @apiSuccess (Response Body){string} name
	* @apiSuccess (Response Body){string} cpf
	* @apiSuccess (Response Body){string} rg
	* @apiSuccess (Response Body){string} destiny
	* @apiSuccess (Response Body){string} email
	* @apiSuccess (Response Body){string} password
	* @apiSuccess (Response Body){string} phone
	* @apiSuccess (Response Body){Number} num_miss
	*
	* @apiUse errors
	*/
	//=======================================================================
	/**
	*
	* @apiGroup User
	* @apiName Find by Id
	* @api{Get} /high_api/v0/user FindById
	*
	* @apiSuccess (Response Body){Number} low_id
	* @apiSuccess (Response Body){string} name
	* @apiSuccess (Response Body){string} cpf
	* @apiSuccess (Response Body){string} rg
	* @apiSuccess (Response Body){string} destiny
	* @apiSuccess (Response Body){string} email
	* @apiSuccess (Response Body){string} password
	* @apiSuccess (Response Body){string} phone
	* @apiSuccess (Response Body){Number} num_miss
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/user", "get", UserHighRoutes::get, 1);

	/**
	*
	* @apiGroup User
	* @apiName post User
	* @api{Get} /high_api/v0/user post User
	*
	* @apiParam {Number} low_id
	* @apiParam {String} name
	* @apiParam {String} cpf
	* @apiParam {String} rg
	* @apiParam {String} destiny
	* @apiParam {String} email
	* @apiParam {String} password
	* @apiParam {String} phone
	* @apiParam {Number} num_miss
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/user", "post", UserHighRoutes::post, 0);

	/**
	*
	* @apiGroup User
	* @apiName put User
	* @api{Get} /high_api/v0/user/:id put User
	*
	* @apiParam {Number} low_id
	* @apiParam {String} name
	* @apiParam {String} cpf
	* @apiParam {String} rg
	* @apiParam {String} destiny
	* @apiParam {String} email
	* @apiParam {String} password
	* @apiParam {String} phone
	* @apiParam {Number} num_miss
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/user", "put", UserHighRoutes::put, 1);

	/**
	*
	* @apiGroup User
	* @apiName Delete User
	* @api{delete} /high_api/v0/user/:id Delete
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/user", "delete", UserHighRoutes::mdelete, 1);

	/**
	*
	* @apiGroup Relation AccessPoint User
	* @apiName Post
	* @api{post} /high_api/v0/relation/access_point/user
	*
	* @apiParam {Number} period
	* @apiParam {String} start hora de inicio
	* @apiParam {Number} user id do usuario
	* @apiParam {Number} access_point id do access point
	* @apiParam {TODO} horas da sememana
	*
	* @apiUse errors
	*/
	this->addRoute("high_api/v0/relation/access_point/user", "post",
				   RelationAccessPointUserHighRoutes::post, 0);

	/**
	*
	* @apiGroup Relation AccessPoint User
	* @apiName Delete
	* @api{post} /high_api/v0/relation/access_point/user/:access_point/:user
	* Delete
	*
	* @apiUse errors
	*/
	this->addRoute("highapi/relation/access_point/user", "delete",
				   RelationAccessPointUserHighRoutes::mdelete, 2);

	//########################################################################//
	//################################# low ##################################//
	//########################################################################//
	//////////////
	/// Access ///
	//////////////

	/**
	*
	* @apiGroup Access
	* @apiName post_Access
	* @api {Post} /low_api/v0/access Post
	*
	* @apiParam {Number} low_id id do accesso no mid server
	* @apiParam {Number} direction direção do accesso
	* @apiParam {Number} user_id usuario do accesso
	* @apiParam {Number} data_id data do accesso
	* @apiParam {Number} [stuff_id] stuff do accesso
	* @apiParam {Number} control_id id da controladora
	* @apiParam {String} time hora do accesso
	* @apiParam {Bool} [denied] se o accesso foi negado
	*
	* @apiParamExample {json} Body
	* {
	*	"low_id":1,
	*	"direction":2,
	*	"denied":0,
	*	"user_id":2,
	*	"data_id":1,
	*	"stuff_id":2,
	*	"control_id":3,
	*	"time":"..."
	* }
	*
	* @apiUse errors
	*/
	this->addRoute("low_api/v0/access", "post", AccessLowRoutes::post, 0);


	/////////////////////
	/// Denied Access ///
	/////////////////////
	/**
	*
	* @apiGroup Denied Access
	* @apiName post_Denied_Access
	* @api {post} /low_api/v0/denied_access post Denied access
	*
	* @apiSuccessExample {json} Response Body:
	* HTTP/1.1 200 OK
	*
	* @apiUse errors
	*/
	this->addRoute("low_api/v0/denied_access", "post",
				   DeniedAccessLowRoutes::post, 0);

	/////////////////////
	/// Access Point ////
	/////////////////////
	/**
	*
	* @apiGroup Access Point
	* @apiName post Access point
	* @api {post} /low_api/v0/access_point post
	*
	* @apiParam {Number} low_id id do accesso no mid server
	* @apiParam {String} name nome do Access Point
	* @apiParam {String} type tipo do access Point
	* @apiParam {String} ipAddress ip do Access Point
	* @apiParam {Number} port porta usada pelo AccessPoint server
	*
	* @apiParamExample {json} Body
	* {
	*	"low_id":1,
	*	"name":"Akuntsu tec.",
	*	"type":"ticket_gate",
	*	"ipAddress":"10.190.60.52",
	*	"port":10000,
	* };
	*
	* @apiUse errors
	*/
	this->addRoute("low_api/v0/access_point", "post",
				   AccessPointLowRoutes::post, 0);

	//////////////////////
	/// Register Kit  ////
	//////////////////////
	/**
	*
	* @apiGroup Register Kit
	* @apiName post Register kit
	* @api {post} /low_api/v0/register_kit post
	*
	* @apiParam {Number} low_id id do accesso no mid server
	* @apiParam {String} name nome do Access Point
	* @apiParam {String} ipAddress ip do Access Point
	* @apiParam {Number} port porta usada pelo AccessPoint server
	*
	* @apiParamExample {json} Body
	* {
	*	"low_id":1,
	*	"name":"guiche 2",
	*	"ipAddress":"10.190.60.52",
	*	"port":10000,
	* };
	*
	* @apiUse errors
	*/
	this->addRoute("low_api/v0/register_kit", "post",
				   RegisterKitLowRoutes::post, 0);


	//########################################################################//
	//################################ Comum #################################//
	//########################################################################//
	/**
	* @apiGroup Comum
	* @apiName panic on
	* @api{put} /comum_api/v0/panic/on/
	*/
	this->addRoute("comum_api/v0/panic/on", "put", OtherRoutes::panicOn, 0);

	/**
	* @apiGroup Comum
	* @apiName panic off
	* @api{put} /comum_api/v0/panic/off/
	*/
	this->addRoute("comum_api/v0/panic/off", "put", OtherRoutes::panicOff, 0);

	/**
	* @apiGroup Comum
	* @apiName ping
	* @api{get} /comum_api/v0/ping/
	*/
	this->addRoute("/comum_api/v0/ping", "get", OtherRoutes::ping, 0);

	/**
	* @apiGroup Comum
	* @apiName time
	* @api{get} /comum_api/v0/time/
	*/
	this->addRoute("/comum_api/v0/time", "get", OtherRoutes::timeget, 0);

	/**
	* @apiGroup Comum
	* @apiName sync
	* @api{get} /comum_api/v0/sync/
	*/
	this->addRoute("/comum_api/v0/sync", "get", OtherRoutes::synchronize, 0);

#ifndef DEBUGER
	this->addRoute("dropall", "delete", OtherRoutes::dropAll, 0);
#endif

	this->addRoute("cadastro", "get", KitSensorRoutes::get, 0);

	HttpRouter::max_route_params = 2;

	io::man.out_log(Msg()) << "Finish to make the routes of the server";
}


//=======================================================

/**
 * @apiDefine errors
 * @apiError (Error) {Number} statusCode HTTP status code
 * @apiErrorExample {json} Internal
 * 		HTTP/1.1 500 Internal Server Error
 *		// Something went wrong, try again in a moment
 * @apiErrorExample {json} Not Fount
 * 		HTTP/1.1 404 Not Fount
 */

//-------------------------------------------------------
