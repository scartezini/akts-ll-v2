#include "../../headers/mananger/RouteMananger.hpp"

// action router
const string ActionRouter::module_name = "ActionRouter";

ActionRouter::ActionRouter() {
	this->makeRoutes();
}

void ActionRouter::runSolver(ActionRouter *router) {
	while (1) {
		Action action = queues::actions.get();
		router->run(action);
	}
}

void ActionRouter::makeRoutes() {
	io::man.out_log(Msg()) << "Start to make the routes of the actions";

	this->addRoute(Action::USER, Action::DELETE, Action::LOW,
				   UserActor::mdelete);

	this->addRoute(Action::DATA, Action::DELETE, Action::LOW,
				   DataActor::mdelete);
	this->addRoute(Action::DATA, Action::POST, Action::LOW,
				   DataActor::propagate);

	this->addRoute(Action::ACCESS, Action::POST, Action::HIGH,
				   AccessActor::post);

	this->addRoute(Action::ACCESS_POINT, Action::POST, Action::HIGH,
				   AccessPointActor::post);
	this->addRoute(Action::ACCESS_POINT, Action::PUT, Action::LOW,
				   AccessPointActor::put);
	this->addRoute(Action::ACCESS_POINT, Action::POST, Action::LOW,
				   OtherActor::panic);

	this->addRoute(Action::REGISTER_KIT, Action::POST, Action::HIGH,
				   RegisterKitActor::post);
	this->addRoute(Action::REGISTER_KIT, Action::PUT, Action::LOW,
				   RegisterKitActor::put);

	this->addRoute(Action::RELATION_ACCESSPOINT_USER, Action::POST, Action::LOW,
				   RelationAccessPointUserActor::post);
	this->addRoute(Action::RELATION_ACCESSPOINT_USER, Action::DELETE,
				   Action::LOW, RelationAccessPointUserActor::mdelete);

	this->addRoute(Action::OTHER, Action::POST, Action::HIGH,
				   OtherActor::setOnline);

	this->addRoute(Action::ROUTES, Action::SYNC, Action::LOW,
				   OtherActor::syncLow);
	this->addRoute(Action::ROUTES, Action::SYNC, Action::HIGH,
				   OtherActor::syncHigh);

	this->addRoute(Action::ROUTES, Action::POST, Action::LOW,
				   BaseActor::post_http);
	this->addRoute(Action::ROUTES, Action::PUT, Action::LOW,
				   BaseActor::put_http);
	this->addRoute(Action::ROUTES, Action::DELETE, Action::LOW,
				   BaseActor::mdelete_http);

	this->addRoute(Action::ROUTES, Action::POST, Action::HIGH,
				   BaseActor::post_http);
	this->addRoute(Action::ROUTES, Action::PUT, Action::HIGH,
				   BaseActor::put_http);
	this->addRoute(Action::ROUTES, Action::DELETE, Action::HIGH,
				   BaseActor::mdelete_http);

	this->addRoute(Action::ROUTES, Action::LOGIN, Action::HIGH,
				   OtherActor::loginHigh);

	io::man.out_log(Msg()) << "Finish to make the routes of the action";
}

void ActionRouter::addRoute(Action::TABLE table, Action::TYPE type,
							Action::DEST dest, actor_func func) {
	actor_map::iterator it;

	it = routes.find(table);
	if (it == routes.end()) {
		map<Action::TYPE, map<Action::DEST, actor_func>> empty_map;
		routes[table] = empty_map;
	}

	map<Action::TYPE, map<Action::DEST, actor_func>>::iterator itLevel2;

	itLevel2 = routes[table].find(type);
	if (itLevel2 == routes[table].end()) {
		map<Action::DEST, actor_func> empty_map;
		routes[table][type] = empty_map;
	}

	routes[table][type][dest] = func;
}

ActionRouter::ERROR ActionRouter::validadeRoute(Action::TABLE table,
												Action::TYPE type,
												Action::DEST dest) {
	actor_map::iterator it;

	it = routes.find(table);
	if (it == routes.end()) {
		return ROUTE_NOT_FOUND;
	};

	map<Action::TYPE, map<Action::DEST, actor_func>>::iterator itLevel2;
	itLevel2 = routes[table].find(type);
	if (itLevel2 == routes[table].end()) {
		return ROUTE_NOT_FOUND;
	}

	map<Action::DEST, actor_func>::iterator ite;
	ite = routes[table][type].find(dest);
	if (ite == routes[table][type].end()) {
		return ROUTE_NOT_FOUND;
	};

	return OK;
}

void ActionRouter::run(Action &action) {
	io::man.out_log(Msg()) << "New requisition of action, tipo: " << action.type
						   << " table: " << action.table << " with destiny on "
						   << action.dest;

	ActionRouter::ERROR route_error;
	route_error =
		ActionRouter::validadeRoute(action.table, action.type, action.dest);

	if (route_error == ROUTE_NOT_FOUND) {
		io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
			<< "Error interno de rotas de actions, tipo: " << action.type
			<< " table: " << action.table << " with destiny on " << action.dest;
	}

	io::man.out_log(Msg()) << "Route ok, starting the action";


	try {
		ActionRouter::routes[action.table][action.type][action.dest](action);
		action.setExecuted();
	} catch (std::invalid_argument error) {
		io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
			<< "std Error " << error.what();
	} catch (std::runtime_error error) {
		io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
			<< "runtime Error " << error.what();
	} catch (const odb::deadlock &e) {
		io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
			<< "odb Error" << e.what();
	} catch (const odb::exception &e) {
		io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
			<< "odb Error" << e.what();
	} catch (std::exception error) {
		io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
			<< "std exept Error " << error.what();
	} catch (...) {
		io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
			<< "Error Desconhecido";
	}
}


// Http solver

// static
route_map HttpRouter::routes;
route_val HttpRouter::routes_validate;
uint HttpRouter::max_route_params = 0;
const string HttpRouter::module_name = "HttpRouter";

HttpRouter::HttpRouter() {
	HttpRouter::max_route_params = 0;

	this->connect();
	this->makeRoutes();
}

void HttpRouter::connect() {
	server_handler handler_;
	HttpServer::options options(handler_);
	ServerDriver::killProcessOnPort(io::configs.tcp_port);

	for (uint i = 0; i < io::configs.tcp_connect_trys; i++) {
		try {
			io::man.out_log(Msg())
				<< "Starting to connect to the tcp server on port number:"
				<< io::configs.tcp_port;


			stringstream msg;
			msg << io::configs.tcp_port;
			this->server = new HttpServer(options.address("0.0.0.0")
											  .port(msg.str())
											  .linger(false)
											  .reuse_address(true));
			break;
		} catch (...) {
			io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
				<< "Force opition is setted, killing the process on port "
				<< io::configs.tcp_port << " to connect tcp server";
			try {
				ServerDriver::killProcessOnPort(io::configs.tcp_port);
			} catch (...) {
				// kill process fail
				io::man.out_log(Msg(ActionRouter::module_name, Logger::ERROR))
					<< "kill process on port " << io::configs.tcp_port
					<< "fail,  making another try to connect tcp server";

				usleep(1000);
			}
		}
	}
}


void HttpRouter::addRoute(string route, string method, res_func funct,
						  uint max_params) {
	std::size_t found = route.find("/");
	if (found == std::string::npos) {
		route = "/" + route;
	} else if (found != 0) {
		route = "/" + route;
	}

	boost::to_upper(method);
	boost::to_upper(route);

	route_map::iterator it;

	it = routes.find(route);
	if (it == routes.end()) {
		map<string, res_func> empty_map;
		map<string, int> empty_map_int;
		routes[route] = empty_map;
		routes_validate[route] = empty_map_int;
	}

	routes[route][method] = funct;
	routes_validate[route][method] = max_params;
}

HttpServer::response HttpRouter::getResponse(HttpServer::request const &req) {
	bool route_found = false;


	io::man.out_log(Msg()) << "New requisition on route: " << req.destination
						   << " method: " << req.method;


	for (uint num_params = 0; num_params <= HttpRouter::max_route_params;
		 num_params++) {
		HttpRouter::ROUTE_ERROR route_error;
		vector<string> params;
		string route;

		try {
			route =
				HttpRoutes::decodeRoute(req.destination, num_params, params);
			boost::to_upper(route);
		} catch (std::invalid_argument error) {
			break;
		}

		route_error = HttpRouter::validadeRoute(route, req.method, num_params);

		if (route_error == ROUTE_NOT_FOUND) {
			continue;
		}

		if (route_error == METHOD_NOT_FOUND) {
			route_found = true;
			continue;
		}

		io::man.out_log(Msg()) << "Route ok, substring of the route: " << route;

		try {
			return HttpRouter::routes[route][req.method](params, req);
		} catch (std::invalid_argument error) {
			io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
				<< "invalid_argument Error " << error.what();

			return HttpRoutes::errorResponse(400);
		} catch (std::runtime_error error) {
			io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
				<< "runtime Error " << error.what();

			return HttpRoutes::errorResponse(500);
		} catch (const odb::exception &e) {
			io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
				<< "odb Error " << e.what();

			return HttpRoutes::errorResponse(503);
		} catch (std::exception error) {
			io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
				<< "std exception Error " << error.what();

			return HttpRoutes::errorResponse(500);
		} catch (...) {
			io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
				<< "Unknow Error";
			return HttpRoutes::errorResponse(500);
		}
	}

	if (!route_found) {
		io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
			<< "Error method realy not found";

		return HttpRoutes::errorResponse(404);
	}

	io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
		<< "Error realy not found";

	return HttpRoutes::errorResponse(405);
}

HttpRouter::ROUTE_ERROR HttpRouter::validadeRoute(string route, string method,
												  uint num_params) {
	route_val::iterator it;

	it = routes_validate.find(route);
	if (it == routes_validate.end()) {
		return ROUTE_NOT_FOUND;
	};

	map<string, int>::iterator ite;
	ite = routes_validate[route].find(method);

	if (ite == routes_validate[route].end()) {
		return METHOD_NOT_FOUND;
	};

	if (routes_validate[route][method] < num_params)
		return ROUTE_NOT_FOUND;

	return OK;
}

void HttpRouter::runSolver(HttpRouter *router) {
	for (uint i = 0; i < io::configs.tcp_connect_trys; i++) {
		try {
			router->server->run();
		} catch (...) {
			io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
				<< "Force opition is setted, killing the process on port "
				<< io::configs.tcp_port << " to connect tcp server";

			try {
				ServerDriver::killProcessOnPort(10000);
			} catch (...) {
				// kill process fail
				io::man.out_log(Msg(HttpRouter::module_name, Logger::ERROR))
					<< "kill process on port " << io::configs.tcp_port
					<< "fail,  making another try to connect tcp server";

				usleep(1000);
			}
		}
	}
}
