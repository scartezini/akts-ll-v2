#include "../../headers/mananger/QueueMananger.hpp"

ActionQueue queues::actions;
const string ActionQueue::module_name = "ActionQueue";

void ActionQueue::push(Action &action) {
	queue_mutex.lock();
	this->actions.push_back(action);
	queue_mutex.unlock();
	new_job.notify_all();
}

void ActionQueue::start() {
	// coloca todas as actions atrasadas na queue
	this->makeStartActions();
	this->getFromDb();
	// thread para pegar e salvar as actions
	threads.push_back(thread(ActionQueue::runSolver, this));
	// thread que vai dar ping na outra a cada tempo
	threads.push_back(thread(ActionQueue::pinger, this));
}

void ActionQueue::makeStartActions() {
	Action action;
	action.type = Action::POST;
	action.table = Action::OTHER;
	action.dest = Action::HIGH;

	this->push(action);
}

void ActionQueue::runSolver(ActionQueue *action) {
	while (1) {
		// std::cout << "runSolver" << std::endl;
		action->run();
	}
}

void ActionQueue::waitSignal() {
	std::unique_lock<std::mutex> lck(queue_mutex);
	new_job.wait(lck);
}

void ActionQueue::run() {
	// espera um signal
	this->waitSignal();

	// pega as que estao na hora do db
	queue_mutex.lock();
	if (this->actions.empty()) {
		this->getFromDb();
		queue_mutex.unlock();
	} else {
		queue_mutex.unlock();
	}

	// caso ainda nao exista nada para fazer retorna
	queue_mutex.lock();
	if (this->actions.empty()) {
		queue_mutex.unlock();
		return;
	} else {
		queue_mutex.unlock();
	}

	// add para alguem fazer
	while (1) {
		queue_mutex.lock();
		if (this->actions.empty()) {
			queue_mutex.unlock();
			break;
		}

		Action action = this->actions.front();
		this->actions.pop_front();
		queue_mutex.unlock();

		this->saveOnDb(action);
		this->pushFinal(action);
	}
}

void ActionQueue::saveOnDb(Action &action) {
	action.exec++;
	action.running = true;

	if (action.exec >= 30) {
		action.setExecuted();
	}

	const uint max_retries = 5;
	typedef odb::result<Action> result;
	typedef odb::query<Action> query;

	unique_ptr<odb::database> db = database_monkey::open_database();
	for (uint retry_count = 0;; retry_count++) {
		try {
			odb::transaction t(db->begin());
			result r(db->query<Action>(query::id == action.id));

			if (r.empty())
				action.id = db->persist(action);
			else {
				action.exec_time += pow(1.5, action.exec) + 120;
				db->update(action);
			}
			t.commit();
			break;
		} catch (odb::recoverable &e) {
			if (retry_count > max_retries)
				io::man.out_log(Msg(ActionQueue::module_name, Logger::ERROR))
					<< "Error on save action on database";
			else
				continue;
		}
	}
	db.release();
}

void ActionQueue::getFromDb() {
	Timestamp now;
	now.now();

	typedef odb::result<Action> result;
	typedef odb::query<Action> query;
	unique_ptr<odb::database> db(database_monkey::open_database());
	transaction t(db->begin());
	try {
		result r(
			db->query<Action>(query::exec_time <= now.transformInSeconds() &&
							  query::running == false));

		if (!r.empty()) {
			for (Action &a : r) {
				this->actions.push_back(a);
			}
		}

	} catch (odb::recoverable &e) {
		io::man.out_log(Msg(ActionQueue::module_name, Logger::ERROR))
			<< "Error on get action from database";
	}

	t.commit();
	db.release();
}

void ActionQueue::pushFinal(Action &action) {
	queue_mutex_final.lock();
	this->actions_final.push_back(action);
	queue_mutex_final.unlock();
	new_job_final.notify_one();
}

Action ActionQueue::get() {
	std::unique_lock<std::mutex> lck(queue_mutex_final);
	while (this->actions_final.empty())
		new_job_final.wait(lck);

	Action action = this->actions_final.front();
	actions_final.pop_front();
	return action;
}

void ActionQueue::pinger(ActionQueue *action) {
	while (1) {
		usleep(1000000);
		action->new_job.notify_all();
	}
}
