#include "../../headers/mananger/TaskMananger.hpp"

const string TaskMananger::module_name = "TackMananger";

void TaskMananger::start() {
	// creating action thread solvers
	try {
		action_router = new ActionRouter();
	} catch (std::runtime_error error) {
		io::man.out_log(Msg(TaskMananger::module_name, Logger::ERROR))
			<< "Not possible make action router instance";

		io::jobs.finish();
		return;
	}

	for (uint i = 0; i <= num_action_routers; ++i)
		action_routers.push_back(
			thread(ActionRouter::runSolver, action_router));

	io::man.out_log(Msg()) << num_action_routers << " threads actions solvers "
													"created";

	// creating http thread solvers
	try {
		http_router = new HttpRouter();
	} catch (std::runtime_error error) {
		io::man.out_log(Msg(TaskMananger::module_name, Logger::ERROR))
			<< "Not possible make action router instance";


		io::jobs.finish();
		return;
	}

	for (uint i = 0; i <= num_http_routers; ++i)
		http_routers.push_back(thread(HttpRouter::runSolver, http_router));

	io::man.out_log(Msg()) << num_http_routers << " threads http solvers "
												  "created";


	// logging
	io::man.out_log(Msg()) << "setup finished";
}

TaskMananger::TaskMananger() {
	this->num_action_routers = io::configs.num_actions_routers;
	this->num_http_routers = io::configs.num_http_routers;
}

TaskMananger::~TaskMananger() {
	delete action_router;
	delete http_router;
}

void TaskMananger::feed_server() {
	while (1) {
		usleep(100000000);
	}
}
void TaskMananger::feed_case() {
	while (1) {
		usleep(100000000);
	}
}
