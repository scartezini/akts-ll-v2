#include "../../headers/http/UdpServer.hpp"

const string UdpServer::module_name = "UdpServer";

UdpServer::UdpServer() {
	io::man.out_log(Msg()) << "Start the udp server";
	if (!io::configs.udp_online) {
		io::man.out_log(Msg()) << "Start set to off, by config";
		return;
	}


	this->connect();

	this->error = false;
	udp_thread = thread(UdpServer::runServer, this);
}

void UdpServer::connect() {
	bool connected = false;

	io::man.out_log(Msg()) << "Make the try to the port "
						   << io::configs.udp_port;
	for (uint i = 0; i < io::configs.num_try_udp_get_connection; i++) {
		try {
			// make the connection
			this->connection = UdpDriver::getConnection(io::configs.udp_port);

			// log it
			io::man.out_log(Msg()) << "Udp server on, connected to port "
								   << io::configs.udp_port;

			// finsh
			connected = true;
			break;
		} catch (runtime_error error) {
			sleep(100);
			// log the fail
			io::man.out_log(Msg(UdpServer::module_name, Logger::ERROR))
				<< "Fail to connecte to port " << io::configs.udp_port
				<< " on udp server";


			if (io::configs.force_udp_connection) {
				// try kill the process on udp port
				io::man.out_log(Msg(UdpServer::module_name, Logger::ERROR))
					<< "Force opition is setted, killing the process on port "
					<< io::configs.udp_port << " to connect udp server";

				try {
					ServerDriver::killUdpProcessOnPort(io::configs.udp_port);
				} catch (runtime_error error) {
					// kill process fail
					io::man.out_log(Msg(UdpServer::module_name, Logger::ERROR))
						<< "kill process on port " << io::configs.udp_port
						<< "fail,  making another try to connect udp server";
					usleep(1000);
				}

			} else {
				// waint to try again
				io::man.out_log(Msg(UdpServer::module_name, Logger::ERROR))
					<< "Force opition not setted, waiting to get the port "
					<< io::configs.udp_port << " to connect udp server";

				usleep(1000);
			}

			// log the next try
			io::man.out_log(Msg()) << "Retry get upd connection, on port "
								   << io::configs.udp_port << " for udp server";
		}
	}

	if (!connected) {
		// if not connected at all
		// cout << "Entrando aqui porra" << endl;
		io::man.out_log(Msg(UdpServer::module_name, Logger::ERROR))
			<< "Impossible to connect on port " << io::configs.udp_port
			<< " for udp server";

		// end singnal for the program
		io::jobs.finish();
	}
}

void UdpServer::runServer(UdpServer *server) {
	while (!io::jobs.finished()) {
		try {
			server->run();
		} catch (runtime_error error) {
			if (string(error.what()).compare("#connection_error") == 0) {
				// log the connection error
				io::man.out_log(Msg(UdpServer::module_name, Logger::ERROR))
					<< "Connection failed, tryng to reconnet";
				// try connection
				server->connect();
			} else {
				// connection error
				io::man.out_log(Msg(UdpServer::module_name, Logger::ERROR))
					<< "Error not expecte: " << error.what();
			}
		}
	}
}

void UdpServer::run() {
	sockaddr_in reciver_ip;

	// wait for message
	uint size = UdpDriver::getMessage(message_input, sizeof(message_input),
									  connection, reciver_ip);
	// tranform message

	this->createHttp(message_input, size);

	// make the treatment
	this->processHttp();

	// get resposnse
	this->buildResponse();

	// send response
	char *message_output = this->transformHttp();
	UdpDriver::sendMessage(message_output, size, connection, reciver_ip);

	free(message_output);
}

void UdpServer::createHttp(char *message, uint size) {
	try {
		io::man.out_log(Msg()) << "Udp request recived";
		io::man.out_log(Msg(UdpServer::module_name, Logger::DEBUG))
			<< "Udp request recived, value:" << message;

		request.makeFromText(message, size);

	} catch (invalid_argument error) {
		io::man.out_log(Msg(UdpServer::module_name, Logger::ERROR))
			<< "Invalid request, error: " << error.what();

		this->error = true;
	}
}

void UdpServer::processHttp() {
	if (error) {
		operation = REQUEST_DIR::ERROR_INVALID_FORMAT;
		return;
	}

	// TODO melhorar o decode aqui
	io::man.out_log(Msg()) << "Udp request recived decoded as a ping mesg";
	operation = REQUEST_DIR::DISCOVERY_PING;

	io::man.out_log(Msg()) << "Udp request, decode ended";
	request.httpContent.freeCont();
}

void UdpServer::buildResponse() {
	io::man.out_log(Msg()) << "Start the generation of the response to the udp "
							  "request";


	switch (operation) {
		case ERROR_INVALID_FORMAT:
			response.error(400);
			break;
		case DISCOVERY_PING:
			this->pingResponse();
			break;
		default:
			response.error(500);
			break;
	}

	io::man.out_log(Msg()) << "Response to the udp request generated";
	response.setAsResponse();
}

void UdpServer::pingResponse() {
	// build header
	io::man.out_log(Msg()) << "Making the response header";

	response.httpHeader.setProtocol("HTTP/1.1");
	response.httpHeader.setResult(201);
	response.httpHeader.setContentType(HttpHeader::JSON);

	// time get
	Timestamp time_now;
	time_now.now();

	// build json
	io::man.out_log(Msg()) << "Making the response content";

	// get parameters
	std::stringstream ss;
	ss << "{" << std::endl;
	ss << "\t"
	   << "\"server_ip\":\"" << ServerDriver::getMyIp().databaseFormat()
	   << "\", " << std::endl;
	ss << "\t"
	   << "\"server_port_tcp\":\"" << io::configs.tcp_port << "\", "
	   << std::endl;
	ss << "\t"
	   << "\"server_port_udp\":\"" << io::configs.udp_port << "\", "
	   << std::endl;
	ss << "\t"
	   << "\"server_alias\":\""
	   << "Monkey-MidServer"
	   << "\" " << std::endl;
	ss << "\t"
	   << "\"server_datatime\":\"" << time_now.stringFormat() << "\" "
	   << std::endl;
	ss << "}";

	// tranform on char*
	std::string json = ss.str();
	char *json_s = (char *)calloc(json.length(), sizeof(char));
	memcpy(json_s, json.c_str(), json.length());

	// end header
	response.httpHeader.setContentLength(json.length());

	// tranform into http
	io::man.out_log(Msg()) << "Tranforming the response to http";

	response.httpContent.set(json_s, json.length());
}

char *UdpServer::transformHttp() {
	// tranform
	char *buffer = response.toText();

	// log
	io::man.out_log(Msg(UdpServer::module_name, Logger::DEBUG))
		<< "Udp request recived, value: \n"
		<< buffer;

	// get data
	this->size = response.getTextSize();

	// free
	response.httpContent.freeCont();
	this->error = false;

	// end function
	return buffer;
}
