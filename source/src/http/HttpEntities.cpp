#include "../../headers/http/HttpEntities.hpp"
/////////////////////////////////////////////////////////////////////////////////////
// Http
/////////////////////////////////////////////////////////////////////////////////////
//////////////////
// Builder
/////////////////

const string Http::module_name = "Http";

Http::Http() {
	this->size = 0;
	this->response = false;
}

//////////////////
// Get
/////////////////
uint Http::getTextSize() {
	return size;
}

//////////////////
// Others
/////////////////
char *Http::toText() {
	std::string valor;
	httpHeader.setContentLength(httpContent.getSize());
	if (response)
		valor = httpHeader.responseText();
	else
		valor = httpHeader.toText();

	this->size = valor.length() + 1 + httpContent.getSize();
	char *text = (char *)calloc(size, sizeof(char));
	char *inter = NULL;

	memcpy(text, valor.c_str(), valor.length());
	inter = text + valor.length() * sizeof(char);
	memcpy(inter, "\n", 1);
	inter++;
	memcpy(inter, httpContent.getContent(), httpContent.getSize());

	return text;
}

void Http::makeFromText(char *text, uint full_size) {
	// fill header
	uint endHeader = HttpHeader::findEnd(text, full_size);

	char *text_h = (char *)malloc((endHeader + 1) * sizeof(char));
	if (text_h == NULL) {
		throw std::runtime_error("Error de alocação de memoria");
	}
	memcpy(text_h, text, endHeader);
	text_h[endHeader] = '\0';
	std::string header = text_h;
	// io::print("Routes", header, Output::PRINT);

	if (this->response)
		this->httpHeader.fromTextResponse(header);
	else
		this->httpHeader.fromText(header);
	free(text_h);

	// fill content
	endHeader++;
	uint contentSize = full_size - endHeader;
	text_h = (char *)calloc(contentSize, sizeof(char));
	text = text + endHeader * sizeof(char);
	memcpy(text_h, text, contentSize);
	this->httpContent.set(text_h, contentSize);
}

void Http::error(uint error_code) {
	std::string text;
	switch (error_code) {
		case 400:
			text = HttpError::textBadRequest();
			this->setBaseError(400, text.size(), text);
			break;
		case 401:
			text = HttpError::textUnauthorized();
			this->setBaseError(401, text.size(), text);
			break;
		case 404:
			text = HttpError::textNotFound();
			this->setBaseError(404, text.size(), text);
			break;
		case 405:
			text = HttpError::textMethodNotAllowed();
			this->setBaseError(405, text.size(), text);
			break;

		// INTERNAL ERRORS
		case 500:
			text = HttpError::textInternalError();
			this->setBaseError(500, text.size(), text);
			break;
		case 501:
			text = HttpError::textNotImplemented();
			this->setBaseError(501, text.size(), text);
			break;
		case 503:
			text = HttpError::textServiceUnavailable();
			this->setBaseError(503, text.size(), text);
			break;
		case 505:
			text = HttpError::textHTTPVersionNotSupported();
			this->setBaseError(505, text.size(), text);
			break;

		default:
			text = HttpError::textInternalError();
			this->setBaseError(500, text.size(), text);
	}
}

/////////////////////////////////////////////////////////////////////////////////////
// HttpHeader
/////////////////////////////////////////////////////////////////////////////////////
//////////////////
// Builder
/////////////////

const string HttpHeader::module_name = "HttpServer";

HttpHeader::HttpHeader() {
	contentLength = 0;
}

//////////////////
// Set
/////////////////
void HttpHeader::setMethod(METHOD method) {
	this->method = method;
}

void HttpHeader::setPath(std::string path) {
	this->path = path;
}

void HttpHeader::setContentType(std::string contentType) {
	this->contentType = contentType;
}

void HttpHeader::setContentType(FILE_TYPE filetype) {
	switch (filetype) {
		case JSON:
			this->contentType = "application/json";
			break;
		case HTML:
			this->contentType = "text/html";
			break;
		default:
			this->contentType = "text/html";
			break;
	}
}

void HttpHeader::setContentLength(uint contentLength) {
	this->contentLength = contentLength;
}

void HttpHeader::setContentLength(std::string contentLength) {
	try {
		uint inter = stoi(contentLength, nullptr);
		setContentLength(inter);
	} catch (std::invalid_argument e) {
		std::stringstream msg;
		msg << "Header parse, content lenght: " << contentLength
			<< " Not a numeric value";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#content_lenght_size_not_int");
	}
}

void HttpHeader::setConnection(std::string connection) {
	this->connection = connection;
}
void HttpHeader::setHost(std::string host) {
	this->host = host;
}

void HttpHeader::setUserAgent(std::string user_agent) {
	this->user_agent = user_agent;
}

void HttpHeader::setOrigin(ORIGIN origin) {
	this->origin = origin;
}

void HttpHeader::setResult(std::string result) {
	this->result = result;
}

HttpHeader::HTTP_RESPONSE HttpHeader::decodeResponse(int response) {
	switch (response) {
		case 200:
			return SUCCESS;
		case 201:
			return CREATED;
		case 202:
			return ACCEPTED;
		case 400:
			return INVALID_REQ;

		case 401:
			return UNAUTHORIZED;

		case 404:
			return PAGE_NOT_FOUND;
		case 405:
			return METHOD_NOT_ALLOWED;
		case 500:
			return INTERNAL_ERROR;
		case 505:
			return PROTOCOL_ERROR;
	}
}

void HttpHeader::setResult(int response) {
	HTTP_RESPONSE res = decodeResponse(response);
	switch (res) {
		case SUCCESS:
			result = "200 OK";
			break;
		case CREATED:
			result = "201 Created";
			break;
		case ACCEPTED:
			result = "202 Accepted";
			break;
		case METHOD_NOT_ALLOWED:
			result = "405 Method Not Allowed";
			break;
		case INVALID_REQ:
			result = "400 Bad Request";
			break;
		case UNAUTHORIZED:
			result = "401 Unauthorized";
			break;
		case PAGE_NOT_FOUND:
			result = "404 Not Found";
			return;
		case INTERNAL_ERROR:
			result = "500 Internal Server Error";
			break;
		case PROTOCOL_ERROR:
			result = "505 Protocol Error";
			break;
		default:
			result = "200 OK";
			break;
	}
}

//////////////////
// Get
/////////////////
std::string HttpHeader::getContentType() const {
	return contentType;
}

uint HttpHeader::getContentLength() const {
	return contentLength;
}

std::string HttpHeader::getConnection() const {
	return connection;
}

std::string HttpHeader::getHost() const {
	return host;
}

HttpHeader::ORIGIN HttpHeader::getOrigin() const {
	if (paths.size() == 0)
		return HttpHeader::OTHER;

	std::string valor = paths[0];

	if (valor.length() == 0)
		return HttpHeader::OTHER;
	if (valor.compare("LOWSERVERAPI") == 0)
		return HttpHeader::LOW;
	if (valor.compare("HIGHSERVERAPI") == 0)
		return HttpHeader::HIGH;

	return HttpHeader::OTHER;
}

std::string HttpHeader::getProtocol() const {
	return protocol;
}

void HttpHeader::setProtocol(std::string protocol) {
	this->protocol = protocol;
}

std::string HttpHeader::getResult() const {
	return result;
}

std::string HttpHeader::getPath() const {
	return path;
}

HttpHeader::METHOD HttpHeader::getMethod() const {
	return method;
}

//////////////////
// Others
/////////////////
HttpHeader::METHOD HttpHeader::stringToMethod(std::string str) {
	if (str.compare("GET")) {
		return HttpHeader::GET;
	}

	if (str.compare("PUT")) {
		return HttpHeader::PUT;
	}

	if (str.compare("POST")) {
		return HttpHeader::POST;
	}

	if (str.compare("DELETE")) {
		return HttpHeader::DELETE;
	}

	std::stringstream msg;
	msg << "Header parse, method: " << str << " Not in the switch case";
	io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
	msg.str("");
	throw std::invalid_argument("#method_not_found");
}

HttpHeader::ORIGIN HttpHeader::stringToOrigin(std::string str) {
	if (str.compare("HIGH")) {
		return HttpHeader::HIGH;
	}

	if (str.compare("LOW")) {
		return HttpHeader::LOW;
	}

	std::stringstream msg;
	msg << "Header parse, Origin: " << str << " Not in the switch case";
	io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
	msg.str("");
	throw std::invalid_argument("#origin_not_found");
}

std::string HttpHeader::getMethod_s() const {
	switch (method) {
		case GET:
			return "GET";
		case PUT:
			return "PUT";
		case POST:
			return "POST";
		case DELETE:
			return "DELETE";
	}

	std::stringstream msg;
	msg << "Header parse: " << method << " Not in the switch case";
	io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
	msg.str("");
	throw std::invalid_argument("#header_method_not_found");
}

std::string HttpHeader::toText() {
	std::string method = getMethod_s();

	std::stringstream ss;
	ss << method << " " << path << " " << protocol << "\r\n";
	ss << "Content-Type: " << contentType << "\r\n";
	ss << "Content-Length: " << contentLength << "\r\n";
	ss << "Host: " << host << "\r\n";
	ss << "User-Agent: " << user_agent << "\r\n";

	return ss.str();
}

uint HttpHeader::findEnd(char *httpText, uint max) {
	char inter = 0;
	for (uint i = 0; i < max - 1; i++) {
		if (httpText[i] == '\n')
			if (httpText[i + 1] == '\n' || httpText[i + 1] == '\r')
				return i;
	}

	std::stringstream msg;
	msg << "Error on parse the header, \\n\\n not found";
	io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
	msg.str("");

	msg << "Http parse: " << endl << httpText;
	io::logger.log(HttpHeader::module_name, Logger::DEBUG, msg.str());
	msg.str("");
	throw std::invalid_argument("#header_parse_error");
}

void HttpHeader::fromText(std::string header) {
	std::stringstream msg;

	std::size_t pos = header.find("\n");
	std::size_t pos2 = header.find("\r");
	std::string firstLine;
	if (pos == std::string::npos) {
		msg << "Error on parse the header first lines, \\n not found";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");

		msg << "Header parse: " << endl << header;
		io::logger.log(HttpHeader::module_name, Logger::DEBUG, msg.str());
		msg.str("");
		throw std::invalid_argument("#header_parse_error");
	}

	if (pos2 == pos - 1)
		firstLine = header.substr(0, pos - 1);
	else
		firstLine = header.substr(0, pos);

	parseFirst(firstLine);
	parseRoute();

	std::string json_s = header.substr(pos + 1);
	json_s = headerToJson(json_s);

	Json::Value json;
	Json::Reader reader;

	bool parsed = reader.parse(json_s, json);
	if (!parsed) {
		msg << "Error on parse the header paramas";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");

		msg << "Header parse: " << endl << header;
		io::logger.log(HttpHeader::module_name, Logger::DEBUG, msg.str());
		msg.str("");
		throw std::invalid_argument("#header_parse_error");
	}

	if (json["Content-Length"] != Json::nullValue)
		contentLength = stoi(json["Content-Length"].asString());

	if (json["Content-Type"] != Json::nullValue)
		contentType = json["Content-Type"].asString();

	if (json["Host"] != Json::nullValue)
		host = json["Host"].asString();

	if (json["Connection"] != Json::nullValue)
		connection = json["Content-Length"].asString();

	if (json["User-Agent"] != Json::nullValue)
		user_agent = json["User-Agent"].asString();
}

void HttpHeader::fromTextResponse(std::string header) {
	std::stringstream msg;

	std::size_t pos = header.find("\n");
	std::size_t pos2 = header.find("\r");
	std::string firstLine;

	if (pos == std::string::npos) {
		msg << "Error on parse the header first lines, \\n not found";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");

		msg << "Header parse: " << endl << header;
		io::logger.log(HttpHeader::module_name, Logger::DEBUG, msg.str());
		msg.str("");
		throw std::invalid_argument("#header_parse_error");
	}

	if (pos2 == pos - 1)
		firstLine = header.substr(0, pos - 1);
	else
		firstLine = header.substr(0, pos);

	parseFirstResponse(firstLine);

	std::string json_s = header.substr(pos + 1);
	json_s = headerToJson(json_s);

	Json::Value json;
	Json::Reader reader;

	bool parsed = reader.parse(json_s, json);
	if (!parsed) {
		msg << "Error on parse the header paramas";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");

		msg << "Header parse: " << endl << header;
		io::logger.log(HttpHeader::module_name, Logger::DEBUG, msg.str());
		msg.str("");
		throw std::invalid_argument("#header_parse_error");
	}

	if (json["Content-Length"] != Json::nullValue)
		contentLength = stoi(json["Content-Length"].asString());

	if (json["Content-Type"] != Json::nullValue)
		contentType = json["Content-Type"].asString();

	if (json["Host"] != Json::nullValue)
		host = json["Host"].asString();

	if (json["Connection"] != Json::nullValue)
		connection = json["Content-Length"].asString();

	if (json["User-Agent"] != Json::nullValue)
		user_agent = json["User-Agent"].asString();
}

std::string HttpHeader::headerToJson(std::string header) {
	std::string json = "{\"";
	std::replace(header.begin(), header.end(), '"', ' ');
	const char *header_c = header.c_str();
	for (uint i = 0; i < header.length(); i++) {
		if (header_c[i] == '\n') {
			json += "\",\"";
			continue;
		}

		if (header_c[i] == '\r') {
			continue;
		}

		if (header_c[i] == ':' && i + 1 < header.length()) {
			if (header_c[i + 1] == ' ') {
				json += "\":\"";
				i++;
				continue;
			}
		}
		json += header_c[i];
	}

	json += "\"}";
	return json;
}

void HttpHeader::parseFirst(std::string fLine) {
	stringstream msg;

	std::transform(fLine.begin(), fLine.end(), fLine.begin(), ::toupper);

	std::string args[3];
	size_t nofspaces = std::count(fLine.begin(), fLine.end(), ' ');
	uint i = 0, j = 0, count = 0;

	if (nofspaces < 2) {
		msg << "Invalid first line: " << fLine << "/ Invalid number of terms";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#first_line_parser_error");
	}

	std::size_t pos = fLine.find(" ");
	if (pos == std::string::npos)
		throw std::invalid_argument("#first_line_parser_error");

	args[0] = fLine.substr(0, pos);
	args[1] = fLine.substr(pos + 1);

	pos = args[1].find(" ");
	if (pos == std::string::npos)
		throw std::invalid_argument("#first_line_parser_error");

	args[2] = args[1].substr(pos + 1);
	args[1] = args[1].substr(0, pos);

	for (i = 0; i < 3; i++) {
		if (args[i].compare("HTTP/1.1") == 0)
			break;
		if (args[i].compare("HTTP/1.0") == 0)
			break;
	}

	if (i == 3) {
		msg << "Invalid first line: " << fLine << "/ dont have protocol";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#first_line_parser_error");
	}

	this->protocol = args[i];

	for (count = 0; count < 3; count++) {
		if (count == i)
			continue;
		if (args[count].compare("GET") == 0) {
			method = GET;
			break;
		}
		if (args[count].compare("POST") == 0) {
			method = POST;
			break;
		}
		if (args[count].compare("DELETE") == 0) {
			method = DELETE;
			break;
		}
		if (args[count].compare("PUT") == 0) {
			method = PUT;
			break;
		}
	}

	if (count == 3) {
		msg << "Invalid first line: " << fLine << "/ invalid protocol";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#first_line_parser_error");
	}

	for (j = 0; j < 3; j++) {
		if (j == count || j == i)
			continue;
		else
			break;
	}

	if (j == 3) {
		msg << "Invalif first line" << fLine;
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#first_line_parser_error");
	}
	path = args[j];
}

void HttpHeader::parseFirstResponse(std::string fLine) {
	std::stringstream msg;
	std::transform(fLine.begin(), fLine.end(), fLine.begin(), ::toupper);

	std::string args[3];
	size_t nofspaces = std::count(fLine.begin(), fLine.end(), ' ');
	uint i = 0, j = 0, count = 0;

	if (nofspaces < 2) {
		msg << "Invalid first line: " << fLine << "/ Invalid number of terms";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#first_line_parser_error");
	}

	std::size_t pos = fLine.find(" ");
	if (pos == std::string::npos)
		throw std::invalid_argument("#first_line_parser_error");

	args[0] = fLine.substr(0, pos);
	args[1] = fLine.substr(pos + 1);

	pos = args[1].find(" ");
	if (pos == std::string::npos)
		throw std::invalid_argument("#first_line_parser_error");

	args[2] = args[1].substr(pos + 1);
	args[1] = args[1].substr(0, pos);

	for (i = 0; i < 3; i++) {
		if (args[i].compare("HTTP/1.1") == 0)
			break;
		if (args[i].compare("HTTP/1.0") == 0)
			break;
	}

	if (i == 3) {
		msg << "Invalid first line: " << fLine << "/ dont have protocol";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#first_line_parser_error");
	}

	this->protocol = args[i];

	for (count = 0; count < 3; count++) {
		if (count == i)
			continue;
		try {
			uint status = stoi(args[count], nullptr, 10);
			this->response_status = this->decodeResponse(status);
			break;
		} catch (std::invalid_argument e) {
			continue;
		}
	}

	if (count == 3) {
		msg << "Invalid first line: " << fLine << "/ dont have status";
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#first_line_parser_error");
	}

	for (j = 0; j < 3; j++) {
		if (j == count || j == i)
			continue;
		else
			break;
	}

	if (j == 3) {
		msg << "Invalif first line" << fLine;
		io::logger.log(HttpHeader::module_name, Logger::ERROR, msg.str());
		msg.str("");
		throw std::invalid_argument("#first_line_parser_error");
	}
	this->status_msg = args[j];
}

void HttpHeader::parseRoute() {
	std::size_t pos = this->path.find('?');
	std::string atom, atom_r;
	std::string route;
	if (pos == std::string::npos)
		route = path.substr(0, pos);
	else
		route = path;

	std::size_t pathPos = route.find("/");

	if (pathPos == 0) {
		route = route.substr(1);
		pathPos = route.find("/");
	}
	while (pathPos != std::string::npos) {
		atom = route.substr(0, pathPos);
		paths.push_back(atom);
		route = route.substr(pathPos + 1);
		pathPos = route.find("/");
	}
	paths.push_back(route);

	if (pos == std::string::npos)
		return;

	std::size_t mid;
	std::string arguments = path.substr(pos + 1);
	pathPos = arguments.find("&");
	while (pathPos != std::string::npos) {
		mid = arguments.find("=");
		atom = arguments.substr(0, mid);
		atom_r = arguments.substr(mid + 1);

		this->args.insert(std::pair<std::string, std::string>(atom, atom_r));

		arguments = arguments.substr(pathPos + 1);
		pathPos = arguments.find("&");
	}

	mid = arguments.find("=");
	atom = arguments.substr(0, mid);
	atom_r = arguments.substr(mid + 1);

	this->args.insert(std::pair<std::string, std::string>(atom, atom_r));
}

void HttpHeader::fromText(char *, uint) {
}

std::string HttpHeader::responseText() {
	std::stringstream ss;
	ss << protocol << " " << result << "\r\n";
	ss << "Content-Type: " << contentType << "\r\n";
	ss << "Content-Length: " << contentLength << "\r\n";

	return ss.str();
}

void Http::setAsResponse() {
	this->response = true;
}
/////////////////////////////////////////////////////////////////////////////////////
// HttpContent
/////////////////////////////////////////////////////////////////////////////////////
///////////////////
// Builder
///////////////////
const string HttpContent::module_name = "HttpContent";

HttpContent::HttpContent() {
	this->content = NULL;
	this->size = 0;
}

HttpContent::HttpContent(char *content, uint size) {
	this->content = NULL;
	this->size = 0;
	set(content, size);
}

///////////////////
// Get
///////////////////
uint HttpContent::getSize() {
	return size;
}

char *HttpContent::getContent() {
	return content;
}

///////////////////
// Set
///////////////////
void HttpContent::set(char *content, uint size) {
	if (this->content != NULL)
		freeCont();
	this->content = content;
	this->size = size;
}

void HttpContent::set(std::string text) {
	char *json_s = (char *)calloc(text.length(), sizeof(char));
	memcpy(json_s, text.c_str(), text.length());
	this->set(json_s, text.length());
}

///////////////////
// Others
///////////////////
void HttpContent::freeCont() {
	if (this->content != NULL)
		free(content);
	content = NULL;
}

void HttpContent::copy(char *valor) {
	memcpy(valor, content, size);
}
