#include "../../headers/http/HttpEntities.hpp"
/////////////////////////////////////////////////////////////////////////////////////
// Http
/////////////////////////////////////////////////////////////////////////////////////

void Http::setBaseError(uint code, uint size, std::string text) {
	this->httpHeader.setResult(code);
	this->httpHeader.setProtocol("HTTP/1.1");
	this->httpHeader.setContentType(HttpHeader::HTML);
	this->httpHeader.setContentLength(size);
	this->httpContent.set(text);
}

/////////////////////////////////////////////////////////////////////////////////////
// HttpError
/////////////////////////////////////////////////////////////////////////////////////
std::string HttpError::textBadRequest() {
	std::stringstream ss;
	ss << "<HTML><HEAD>" << endl;
	ss << "<TITLE>400 Bad Request</TITLE></HEAD><BODY>" << endl;
	ss << "<H1>400 Bad Request</H1>" << endl;
	ss << "Bad Request" << endl;
	ss << "Por favor, acesse o manual, ou tente / para melhores explicações"
	   << endl;
	ss << "</BODY></HTML>" << endl;
	return ss.str();
}
std::string HttpError::textUnauthorized() {
	std::stringstream ss;
	ss << "<HTML><HEAD>" << endl;
	ss << "<TITLE>401 Unauthorized</TITLE></HEAD><BODY>" << endl;
	ss << "<H1>401 Unauthorized</H1>" << endl;
	ss << "Unauthorized" << endl;
	ss << "Por favor, acesse o manual, ou tente / para melhores explicações"
	   << endl;
	ss << "</BODY></HTML>" << endl;
	return ss.str();
}
std::string HttpError::textNotFound() {
	std::stringstream ss;
	ss << "<HTML><HEAD>" << endl;
	ss << "<TITLE>404 Page Not Found</TITLE></HEAD><BODY>" << endl;
	ss << "<H1>404 Page Not Found</H1>" << endl;
	ss << "Page Not Found" << endl;
	ss << "Por favor, acesse o manual, ou tente / para melhores explicações"
	   << endl;
	ss << "</BODY></HTML>" << endl;
	return ss.str();
}
std::string HttpError::textMethodNotAllowed() {
	std::stringstream ss;
	ss << "<HTML><HEAD>" << endl;
	ss << "<TITLE>405 Method Not Allowed</TITLE></HEAD><BODY>" << endl;
	ss << "<H1>405 Method Not Allowed</H1>" << endl;
	ss << "Method Not Allowed" << endl;
	ss << "Por favor, acesse o manual, ou tente / para melhores explicações"
	   << endl;
	ss << "</BODY></HTML>" << endl;
	return ss.str();
}

// INTERNAL ERRORS
std::string HttpError::textInternalError() {
	std::stringstream ss;
	ss << "<HTML><HEAD>" << endl;
	ss << "<TITLE>500 Internal Error</TITLE></HEAD><BODY>" << endl;
	ss << "<H1>500 Internal Error</H1>" << endl;
	ss << "Internal Error" << endl;
	ss << "Por favor, acesse o manual, ou tente / para melhores explicações"
	   << endl;
	ss << "</BODY></HTML>" << endl;
	return ss.str();
}
std::string HttpError::textNotImplemented() {
	std::stringstream ss;
	ss << "<HTML><HEAD>" << endl;
	ss << "<TITLE>404 Page Not Found</TITLE></HEAD><BODY>" << endl;
	ss << "<H1>404 Page Not Found</H1>" << endl;
	ss << "Page Not Found" << endl;
	ss << "Por favor, acesse o manual, ou tente / para melhores explicações"
	   << endl;
	ss << "</BODY></HTML>" << endl;
	return ss.str();
}
std::string HttpError::textServiceUnavailable() {
	std::stringstream ss;
	ss << "<HTML><HEAD>" << endl;
	ss << "<TITLE>404 Page Not Found</TITLE></HEAD><BODY>" << endl;
	ss << "<H1>404 Page Not Found</H1>" << endl;
	ss << "Page Not Found" << endl;
	ss << "Por favor, acesse o manual, ou tente / para melhores explicações"
	   << endl;
	ss << "</BODY></HTML>" << endl;
	return ss.str();
}
std::string HttpError::textHTTPVersionNotSupported() {
	std::stringstream ss;
	ss << "<HTML><HEAD>" << endl;
	ss << "<TITLE>404 Page Not Found</TITLE></HEAD><BODY>" << endl;
	ss << "<H1>404 Page Not Found</H1>" << endl;
	ss << "Page Not Found" << endl;
	ss << "Por favor, acesse o manual, ou tente / para melhores explicações"
	   << endl;
	ss << "</BODY></HTML>" << endl;
	return ss.str();
}
