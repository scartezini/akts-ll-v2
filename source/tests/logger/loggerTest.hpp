#ifndef LOGGER_TEST_H
#define LOGGER_TEST_H

#include <string>
#include <iostream>
#include <fstream>

#include <cppunit/extensions/HelperMacros.h>
#include <stdlib.h>

#include "../../headers/Logger.hpp"

class LoggerTestSuit : public CppUnit::TestCase {
	CPPUNIT_TEST_SUITE(LoggerTestSuit);
	CPPUNIT_TEST(lowFlow);
	CPPUNIT_TEST(heavyFlow);
	CPPUNIT_TEST_SUITE_END();

public:

	void setUp();
	void tearDown();

	// low flow test
	void lowFlow();

	// heavy flow test
	void heavyFlow();

	// known errors
};

#endif
