#include "loggerTest.hpp"

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(LoggerTestSuit);

void LoggerTestSuit::setUp() {}

void LoggerTestSuit::tearDown() {}

// low flow test
void LoggerTestSuit::lowFlow() {
	std::ifstream file;
	std::string str;

	CPPUNIT_ASSERT_NO_THROW(logger.log(INFO, "info1"));
	sleep(2);
	CPPUNIT_ASSERT_NO_THROW(logger.log(ERROR, "error1"));
	sleep(4);
	CPPUNIT_ASSERT_NO_THROW(logger.log(INFO, "info2"));
	sleep(1);

	file.open("log.txt", std::ios::in);
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("INFO - info1"));
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("ERROR - error1"));
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("INFO - info2"));
	file.close();
}

// heavy flow test
void LoggerTestSuit::heavyFlow() {
	std::ifstream file;
	std::string str;

	// create a clean log file
	file.open("log.txt", std::ios::trunc | std::ios::out);
	file.close();

	CPPUNIT_ASSERT_NO_THROW(logger.log(INFO, "info1"));
	CPPUNIT_ASSERT_NO_THROW(logger.log(ERROR, "error1"));
	CPPUNIT_ASSERT_NO_THROW(logger.log(INFO, "info2"));
	CPPUNIT_ASSERT_NO_THROW(logger.log(ERROR, "error2"));
	CPPUNIT_ASSERT_NO_THROW(logger.log(ERROR, "error3"));
	CPPUNIT_ASSERT_NO_THROW(logger.log(INFO, "info3"));
	CPPUNIT_ASSERT_NO_THROW(logger.log(INFO, "info4"));

	file.open("log.txt", std::ios::in);
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("INFO - info1"));
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("ERROR - error1"));
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("INFO - info2"));
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("ERROR - error2"));
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("ERROR - error3"));
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("INFO - info3"));
	std::getline(file, str);
	CPPUNIT_ASSERT(0==str.compare("INFO - info4"));
	file.close();
}

// known errors