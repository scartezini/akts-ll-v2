#include <iostream>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>


#include "../libs/output/IOMananger.hpp"
using namespace std;

int main(int argc, char* argv[]){


    //system("rm -rf ./resources/monkey.db");
    io::configs.openFile("./resources/db_config.json");
    io::configs.setConfigs();
    cout << "Teste" << endl;
    CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest();
    CppUnit::TextUi::TestRunner runner;
    runner.addTest( suite );
    runner.setOutputter( new CppUnit::CompilerOutputter( &runner.result(), std::cerr));
    bool wasSucessful = runner.run("", false, true, true);
    return wasSucessful ? 0 : 1;
}
