#include "jsonTest.hpp"

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(JsonTestSuit);

void JsonTestSuit::setUp() {}

void JsonTestSuit::tearDown() {}

void JsonTestSuit::user() {
	User u1;
	User u2;

	u1.setId("13");
	u1.setName("teste");
	u1.setCpf("123");
	u1.setRg("456");
	u1.setDestiny("dest");
	u1.setEmail("mail");
	u1.setPassword("pswd");
	u1.setPhone("987");

	u2.fromJson(u1.toJson());

	CPPUNIT_ASSERT(u1==u2);
}

void JsonTestSuit::dataType() {
	DataType d1;
	DataType d2;

	d1.setName("name");
	d1.setSensor("sens");
	d1.setManufacturer("MAN");

	d2.fromJson(d1.toJson());

	CPPUNIT_ASSERT(d1==d2);
}

void JsonTestSuit::accessPoint() {
	AccessPoint a1;
	AccessPoint a2;

	a1.setId("96");
	a1.setName("teus");
	a1.setType("t2");
	a1.setIpAddress(IpAddress(42,12,45,1));

	a2.fromJson(a1.toJson());

	CPPUNIT_ASSERT(a1==a2);

}

void JsonTestSuit::data() {
	User u1;
	DataType dt;
	Data d1;
	Data d2;

	u1.setId("13");
	u1.setName("teste");
	u1.setCpf("123");
	u1.setRg("456");
	u1.setDestiny("dest");
	u1.setEmail("mail");
	u1.setPassword("pswd");
	u1.setPhone("987");

	dt.setName("name");
	dt.setSensor("sens");
	dt.setManufacturer("MAN");

	d1.setId("34");
	d1.setUser(u1);
	d1.setDataType(dt);
	d1.setValue("char", 4);

	d2.fromJson(d1.toJson());

	CPPUNIT_ASSERT(d1==d2);
}

void JsonTestSuit::stuff() {
	Stuff s1;
	Stuff s2;
	User u1;

	u1.setId("13");
	u1.setName("teste");
	u1.setCpf("123");
	u1.setRg("456");
	u1.setDestiny("dest");
	u1.setEmail("mail");
	u1.setPassword("pswd");
	u1.setPhone("987");

	s1.setId("72");
	s1.setName("tyhn");
	s1.setDescription("desc cc");
	s1.setUser(u1);

	s2.fromJson(s1.toJson());

	CPPUNIT_ASSERT(s1==s2);
}

void JsonTestSuit::dataAccess() {
	DataAccess da1;
	DataAccess da2;
	AccessPoint ap;
	Data d1;
	User u1;
	DataType dt;

	ap.setId("96");
	ap.setName("teus");
	ap.setType("t2");
	ap.setIpAddress(IpAddress(42,12,45,1));

	u1.setId("13");
	u1.setName("teste");
	u1.setCpf("123");
	u1.setRg("456");
	u1.setDestiny("dest");
	u1.setEmail("mail");
	u1.setPassword("pswd");
	u1.setPhone("987");

	dt.setName("name");
	dt.setSensor("sens");
	dt.setManufacturer("MAN");

	d1.setId("34");
	d1.setUser(u1);
	d1.setDataType(dt);
	d1.setValue("char", 4);

	da1.setTranslatedId("81");
	da1.setAccessPoint(ap);
	da1.setData(d1);

	da2.fromJson(da1.toJson());

	CPPUNIT_ASSERT(da1==da2);
}

void JsonTestSuit::access() {
	Access a1;
	Access a2;
	User u1;
	Stuff s1;
	DataType dt;
	Data d1;
	AccessPoint ap;
	Timestamp ts;

	u1.setId("13");
	u1.setName("teste");
	u1.setCpf("123");
	u1.setRg("456");
	u1.setDestiny("dest");
	u1.setEmail("mail");
	u1.setPassword("pswd");
	u1.setPhone("987");

	s1.setId("72");
	s1.setName("tyhn");
	s1.setDescription("desc cc");
	s1.setUser(u1);

	dt.setId("34");
	dt.setName("name");
	dt.setSensor("sens");
	dt.setManufacturer("MAN");

	d1.setId("34");
	d1.setUser(u1);
	d1.setDataType(dt);
	d1.setValue("char", 8);

	ap.setId("96");
	ap.setName("teus");
	ap.setType("2");
	ap.setIpAddress(IpAddress(42,12,45,1));

	ts = Timestamp(10,10,10,10,10,10,10);

	a1.setId("28");
	a1.setTimestamp(ts);
	a1.setType("4");
	a1.setUser(u1);
	a1.setAccessPoint(ap);
	a1.setData(d1);
	a1.setStuff(s1);

	a2.fromJson(a1.toJson());

	CPPUNIT_ASSERT(a1==a2);
}