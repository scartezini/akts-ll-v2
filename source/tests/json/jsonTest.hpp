#ifndef JSON_TEST_H
#define JSON_TEST_H

#include <string>

#include <cppunit/extensions/HelperMacros.h>
#include <stdlib.h>

#include "../../headers/Entities.hpp"

class JsonTestSuit : public CppUnit::TestCase {
	CPPUNIT_TEST_SUITE(JsonTestSuit);
	CPPUNIT_TEST(user);
	CPPUNIT_TEST(dataType);
	CPPUNIT_TEST(accessPoint);
	CPPUNIT_TEST(data);
	CPPUNIT_TEST(stuff);
	CPPUNIT_TEST(dataAccess);
	CPPUNIT_TEST(access);
	CPPUNIT_TEST_SUITE_END();

public:

	void setUp();
	void tearDown();

	void user();
	void dataType();
	void accessPoint();
	void data();
	void stuff();
	void dataAccess();
	void access();

};

#endif
