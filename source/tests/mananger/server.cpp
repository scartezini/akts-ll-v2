#include <iostream>

#include "../../headers/ThreadPool.hpp"
#include "../../headers/tcpCom.hpp"

using namespace std;

int main() {
	TCP_Listener listener(1234);
	int socket;
	cout << "[main] creating pool" << endl;
	createThreadPool(3);
	cout << "[server] start listener" << endl;
	listener.start();
	for(;;){}

}