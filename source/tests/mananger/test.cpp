#include <iostream>

#include "../../headers/ThreadPool.hpp"
#include "../../headers/tcpCom.hpp"

using namespace std;

int main() {
	int socket;
	
	cout << "[main] connecting" << endl;
	socket = tcpConnect("10.190.60.68", 1234);
	cout << "[main] done" << endl;
}