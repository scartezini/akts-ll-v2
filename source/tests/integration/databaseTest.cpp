#include "databaseTest.hpp"

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( DataBaseTestSuccess );

static int i_inter = 0;

// void DataBaseTestSuccess::setUp(){
// 	u.setName("Nome1");
//     u.setCpf("032.093.481-03");
//     u.setRg("1111111");
//     u.setDestiny("Akuntsu");
//     u.setEmail("Nome1@akuntsu.com");
//     u.setPassword("12345678");
//     u.setPhone("(61)997748810");
// 	char *valor = (char*)calloc(11, sizeof(char));
//  	memcpy(valor, "0123456789", 11);
// 	Photo photo(valor, 11);
// 	free(valor);
//     u.setPhoto(photo);
//
// 	//AccessPoint
// 	ap.setName("AP1");
// 	ap.setType("Tipo1");
// 	ap.setIpAddress(std::string("192.168.1.1"));
//
// 	dt.setName("DataType 1");
// 	dt.setSensor("NFC");
// 	dt.setManufacturer("Proprietario");
//
// 	valor = (char*)calloc(11, sizeof(char));
// 	memcpy(valor, "1234", 5);
// 	Blob blob(valor, 5);
// 	data.setValue(blob); //std::string, int
// 	data.setUser(u); //user*
// 	data.setDataType(dt); //datatype*
//
// 	stuff.setName("PC");
// 	stuff.setDescription("pc bolado");
// 	stuff.setUser(u);
//
// 	Timestamp ts("2015-07-14T17:15:25.014Z");
// 	access.setUser(u);
// 	access.setAccessPoint(ap);
// 	access.setData(data);
// 	access.setStuff(stuff);
// 	access.setTimestamp(ts);
//
// 	stuff.setName("PC");
// 	stuff.setDescription("pc bolado");
// 	stuff.setUser(u);
//
// 	dataAccess.setAccessPoint(ap);
// 	dataAccess.setData(data);
// }
//
// void DataBaseTestSuccess::tearDown(){}
//
// void DataBaseTestSuccess::reset(){
// 	if(i_inter == 0){
// 		system("rm -f monkey.db");
// 		system("rm -f monkeyC.db");
// 		Database database;
// 		system("cp monkey.db monkeyC.db");
// 		i_inter++;
// 	}else{
// 		system("rm -f monkey.db");
// 		system("cp monkeyC.db monkey.db");
// 	}
// 	u = User();
// 	ap = AccessPoint();
// 	dt = DataType();
// 	data = Data();
// 	stuff = Stuff();
// 	access = Access();
// 	dataAccess = DataAccess();
//
// 	u.setName("Nome1");
//     u.setCpf("032.093.481-03");
//     u.setRg("1111111");
//     u.setDestiny("Akuntsu");
//     u.setEmail("Nome1@akuntsu.com");
//     u.setPassword("12345678");
//     u.setPhone("(61)997748810");
// 	char *valor = (char*)calloc(11, sizeof(char));
//  	memcpy(valor, "0123456789", 11);
// 	Photo photo(valor, 11);
//     u.setPhoto(photo);
//
// 	//AccessPoint
// 	ap.setName("AP1");
// 	ap.setType("Tipo1");
// 	ap.setIpAddress(std::string("192.168.1.1"));
//
// 	dt.setName("DataType 1");
// 	dt.setSensor("NFC");
// 	dt.setManufacturer("Proprietario");
//
// 	data.setValue("1234", 4); //std::string, int
//
// 	stuff.setName("PC");
// 	stuff.setDescription("pc bolado");
//
// 	Timestamp ts("2015-07-14T17:15:25.014Z");
//
//
// 	stuff.setName("PC");
// 	stuff.setDescription("pc bolado");
//
// 	dataAccess.setTranslatedId(10);
// }
//
// //start
// void DataBaseTestSuccess::executeComand(){
// }
//
// void DataBaseTestSuccess::databaseSetup(){
// 	system("rm -f monkey.db");
// 	Database database;
// 	database.openConnection();
//
// 	CPPUNIT_ASSERT(database.existsTable("USER"));
// 	CPPUNIT_ASSERT(database.existsTable("ACCESSPOINT"));
// 	CPPUNIT_ASSERT(database.existsTable("ACCESS"));
// 	CPPUNIT_ASSERT(database.existsTable("STUFF"));
// 	CPPUNIT_ASSERT(database.existsTable("DATA"));
// 	CPPUNIT_ASSERT(database.existsTable("DATATYPE"));
// 	CPPUNIT_ASSERT(database.existsTable("DATA_AP_RELATION"));
// 	CPPUNIT_ASSERT(database.existsTable("U_AP_RELATION"));
//
// 	//TODO Fazer a veri para todas as sequencias
// 	CPPUNIT_ASSERT(database.existsSequence("USER_SEQ"));
// 	CPPUNIT_ASSERT(database.existsSequence("ACCESSPOINT_SEQ"));
// 	CPPUNIT_ASSERT(database.existsSequence("ACCESS_SEQ"));
// 	CPPUNIT_ASSERT(database.existsSequence("STUFF_SEQ"));
// 	CPPUNIT_ASSERT(database.existsSequence("DATA_SEQ"));
// 	CPPUNIT_ASSERT(database.existsSequence("DATATYPE_SEQ"));
//
//     database.closeConnection();
// }
//
// //saving
// void DataBaseTestSuccess::saveUser(){
// 	this->reset();
//     Database::saveUser(&u);
//     User *user = Database::getUser(u.getId());
// 	CPPUNIT_ASSERT(u == *user);
// 	delete user;
// }
// void DataBaseTestSuccess::saveAccessPoint(){
// 	this->reset();
// 	Database::saveAccessPoint(&ap);
// 	AccessPoint *apoint = Database::getAccessPoint(ap.getId());
// 	CPPUNIT_ASSERT(ap == *apoint);
// 	delete apoint;
// }
//
// void DataBaseTestSuccess::saveUserApRelation(){
// 	this->reset();
//
// 	Database::saveUser(&u);
// 	Database::saveAccessPoint(&ap);
// 	Database::saveUserApRelation(&u, &ap);
// 	Database::getUserAccessPoints(&u);
// 	CPPUNIT_ASSERT(u.getAccessPointsList()->size() == 1);
// 	AccessPoint accessPoint = (*(u.getAccessPointsList()))[0];
// 	CPPUNIT_ASSERT(accessPoint == ap);
// }
//
// void DataBaseTestSuccess::saveDatatype(){
// 	this->reset();
//
// 	Database::saveDatatype(&dt);
// 	DataType *d = Database::getDataTypes(dt.getId());
// 	CPPUNIT_ASSERT(*d == dt);
// 	delete d;
// }
//
// void DataBaseTestSuccess::saveData(){
// 	this->reset();
//
// 	Database::saveData(&data);
// 	Data *d = Database::getData(data.getId());
// 	CPPUNIT_ASSERT(*d == data);
// 	delete d;
// }
//
// void DataBaseTestSuccess::saveStuff(){
// 	this->reset();
//
// 	Database::saveUser(&u);
// 	stuff.setUser(u);
// 	Database::saveStuff(&stuff);
//
// 	Database::getMyStuffs(&u);
// 	CPPUNIT_ASSERT(u.getStuffsList()->size() == 1);
// 	Stuff stuffLocal = (*(u.getStuffsList()))[0];
// 	CPPUNIT_ASSERT(stuff == stuffLocal);
// }
//
// void DataBaseTestSuccess::saveAccess(){
// 	this->reset();
//
// 	Database::saveUser(&u);
//
// 	//accessPoint
// 	Database::saveAccessPoint(&ap);
// 	Database::saveUserApRelation(&u, &ap);
//
// 	//dataType
// 	Database::saveDatatype(&dt);
//
// 	//data
// 	data.setUser(u);
// 	data.setDataType(dt);
// 	Database::saveData(&data);
//
// 	//stuff
// 	stuff.setUser(u);
// 	Database::saveStuff(&stuff);
//
// 	//access
// 	access.setUser(u);
// 	access.setData(data);
// 	access.setStuff(stuff);
// 	access.setAccessPoint(ap);
// 	Database::saveAccess(&access);
//
// 	Access *accessLocal = Database::getAccess(access.getId());
// 	CPPUNIT_ASSERT(*accessLocal == access);
// 	delete accessLocal;
// }
//
// void DataBaseTestSuccess::saveDataAccess(){
// 	this->reset();
// 	Database::saveUser(&u);
// 	Database::saveAccessPoint(&ap);
// 	Database::saveDatatype(&dt);
// 	data.setUser(u);
// 	data.setDataType(dt);
// 	Database::saveData(&data);
// 	dataAccess.setAccessPoint(ap);
// 	dataAccess.setData(data);
// 	Database::saveDataAccess(&dataAccess);
//
// 	Database::getAccessPointDataAccess(&ap);
// 	CPPUNIT_ASSERT(ap.getDataAccessList()->size() > 0);
// 	DataAccess datAss = (*(ap.getDataAccessList()))[0];
// 	CPPUNIT_ASSERT(dataAccess == datAss);
// }

// //retrive
// //User
// void DataBaseTestSuccess::getUser(){
// 	this->reset();
// 	Database::saveUser(&u);
// 	User *user = Database::getUser(u.getId());
// 	CPPUNIT_ASSERT(*user == u);
// }

// void DataBaseTestSuccess::getUserAccess(){
// 	this->reset();
// 	Database::saveUser(&u);
// 	access.setUser(u);
// 	Database::saveAccess(&access);
// 	Database::saveAccess(&access);
// 	Database::saveAccess(&access);
// 	Database::saveAccess(&access);
// 	Database::getUserAccess(&u);
// 	std::vector<Access> *allAccess = u.getAccessList();
// 	CPPUNIT_ASSERT(allAccess->size() == 4);
// 	for(int i = 0; i < 4; i++){
// 		access.setId(i);
// 		CPPUNIT_ASSERT((*allAccess)[i] == access);
// 	}
// }

// void DataBaseTestSuccess::getUserAccessPoints(){
// 	this->reset();
// 	Database::saveUser(&u);
// 	Database::saveAccessPoint(&ap);
// 	Database::saveUserApRelation(&u, &ap);
// 	Database::saveAccessPoint(&ap);
// 	Database::saveUserApRelation(&u, &ap);
// 	Database::saveAccessPoint(&ap);
// 	Database::saveUserApRelation(&u, &ap);
// 	Database::saveAccessPoint(&ap);
// 	Database::saveUserApRelation(&u, &ap);
// 	Database::getUserAccessPoints(&u);
// 	std::vector<AccessPoint> *all = u.getAccessPointsList();
// 	CPPUNIT_ASSERT(all->size() == 4);
// 	for(int i = 0; i < 4; i++){
// 		ap.setId(i);
// 		CPPUNIT_ASSERT((*all)[i] == ap);
// 	}
// }

// void DataBaseTestSuccess::getUserDatas(){
// 	this->reset();
// 	Database::saveUser(&u);
// 	Database::saveDatatype(&dt);
// 	data.setUser(u);
// 	data.setDataType(dt);
// 	Database::saveData(&data);
// 	Database::saveData(&data);
// 	Database::saveData(&data);
// 	Database::saveData(&data);
// 	Database::getUserDatas(&u);
// 	std::vector<Data> *all = u.getDatasList();
// 	CPPUNIT_ASSERT(all->size() == 4);
// 	for(int i = 0; i < 4; i++){
// 		data.setId(i);
// 		CPPUNIT_ASSERT((*all)[i] == data);
// 	}
// }


void DataBaseTestSuccess::getMyStuffs(){
	this->reset();
	Database::saveUser(&u);
	stuff.setUser(u);
	Database::saveStuff(&stuff);
	Database::saveStuff(&stuff);
	Database::saveStuff(&stuff);
	Database::saveStuff(&stuff);
	Database::getMyStuffs(&u);
	std::vector<Stuff> *all = u.getStuffsList();
	CPPUNIT_ASSERT(all->size() == 4);
	for(int i = 0; i < 4; i++){
		stuff.setId(i);
		CPPUNIT_ASSERT((*all)[i] == stuff);
	}
}

//Access
void DataBaseTestSuccess::getAccess(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);

	//stuff
	stuff.setUser(u);
	Database::saveStuff(&stuff);

	//access
	access.setUser(u);
	access.setData(data);
	access.setStuff(stuff);
	access.setAccessPoint(ap);
	Database::saveAccess(&access);

	Access *accessLocal = Database::getAccess(access.getId());
	CPPUNIT_ASSERT(*accessLocal == access);
	delete accessLocal;
}

//AcessPoint
void DataBaseTestSuccess::getAccessPoint(){
	this->reset();
	Database::saveAccessPoint(&ap);
	AccessPoint *accessPoint = Database::getAccessPoint(ap.getId());
	CPPUNIT_ASSERT(*accessPoint == ap);
	delete accessPoint;
}

void DataBaseTestSuccess::getAccessPointUsers(){
	this->reset();
	//user
	Database::saveAccessPoint(&ap);

	Database::saveUser(&u);
	Database::saveUserApRelation(&u, &ap);
	Database::saveUser(&u);
	Database::saveUserApRelation(&u, &ap);
	Database::saveUser(&u);
	Database::saveUserApRelation(&u, &ap);
	Database::saveUser(&u);
	Database::saveUserApRelation(&u, &ap);
	Database::getAccessPointUsers(&ap);
	std::vector<User> *all = ap.getUsersList();
	CPPUNIT_ASSERT(all->size() == 4);
	for(int i = 0; i < 4; i++){
		u.setId(i);
		CPPUNIT_ASSERT((*all)[i] == u);
	}
}

void DataBaseTestSuccess::getAccessPointAccess(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);

	//stuff
	stuff.setUser(u);
	Database::saveStuff(&stuff);

	//access
	access.setUser(u);
	access.setData(data);
	access.setStuff(stuff);
	access.setAccessPoint(ap);
	Database::saveAccess(&access);
	Database::saveAccess(&access);
	Database::saveAccess(&access);
	Database::saveAccess(&access);
	Database::getAccessPointAccess(&ap);
	std::vector<Access> *all = ap.getAccessList();
	CPPUNIT_ASSERT(all->size() == 4);
	for(int i = 0; i < 4; i++){
		access.setId(i);
		CPPUNIT_ASSERT((*all)[i] == access);
	}
}

void DataBaseTestSuccess::getAccessPointDataAccess(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);


	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);

	//dataAccess
	dataAccess.setAccessPoint(ap);
	dataAccess.setData(data);
	Database::saveDataAccess(&dataAccess);
	Database::saveData(&data);
	dataAccess.setData(data);
	Database::saveDataAccess(&dataAccess);
	Database::saveData(&data);
	dataAccess.setData(data);
	Database::saveDataAccess(&dataAccess);
	Database::saveData(&data);
	dataAccess.setData(data);
	Database::saveDataAccess(&dataAccess);
	Database::getAccessPointDataAccess(&ap);
	std::vector<DataAccess> *all = ap.getDataAccessList();
	CPPUNIT_ASSERT(all->size() == 4);
	for(int i = 0; i < 4; i++){
		data.setId(i);
		dataAccess.setData(data);
		CPPUNIT_ASSERT((*all)[i] == dataAccess);
	}
}

//Data

//stuff
void DataBaseTestSuccess::getStuffAccess(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);

	//stuff
	stuff.setUser(u);
	Database::saveStuff(&stuff);

	//access
	access.setUser(u);
	access.setData(data);
	access.setStuff(stuff);
	access.setAccessPoint(ap);
	Database::saveAccess(&access);
	Database::saveAccess(&access);
	Database::saveAccess(&access);
	Database::saveAccess(&access);
	Database::getStuffAccess(&stuff);

	std::vector<Access> *all = stuff.getAccessList();
	CPPUNIT_ASSERT(all->size() == 4);
	for(int i = 0; i < 4; i++){
		access.setId(i);
		CPPUNIT_ASSERT((*all)[i] == access);
	}

}

//DataType
void DataBaseTestSuccess::getDataTypes(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	DataType *type = Database::getDataTypes(dt.getId());
	CPPUNIT_ASSERT(*type == dt);
	delete type;
}

void DataBaseTestSuccess::getDataTypeData(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);
	Database::saveData(&data);
	Database::saveData(&data);
	Database::saveData(&data);


	Database::getDataTypeData(&dt);
	std::vector<Data> *all = dt.getDatasList();
	CPPUNIT_ASSERT(all->size() == 4);
	for(int i = 0; i < 4; i++){
		data.setId(i);
		CPPUNIT_ASSERT((*all)[i] == data);
	}
}

//update and delete
void DataBaseTestSuccess::updateAccess(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);

	//stuff
	stuff.setUser(u);
	Database::saveStuff(&stuff);

	//access
	access.setUser(u);
	access.setData(data);
	access.setStuff(stuff);
	access.setAccessPoint(ap);
	Timestamp timest("2015-07-14T17:15:25.014Z");
	access.setTimestamp(timest);
	Database::saveAccess(&access);
	timest = Timestamp("2015-07-14T17:16:25.014Z");
	access.setTimestamp(timest);
	Database::updateAccess(&access);

	Access *accessLocal = Database::getAccess(access.getId());
	CPPUNIT_ASSERT(*accessLocal == access);
	delete accessLocal;
}

void DataBaseTestSuccess::deleteAccess(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);

	//stuff
	stuff.setUser(u);
	Database::saveStuff(&stuff);

	//access
	access.setUser(u);
	access.setData(data);
	access.setStuff(stuff);
	access.setAccessPoint(ap);
	Database::saveAccess(&access);
	Database::deleteAccess(&access);

	Access *accessLocal = Database::getAccess(access.getId());
	CPPUNIT_ASSERT(accessLocal->getId() == UINT_MAX);
	delete accessLocal;
}

//data
void DataBaseTestSuccess::updateData(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	//data
	data.setUser(u);
	data.setDataType(dt);
	data.setValue("3333", 4);
	Database::saveData(&data);
	data.setValue("4444", 4);
	Database::updateData(&data);

	Data *dataLocal = Database::getData(data.getId());
	CPPUNIT_ASSERT(*dataLocal == data);
	delete dataLocal;
}

void DataBaseTestSuccess::deleteData(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);

	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);
	Database::deleteData(&data);

	Data *dataLocal = Database::getData(data.getId());
	CPPUNIT_ASSERT(dataLocal->getId() == UINT_MAX);
	delete dataLocal;
}

//stuffs
void DataBaseTestSuccess::updateStuff(){
	this->reset();
	//user
	Database::saveUser(&u);
	//stuff
	stuff.setUser(u);
	stuff.setDescription("Tipo 1 tipo 1");
	Database::saveStuff(&stuff);
	stuff.setDescription("Tipo 1 tipo 2");
	Database::updateStuff(&stuff);

	Database::getMyStuffs(&u);
	CPPUNIT_ASSERT(u.getStuffsList()->size() == 1);
	Stuff stuffLocal = (*(u.getStuffsList()))[0];
	CPPUNIT_ASSERT(stuffLocal == stuff);
}

void DataBaseTestSuccess::deleteStuff(){
	this->reset();
	//user
	Database::saveUser(&u);
	//stuff
	stuff.setUser(u);
	Database::saveStuff(&stuff);
	Database::deleteStuff(&stuff);

	Database::getMyStuffs(&u);
	CPPUNIT_ASSERT(u.getStuffsList()->size() == 0);
}

//datatype
void DataBaseTestSuccess::updateDatatype(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);
	Database::updateDatatype(&dt);


	DataType *dataLocal = Database::getDataTypes(dt.getId());
	CPPUNIT_ASSERT(*dataLocal == dt);
	delete dataLocal;
}

void DataBaseTestSuccess::deleteDatatype(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);
	Database::deleteDatatype(&dt);


	DataType *dataLocal = Database::getDataTypes(dt.getId());
	CPPUNIT_ASSERT(dataLocal->getId() == UINT_MAX);
	delete dataLocal;
}

//DataAccess
void DataBaseTestSuccess::updateDataAccess(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);


	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);

	//dataAccess
	dataAccess.setAccessPoint(ap);
	dataAccess.setData(data);
	dataAccess.setTranslatedId(1);
	Database::saveDataAccess(&dataAccess);
	dataAccess.setTranslatedId(15);
	Database::updateDataAccess(&dataAccess);
	Database::getAccessPointDataAccess(&ap);
	CPPUNIT_ASSERT(ap.getDataAccessList()->size() > 0);
	DataAccess dataLocal = ((*(ap.getDataAccessList()))[0]);

	CPPUNIT_ASSERT(dataLocal == dataAccess);
}

void DataBaseTestSuccess::deleteDataAccess(){
	this->reset();
	//user
	Database::saveUser(&u);

	//accessPoint
	Database::saveAccessPoint(&ap);
	Database::saveUserApRelation(&u, &ap);

	//dataType
	Database::saveDatatype(&dt);


	//data
	data.setUser(u);
	data.setDataType(dt);
	Database::saveData(&data);

	//dataAccess
	dataAccess.setAccessPoint(ap);
	dataAccess.setData(data);
	dataAccess.setTranslatedId(1);
	Database::saveDataAccess(&dataAccess);
	dataAccess.setTranslatedId(15);
	Database::deleteDataAccess(&dataAccess);

	Database::getAccessPointDataAccess(&ap);
	CPPUNIT_ASSERT(ap.getDataAccessList()->size() == 0);
}
