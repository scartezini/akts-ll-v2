#ifndef DATABASETEST_H
#define DATABASETEST_H

#include <memory>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../../headers/Database.hpp"
#include <stdlib.h>



class DataBaseTestSuccess : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE( DataBaseTestSuccess );
    CPPUNIT_TEST(databaseSetup);
    CPPUNIT_TEST(saveUser);
    CPPUNIT_TEST(saveAccessPoint);
    CPPUNIT_TEST(saveUserApRelation);
    CPPUNIT_TEST(saveDatatype);
    CPPUNIT_TEST(saveData);
    CPPUNIT_TEST(saveStuff);
    CPPUNIT_TEST(saveAccess);
    CPPUNIT_TEST(saveDataAccess);
    CPPUNIT_TEST(getUser);
    CPPUNIT_TEST(getUserAccess);

    CPPUNIT_TEST(getUserAccessPoints);
    CPPUNIT_TEST(getUserDatas);
    CPPUNIT_TEST(getMyStuffs);

    CPPUNIT_TEST(getAccess);

    //AcessPoint
    CPPUNIT_TEST(getAccessPoint);
    CPPUNIT_TEST(getAccessPointUsers);
    CPPUNIT_TEST(getAccessPointAccess);
    CPPUNIT_TEST(getAccessPointDataAccess);

    //Data

    //stuff
    CPPUNIT_TEST(getStuffAccess);

    //DataType
    CPPUNIT_TEST(getDataTypes);
    CPPUNIT_TEST(getDataTypeData);

    //update and delete
    CPPUNIT_TEST(updateAccess);
    CPPUNIT_TEST(deleteAccess);

    //data
    CPPUNIT_TEST(updateData);
    CPPUNIT_TEST(deleteData);

    //stuffs
    CPPUNIT_TEST(updateStuff);
    CPPUNIT_TEST(deleteStuff);

    //datatype
    CPPUNIT_TEST(updateDatatype);
    CPPUNIT_TEST(deleteDatatype);

    //DataAcess
    CPPUNIT_TEST(updateDataAccess);
    CPPUNIT_TEST(deleteDataAccess);

    CPPUNIT_TEST_SUITE_END();

public:

    void setUp();
    void tearDown();

    //start
    void executeComand();
    void databaseSetup();

    //saving
    void saveUser();
    void saveAccessPoint();
    void saveUserApRelation();
    void saveDatatype();
    void saveData();
    void saveStuff();
    void saveAccess();
    void saveDataAccess();

    //retrive
    //User
    void getUser();
    void getUserAccess();
    void getUserAccessPoints();
    void getUserDatas();
    void getMyStuffs();

    //Access
    void getAccess();

    //AcessPoint
    void getAccessPoint();
    void getAccessPointUsers();
    void getAccessPointAccess();
    void getAccessPointDataAccess();

    //Data

    //stuff
    void getStuffAccess();

    //DataType
    void getDataTypes();
    void getDataTypeData();

    //update and delete
    void updateAccess();
    void deleteAccess();

    //data
    void updateData();
    void deleteData();

    //stuffs
    void updateStuff();
    void deleteStuff();

    //datatype
    void updateDatatype();
    void deleteDatatype();

    //DataAcess
    void updateDataAccess();
    void deleteDataAccess();
private:
    User u;
    AccessPoint ap;
    DataType dt;
    Data data;
    Stuff stuff;
    Access access;
    DataAccess dataAccess;

    void reset();

};


#endif
