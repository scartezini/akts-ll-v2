#include "entites_models_test.hpp"

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(DataBaseTestSuccess);

void DataBaseTestSuccess::setUp() {
	database_monkey::create_database();
}
void DataBaseTestSuccess::pointers() {
	try {
		std::vector<char> valor = {'1', '2', '3', '4', '5'};
		Blob blob(valor, 5);

		Timestamp ts("2015-07-14T17:15:25.014Z");


		User *u = new User();
		u->setNumMiss(3);
		u->setName("Nome1");
		u->setCpf("032.093.481-03");
		u->setRg("1111111");
		u->setDestiny("Akuntsu");
		u->setEmail("Nome1@akuntsu.com");
		u->setPassword("12345678");
		u->setPhone("(61)997748810");
		u->setId(database_monkey::persist(*u));

		DataType *dt = new DataType();
		dt->setName("DataType 1");
		dt->setSensor("1");
		dt->setModel("Proprietario");
		dt->setId(database_monkey::persist(*dt));

		Data *data = new Data();
		data->setValue(blob);	// std::string, int
		data->setUser(*u);		 // user*
		data->setDataType(*dt);  // datatype*
		data->setId(database_monkey::persist(*data));

		{
			unique_ptr<database> db(open_database());
			typedef odb::query<User> query;

			transaction t(db->begin());


			for (auto &e : db->query<User>(query::name == "Nome1")) {
				typedef std::vector<lazy_weak_ptr<Data>> Datas;
				std::vector<odb::lazy_weak_ptr<Data>> es(e.datas);
				for (Datas::iterator i(es.begin()); i != es.end(); ++i) {
					// We are only interested in employees with object id less
					// than
					// 100.
					//
					lazy_weak_ptr<Data> &lwp(*i);
					cout << lwp.object_id<Data>() << endl;
					/*
					if (lwp.object_id<Data> () < 100)
					// C++11: if (lwp.object_id () < 100)
					{
					  shared_ptr<Data> e (lwp.load ()); // Load and lock.
					  cout << e->first_ << " " << e->last_ << endl;
					}
					*/
				}
			}


			t.commit();
		}

		free(u);

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}
void DataBaseTestSuccess::saveUser() {
	try {
		User *u = new User();
		u->setNumMiss(3);
		u->setName("Nome1");
		u->setCpf("032.093.481-03");
		u->setRg("1111111");
		u->setDestiny("Akuntsu");
		u->setEmail("Nome1@akuntsu.com");
		u->setPassword("12345678");
		u->setPhone("(61)997748810");

		std::vector<char> valor = {'1', '2', '3', '4', '5'};
		Photo photo(valor, 11);


		u->setPhoto(photo);
		{ database_monkey::persist(*u); }
		{
			unique_ptr<database> db(open_database());
			typedef odb::query<User> query;

			transaction t(db->begin());


			for (auto &e : db->query<User>(query::name == "Nome1")) {
				CPPUNIT_ASSERT(e.getName() == u->getName());
				CPPUNIT_ASSERT_EQUAL(e.getCpf(), u->getCpf());
			}


			t.commit();
		}

		free(u);

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}

void DataBaseTestSuccess::saveAccessPoint() {
	try {
		unique_ptr<database> db(open_database());

		AccessPoint *ap = new AccessPoint();
		ap->setName("AP1");
		ap->setType("1");
		ap->setIpAddress(IpAddress("192.168.1.1"));

		database_monkey::persist(*ap);

		{
			typedef odb::query<AccessPoint> query;

			transaction t(db->begin());


			for (auto &e : db->query<AccessPoint>(query::name == "AP1")) {
				// CPPUNIT_ASSERT_EQUAL(e.getName(), ap->getName());
				CPPUNIT_ASSERT_EQUAL(e.getType(), ap->getType());
			}

			t.commit();
		}

		free(ap);

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}

void DataBaseTestSuccess::saveUserApRelation() {
	try {
		unique_ptr<database> db(open_database());

		Timestamp ts("2015-07-14T17:15:25.014Z");

		User *u = new User(4);
		u->setNumMiss(3);
		u->setName("Nome1");
		u->setCpf("032.093.481-03");
		u->setRg("1111111");
		u->setDestiny("Akuntsu");
		u->setEmail("Nome1@akuntsu.com");
		u->setPassword("12345678");
		u->setPhone("(61)997748810");
		u->setId(database_monkey::persist(*u));


		AccessPoint *ap = new AccessPoint();
		ap->setName("AP1");
		ap->setType("1");
		ap->setIpAddress(IpAddress("192.168.1.1"));
		ap->setId(database_monkey::persist(*ap));

		Relation_User_AccessPoint *ruap = new Relation_User_AccessPoint();
		ruap->setPeriod(1);
		ruap->setWeekDay('R');
		ruap->setStart(ts);
		ruap->setUser(*u);
		ruap->setAccessPoint(*ap);


		database_monkey::persist(*ruap);

		{
			typedef odb::query<Relation_User_AccessPoint> query;

			transaction t(db->begin());
			for (auto &e :
				 db->query<Relation_User_AccessPoint>(query::weekDay == 'R')) {
				CPPUNIT_ASSERT_EQUAL(e.getPeriod(), ruap->getPeriod());
				CPPUNIT_ASSERT_EQUAL(e.getUser().getName(), u->getName());
				CPPUNIT_ASSERT_EQUAL(e.getAccessPoint().getName(),
									 ap->getName());
				CPPUNIT_ASSERT_EQUAL(e.getStart()->stringFormat(),
									 ruap->getStart()->stringFormat());
				CPPUNIT_ASSERT(e.getStart()->stringFormat() ==
							   ts.stringFormat());
				break;
			}

			t.commit();
		}

		free(u);
		free(ap);
		free(ruap);

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}


void DataBaseTestSuccess::saveDatatype() {
	try {
		unique_ptr<database> db(open_database());


		DataType *dt = new DataType();
		dt->setName("DataType 1");
		dt->setSensor("1");
		dt->setModel("Proprietario");

		database_monkey::persist(*dt);

		{
			typedef odb::query<DataType> query;

			transaction t(db->begin());
			for (auto &e : db->query<DataType>(query::name == "DataType 1")) {
				CPPUNIT_ASSERT_EQUAL(e.getName(), dt->getName());
			}

			t.commit();
		}

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}

void DataBaseTestSuccess::saveData() {
	try {
		unique_ptr<database> db(open_database());


		std::vector<char> valor = {'1', '2', '3', '4', '5'};
		Blob blob(valor, 5);

		Timestamp ts("2015-07-14T17:15:25.014Z");

		User *u = new User();
		u->setNumMiss(3);
		u->setName("Nome1");
		u->setCpf("032.093.481-03");
		u->setRg("1111111");
		u->setDestiny("Akuntsu");
		u->setEmail("Nome1@akuntsu.com");
		u->setPassword("12345678");
		u->setPhone("(61)997748810");
		u->setId(database_monkey::persist(*u));

		DataType *dt = new DataType();
		dt->setName("DataType 1");
		dt->setSensor("1");
		dt->setModel("Proprietario");
		dt->setId(database_monkey::persist(*dt));

		Data *data = new Data();
		data->setValue(blob);	// std::string, int
		data->setUser(*u);		 // user*
		data->setDataType(*dt);  // datatype*
		data->setId(database_monkey::persist(*data));


		{
			typedef odb::query<Data> query;

			transaction t(db->begin());

			for (auto &e : db->query<Data>(true)) {
				CPPUNIT_ASSERT_EQUAL(e.getUser().getName(), u->getName());
				CPPUNIT_ASSERT_EQUAL(e.getUser().getId(), u->getId());
				CPPUNIT_ASSERT_EQUAL(e.getDataType().getId(), dt->getId());
				break;
			}

			t.commit();
		}

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}

void DataBaseTestSuccess::saveStuff() {
	try {
		unique_ptr<database> db(open_database());


		User *u = new User(4);
		u->setNumMiss(3);
		u->setName("Nome1");
		u->setCpf("032.093.481-03");
		u->setRg("1111111");
		u->setDestiny("Akuntsu");
		u->setEmail("Nome1@akuntsu.com");
		u->setPassword("12345678");
		u->setPhone("(61)997748810");

		Stuff *stuff = new Stuff(2);
		stuff->setName("PC");
		stuff->setDescription("pc bolado");
		stuff->setUser(*u);


		database_monkey::persist(*u);
		database_monkey::persist(*stuff);

		{
			typedef odb::query<Stuff> query;

			transaction t(db->begin());

			for (auto &e : db->query<Stuff>(query::id == 1)) {
				CPPUNIT_ASSERT_EQUAL(e.getName(), stuff->getName());
				CPPUNIT_ASSERT_EQUAL(e.getUser().getId(), u->getId());
			}

			t.commit();
		}


	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}

void DataBaseTestSuccess::saveAccess() {
	try {
		unique_ptr<database> db(open_database());

		Timestamp ts("2015-07-14T17:15:25.014Z");

		std::vector<char> valor = {'1', '2', '3', '4', '5'};
		Blob blob(valor, 5);

		AccessPoint *ap = new AccessPoint();
		ap->setName("AP1");
		ap->setType("1");
		ap->setIpAddress(IpAddress("192.168.1.1"));
		ap->setId(database_monkey::persist(*ap));

		DataType *dt = new DataType();
		dt->setName("DataType 1");
		dt->setSensor("1");
		dt->setModel("Proprietario");
		dt->setId(database_monkey::persist(*dt));

		User *u = new User();
		u->setNumMiss(3);
		u->setName("Nome1");
		u->setCpf("032.093.481-03");
		u->setRg("1111111");
		u->setDestiny("Akuntsu");
		u->setEmail("Nome1@akuntsu.com");
		u->setPassword("12345678");
		u->setPhone("(61)997748810");
		u->setId(database_monkey::persist(*u));

		Data *data = new Data();
		data->setValue(blob);	// std::string, int
		data->setUser(*u);		 // user*
		data->setDataType(*dt);  // datatype*
		data->setId(database_monkey::persist(*data));

		Stuff *stuff = new Stuff();
		stuff->setName("PC");
		stuff->setDescription("pc bolado");
		stuff->setUser(*u);
		stuff->setId(database_monkey::persist(*stuff));

		Access *access = new Access();
		access->setType("1");
		access->setHora(ts);
		access->setUser(*u);
		access->setAccessPoint(*ap);
		access->setData(*data);
		access->setStuff(*stuff);
		access->setId(database_monkey::persist(*access));


		{
			typedef odb::query<Access> query;

			transaction t(db->begin());

			for (auto &e : db->query<Access>(true)) {
				CPPUNIT_ASSERT_EQUAL(e.getType(), access->getType());
				CPPUNIT_ASSERT_EQUAL(e.getId(), access->getId());
				CPPUNIT_ASSERT_EQUAL(e.getUser().getId(), u->getId());
				CPPUNIT_ASSERT_EQUAL(e.getAccessPoint().getId(), ap->getId());
				CPPUNIT_ASSERT_EQUAL(e.getData().getId(), data->getId());
				CPPUNIT_ASSERT_EQUAL(e.getStuff().getId(), stuff->getId());
				break;
			}

			t.commit();
		}

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}

void DataBaseTestSuccess::saveJustification() {
	try {
		unique_ptr<database> db(open_database());

		Timestamp ts("2015-07-14T17:15:25.014Z");

		User *u = new User();
		u->setNumMiss(3);
		u->setName("Nome1");
		u->setCpf("032.093.481-03");
		u->setRg("1111111");
		u->setDestiny("Akuntsu");
		u->setEmail("Nome1@akuntsu.com");
		u->setPassword("12345678");
		u->setPhone("(61)997748810");
		u->setId(database_monkey::persist(*u));

		Justification *justification = new Justification();
		justification->setNumBusinessDay(0);
		justification->setType(2);
		justification->setDescription("oloco");
		justification->setEnd(ts);
		justification->setStart(ts);
		justification->setUser(*u);

		Document d = Document();
		justification->setDocument(d);

		justification->setId(database_monkey::persist(*justification));


		{
			typedef odb::query<Justification> query;

			transaction t(db->begin());

			for (auto &e : db->query<Justification>(query::id == 1)) {
				CPPUNIT_ASSERT_EQUAL(e.getId(), justification->getId());
				CPPUNIT_ASSERT_EQUAL(e.getDescription(),
									 justification->getDescription());
				CPPUNIT_ASSERT_EQUAL(e.getType(), justification->getType());
			}

			t.commit();
		}

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}

void DataBaseTestSuccess::saveDataApRelation() {
	try {
		unique_ptr<database> db(open_database());

		std::vector<char> valor = {'1', '2', '3', '4', '5'};
		Blob blob(valor, 5);


		AccessPoint *ap = new AccessPoint();
		ap->setName("AP1");
		ap->setType("1");
		ap->setIpAddress(IpAddress("192.168.1.1"));
		ap->setId(database_monkey::persist(*ap));

		User *u = new User();
		u->setNumMiss(3);
		u->setName("Nome1");
		u->setCpf("032.093.481-03");
		u->setRg("1111111");
		u->setDestiny("Akuntsu");
		u->setEmail("Nome1@akuntsu.com");
		u->setPassword("12345678");
		u->setPhone("(61)997748810");
		u->setId(database_monkey::persist(*u));

		DataType *dt = new DataType();
		dt->setName("DataType 1");
		dt->setSensor("1");
		dt->setModel("Proprietario");
		dt->setId(database_monkey::persist(*dt));

		Data *data = new Data();
		data->setValue(blob);	// std::string, int
		data->setUser(*u);		 // user*
		data->setDataType(*dt);  // datatype*
		data->setId(database_monkey::persist(*data));


		Relation_Data_AccessPoint *rdap = new Relation_Data_AccessPoint();
		rdap->setTranslatedId(1);
		rdap->setData(*data);
		rdap->setAccessPoint(*ap);
		rdap->setId(database_monkey::persist(*rdap));


		{
			typedef odb::query<Relation_Data_AccessPoint> query;

			transaction t(db->begin());

			for (auto &e : db->query<Relation_Data_AccessPoint>(
					 query::id == rdap->getId())) {
				CPPUNIT_ASSERT_EQUAL(e.getId(), rdap->getId());
				CPPUNIT_ASSERT_EQUAL(e.getAccessPoint().getId(), ap->getId());
				CPPUNIT_ASSERT_EQUAL(e.getData().getId(), data->getId());
				CPPUNIT_ASSERT_EQUAL(e.getData().getUser().getId(), u->getId());
				CPPUNIT_ASSERT_EQUAL(e.getData().getDataType().getId(),
									 dt->getId());
			}

			t.commit();
		}

	} catch (const odb::exception &e) {
		// std::cout <<"erro: "<< e.what() << std::endl;
		CPPUNIT_ASSERT(false);
	}
}


CPPUNIT_TEST_SUITE_REGISTRATION(JsonTestSuccess);


void JsonTestSuccess::userFromJson() {
	std::string userJson =
		"{\"id\":1,\"name\":\"julia\",\"cpf\":\"00000000000\",\"rg\":"
		"\"00000000\",\"destiny\":\"casa das "
		"prima\",\"email\":\"bola@bola.com\",\"password\":"
		"\"987ehumasenhaforte\",\"phone\":\"89099023\",\"numMiss\":2}";

	User expected =
		User(1, 2, "00000000", "00000000000", "julia", "bola@bola.com",
			 "89099023", "casa das prima", "987ehumasenhaforte");


	User atual = User();
	atual.fromJson(userJson);

	CPPUNIT_ASSERT_EQUAL(expected.getId(), atual.getId());
	CPPUNIT_ASSERT_EQUAL(expected.getName(), atual.getName());
	CPPUNIT_ASSERT_EQUAL(expected.getEmail(), atual.getEmail());
	CPPUNIT_ASSERT_EQUAL(expected.getDestiny(), atual.getDestiny());
	CPPUNIT_ASSERT_EQUAL(expected.getPhone(), atual.getPhone());
}

void JsonTestSuccess::userToJson() {
	Json::Value parsed_atual, parsed_expected;
	Json::Reader reader_atual, reader_expected;

	std::string userJson = "{\"low_id\":2,\"name\":\"nome boladao\",";
	userJson += "\"cpf\":\"quem disse que tem que ser numerico\",";
	userJson += "\"rg\":\"tambem nao precisa\",";
	userJson += "\"destiny\":\"terra do nunca\",";
	userJson += "\"email\":\"peter@peter.peter\",";
	userJson += "\"password\":\"senha de favela\",";
	userJson += "\"phone\":\"123\",";
	userJson += "\"numMiss\":900}";

	User user = User();
	user.setId(2);
	user.setCpf(std::string("quem disse que tem que ser numerico"));
	user.setRg(std::string("tambem nao precisa"));
	user.setDestiny(std::string("terra do nunca"));
	user.setEmail(std::string("peter@peter.peter"));
	user.setPassword(std::string("senha de favela"));
	user.setPhone(std::string("123"));
	user.setNumMiss(900);
	std::string str = user.toJson();


	reader_expected.parse(userJson, parsed_expected);
	reader_atual.parse(str, parsed_atual);

	CPPUNIT_ASSERT_EQUAL(parsed_expected, parsed_atual);
	// CPPUNIT_ASSERT_EQUAL();
	// CPPUNIT_ASSERT_EQUAL();
	// CPPUNIT_ASSERT_EQUAL();
	// CPPUNIT_ASSERT_EQUAL();
	// CPPUNIT_ASSERT_EQUAL();
	// CPPUNIT_ASSERT_EQUAL();
	// CPPUNIT_ASSERT_EQUAL();
}

void JsonTestSuccess::userToShortJson() {
	Json::Value parsed_atual, parsed_expected;
	Json::Reader reader_atual, reader_expected;

	std::string userShortJson = "{\"ID\":1,\"NAME\":\"mendelson\"}";

	User user =
		User(1, 2, "00000000", "00000000000", "mendelson", "bola@bola.com",
			 "89099023", "casa das prima", "987ehumasenhaforte");

	std::string str = user.toShortJson();

	uint id_expected, id_atual;
	std::string str_expected, str_atual;

	reader_expected.parse(userShortJson, parsed_expected);
	id_expected = parsed_expected["ID"].asInt();
	str_expected = parsed_expected["NAME"].asString();

	reader_atual.parse(str, parsed_atual);
	id_atual = parsed_atual["ID"].asInt();
	str_atual = parsed_atual["NAME"].asString();

	CPPUNIT_ASSERT_EQUAL(id_atual, id_expected);
	CPPUNIT_ASSERT_EQUAL(str_atual, str_expected);
}


void JsonTestSuccess::accessFromJson() {
	std::string accessJson = "{\"id\":1,\"direction\":1,\"hora\":\"2015-07-"
							 "14T17:15:25.014Z\",\"user_id\":1,\"data_id\":2,"
							 "\"stuff_id\":3,\"control_id\":8}";

	Access expected = Access(1);
	expected.setHora("2015-07-14T17:15:25.014Z");
	expected.setType(1);
	User u = User(1);
	expected.setUser(u);
	Data data = Data(2);
	expected.setData(data);
	Stuff stuff = Stuff(3);
	expected.setStuff(stuff);
	AccessPoint ap = AccessPoint(8);
	expected.setAccessPoint(ap);

	Access atual = Access();
	atual.fromJson(accessJson);

	CPPUNIT_ASSERT_EQUAL(expected.getId(), atual.getId());
	CPPUNIT_ASSERT_EQUAL(expected.getHora()->stringFormat(),
						 atual.getHora()->stringFormat());
	CPPUNIT_ASSERT_EQUAL(expected.getType(), atual.getType());
	CPPUNIT_ASSERT_EQUAL(expected.getUser().getId(), atual.getUser().getId());
	CPPUNIT_ASSERT_EQUAL(expected.getData().getId(), atual.getData().getId());
	CPPUNIT_ASSERT_EQUAL(expected.getStuff().getId(), atual.getStuff().getId());
	CPPUNIT_ASSERT_EQUAL(expected.getAccessPoint().getId(),
						 atual.getAccessPoint().getId());
}

void JsonTestSuccess::accessToJson() {
}

void JsonTestSuccess::accessPointFromJson() {
	std::string accessPointJson = "{\"id\":2,\"name\":\"um nome "
								  "qualquer\",\"type\":\"door_front\","
								  "\"ipAddress\":\"192.168.1.1\"}";

	AccessPoint atual = AccessPoint();
	atual.fromJson(accessPointJson);

	AccessPoint expected = AccessPoint(2);
	expected.setName(std::string("um nome qualquer"));
	expected.setType(0);
	expected.setIpAddress(IpAddress("192.168.1.1"));

	CPPUNIT_ASSERT_EQUAL(expected.getId(), atual.getId());
	CPPUNIT_ASSERT_EQUAL(expected.getName(), atual.getName());
	CPPUNIT_ASSERT_EQUAL(expected.getType(), atual.getType());
	CPPUNIT_ASSERT_EQUAL(expected.getIpAddress()->databaseFormat(),
						 atual.getIpAddress()->databaseFormat());
}
void JsonTestSuccess::accessPointToJson() {
}

void JsonTestSuccess::dataTypeFromJson() {
	std::string dataTypeJson = "{\"id\":3,\"name\":\"nome "
							   "qualquer\",\"sensor\":\"NFC\",\"model\":"
							   "\"ola\"}";

	DataType atual = DataType();
	atual.fromJson(dataTypeJson);

	DataType expected = DataType(3);
	expected.setName("nome qualquer");
	expected.setSensor(0);
	expected.setModel("ola");

	CPPUNIT_ASSERT_EQUAL(expected.getId(), atual.getId());
	CPPUNIT_ASSERT_EQUAL(expected.getName(), atual.getName());
	CPPUNIT_ASSERT_EQUAL(expected.getSensor(), atual.getSensor());
	CPPUNIT_ASSERT_EQUAL(expected.getModel(), atual.getModel());
}

void JsonTestSuccess::dataTypeToJson() {
}

void JsonTestSuccess::dataFromJson() {
	std::string dataJson = "{\"id\":3,\"value\":\"0100111001001101010010\","
						   "\"user_id\":1,\"type\":2}";

	Data atual = Data();
	atual.fromJson(dataJson);

	Data expected = Data(3);
	User u = User(1);
	expected.setUser(u);
	DataType dt = DataType(3);
	expected.setDataType(dt);

	CPPUNIT_ASSERT_EQUAL(expected.getId(), atual.getId());
	CPPUNIT_ASSERT_EQUAL(expected.getUser().getId(), atual.getUser().getId());
	CPPUNIT_ASSERT_EQUAL(expected.getDataType().getId(),
						 atual.getDataType().getId());
}
void JsonTestSuccess::dataToJson() {
}
void JsonTestSuccess::dataToShortJson() {
}

void JsonTestSuccess::stuffFromJson() {
	std::string stuffJson = "{\"id\":9999999,\"name\":\"noteBoladao\","
							"\"description\":\"cara de "
							"mamao\",\"serial\":\"9090098\",\"user_id\":2}";

	Stuff atual = Stuff();
	atual.fromJson(stuffJson);

	Stuff expected = Stuff(9999999);
	expected.setName("noteBoladao");
	expected.setDescription("cara de mamao");
	expected.setSerial("9090098");
	User u = User(2);
	expected.setUser(u);


	CPPUNIT_ASSERT_EQUAL(expected.getId(), atual.getId());
	CPPUNIT_ASSERT_EQUAL(expected.getName(), atual.getName());
	CPPUNIT_ASSERT_EQUAL(expected.getDescription(), atual.getDescription());
	CPPUNIT_ASSERT_EQUAL(expected.getSerial(), atual.getSerial());
	CPPUNIT_ASSERT_EQUAL(expected.getUser().getId(), atual.getUser().getId());
}
void JsonTestSuccess::stuffToJson() {
}

void JsonTestSuccess::relation_data_accessPoint_FromJson() {
	std::string rdapJson = "{\"id\":1,\"translated_id\":3,\"data\":1,"
						   "\"accessPoint\":2}";

	Relation_Data_AccessPoint expected = Relation_Data_AccessPoint(1);
	expected.setTranslatedId(3);
	Data data = Data(1);
	expected.setData(data);
	AccessPoint ap = AccessPoint(2);
	expected.setAccessPoint(ap);

	Relation_Data_AccessPoint atual = Relation_Data_AccessPoint();
	atual.fromJson(rdapJson);

	CPPUNIT_ASSERT_EQUAL(expected.getId(), atual.getId());
	CPPUNIT_ASSERT_EQUAL(expected.getTranslated_id(), atual.getTranslated_id());
	CPPUNIT_ASSERT_EQUAL(expected.getData().getId(), atual.getData().getId());
	CPPUNIT_ASSERT_EQUAL(expected.getAccessPoint().getId(),
						 atual.getAccessPoint().getId());
}

void JsonTestSuccess::relation_data_accessPoint_ToJson() {
}


void JsonTestSuccess::justificationFromJson() {
	std::string justificationJson = "{\"id\":2,\"numBusinessDay\":3,"
									"\"description\":\"gustavou\",\"end\":"
									"\"2015-07-14T17:15:25.014Z\",\"start\":"
									"\"2015-07-14T17:15:25.014Z\",\"user\":2}";

	Justification atual = Justification();
	atual.fromJson(justificationJson);

	Justification expected = Justification(2);
	expected.setNumBusinessDay(3);
	expected.setDescription("gustavou");
	expected.setEnd("2015-07-14T17:15:25.014Z");
	expected.setStart("2015-07-14T17:15:25.014Z");
	User u = User(2);
	expected.setUser(u);

	CPPUNIT_ASSERT_EQUAL(expected.getId(), atual.getId());
	CPPUNIT_ASSERT_EQUAL(expected.getDescription(), atual.getDescription());
	CPPUNIT_ASSERT_EQUAL(expected.getEnd()->stringFormat(),
						 atual.getEnd()->stringFormat());
	CPPUNIT_ASSERT_EQUAL(expected.getStart()->stringFormat(),
						 atual.getStart()->stringFormat());
	CPPUNIT_ASSERT_EQUAL(expected.getUser().getId(), atual.getUser().getId());
}
void JsonTestSuccess::justificationToJson() {
}


void JsonTestSuccess::relation_user_accessPoint_FromJson() {
	std::string ruapJson = "{\"id\":3,\"period\":2,\"weekDay\":\"K\",\"start\":"
						   "\"2015-07-14T17:15:25.014Z\",\"user\":2,\"access_"
						   "point\":6}";

	Relation_User_AccessPoint ruap = Relation_User_AccessPoint();
	ruap.fromJson(ruapJson);

	CPPUNIT_ASSERT_EQUAL((uint)3, ruap.getId());
	CPPUNIT_ASSERT_EQUAL((uint)2, ruap.getPeriod());
	CPPUNIT_ASSERT_EQUAL('K', ruap.getWeekDay());
	CPPUNIT_ASSERT_EQUAL(Timestamp("2015-07-14T17:15:25.014Z").stringFormat(),
						 ruap.getStart()->stringFormat());
	CPPUNIT_ASSERT_EQUAL((uint)2, ruap.getUser().getId());
	CPPUNIT_ASSERT_EQUAL((uint)6, ruap.getAccessPoint().getId());
}
void JsonTestSuccess::relation_user_accessPoint_ToJson() {
}
