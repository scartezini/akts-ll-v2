#ifndef ENTITIES_MODELS_TEST_H
#define DATABASETEST_TEST_H

#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <stdlib.h>
#include <memory>

#include "../../headers/models/base_models.hpp"


#include "../../headers/models/database.hpp"

#include "../../libs/json_parser/json.h"

#include <odb/database.hxx>
#include <odb/transaction.hxx>

using namespace odb::core;
using namespace database_monkey;
// using odb::database;
// using odb::transaction;
using std::tr1::shared_ptr;


class DataBaseTestSuccess : public CppUnit::TestCase {
	CPPUNIT_TEST_SUITE(DataBaseTestSuccess);

	CPPUNIT_TEST(pointers);
	// CPPUNIT_TEST(saveUser);
	// CPPUNIT_TEST(saveAccessPoint);
	// CPPUNIT_TEST(saveUserApRelation);
	// CPPUNIT_TEST(saveDatatype);
	// CPPUNIT_TEST(saveData);
	// CPPUNIT_TEST(saveStuff);
	// CPPUNIT_TEST(saveAccess);
	// CPPUNIT_TEST(saveJustification);
	// CPPUNIT_TEST(saveDataApRelation);

	CPPUNIT_TEST_SUITE_END();

  public:
	void setUp();
	void pointers();
	// saving
	void saveUser();
	void saveAccessPoint();
	void saveUserApRelation();
	void saveDatatype();
	void saveData();
	void saveStuff();
	void saveAccess();
	void saveJustification();
	void saveDataApRelation();
};

class JsonTestSuccess : public CppUnit::TestCase {
	CPPUNIT_TEST_SUITE(JsonTestSuccess);

	// CPPUNIT_TEST(userFromJson);
	// CPPUNIT_TEST(userToJson);
	// CPPUNIT_TEST(userToShortJson);
	//
	// CPPUNIT_TEST(accessFromJson);
	// CPPUNIT_TEST(accessToJson);
	//
	// CPPUNIT_TEST(accessPointFromJson);
	// CPPUNIT_TEST(accessPointToJson);
	//
	// CPPUNIT_TEST(dataTypeFromJson);
	// CPPUNIT_TEST(dataTypeToJson);
	//
	// CPPUNIT_TEST(dataFromJson);
	// CPPUNIT_TEST(dataToJson);
	// CPPUNIT_TEST(dataToShortJson);
	//
	// CPPUNIT_TEST(stuffFromJson);
	// CPPUNIT_TEST(stuffToJson);
	//
	// CPPUNIT_TEST(relation_data_accessPoint_FromJson);
	// CPPUNIT_TEST(relation_data_accessPoint_ToJson);
	//
	// CPPUNIT_TEST(justificationFromJson);
	// CPPUNIT_TEST(justificationToJson);
	//
	// CPPUNIT_TEST(relation_user_accessPoint_FromJson);
	// CPPUNIT_TEST(relation_user_accessPoint_ToJson);

	CPPUNIT_TEST_SUITE_END();

  public:
	void userFromJson();
	void userToJson();
	void userToShortJson();

	void accessFromJson();
	void accessToJson();

	void accessPointFromJson();
	void accessPointToJson();

	void dataTypeFromJson();
	void dataTypeToJson();

	void dataFromJson();
	void dataToJson();
	void dataToShortJson();

	void stuffFromJson();
	void stuffToJson();

	void relation_data_accessPoint_FromJson();
	void relation_data_accessPoint_ToJson();

	void justificationFromJson();
	void justificationToJson();

	void relation_user_accessPoint_FromJson();
	void relation_user_accessPoint_ToJson();
};

#endif
