// Invoke 'strict' JavaScript mode
'use strict';

var moment = require('moment');

// Create a new controller method that renders the signin page
exports.get = function(req, res, next) {
    var access = {}

    access.direction = Math.round(Math.random() * 5)

    var d = new Date();
    var n = d.getTime();
    n += Math.round(Math.random() * 100000000)
    access.hora = moment(new Date(n))

    access.user_id = Math.round(Math.random() * 5)
    access.control_id = Math.round(Math.random() * 5)
    access.data_id = Math.round(Math.random() * 5)

    if (Math.round(Math.random() * 30) == 25)
        res.status(404).json({})
    else
        res.status(200).json(access)
};

