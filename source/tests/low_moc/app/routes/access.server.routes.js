// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var access = require('../../app/controllers/access.server.controller');

// Define the routes module' method
module.exports = function(app) {
	// Set up the 'signup' routes 
	app.route('/not_save_access')
	   .get(access.get)
};