// Invoke 'strict' JavaScript mode
'use strict';

// Set the 'NODE_ENV' variable
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Load the module dependencies
var	express = require('./config/express');

//start the apps

var apps = []
for(var i = 0; i < 50; i++){
    apps.push(express());
    apps[i].listen(10001 + i)
}

// Log the server status to the console
console.log('Server running at http://localhost:10001/');

module.exports = apps;