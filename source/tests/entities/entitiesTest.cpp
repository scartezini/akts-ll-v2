#include "./entitiesTest.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(BaseTypesTestSuccess);
CPPUNIT_TEST_SUITE_REGISTRATION(BaseTypesTestCommomFail);
CPPUNIT_TEST_SUITE_REGISTRATION(EntitiesTestFail);
CPPUNIT_TEST_SUITE_REGISTRATION(EntitiesTestSuccess);


void BaseTypesTestSuccess::timestamp_sg(){
	Timestamp timestamp;
	string time_s = "2015-07-14T17:15:25.014Z";

	timestamp.setAll(time_s);
	string back = timestamp.databaseFormat();
	CPPUNIT_ASSERT(back.compare(time_s) == 0);

	timestamp = Timestamp();
	timestamp.setAll(07, 14, 2015, 17, 15, 25, 14);
	back = timestamp.databaseFormat();
	CPPUNIT_ASSERT(back.compare(time_s) == 0);
}

void BaseTypesTestSuccess::ipAddress_sg(){
	IpAddress ip;
	string time_s = "10.1.1.1";
	ip.setIp(time_s);
	string back = ip.databaseFormat();
	CPPUNIT_ASSERT(back.compare(time_s) == 0);

	ip = IpAddress();
	ip.setIp(10, 1, 1, 1);
	back = ip.databaseFormat();
	CPPUNIT_ASSERT(back.compare(time_s) == 0);

}

void BaseTypesTestSuccess::photo_sg(){
	Photo photo;
	char *pointer = photo.databaseFormat();
	uint size = photo.getSize();
	CPPUNIT_ASSERT(pointer == NULL);
	CPPUNIT_ASSERT(size == 0);

	char *local = (char*)calloc(10, sizeof(char));
	for(int i =0; i < 10; i++)
		local[i] = i;

	char local2[10];
	memcpy(local2, local, 10);

	photo.setPhoto(local, 10);
	free(local);

	pointer = photo.databaseFormat();
	size = photo.getSize();
	CPPUNIT_ASSERT(memcmp(pointer, local2, 10)==0);
	CPPUNIT_ASSERT(size == 10);

	for(int i =0; i < 10; i++)
		local2[i] = i*2;

	Photo *photo2 = new Photo(photo);
	photo.setPhoto(local2, 10);

	pointer = photo2->databaseFormat();
	size = photo2->getSize();

	local = (char*)calloc(10, sizeof(char));
	for(int i =0; i < 10; i++)
		local[i] = i;

	CPPUNIT_ASSERT(memcmp(pointer, local, 10)==0);
	CPPUNIT_ASSERT(size == 10);
	free(local);
	delete photo2;

	#warning verifica se a memory e liberada;
}

void BaseTypesTestSuccess::port_sg(){
	Port port;
	port.setPort(1000);
	uint port_i = port.getPort();
	CPPUNIT_ASSERT(1000 == port_i);
	port.setPort("1000");
	port_i = port.getPort();
	CPPUNIT_ASSERT(1000 == port_i);
}

void BaseTypesTestSuccess::blob_sg(){

	Blob blob;
	char *pointer = blob.databaseFormat();
	uint size = blob.getSize();
	CPPUNIT_ASSERT(pointer == NULL);
	CPPUNIT_ASSERT(size == 0);

	char *local = (char*)calloc(10, sizeof(char));
	for(int i =0; i < 10; i++)
		local[i] = i;

	char local2[10];
	memcpy(local2, local, 10);

	blob.setValue(local, 10);
	free(local);

	pointer = blob.databaseFormat();
	size = blob.getSize();
	CPPUNIT_ASSERT(memcmp(pointer, local2, 10)==0);
	CPPUNIT_ASSERT(size == 10);

	for(int i =0; i < 10; i++)
		local2[i] = i*2;

	Blob *blob2 = new Blob(blob);
	blob.setValue(local2, 10);
	pointer = blob2->databaseFormat();
	size = blob2->getSize();

	local = (char*)calloc(10, sizeof(char));
	for(int i =0; i < 10; i++)
		local[i] = i;

	CPPUNIT_ASSERT(memcmp(pointer, local, 10)==0);
	CPPUNIT_ASSERT(size == 10);
	free(local);
	delete blob2;
	#warning verifica se a memory e liberada;

}

//////////////////////////////////////////////////////////////
//Builders
//////////////////////////////////////////////////////////////
void BaseTypesTestSuccess::timestamp_build(){
	Timestamp timestamp;
	string time_f = timestamp.databaseFormat();
	string zero_time = "0000-00-00T00:00:00.000Z";
	CPPUNIT_ASSERT(zero_time.compare(time_f) == 0);

	string time_s = "2015-07-14T17:15:25.014Z";
	timestamp = Timestamp(time_s);
	string time_inter = timestamp.databaseFormat();
	CPPUNIT_ASSERT(time_inter.compare(time_s) == 0);

	timestamp = Timestamp(07, 14, 2015, 17, 15, 25, 14);
	string back = timestamp.databaseFormat();
	CPPUNIT_ASSERT(back.compare(time_s) == 0);
}

void BaseTypesTestSuccess::ipAddress_build(){
	IpAddress ip;
	string back = ip.databaseFormat();
	string zero_time = "0.0.0.0";
	CPPUNIT_ASSERT(zero_time.compare(back) == 0);

	ip = IpAddress("10.1.1.1");
	back = ip.databaseFormat();
	zero_time = "10.1.1.1";
	CPPUNIT_ASSERT(zero_time.compare(back) == 0);

	ip = IpAddress(10, 1, 1, 1);
	back = ip.databaseFormat();
	zero_time = "10.1.1.1";
	CPPUNIT_ASSERT(zero_time.compare(back) == 0);
}


void BaseTypesTestSuccess::port_build(){
	Port port;
	uint port_i = port.getPort();
	CPPUNIT_ASSERT(port_i == 0);

	port = Port(1000);
	port_i = port.getPort();
	CPPUNIT_ASSERT(port_i == 1000);

	port = Port(1001);
	port_i = port.getPort();
	CPPUNIT_ASSERT(port_i == 1001);
}

void BaseTypesTestSuccess::photo_build(){
	Photo photo;
	char *pointer = photo.databaseFormat();
	uint size = photo.getSize();
	CPPUNIT_ASSERT(pointer == NULL);
	CPPUNIT_ASSERT(size == 0);

	char *local = (char*)calloc(10, sizeof(char));
	for(int i =0; i < 10; i++)
		local[i] = i;

	photo = Photo(local, 10);
	pointer = photo.databaseFormat();
	size = photo.getSize();
	CPPUNIT_ASSERT(memcmp(pointer, local, 10)==0);
	CPPUNIT_ASSERT(size == 10);

	free(local);
}


void BaseTypesTestSuccess::blob_build(){

	Blob blob;
	char *pointer = blob.databaseFormat();
	uint size = blob.getSize();
	CPPUNIT_ASSERT(pointer == NULL);
	CPPUNIT_ASSERT(size == 0);

	char *local = (char*)calloc(10, sizeof(char));
	for(int i =0; i < 10; i++)
		local[i] = i;

	blob = Blob(local, 10);
	pointer = blob.databaseFormat();
	size = blob.getSize();
	CPPUNIT_ASSERT(memcmp(pointer, local, 10)==0);
	CPPUNIT_ASSERT(size == 10);

	free(local);

}

//////////////////////////////////////////////////////////////
//== and != operators
//////////////////////////////////////////////////////////////
void BaseTypesTestSuccess::timestamp_compare(){
	string time_s = "2015-07-14T17:15:25.014Z";
	string zero_time = "0000-00-00T00:00:00.000Z";
	Timestamp timestamp;
	string time_f = timestamp.databaseFormat();
	Timestamp timestamp2(zero_time);
	CPPUNIT_ASSERT(timestamp == timestamp2);

	timestamp2 = Timestamp(time_s);
	CPPUNIT_ASSERT(timestamp2 != timestamp);

	timestamp = Timestamp(07, 14, 2015, 17, 15, 25, 14);
	string back = timestamp.databaseFormat();
	CPPUNIT_ASSERT(timestamp == timestamp2);
}

void BaseTypesTestSuccess::ipAddress_compare(){
	IpAddress ip;
	string zero_time = "0.0.0.0";
	IpAddress ip2(zero_time);
	CPPUNIT_ASSERT(ip == ip2);

	ip2 = IpAddress("10.1.1.1");
	CPPUNIT_ASSERT(ip2 != ip);

	ip = IpAddress(10, 1, 1, 1);
	CPPUNIT_ASSERT(ip2 == ip);
}

void BaseTypesTestSuccess::port_compare(){
	Port port;
	Port port2(0);
	CPPUNIT_ASSERT(port == port2);

	port = Port(1000);
	CPPUNIT_ASSERT(port != port2);

	port2.setPort(1000);
	CPPUNIT_ASSERT(port == port2);
}


void BaseTypesTestSuccess::photo_compare(){
	Photo photo, photo2;
	CPPUNIT_ASSERT(photo == photo2);

	char *local = (char*)calloc(10, sizeof(char));
	for(int i =0; i < 10; i++)
		local[i] = i;

	char local2[10];
	memcpy(local2, local, 10);

	photo.setPhoto(local, 10);
	CPPUNIT_ASSERT(photo != photo2);
	photo2.setPhoto(local, 10);
	CPPUNIT_ASSERT(photo == photo2);

	for(int i =0; i < 10; i++)
		local[i] = i*2;
	photo.setPhoto(local, 10);
	CPPUNIT_ASSERT(photo != photo2);
	free(local);
}

void BaseTypesTestSuccess::blob_compare(){
	Blob blob, blob2;
	CPPUNIT_ASSERT(blob == blob2);

	char *local = (char*)calloc(10, sizeof(char));
	for(int i =0; i < 10; i++)
		local[i] = i;

	char local2[10];
	memcpy(local2, local, 10);

	blob.setValue(local, 10);
	CPPUNIT_ASSERT(blob != blob2);
	blob2.setValue(local, 10);
	CPPUNIT_ASSERT(blob == blob2);

	for(int i =0; i < 10; i++)
		local[i] = i*2;
	blob.setValue(local, 10);
	CPPUNIT_ASSERT(blob != blob2);
	free(local);

}

//////////////////////////////////////////////////////////////////////
void EntitiesTestSuccess::setUp(){
	char *valorl = (char*)malloc(100*sizeof(char));

	string valor;
	valor = "user1";
	user_g.setName(valor);
	valor = "000.000.000-00";
	user_g.setCpf(valor);
	valor = "Localparaondevai";
	user_g.setDestiny(valor);
	valor = "user1@gmail.com";
	user_g.setEmail(valor);
	valor = "password";
	user_g.setPassword(valor);
	valor = "phone";
	user_g.setPhone(valor);
	Photo photo(valorl, 100);
	user_g.setPhoto(photo);

	//datatype
	datatype_g.setId(10);
	valor = "nfc_sensor";
	datatype_g.setName(valor);
	valor = "NFC";
	datatype_g.setSensor(valor);
	valor = "Nosso";
	datatype_g.setManufacturer(valor);

	//stuff
	stuff_g.setId(10);
	valor = "Material de time";
	stuff_g.setName(valor);
	valor = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";
	stuff_g.setDescription(valor);

	//data
	valor = "10";
	data_g.setId(valor);
	Blob blob(valorl, 100);
	data_g.setValue(blob);

	//accessPoint
	valor = "porta1";
	accessPoint_g.setName(valor);
	valor = "porta";
	accessPoint_g.setType(valor);
	IpAddress ip(10, 1, 1, 1);
	accessPoint_g.setIpAddress(ip);
	accessPoint_g.setId(10);

	free(valorl);

}

//////////////////////////////////////////////////////////////////////
///Entities Set Get
//////////////////////////////////////////////////////////////////////
void EntitiesTestSuccess::user_sg(){
	string valor = "user1";
	string result;
	User user;
	user.setName(valor);
	result = user.getName();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "000.000.000-00";
	user.setCpf(valor);
	result = user.getCpf();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "Localparaondevai";
	user.setDestiny(valor);
	result = user.getDestiny();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "user1@gmail.com";
	user.setEmail(valor);
	result = user.getEmail();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "password";
	user.setPassword(valor);
	result = user.getPassword();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "phone";
	user.setPhone(valor);
	result = user.getPhone();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	char *valorl = (char*)malloc(100*sizeof(char));
	Photo photo(valorl, 100);
	user.setPhoto(photo);
	Photo *thephoto = user.getPhoto();
	CPPUNIT_ASSERT(*thephoto == photo);
	free(valorl);


	valor = "10";
	user.setId(valor);
	uint result_i = user.getId();
	CPPUNIT_ASSERT(result_i == 10);
	user.setId(10);
	result_i = user.getId();
	CPPUNIT_ASSERT(result_i == 10);
}

void EntitiesTestSuccess::accessPoint_sg(){
	AccessPoint accessPoint;
	string valor, result;

	valor = "porta1";
	accessPoint.setName(valor);
	result = accessPoint.getName();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "porta";
	accessPoint.setType(valor);
	result = accessPoint.getType();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	IpAddress ip(10, 1, 1, 1);
	accessPoint.setIpAddress(ip);
	IpAddress *theip = accessPoint.getIp();
	CPPUNIT_ASSERT(*theip == ip);

	valor = "10";
	accessPoint.setId(valor);
	uint result_i = accessPoint.getId();
	CPPUNIT_ASSERT(result_i == 10);
	accessPoint.setId(10);
	result_i = accessPoint.getId();
	CPPUNIT_ASSERT(result_i == 10);
}

void EntitiesTestSuccess::dataType_sg(){
	DataType datatype;
	string valor, result;

	valor = "nfc_sensor";
	datatype.setName(valor);
	result = datatype.getName();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "NFC";
	datatype.setSensor(valor);
	result = datatype.getSensor();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "Nosso";
	datatype.setManufacturer(valor);
	result = datatype.getManufacturer();
	CPPUNIT_ASSERT(valor.compare(result) == 0);


	valor = "10";
	datatype.setId(valor);
	uint result_i = datatype.getId();
	CPPUNIT_ASSERT(result_i == 10);
	datatype.setId(10);
	result_i = datatype.getId();
	CPPUNIT_ASSERT(result_i == 10);

}

void EntitiesTestSuccess::data_sg(){
	Data data;
	string valor, result;

	valor = "10";
	data.setId(valor);
	uint result_i = data.getId();
	CPPUNIT_ASSERT(result_i == 10);
	data.setId(10);
	result_i = data.getId();
	CPPUNIT_ASSERT(result_i == 10);

	char *valorl = (char*)malloc(100*sizeof(char));
	Blob blob(valorl, 100);
	data.setValue(blob);
	Blob *theblob = data.getValue();
	CPPUNIT_ASSERT(*theblob == blob);
	valorl[20] = 12;
	data.setValue(valorl, 100);
	blob.setValue(valorl, 100);
	theblob = data.getValue();
	CPPUNIT_ASSERT(*theblob == blob);


	data.setUser(user_g);
	User *theuser = data.getUser();
	CPPUNIT_ASSERT(*theuser == user_g);

	data.setDataType(datatype_g);
	DataType *thedatatype = data.getDataType();
	CPPUNIT_ASSERT(*thedatatype == datatype_g);
}


void EntitiesTestSuccess::stuff_sg(){
	string valor, result;
	Stuff stuff;

	valor = "10";
	stuff.setId(valor);
	uint result_i = stuff.getId();
	CPPUNIT_ASSERT(result_i == 10);
	stuff.setId(10);
	result_i = stuff.getId();
	CPPUNIT_ASSERT(result_i == 10);

	valor = "Material de time";
	stuff.setName(valor);
	result = stuff.getName();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	valor = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";

	stuff.setDescription(valor);
	result = stuff.getDescription();
	CPPUNIT_ASSERT(valor.compare(result) == 0);

	stuff.setUser(user_g);
	User *theuser = stuff.getUser();
	CPPUNIT_ASSERT(*theuser == user_g);
}

void EntitiesTestSuccess::access_sg(){
	string valor, result;
	Access access;

	valor = "10";
	access.setId(valor);
	uint result_i = access.getId();
	CPPUNIT_ASSERT(result_i == 10);
	access.setId(10);
	result_i = access.getId();
	CPPUNIT_ASSERT(result_i == 10);

	valor = "2015-07-14T17:15:25.014Z";;
	access.setTimestamp(valor);
	Timestamp *thetime = access.getTimestamp();
	CPPUNIT_ASSERT(valor.compare(thetime->databaseFormat()) == 0);

	Timestamp timestamp(valor);
	access.setTimestamp(timestamp);
	thetime = access.getTimestamp();
	CPPUNIT_ASSERT(*thetime == timestamp);

	access.setType(Access::PERSON_INPUT);
	Access::TYPE type = access.getType();
	CPPUNIT_ASSERT(Access::PERSON_INPUT == type);

	access.setType(2);
	type = access.getType();
	CPPUNIT_ASSERT(Access::PERSON_INPUT == type);

	valor = "2";
	access.setType(valor);
	type = access.getType();
	CPPUNIT_ASSERT(Access::PERSON_INPUT == type);


	access.setUser(user_g);
	User *theuser = access.getUser();
	CPPUNIT_ASSERT(*theuser == user_g);

	access.setAccessPoint(accessPoint_g);
	AccessPoint *theaccessPoint = access.getAccessPoint();
	CPPUNIT_ASSERT(*theaccessPoint == accessPoint_g);

	access.setData(data_g);
	Data *thedata = access.getData();
	CPPUNIT_ASSERT(*thedata == data_g);

	access.setStuff(stuff_g);
	Stuff *thestuff = access.getStuff();
	CPPUNIT_ASSERT(*thestuff == stuff_g);
}

void EntitiesTestSuccess::dataAccess_sg(){
	string valor, result;
	DataAccess dataAccess;

	valor = "10";
	dataAccess.setTranslatedId(valor);
	uint result_i = dataAccess.getTranslatedId();
	CPPUNIT_ASSERT(result_i == 10);
	dataAccess.setTranslatedId(10);
	result_i = dataAccess.getTranslatedId();
	CPPUNIT_ASSERT(result_i == 10);

	dataAccess.setData(data_g);
	Data *thedata = dataAccess.getData();
	CPPUNIT_ASSERT(*thedata == data_g);

	dataAccess.setAccessPoint(accessPoint_g);
	AccessPoint *thepoint = dataAccess.getAccessPoint();
	CPPUNIT_ASSERT(*thepoint == accessPoint_g);
}

//////////////////////////////////////////////////////////////////////
//entities builders
//////////////////////////////////////////////////////////////////////
void EntitiesTestSuccess::user_builder(){
	User user;
	string valor;
	valor = user.getName();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = user.getCpf();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = user.getRg();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = user.getDestiny();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = user.getEmail();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = user.getPassword();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = user.getPhone();
	CPPUNIT_ASSERT(valor.size() == 0);

	Photo *photo = user.getPhoto();
	CPPUNIT_ASSERT(photo->databaseFormat() == NULL);
	CPPUNIT_ASSERT(photo->getSize() == 0);


	uint id = user.getId();
	CPPUNIT_ASSERT(id == UINT_MAX);
}

void EntitiesTestSuccess::accessPoint_builder(){
	AccessPoint accessPoint;
	string valor;
	valor = accessPoint.getName();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = accessPoint.getType();
	CPPUNIT_ASSERT(valor.size() == 0);

	IpAddress *ip = accessPoint.getIp();
	CPPUNIT_ASSERT(ip->databaseFormat().compare("0.0.0.0") == 0);


	uint id = accessPoint.getId();
	CPPUNIT_ASSERT(id == UINT_MAX);
}

void EntitiesTestSuccess::dataType_builder(){
	DataType datatype;
	string valor;
	valor = datatype.getName();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = datatype.getSensor();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = datatype.getManufacturer();
	CPPUNIT_ASSERT(valor.size() == 0);

	uint id = datatype.getId();
	CPPUNIT_ASSERT(id == UINT_MAX);
}

void EntitiesTestSuccess::stuff_builder(){
	Stuff stuff;
	string valor;
	valor = stuff.getName();
	CPPUNIT_ASSERT(valor.size() == 0);

	valor = stuff.getDescription();
	CPPUNIT_ASSERT(valor.size() == 0);

	User *user = stuff.getUser();
	CPPUNIT_ASSERT(*user == User());

	uint id = stuff.getId();
	CPPUNIT_ASSERT(id == UINT_MAX);
}

void EntitiesTestSuccess::data_builder(){
	Data data;
	Blob *blob = data.getValue();
	CPPUNIT_ASSERT(blob->databaseFormat() == NULL);
	CPPUNIT_ASSERT(blob->getSize() == 0);

	User *user = data.getUser();
	CPPUNIT_ASSERT(*user == User());

	DataType *datatype = data.getDataType();
	CPPUNIT_ASSERT(*datatype == DataType());

	uint id = data.getId();
	CPPUNIT_ASSERT(id == UINT_MAX);
}
void EntitiesTestSuccess::access_builder(){
	Access access;
	string valor;

	Timestamp *time_s = access.getTimestamp();
	CPPUNIT_ASSERT(time_s->databaseFormat().compare("0000-00-00T00:00:00.000Z") == 0);

	User *user = access.getUser();
	CPPUNIT_ASSERT(*user == User());

	AccessPoint *accessPoint = access.getAccessPoint();
	CPPUNIT_ASSERT(*accessPoint == AccessPoint());

	Data *data = access.getData();
	CPPUNIT_ASSERT(*data == Data());

	Stuff *stuff = access.getStuff();
	CPPUNIT_ASSERT(*stuff == Stuff());

	uint id = access.getId();
	CPPUNIT_ASSERT(id == UINT_MAX);
}

void EntitiesTestSuccess::dataAccess_builder(){
	DataAccess dataAccess;
	AccessPoint *accessPoint = dataAccess.getAccessPoint();
	CPPUNIT_ASSERT(*accessPoint == AccessPoint());

	Data *data = dataAccess.getData();
	CPPUNIT_ASSERT(*data == Data());

	uint id = dataAccess.getTranslatedId();
	CPPUNIT_ASSERT(id == UINT_MAX);
}


//////////////////////////////////////////////////////////////////////
//entities method == and !=
//////////////////////////////////////////////////////////////////////
void EntitiesTestSuccess::user_compare(){}
void EntitiesTestSuccess::accessPoint_compare(){}
void EntitiesTestSuccess::dataType_compare(){}
void EntitiesTestSuccess::data_compare(){}
void EntitiesTestSuccess::stuff_compare(){}
void EntitiesTestSuccess::access_compare(){}
void EntitiesTestSuccess::dataAccess_compare(){}
