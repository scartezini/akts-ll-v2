#ifndef ENTITIES_TEST_H
#define ENTITIES_TEST_H

#include <memory>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../../headers/Database.hpp"
#include <stdlib.h>

using namespace std;



class BaseTypesTestSuccess : public CppUnit::TestCase{
	CPPUNIT_TEST_SUITE(BaseTypesTestSuccess);
	CPPUNIT_TEST(timestamp_sg);
	CPPUNIT_TEST(ipAddress_sg);
	CPPUNIT_TEST(photo_sg);
	CPPUNIT_TEST(port_sg);
	CPPUNIT_TEST(blob_sg);

	CPPUNIT_TEST(timestamp_build);
	CPPUNIT_TEST(ipAddress_build);
	CPPUNIT_TEST(photo_build);
	CPPUNIT_TEST(port_build);
	CPPUNIT_TEST(blob_build);

	CPPUNIT_TEST(timestamp_compare);
	CPPUNIT_TEST(ipAddress_compare);
	CPPUNIT_TEST(photo_compare);
	CPPUNIT_TEST(port_compare);
	CPPUNIT_TEST(blob_compare);

	CPPUNIT_TEST_SUITE_END();
public:

    void setUp(){}
    void tearDown(){}

	//basetypes set and get
	void timestamp_sg();
	void ipAddress_sg();
	void photo_sg();
	void port_sg();
	void blob_sg();

	//basetypes builders;
	void timestamp_build();
	void ipAddress_build();
	void photo_build();
	void port_build();
	void blob_build();

	//basetype method == and !=
	void timestamp_compare();
	void ipAddress_compare();
	void photo_compare();
	void port_compare();
	void blob_compare();
};

class BaseTypesTestCommomFail : public CppUnit::TestCase{
	CPPUNIT_TEST_SUITE(BaseTypesTestCommomFail);
	CPPUNIT_TEST(invalidIp);
	CPPUNIT_TEST_SUITE_END();
public:

    void setUp(){}
    void tearDown(){}

	//basetypes set
	void invalidIp(){}
	void invalidTimeFormatString(){}
	void invalidTimeExecedTheLimits(){}
};

class EntitiesTestSuccess : public CppUnit::TestCase
{
	CPPUNIT_TEST_SUITE(EntitiesTestSuccess);
	CPPUNIT_TEST(user_sg);
	CPPUNIT_TEST(accessPoint_sg);
	CPPUNIT_TEST(dataType_sg);
	CPPUNIT_TEST(data_sg);
	CPPUNIT_TEST(stuff_sg);
	CPPUNIT_TEST(access_sg);
	CPPUNIT_TEST(dataAccess_sg);

	CPPUNIT_TEST(user_builder);
	CPPUNIT_TEST(accessPoint_builder);
	CPPUNIT_TEST(dataType_builder);
	CPPUNIT_TEST(data_builder);
	CPPUNIT_TEST(access_builder);
	CPPUNIT_TEST(dataAccess_builder);

	CPPUNIT_TEST(user_compare);
	CPPUNIT_TEST(accessPoint_compare);
	CPPUNIT_TEST(dataType_compare);
	CPPUNIT_TEST(data_compare);
	CPPUNIT_TEST(access_compare);
	CPPUNIT_TEST(dataAccess_compare);

	CPPUNIT_TEST_SUITE_END();

	User user_g;
	DataType datatype_g;
	Data data_g;
	Stuff stuff_g;
	AccessPoint accessPoint_g;
	Access access_g;


public:

    void setUp();

    void tearDown(){}

	//entities set get
	void user_sg();
	void accessPoint_sg();
	void dataType_sg();
	void data_sg();
	void stuff_sg();
	void access_sg();
	void dataAccess_sg();

	//entities builders
	void user_builder();
	void accessPoint_builder();
	void dataType_builder();
	void data_builder();
	void stuff_builder();
	void access_builder();
	void dataAccess_builder();

	//entities method == and !=
	void user_compare();
	void accessPoint_compare();
	void dataType_compare();
	void data_compare();
	void stuff_compare();
	void access_compare();
	void dataAccess_compare();

};

class EntitiesTestFail : public CppUnit::TestCase
{

    CPPUNIT_TEST_SUITE(EntitiesTestFail);
	//CPPUNIT_TEST(invalidId);
	CPPUNIT_TEST_SUITE_END();

public:

    void setUp(){}
    void tearDown(){}
	//basetypes set
	void invalidId(){}

};



#endif
