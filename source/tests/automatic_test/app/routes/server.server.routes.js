var queue = require('../controllers/queue.server.controller.js')

module.exports = function(app) {
    app.route('/api/access')
        .post(queue.add)
        .get(queue.add)
    app.route('/api/access/:accessId')
        .get(queue.add)
        .put(queue.add)
        .patch(queue.add)
        .delete(queue.add)
    app.param('accessId', queue.set_id)
    
    app.route('/api/access_point')
        .post(queue.add)
        .get(queue.add)
    app.route('/api/access_point/:accessPointId')
        .get(queue.add)
        .put(queue.add)
        .patch(queue.add)
        .delete(queue.add)
    app.param('accessPointId', queue.set_id)
}
