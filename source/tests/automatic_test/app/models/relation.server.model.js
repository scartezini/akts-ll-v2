"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var queue = require('../controllers/queue.server.controller.js')

class BaseModel{
    constructor() {
    }
    
 
    user_ap_relation(user, access_point) {
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/relation/access_point/user/"
        var response = request('POST', this.route, {
            headers:{
                'User-Agent': 'high_level',
                'Content-Type': 'application/json'
            },
            json: {
                user:user.low_id,
                access_point:access_point.low_id
            }
        })
        if (response.statusCode != 201) {
            throw new Error("invalid response on save")
        }

        var body = JSON.parse(response.body)
        if(body.id == undefined){
            throw new Error("no id on response")
        }
        
        this.low_id = body.id
        assert.ok(true);
    }
    
    user_ap_relation_delete(user, access_point) {
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/relation/access_point/user/" + access_point.low_id + "/" + user.low_id
        var response = request('DELETE', this.route, {
            headers:{
                'User-Agent': 'high_level',
                'Content-Type': 'application/json'
            },
        })
        
        if (response.statusCode != 200) {
            throw new Error("invalid response on save")
        }
    }
}

module.exports = BaseModel