"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var BaseModel = require('./BaseModel.server.model.js')

class Data extends BaseModel{
    constructor(user) {
        super()
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/data/"
        this.update_route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/data/"
        this.get_route = "http://" + config.mid_ip + ":" + config.mid_port + "/restapi/data/"
        this.local_user = user
        this.fill()
    }

    fill(){
        if(this.local_user != undefined)
            this.user_id = this.local_user.low_id
        this.type = Math.round(Math.random()*2)
        this.value = []

        if (this.type == 0){
            for(var i = 0; i < 4; i++)
                this.value.push(Math.round(Math.random()*255))
        }else if(this.type == 1){
            for(var i = 0; i < 32; i++)
                this.value.push(Math.round(Math.random()*255))
        }else if(this.type == 2){
            for(var i = 0; i < 4; i++)
                this.value.push(Math.round(Math.random()*255))
        }
    }

    compare(json){
        if(this.low_id != json.id){
            return false;
        }


        if(this.user_id != json.fk_user){
            return false
        }

        if (json.TYPE == 0){
            if(json.value.length != 4)
                return false
            for(var i = 0; i < 4; i++)
                if(this.value[i] != json.value[i])
                    return false
        }else if(json.type == 1 && this.type != 'rfid'){
            if(json.value.length != 32)
                return false
            for(var i = 0; i < 32; i++)
                if(this.value[i] != json.value[i])
                    return false
        }else if(json.type == 2 && this.type != 'biometric'){
            if(json.value.length != 4)
                return false
            for(var i = 0; i < 4; i++)
                if(this.value[i] != json.value[i])
                    return false
        }

        return true;
    }

    isRecived(){
    }
}

module.exports = Data
