"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var loremIpsum = require('lorem-ipsum')
var BaseModel = require('./BaseModel.server.model.js')
var moment = require('moment');

class Justification extends BaseModel{
    constructor(user) {
        super()
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/justification/"
        this.update_route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/justification/"
        this.get_route = "http://" + config.mid_ip + ":" + config.mid_port + "/restapi/justification/"

        this.local_user = user
        this.fill()
    }

    fill(){
        this.num_business_day = Math.round(Math.random()*100)
        // this.description = loremIpsum({
        //     count: Math.round(Math.random()*1000) + 20
        //     , units: 'sentences'
        //     , sentenceLowerBound: Math.round(Math.random()*10)
        //     , sentenceUpperBound: Math.round(Math.random()*25)
        // , paragraphLowerBound: Math.round(Math.random()*10)
        // , paragraphUpperBound: Math.round(Math.random()*20)
        // , format: 'plain'
        // , random: Math.random
        // })

        var d = new Date();
        var n = d.getTime();
        var rand =  Math.round(Math.random()*100000000)
        this.start = moment(new Date(n + rand))
        this.end = moment(new Date(n + rand + (this.num_business_day * 3600 * 1000 * 24)))

        if(this.local_user != undefined)
            this.user = this.local_user.low_id
    }

    compare(json){
        if(this.low_id != json.id){
            return false
        }
        if(this.numBusinessDay != json.num_business_day){
            return false
        }

        if(this.description != json.description){
            return false
        }
        if(this.start.format() != moment(json.start).format()){
            return false
        }

        if(this.end.format() != moment(json.end).format()){
            return false
        }


        if(this.user != json.user){
            return false
        }

        return true
    }

}

module.exports = Justification
