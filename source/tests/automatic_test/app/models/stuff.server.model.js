"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var BaseModel = require('./BaseModel.server.model.js')
var loremIpsum = require('lorem-ipsum')

class Stuff extends BaseModel{
    constructor(user) {
        super()
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/stuff/"
        this.update_route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/stuff/"
        this.get_route = "http://" + config.mid_ip + ":" + config.mid_port + "/restapi/stuff/"
        this.local_user = user
        this.fill()
    }

    fill(){
        this.name = "Material " + Math.round(Math.random()*10000).toString();
        this.description =  loremIpsum({
            count: Math.round(Math.random()*1000) + 20
            , units: 'sentences'
            , sentenceLowerBound: Math.round(Math.random()*10)
            , sentenceUpperBound: Math.round(Math.random()*25)
        , paragraphLowerBound: Math.round(Math.random()*10)
        , paragraphUpperBound: Math.round(Math.random()*20)
        , format: 'plain'
        , random: Math.random
        })

        this.serial =  Math.round(Math.random()*100000000).toString();

        if(this.local_user != undefined)
            this.user_id = this.local_user.low_id
    }

    compare(json){
        if(this.low_id != json.id){
            return false
        }
        if(this.name != json.name){
            return false
        }
        if(this.description != json.description){
            return false
        }
        if(this.serial != json.serial){
            return false
        }

        if(this.user_id != json.fk_user){
            return false
        }
        return true
    }

}

module.exports = Stuff
