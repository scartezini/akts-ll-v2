"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var BaseModel = require('./BaseModel.server.model.js')

class User extends BaseModel{
    constructor() {
        super()
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/user/"
        this.update_route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/user/"
        this.get_route = "http://" + config.mid_ip + ":" + config.mid_port + "/restapi/user/"
        this.fill(); 
    }
    
    fill(){
        this.name = "Nome de teste" + Math.round(Math.random()*10000).toString();
        this.destiny = "Andar " + Math.round(Math.random()*10000).toString();
        this.cpf = Math.round(Math.random()*100000000000).toString();
        this.rg = Math.round(Math.random()*100000000).toString();
        this.email = "email.teste" + Math.round(Math.random()*10000).toString() + "@teste.com.br"
        this.rg = Math.round(Math.random()*100000000).toString();
        this.numMiss = 0
        this.password = Math.round(Math.random()*10000000000000).toString(); 
        this.phone = Math.round(Math.random()*10000000000000).toString();
    }
    
    compare(json){
        if(this.name != json.name){
            return false
        }
        if(this.destiny != json.destiny){
            return false
        }
        if(this.cpf != json.cpf){
            return false
        }
        if(this.name != json.name){
            return false
        }
        if(this.rg != json.rg){
            return false
        }
        if(this.email != json.email){
            return false
        }
        if(this.numMiss != json.numMiss){
            return false
        }
        if(this.password != json.password){
            return false
        }
        if(this.phone != json.phone){
            return false
        }
        return true
    }   
     
    isRecived(){
        
    }
}

module.exports = User