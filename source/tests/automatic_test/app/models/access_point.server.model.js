"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var BaseModel = require('./BaseModel.server.model.js')

class AccessPoint extends BaseModel{
    
    constructor() {
        super()
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/lowserverapi/ap/"
        this.update_route = "http://" + config.mid_ip + ":" + config.mid_port + "/lowserverapi/access_point/"
        this.get_route = "http://" + config.mid_ip + ":" + config.mid_port + "/restapi/access_point/"
        this.ent_type = 'access_point'
        this.fill()        
    }
    
    fill(){
        this.name = "Porta N:" + Math.round(Math.random()*10000).toString();
        
        
        var type = Math.round(Math.random()*3)
        if (type == 2){
            this.type = 'door_front_back'
        }else if(type == 3){
            this.type = 'ticket_gate'
        }else if(type == 1){
            this.type = 'door_back'
        }else if(type == 0){
            this.type = 'door_front'
        }
        
        this.ipAddress = '127.0.0.1'
        this.port = (10001 +  Math.round(Math.random()*10));
    }
    
    compare(json){
        if(this.name != json.name){
            return false
        }

        if(this.low_id != json.low_id){
            return false
        }
        
        if(json.activated != true){
            return false
        }
        
        
        if(this.ipAddress != json.ip){
            return false
        }
        
        if(this.port != json.port){
            return false
        }
        
        if (json.type == 2 && this.type != 'door_front_back'){
            return false
        }else if(json.type == 3 && this.type != 'ticket_gate'){
            return false
        }else if(json.type == 1 && this.type != 'door_back'){
            return false
        }else if(json.type == 0 && this.type != 'door_front'){
            return false
        }
        
        return true
    }   
     
}

module.exports = AccessPoint