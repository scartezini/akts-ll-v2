"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var BaseModel = require('./BaseModel.server.model.js')
var moment = require('moment');

class Access extends BaseModel{
    constructor(user, access_point, data, stuff) {
        super()
        this.local_user = user
        this.local_access_point = access_point
        this.local_data = data
        this.local_stuff = stuff
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/lowserverapi/access/"
        this.update_route = "http://" + config.mid_ip + ":" + config.mid_port + "/lowserverapi/access/"
        this.get_route = "http://" + config.mid_ip + ":" + config.mid_port + "/restapi/access/"
        this.ent_type = 'access'
        this.fill()
    }
    
    fill(){
        this.direction = Math.round(Math.random()*5)
        
        var d = new Date();
        var n = d.getTime();
        n +=  Math.round(Math.random()*100000000)
        this.hora = moment(new Date(n))
        
        
        if(this.local_user != undefined)
            this.user_id = this.local_user.low_id
        
        if(this.local_access_point != undefined)
            this.control_id = this.local_access_point.low_id
        
        if(this.local_data != undefined)
            this.data_id = this.local_data.low_id
        
        if(this.local_stuff != undefined)
            this.stuff_id = this.local_stuff.low_id
    }
 
    compare(json){
        if(this.low_id != json.low_id){
            return false
        }
        if(this.direction != json.type){
            return false
        }
        if(this.hora.format() != moment(json.timestamp).format()){
            return false
        }
        if(this.user_id != json.user_low_id){
            return false
        }
        if(this.control_id != json.accessPoint_low_id){
            return false
        }
        if(this.stuff_id != json.stuff_low_id){
            return false
        }
        if(this.data_id != json.data_low_id){
            return false
        }
        
        return true
    }      
}

module.exports = Access