"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var BaseModel = require('./BaseModel.server.model.js')

class DataType extends BaseModel{
    constructor() {
        super()
        this.route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/data_type/"
        this.update_route = "http://" + config.mid_ip + ":" + config.mid_port + "/highapi/data_type/"
        this.get_route = "http://" + config.mid_ip + ":" + config.mid_port + "/restapi/data_type/"

        this.fill()
    }
    
    fill(){
        this.name = "Sensor" + Math.round(Math.random()*10000).toString();
        this.model = "model" + Math.round(Math.random()*10000).toString();
        
        var type = Math.round(Math.random()*2)
        if (type == 0){
            this.type = 'nfc'
        }else if(type == 1){
            this.type = 'rfid'
        }else if(type == 2){
            this.type = 'biometric'
        }
    }
 
    compare(json){
        if(this.low_id != json.id){
            return false;
        }
        
        if(this.name != json.name){
            return false
        }
        if(this.model != json.model){
            return false
        }
        
        if (json.type == 0 && this.type != 'nfc'){
            return false
        }else if(json.type == 1 && this.type != 'rfid'){
            return false
        }else if(json.type == 2 && this.type != 'biometric'){
            return false
        }
        return true;
    }   
     
    isRecived(){   
        
    }
}

module.exports = DataType