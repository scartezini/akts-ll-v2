"use strict";

var assert = require('assert');
var request = require('sync-request')
var config = require('../../config/config')
var queue = require('../controllers/queue.server.controller.js')

class BaseModel{
    constructor() {
    }


    save() {
        var response = request('POST', this.route, {
            headers:{
                'User-Agent': 'high_level',
                'Content-Type': 'application/json'
            },
            json: this
        })
        if (response.statusCode != 201) {
            throw new Error("invalid response on save")
        }

        var body = JSON.parse(response.body)
        if(body.id == undefined){
            throw new Error("no id on response")
        }

        this.low_id = body.id
        assert.ok(true);
    }

    update(){
        this.fill()
        var route = this.update_route + this.low_id
        var response = request('PUT', route, {
            headers:{
                'User-Agent': 'high_level',
                'Content-Type': 'application/json'
            },
            json: this
        })
        if (response.statusCode != 200) {
            //console.log(response)
            throw new Error("invalid response on update")
        }

        var body = JSON.parse(response.body)
        if(body.id == undefined){
            throw new Error("no id on response")
        }

        if(this.low_id == body.id){
            return
        }else{
            throw new Error("error no update")
        }
    }

    delete(){
        this.fill()
        var route = this.update_route + this.low_id
        var response = request('DELETE', route, {
            headers:{
                'User-Agent': 'high_level',
                'Content-Type': 'application/json'
            }
        })

        if (response.statusCode != 200) {
            //console.log(response)
            throw new Error("invalid response on delete")
        }
    }

    get(exists) {
        var route = this.get_route + this.low_id
        var response = request('GET', route, {
            headers:{
                'User-Agent': 'high_level',
            }
        })

        if (response.statusCode == 404 && !exists) {
            return
        }

        if(!exists){
            //console.log(response)
            throw new Error("Incorret response on not exist")
        }

        if (response.statusCode != 200) {
            //console.log(response)
            throw new Error("invalid response on get")
        }

        var body = JSON.parse(response.body)
        if(this.compare(body)){
            assert.ok(true);
        }else{
            // console.log(this)
			// console.log(body)
            throw new Error("Differnt content on body")
        }
    }


    isRecived(method){
        var id = undefined
        if(method != 'POST')
            id = this.low_id

        var data = queue.verify(this.ent_type, method, id)
	}
}

module.exports = BaseModel
