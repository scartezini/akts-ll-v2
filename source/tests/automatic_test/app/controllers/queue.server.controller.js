/*global global*/

var sleep = require('sleep');
var Promise = require('promise');
var async = require('async')

exports.add = function(req, res) {
    var test = 1
    var _id = undefined

    var params = (req.url).split('/')
    if(req.method != 'POST'){
        test = 2
        _id = (params[params.length - 1]).toInt()
    }
    global.stack.push({
        type:params[params.length - test],
        method:req.method,
        id:_id,
        data:req.body
    })
    res.status(200).json({})
}

exports.set_id = function(req, res, next, _id) {
    req.requisition = { id: _id }
    next()
}

exports.verify = function(type, method, id, body){
    var find = false;
    var data = null;

    function findOnStack() {
        for(var i = 0; i < global.stack.length; i++){
            var value = global.stack[i]
            global.stack.splice(i, 1);
            global.old_stack.push(value)

            if(value.method != method)
                continue

            if(value.type != type)
                continue

            if(value.id != id)
                continue

            //console.log(global.old_stack.length)
            global.old_stack.pop()
            //console.log(global.old_stack.length)

            data = value.data
            return false
        }

        return true
    }

    for(var i = 0; i < global.old_stack.length; i++){
        var value = global.old_stack[i]
        if(value.method != method)
            continue

        if(value.type != type)
            continue

        if(value.id != id)
            continue

        global.old_stack.splice(i, 1);
        data = value.data
        find = true
    }

    if(find)
        return data

    while(findOnStack()) {require('deasync').sleep(1);}
    return data;
}

/*
    function time() {
        new Promise(function (resolve) {
            var guid = 'some-guid';
            // enqueue in redis or whatever you like and save the guid to match later.
            global.stack.push({
                data: guid,
                resolve: resolve
            });
            resolve()
        }).then(function () {
            console.log('here')
            sync = false
        });
    }

    async.series([
        time
    ])
    */

    // queue consumer calls this (probably with the guid parameter)
    //global.stack.length && global.stack.some(function (item) {
    //    if (item.data === 'some-guid') {
    //        item.resolve();
    //    }
    //});
