/* global describe */
/* global it */
/* global beforeEach */
/* global afterEach */
/* global before */
// Invoke 'strict' JavaScript mode
'use strict';

// Load the test dependencies
var should = require('should')
var assert = require('assert');

var User = require("../app/models/user.server.model.js")
var Data = require("../app/models/data.server.model.js")
var AccessPoint = require("../app/models/access_point.server.model.js")

var function_block = []

var function_block1 = require("./CrudTest.js")
var function_block2 = require("./BasicTest.js")

// function_block = function_block.concat(function_block1)
function_block = function_block.concat(function_block2)

var sucess = 0
var failures = 0
var total  = 0;

module.exports = function name(params) {
    for(var i = 0; i < function_block.length;i++){
        console.log("---------------------------------")
        console.log('  ' + function_block[i].block_name)
        var functions = function_block[i].functions;
        for(var j = 0; j < functions.length; j++){
            try{
                functions[j].todo()
                sucess++;
                console.log('      ' + functions[j].function_name + ': \u2713')
            }catch(e){
                failures++;
                console.log(e)
                console.log('      ' + functions[j].function_name + ': \u2717')
            }
        }
    }
    console.log("=============================")
    console.log('Sucess:'+ sucess)
    console.log('Failures:'+ failures)
    console.log("=============================")
}
