/* global describe */
/* global it */
/* global beforeEach */
/* global afterEach */
/* global before */
// Invoke 'strict' JavaScript mode
'use strict';

// Load the test dependencies
var app = require('../server.js')
var should = require('should')

var User = require("../app/models/user.server.model.js")
var Data = require("../app/models/data.server.model.js")
var AccessPoint = require("../app/models/access_point.server.model.js")
