/* global describe */
/* global it */
/* global beforeEach */
/* global afterEach */
/* global before */
/* global after */
// Invoke 'strict' JavaScript mode
// Load the test dependencies

var app = require('../server.js')
var should = require('should')
var assert = require('assert');



var User = require("../app/models/user.server.model.js")
var Data = require("../app/models/data.server.model.js")
var AccessPoint = require("../app/models/access_point.server.model.js")
var Justification = require("../app/models/justification.server.model.js")
var DataType = require("../app/models/data_type.server.model.js")
var Stuff = require("../app/models/stuff.server.model.js")
var Access = require("../app/models/access.server.model.js")

//Com excção das justifications testes passando, nenhum quebrando o sistema
module.exports = [
    {
        block_name:'Basic saves',
        functions:[{
            function_name:'user save',
            todo:function(){
					//Passou nos testes
                    var user = new User()
                    user.save()
                    user.get(true)
                }
            },{
                function_name:'data save',
                todo:function(){
					//Passou nos testes
                    var user = new User()
                    user.save()
                    user.get(true)
                    var data = new Data(user)
                    data.save()
                    data.get(true)
                }
            },{
                function_name:'access_point save',
                todo:function(){
					//Passou nos testes
                    var access_point = new AccessPoint()
                    access_point.save()
                    access_point.isRecived('POST')
                    access_point.get(true)
                }
            },{
                function_name:'justification save',
                todo:function(){
					//nao passou mas nao quebrou, erro no json
                    var user = new User()
                    user.save()
                    user.get(true)
                    var justification = new Justification(user)
                    justification.save();
                    justification.get(true);

                }
            },{
                function_name:'stuff save',
                todo:function(){
					//Passou nos testes
                    var user = new User()
                    user.save()
                    user.get(true)
                    var stuff = new Stuff(user)
                    stuff.save();
                    stuff.get(true);
                }
            },{
                function_name:'access save',
                todo:function(){
                    var access = new Access()
                    access.save()
                    access.isRecived('POST')
                    access.get(true)
                }
            },{
                function_name:'Datatype save',
                todo:function(){
                    var dataType = new DataType()
                    dataType.save();
                    dataType.get(true);
                }
            }
        ]
    },{
        block_name:'Basic updates',
        functions:[{
            function_name:'user update',
            todo:function(){
                    var user = new User()
                    user.save()
                    user.get(true)
                    user.update()
                    user.get(true)
                }
            },{
                function_name:'access_point update',
                todo:function(){
                    var access_point = new AccessPoint()
                    access_point.save()
                    access_point.isRecived("POST")
                    access_point.get(true)
                    access_point.update()
                    access_point.get(true)
                }
            },{
                function_name:'justification update',
                todo:function(){
                    var justification = new Justification()
                    justification.save()
                    justification.get(true)
                    justification.update()
                    justification.get(true)

                }
            },{
                function_name:'stuff update',
                todo:function(){
                    var stuff = new Stuff()
                    stuff.save()
                    stuff.get(true)
                    stuff.update()
                    stuff.get(true)
                }
            }
        ]
    },{
        block_name:'Basic deletes',
        functions:[{
            function_name:'user delete',
            todo:function(){
                    var user = new User()
                    user.save()
                    user.get(true)
                    user.delete()
                    user.get(false)
                }
            },{
                function_name:'data delete',
                todo:function(){
                    var user = new User()
                    user.save()
                    user.get(true)
                    var data = new Data(user)
                    data.save()
                    data.get(true)
                    data.delete()
                    data.get(false)
                }
            },{
                function_name:'access_point delete',
                todo:function(){
                    var access_point = new AccessPoint()
                    access_point.save()
                    access_point.isRecived("POST")
                    access_point.get(true)
                    access_point.delete()
                    access_point.get(false)
                }
            },{
                function_name:'justification delete',
                todo:function(){
                    var justification = new Justification()
                    justification.save()
                    justification.get(true)
                    justification.delete()
                    justification.get(false)

                }
            },{
                function_name:'stuff delete',
                todo:function(){
                    var stuff = new Stuff()
                    stuff.save()
                    stuff.get(true)
                    stuff.delete()
                    stuff.get(false)
                }
            },{
                function_name:'Datatype delete',
                todo:function(){
                    var dataType = new DataType()
                    dataType.save()
                    dataType.get(true)
                    dataType.delete()
                    dataType.get(false)
                }
            }
        ]
    }
]
