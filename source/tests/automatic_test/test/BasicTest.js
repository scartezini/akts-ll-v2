/* global describe */
/* global it */
/* global beforeEach */
/* global afterEach */
/* global before */
// Invoke 'strict' JavaScript mode
'use strict';

// Load the test dependencies
var app = require('../server.js')
var should = require('should')

var User = require("../app/models/user.server.model.js")
var Data = require("../app/models/data.server.model.js")
var AccessPoint = require("../app/models/access_point.server.model.js")
var Justification = require("../app/models/justification.server.model.js")
var DataType = require("../app/models/data_type.server.model.js")
var Stuff = require("../app/models/stuff.server.model.js")
var Access = require("../app/models/access.server.model.js")
var Relation = require("../app/models/relation.server.model.js")

module.exports = [
    {
        block_name:'Basic saves on controller',
        functions:[{
            function_name:'user save on controller',
            todo:function(){
                var user = new User()
                user.save()
                user.get(true)

                var data = new Data(user)
                data.save()
                data.get(true)

                var access_point = new AccessPoint()
                access_point.save()
                access_point.isRecived('POST')
                access_point.get(true)

                var relation = new Relation()
                relation.user_ap_relation(user, access_point)

                user.isRecived()
                data.isRecived()
            }
        },{
            function_name:'user save on controller 2 datas',
            todo:function(){
                var user = new User()
                user.save()
                user.get(true)

                var data = new Data(user)
                data.save()
                data.get(true)

                var data2 = new Data(user)
                data2.save()
                data2.get(true)
				//
                var access_point = new AccessPoint()
                access_point.save()
				console.log('Antes')
                access_point.isRecived('POST')
				console.log('depois')
                access_point.get(true)

				console.log('Antes2')
                var relation = new Relation()
                relation.user_ap_relation(user, access_point)
				console.log('depois2')

                user.isRecived()
                data.isRecived()
                data2.isRecived()
            }
        },{
            function_name:'dois acesspoints # arrumar o test',
            todo:function(){
                var user = new User()
                user.save()
                user.get(true)

                var data = new Data(user)
                data.save()
                data.get(true)

                var data2 = new Data(user)
                data2.save()
                data2.get(true)


                var access_point = new AccessPoint()
                access_point.save()
                access_point.isRecived('POST')
                access_point.get(true)

                var access_point2 = new AccessPoint()
                access_point2.save()
                access_point2.isRecived('POST')
                access_point2.get(true)

                var relation = new Relation()
                relation.user_ap_relation(user, access_point)

                // user.isRecived()
                // data.isRecived()
                // data2.isRecived()
            }
        },{
            function_name:'delete relation',
            todo:function(){
                    var user = new User()
                    user.save()
                    user.get(true)

                    var data = new Data(user)
                    data.save()
                    data.get(true)

                    var access_point = new AccessPoint()
                    access_point.save()
                    access_point.isRecived('POST')
                    access_point.get(true)

                    var relation = new Relation()
                    relation.user_ap_relation(user, access_point)

                    user.isRecived('POST')
                    data.isRecived('POST')

                    relation.user_ap_relation_delete(user, access_point)

                }
            },{
                function_name:'data propagate',
                todo:function(){
                    var user = new User()
                    user.save()
                    user.get(true)

                    var access_point = new AccessPoint()
                    access_point.save()
                    access_point.isRecived('POST')
                    access_point.get(true)

                    var relation = new Relation()
                    relation.user_ap_relation(user, access_point)

                    var data = new Data(user)
                    data.save()
                    data.get(true)

                    data.isRecived('POST')

                }
            }
        ]
    }
]
