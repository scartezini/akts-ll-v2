var config = require("./config")
var express = require('express')
var morgan = require('morgan')
var compress = require('compression')
var bodyParser = require('body-parser')
var methodOverride = require('method-override')
var session = require('express-session')
var flash = require('connect-flash')
var passport = require('passport')
var moment = require('moment')
var http = require('http')

module.exports = function(db) {
	// Create a new Express application instance
	var app = express();

	// Create a new HTTP server
    var server = http.createServer(app);

	// Use the 'NDOE_ENV' variable to activate the 'morgan' logger or 'compress' middleware
	if (process.env.NODE_ENV === 'development') {
		//app.use(morgan('dev'));
	} else if (process.env.NODE_ENV === 'production') {
		//app.use(compress());
	}

    //app.use(morgan('dev'));
	// Use the 'body-parser' and 'method-override' middleware functions
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(bodyParser.json());
	app.use(methodOverride());

	// Set the application view engine and 'views' folder
	app.set('views', './app/views');
	app.set('view engine', 'ejs');

	// Configure the flash messages middleware
	app.use(flash());

	// Configure the Passport middleware
	app.use(passport.initialize());
	app.use(passport.session());

	// Load the routing files
	require('../app/routes/server.server.routes.js')(app);
	require('../app/routes/port.server.routes.js')(app);

	// Configure static file serving
	app.use(express.static('./public'));

	// Return the Server instance
	return server;
};
