
// Invoke 'strict' JavaScript mode
'use strict';

// Set the 'NODE_ENV' variable
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Load the module dependencies
var	express = require('./config/express')

// Create a new Express application instance
var app = express();
var apps = []
for(var i = 0; i < 50; i++){
    apps.push(express());
    apps[i].listen(10001 + i)
}

app.listen(1200);

// Log the server status to the console
console.log('Server running at http://localhost:3000/');

global.stack = []
global.old_stack = []

var	tester = require('./test/MainTest.js')
tester();
// Use the module.exports property to expose our Express application instance for external usage
module.exports = app;
