#include <iostream>
#include "../../headers/tcpCom.hpp"

using namespace std;

int main() {
	TCP_Listener listener(12345);
	int inp;

	listener.start();
	cin >> inp;
	listener.stop();
	return 0;
}