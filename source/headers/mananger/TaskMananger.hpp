/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef TASK_MANANGER_H
#define TASK_MANANGER_H

// includes
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

#include "../mananger/RouteMananger.hpp"

// defines

// namespace
using namespace std;

class TaskMananger {
	// name
	static const string module_name;
	// routers
	ActionRouter *action_router;
	HttpRouter *http_router;

	// threads
	uint num_action_routers;
	uint num_http_routers;

	vector<thread> action_routers;
	vector<thread> http_routers;

  public:
	TaskMananger();
	~TaskMananger();
	void start();

	void feed_server();
	void feed_case();
};

#endif
