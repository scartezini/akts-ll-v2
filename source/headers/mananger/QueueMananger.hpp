/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef QUEUE_MANANGER_H
#define QUEUE_MANANGER_H

// includes
#include <unistd.h>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../http/HttpEntities.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

#include "../models/base_models.hpp"
#include "./RouteMananger.hpp"
// defines

// namespace
using namespace std;
using namespace odb;

// declarations

// class
class ActionQueue {
  private:
	list<Action> actions;
	list<Action> actions_final;

	std::mutex queue_mutex;
	std::mutex queue_mutex_final;

	std::condition_variable new_job;
	std::condition_variable new_job_final;

	vector<thread> threads;

	void run();
	static void runSolver(ActionQueue *);
	static void pinger(ActionQueue *action);

	void getFromDb();
	void makeStartActions();
	void saveOnDb(Action &);

	void waitSignal();

	void pushFinal(Action &);

  public:
	static const string module_name;
	void push(Action &);
	Action get();
	void start();
};

// namespace
//------------------------------------------------------------------------------
/**	\namespace queues
\brief XXXXXXXXXXXXXX namespace que guarda as funcoes basicas de respostas http
relacoes de pontos de acesso com usuarios por meio de uma interface restfull a
qual faz dos metodos que sao permitidos para um acesso, o que spor sua vez se da
pelos sequinte metodos: POST, DELETE, PUT. Estes por sua vez sequem o padrao de
json espesificados em \ref relation_access_point_user_json.
**/
//------------------------------------------------------------------------------
namespace queues {
	extern ActionQueue actions;
};

#endif
