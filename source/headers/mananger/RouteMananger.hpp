/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef ROUTE_MANANGER_H
#define ROUTE_MANANGER_H

// includes
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

#include <boost/algorithm/string.hpp>
#include <boost/network/include/http/server.hpp>
#include <boost/network/uri.hpp>

namespace http = boost::network::http;

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../controllers/entities_controllers.hpp"
#include "../controllers/sensor_controllers.hpp"
#include "../controllers/system_controller.hpp"

#include "../controllers/system_actors_controller.hpp"

#include "../drivers/ServerDriver.hpp"
#include "../http/HttpEntities.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"


// defines

// namespace
using namespace std;

// declarations
class HttpRouter;

// struct
struct server_handler;
typedef boost::network::http::server<server_handler> HttpServer;
typedef HttpServer::response (*res_func)(vector<string>&,
										 HttpServer::request const&);

typedef std::map<std::string, map<std::string, res_func>> route_map;
typedef std::map<std::string, map<std::string, int>> route_val;

class server_handler {
  public:
	void operator()(HttpServer::request const& req, HttpServer::response& res);
	void log(HttpServer::string_type const& info);
};

// class
class HttpRouter {
	// controllers
	static const string module_name;

	// variables
	HttpServer* server;
	static uint max_route_params;

	// intern builder
	void connect();
	void makeRoutes();
	void addRoute(string, string, res_func, uint);

	static std::map<std::string, map<std::string, res_func>> routes;
	static route_val routes_validate;

	// validação
	enum ROUTE_ERROR { ROUTE_NOT_FOUND, METHOD_NOT_FOUND, OK };
	static ROUTE_ERROR validadeRoute(string, string, uint);

  public:
	HttpRouter();

	static HttpServer::response getResponse(HttpServer::request const&);
	static void runSolver(HttpRouter*);
};

/////////bin/
// action router

typedef void (*actor_func)(Action&);
typedef std::map<Action::TABLE, map<Action::TYPE, map<Action::DEST, actor_func>>>
	actor_map;

class ActionRouter {
  public:
	enum ERROR { ROUTE_NOT_FOUND, OK };

	// controllers
	static const string module_name;

	actor_map routes;

	void run(Action&);

	void makeRoutes();
	void addRoute(Action::TABLE, Action::TYPE, Action::DEST, actor_func);

	ERROR validadeRoute(Action::TABLE, Action::TYPE, Action::DEST);

	ActionRouter();

	static void runSolver(ActionRouter*);
};

inline void server_handler::operator()(HttpServer::request const& req,
									   HttpServer::response& res) {
	res = HttpRouter::getResponse(req);
}

inline void server_handler::log(HttpServer::string_type const& info) {
	std::cerr << "ERROR: " << info << '\n';
}

#endif
