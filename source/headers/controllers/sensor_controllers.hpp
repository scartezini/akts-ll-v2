/** \file sensor_controllers.hpp
	\brief Arquivo responsavel por armazenar os controladores que representam os
 sensores do sistema.

 Arquivo a qual as controladoras de sensores sao armazenadas, tendo esta a
 funcao  de fazer um inteface http entre os sensores presentes no sistema e
 o mundo exterior via um interface restfull
 */
#ifndef SENSOR_CONTROLLER_H
#define SENSOR_CONTROLLER_H

// includes
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

#include <boost/network/include/http/server.hpp>
#include <boost/network/uri.hpp>

namespace http = boost::network::http;

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../http/HttpEntities.hpp"
#include "./system_controller.hpp"

#include "../mananger/QueueMananger.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

#include "../models/base_models.hpp"


// local
#include "../io_core/Actuator.hpp"
#include "../io_core/IO-Core.hpp"
#include "../io_core/Sensor.hpp"

// defines
typedef void (*actionfunc)(uint);

// namespace
using namespace std;
using namespace odb::core;
using namespace database_monkey;

// struct
struct server_handler;
typedef boost::network::http::server<server_handler> HttpServer;
typedef HttpServer::response (*res_func)(vector<string> &,
										 HttpServer::request const &);

// class / namespaces
//------------------------------------------------------------------------------
/**	\namespace KitSensorRoutes
	\brief namespace que guarda as funcoes basicas interface com o kit de
cadastramento que por sua vez permite o controle do mesmo via uma interface
restfull, permitindo que seja possivel fazer requisicoes pelos dados dos mesmo,
ou ate mesmo fazer alteracao no estado dos mesmo.
**/
//------------------------------------------------------------------------------
namespace KitSensorRoutes {
	HttpServer::response get(vector<string> &, HttpServer::request const &);
};

#endif
