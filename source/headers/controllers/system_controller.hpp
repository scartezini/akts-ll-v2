/** \file system_actors_controller.hpp
*  \brief Arquivo responsavel por armazenar os controladores que representam as
* entidade do sistema.
*
*  Arquivo a qual as entidades de contecto do sistemas, sendo estas as
* responsaveis pelo funcionamento geral
* e resposata das requisicoes http do sistema, sendo a mesmas apenas realcionar
* ao funcionamento e controlle
* interno e externo de tudo aqui que e relacionado ao sistema
*/
#ifndef SYSTEM_CONTROLLER_H
#define SYSTEM_CONTROLLER_H

// includes
#include <condition_variable>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

#include <boost/network/include/http/server.hpp>
#include <boost/network/uri.hpp>

namespace http = boost::network::http;

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../http/HttpEntities.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"
#include "../models/base_models.hpp"

#include "../mananger/QueueMananger.hpp"

// defines

// namespace
using namespace std;

// struct
struct server_handler;
typedef boost::network::http::server<server_handler> HttpServer;
typedef HttpServer::response (*res_func)(vector<string> &,
										 HttpServer::request const &);

//------------------------------------------------------------------------------
/**	\namespace HttpRoutes
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace HttpRoutes {
	/**
	 * \fn string decodeRoute(string ori_route, uint num_params, vector<string>
	 &params)
	 * \brief funcao responsavel pelo split da trota atual o que por sua vez e
	 feito
	 tranformando a rota em um rota, e argumentos
	 * \param ori_route rota original
	 \param num_params numreor de elementos que se deseja remover
	 \param params numero de argumentos gerados pela
	 \return rota finall
	 */
	string decodeRoute(string ori_route, uint num_params,
					   vector<string> &params);

	/** \fn HttpServer::response errorResponse(uint error);
		\brief função que monta uma resposta http de algum error
		\param error codigo de erro
		\return resposta em formato http
		\sa string decodeError(uint error_value);
	*/
	HttpServer::response errorResponse(uint);

	/** \fn string decodeError(uint error);
		\brief função que apartir de um codigo de erro monta a mensagem de erro
		\param error codigo de erro
		\return string com a mensagem de erro
		\sa HttpServer::response errorResponse(uint);
	*/
	string decodeError(uint);
}

//------------------------------------------------------------------------------
/**	\namespace OtherRoutes
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace OtherRoutes {
#ifndef DEBUGER

	/** \fn HttpServer::response dropAll(vector<string> &, HttpServer::request
	 * const &);
		\brief função de DEBUGER que dropa todo o banco de dados do mid, somente
	 é compilada com o define de DEBUGER
		 \param arguments sao os argumentos que estao presentes na rota http que
	 gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	 ou para direcionar a query do sistema, neste caso sendo os ids presentes na
	 rota
		 \param request parametro que define a requisicao que foi recebida pelo
	 server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	 de outros parametros que possam vir agregados a mesma
		 \return a resposta http correspondente a requisicao get, podendo ser
	 este a resposta da query solicita, ou um erro http devido a ma formatacao
	 ou perquisas inexistentes, sendo que este erro seguira o padrao http
	*/
	HttpServer::response dropAll(vector<string> &, HttpServer::request const &);
#endif


	void actionSyncLow(uint, std::string);
	void actionSyncHigh();
	void actionPanic(bool);

	void actionLoginHigh();

	HttpServer::response synchronize(std::vector<string> &,
									 HttpServer::request const &);
	HttpServer::response ping(vector<string> &, HttpServer::request const &);
	HttpServer::response timeget(vector<string> &, HttpServer::request const &);
	HttpServer::response panicOn(vector<string> &, HttpServer::request const &);
	HttpServer::response panicOff(vector<string> &,
								  HttpServer::request const &);
}

#endif
