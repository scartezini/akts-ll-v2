/**
\file entities_controllers.hpp
\brief Arquivo responsável por armazenar os controladores que representamas
entidades de contexto do sistema.

Arquivo a qual as controladoras das entidades de contexto são armazenadas.
Sendo entidades de contexto as entidades de banco de dados que são
represatações de modelos presentes no mundo real que estão presentes nos
requisitos do sistema.
Tais controladores tem como responsabilidade a geração de respostas das
requisicoes http, que tem como objetido de analise estas entidade ditas como
de contexto. Logo tais controladoras são as que possuiem os metodos http query
mapeiam o CRUD basico de banco de dados destas entidades.
*/

#ifndef ENTITIES_CONTROLLER_H
#define ENTITIES_CONTROLLER_H

// includes
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

#include <boost/network/include/http/server.hpp>
#include <boost/network/uri.hpp>

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../http/HttpEntities.hpp"
#include "./system_controller.hpp"

#include "../mananger/QueueMananger.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

#include "../models/base_models.hpp"


// namespace
namespace http = boost::network::http;
using namespace std;
using namespace odb::core;
using namespace database_monkey;

/**	\typedef typedef void (*actionfunc)(uint);
	\brief typedef que representa o tipo basico das funcoes que geram
AccessRoutes, que sao as que sao chamadas para adiconar as acoes resultades de
uma exitacao do sistema a lista de acoes a serem axecutadas
*/
// TODO document
struct server_handler;

/**	\typedef typedef void (*actionfunc)(uint);
	\brief typedef que representa o tipo basico das funcoes que geram
AccessRoutes, que sao as que sao chamadas para adiconar as acoes resultades de
uma exitacao do sistema a lista de acoes a serem axecutadas
*/
// TODO document
typedef boost::network::http::server<server_handler> HttpServer;

/**	\typedef typedef void (*actionfunc)(uint);
	\brief typedef que representa o tipo basico das funcoes que geram
AccessRoutes, que sao as que sao chamadas para adiconar as acoes resultades de
uma exitacao do sistema a lista de acoes a serem axecutadas
*/
// TODO document
typedef HttpServer::response (*res_func)(vector<string> &,
										 HttpServer::request const &);

/**	\typedef typedef void (*actionfunc)(uint);
	\brief typedef que representa o tipo basico das funcoes que geram
AccessRoutes, que sao as que sao chamadas para adiconar as acoes resultades de
uma exitacao do sistema a lista de acoes a serem axecutadas
*/
typedef void (*actionfunc)(uint);

//------------------------------------------------------------------------------
/**	\namespace BaseRoutes
	\brief namespace que guarda as funcoes basicas de respostas de uma interface
restfull a qual faz com que todo os metdos possuam uma chamada as estas fucoes
garntindo assim uma homogenizacao das respostas sem replicacao de codigo
**/
//------------------------------------------------------------------------------
namespace BaseRoutes {
	/**	\fn HttpServer::response get(vector<string> &arguments,
	HttpServer::requestconst &request);
		\brief funcao que responde a uma requisicao GET seguindo o padrao
	especificado como basico para um interface de comunicacao restfull
	sendo entao esta a funcao que e chamada por todas as rotas depois do
	processamento especifico de cada uma delas
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar a query do sistema, neste caso sendo os ids presentes na
	rota
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma
		\return a resposta http correspondente a requisicao get, podendo ser
	este a resposta da query solicita, ou um erro http devido a ma formatacao
	ou perquisas inexistentes, sendo que este erro seguira o padrao http
	*/
	template <typename E = Entity>
	HttpServer::response get(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response post(vector<string> &arguments,
	HttpServer::request const &request, vector<actionfunc> actions);
		\brief funcao que responde a uma requisicao POST seguindo o padrao
	especificado como basico para um interface de comunicacao restfull
	sendo entao esta a funcao que e chamada por todas as rotas depois do
	processamento especifico de cada uma delas, sendo esta funcao um mapeamento
	do post que tem a responsabilidade de persistencia no banco de dados
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar a operacao atual, neste caso este sendo vazios, vide que
	nao existe na padrao rest nenhuma utilidade para os memos em uma requisicao
	de post
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma
		\param actions parametro que define quais serao as acoes resultantes
	desta operacao de post, estas sao funcoes as quais tem como objetivo gerar
	as acoes que permitiram a propragacao desta acao, ou a tomada de operacoes
	que garantiram a consistencia e integridade do sistema
		\return a resposta http correspondente a requisicao post, podendo ser
	esta o id do objeto criado , ou um erro http devido a ma formatacao
	ou parametros inexistentes, sendo que este erro seguira o padrao http
	*/
	template <typename E = Entity>
	HttpServer::response post(vector<string> &params,
							  HttpServer::request const &req,
							  vector<actionfunc> actions);

	/**	\fn HttpServer::response BaseRoutes::put(vector<string> &arguments,
	HttpServer::request const &request, vector<actionfunc> actions) throw
	(std::invalid_argument)
		\brief funcao que responde a uma requisicao PUT seguindo o padrao
	especificado como basico para um interface de comunicacao restfull
	sendo entao esta a funcao que e chamada por todas as rotas depois do
	processamento especifico de cada uma delas, sendo esta funcao um mapeamento
	do post que tem a responsabilidade de atualizar entidades no banco de dados
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar a operacao atual, neste caso possuindo o parametro id, o
	qual e responsável por direcionar qual sera a entidade a ser atualizada com
	o nova conteudo passado
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma
		\param actions parametro que define quais serao as acoes resultantes
	desta operacao de put, estas sao funcoes as quais tem como objetivo gerar
	as acoes que permitiram a propragacao desta acao, ou a tomada de operacoes
	que garantiram a consistencia e integridade do sistema
		\return a resposta http correspondente a requisicao post, podendo ser
	esta o id do objeto atualizado , ou um erro http devido a ma formatacao
	ou parametros inexistentes, sendo que este erro seguira o padrao http
	*/
	template <typename E = Entity>
	HttpServer::response put(
		vector<string> &params, HttpServer::request const &req,
		vector<actionfunc> actions) throw(std::invalid_argument);

	/**	\fn HttpServer::response mdelete(vector<string> &arguments,
	HttpServer::request const &request, vector<actionfunc> actions) throw
	(std::invalid_argument)
		\brief funcao que responde a uma requisicao DELETE seguindo o padrao
	especificado como basico para um interface de comunicacao restfull
	sendo entao esta a funcao que e chamada por todas as rotas depois do
	processamento especifico de cada uma delas, sendo esta funcao um mapeamento
	do detele que tem a responsabilidade de deletar entidades do banco de dados
		\param arguments sao os argumentos que estao presentes na rota http
	que gerou esta requisicao, sendo estes caso paramentros que serao utilizados
	ou para direcionar a operacao atual ou para defini-la, neste caso este sendo
	o parametro o id da entidade que sera deletada do banco de dados
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma
		\param actions parametro que define quais serao as acoes resultantes
	desta operacao de delete, estas sao funcoes as quais tem como objetivo gerar
	as acoes que permitiram a propragacao desta acao, ou a tomada de operacoes
	que garantiram a consistencia e integridade do sistema
		\return a resposta http correspondente a requisicao delete, podendo ser
	esta um json com o valor do status da operacao , ou um erro http devido a ma
	formatacao ou parametros inexistentes, sendo que este erro seguira o padrao
	http
	*/
	template <typename E = Entity>
	HttpServer::response mdelete(
		vector<string> &params, HttpServer::request const &req,
		vector<actionfunc> actions) throw(std::invalid_argument);
};


//------------------------------------------------------------------------------
/**	\namespace DataTypeRoutes
	\brief namespace que guarda as funcoes basicas de respostas http de data
type por meio de uma interface restfull a qual faz dos metodos que sao
permitidos para um acesso, o que spor sua vez se da pelos sequinte metodos:
POST, GET, DELETE, PUT. Estes por sua vez sequem o padrao de json espesificados
em \ref datatype_json.
**/
//------------------------------------------------------------------------------
namespace DataTypeRoutes {
	/**	\fn HttpServer::response get(vector<string>& arguments,
	HttpServer::request const& request)
			\brief funcao que responde a uma requisicao GET para um tipo data
	seguindo o padrao especificado como basico para um interface de comunicacao
	restfull, o que por sua vez segue o padrao de um json desta estidade
	especificado em \ref datatype_json.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar a query do sistema, neste caso sendo os ids presentes na
	rota
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma
		\return a resposta http correspondente a requisicao get, podendo ser
	este a resposta da query solicita, ou um erro http devido a ma formatacao
	ou perquisas inexistentes, sendo que este erro seguira o padrao http
	*/
	HttpServer::response get(vector<string> &arguments,
							 HttpServer::request const &request);

	/**	\fn HttpServer::response post(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao POST para tipos de dados
	seguindo o padrao especificado como basico para um interface de comunicacao
	restfull, o que por sua vez segue o padrao de um json desta estidade
	especificado pelo \ref datatype_json.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros vazios, visto que
	um post nao requere nenhum parametro
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	aquilo que formara a entidate que sera salva no database
		\return a resposta http correspondente a requisicao post, podendo ser
	este um id da entidade que foi criada, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response post(vector<string> &arguments,
							  HttpServer::request const &request);

	/**	\fn HttpServer::response put(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao PUT para tipos de dados
	seguindo o padrao especificado como basico para um interface de comunicacao
	restfull, o que por sua vez segue o padrao de um json desta estidade
	especificado pelo \ref datatype_json.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o update do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera atualizada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	aquilo que sera o valor de atuzalizacao da entidate que sera atualiza no
	database
		\return a resposta http correspondente a requisicao put, podendo ser
	este um id da entidade que foi atualizada, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response put(vector<string> &arguments,
							 HttpServer::request const &request);

	/**	\fn HttpServer::response mdelete(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao DELETE para datatype
	seguindo o padrao especificado como basico para um interface de comunicacao
	restfull.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o delete do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera deletada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	valor vazio visto que nao e necessario nenum parametro para a exucucao do
	delete
		\return a resposta http correspondente a requisicao delete, podendo ser
	este o estado da delecao que ocorreu, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response mdelete(vector<string> &, HttpServer::request const &);
};


//###########################################################################
//#############################	HIGH ########################################
//###########################################################################

//-----------------------------------------------------------------------------
/** \namespace AccessHighRoutes
	\brief namespace o qual pertence as funções lançadas para tratamento das
   requisições feita pelo High sever no que desrespeita à acesso;
*/
//-----------------------------------------------------------------------------
namespace AccessHighRoutes {
	/**	\fn HttpServer::response get(vector<string>& arguments,
		HttpServer::request const& request)
			\brief funcao que responde a uma requisicao GET para acesso seguindo
	   o padrao especificado como basico para um interface de comunicacao
	   restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response get(vector<string> &, HttpServer::request const &);
}
//-----------------------------------------------------------------------------
/** \namespace AccessPointHighRoutes
	\brief namespace o qual pertence as funções lançadas para tratamento das
   requisições feita pelo High sever no que desrespeita à ponto de acesso;
*/
//-----------------------------------------------------------------------------
namespace AccessPointHighRoutes {

	/** \fn void propagatePutOnLow(uint id)
	\brief função responsável para lançar a action que vai porpagar alterações
	de um ponto de acesso para o atuador corresnpondente
	\params id identificador do atuador que foi alterado
	*/
	void propagatePutOnLow(uint);

	/**	\fn HttpServer::response get(vector<string>& arguments,
		HttpServer::request const& request)
			\brief funcao que responde a uma requisicao GET para acesso seguindo
	   o padrao especificado como basico para um interface de comunicacao
	   restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response get(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response put(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao PUT para pontos de acesso
	seguindo o padrao especificado como basico para um interface de comunicacao
	restfull.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o update do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera atualizada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	aquilo que sera o valor de atuzalizacao da entidate que sera atualiza no
	database
		\return a resposta http correspondente a requisicao put, podendo ser
	este um id da entidade que foi atualizada, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response put(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response mdelete(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao DELETE
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o delete do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera deletada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	valor vazio visto que nao e necessario nenum parametro para a exucucao do
	delete
		\return a resposta http correspondente a requisicao delete, podendo ser
	este o estado da delecao que ocorreu, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response mdelete(vector<string> &, HttpServer::request const &);
}


//-----------------------------------------------------------------------------
/** \namespace RegisterKitHighRoutes
	\brief namespace o qual pertence as funções lançadas para tratamento das
   requisições feita pelo High sever no que desrespeita ao kit de registro;
*/
//-----------------------------------------------------------------------------
namespace RegisterKitHighRoutes {
	/** \fn void propagatePutOnLow(uint id)
	\brief função responsável para lançar a action que vai porpagar alterações
	de um kit de registro para o atuador corresnpondente
	\params id identificador do kit que foi alterado
	*/
	void propagatePutOnLow(uint);


	/**	\fn HttpServer::response get(vector<string>& arguments,
		HttpServer::request const& request)
			\brief funcao que responde a uma requisicao GET seguindo
	   o padrao especificado como basico para um interface de comunicacao
	   restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response get(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response put(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao PUT	seguindo o padrao
	especificado como basico para um interface de comunicacao
	restfull.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o update do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera atualizada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	aquilo que sera o valor de atuzalizacao da entidate que sera atualiza no
	database
		\return a resposta http correspondente a requisicao put, podendo ser
	este um id da entidade que foi atualizada, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response put(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response mdelete(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao DELETE
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o delete do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera deletada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	valor vazio visto que nao e necessario nenum parametro para a exucucao do
	delete
		\return a resposta http correspondente a requisicao delete, podendo ser
	este o estado da delecao que ocorreu, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response mdelete(vector<string> &, HttpServer::request const &);
}

//-----------------------------------------------------------------------------
/** \namespace DataHighRoutes
	\brief namespace o qual pertence as funções lançadas para tratamento das
   requisições feita pelo High sever no que desrespeita à data (cartoes de
   acessos);
*/
//-----------------------------------------------------------------------------
namespace DataHighRoutes {

	/**	\fn void deleteDataOnLow(uint);
		\brief função responsável por lançar a action para propagar o delete de
	   uma data(cartao, celular, ...) nos atuadores correspondentes
	   \param id indentificador da data(cartao, celular, ...)
	*/
	void deleteDataOnLow(uint);

	/** \fn void propagateDataOnLow(uint);
		\brief função responsável por lanção a action para propagar uma nova
	   data(cartão, celular, ...) para os atuadores.
	   \param id identificador da data(cartão, celular, ...)
	*/
	void propagateDataOnLow(uint id);

	/**	\fn HttpServer::response get(vector<string>& arguments,
		HttpServer::request const& request)
			\brief funcao que responde a uma requisicao GET seguindo
	   o padrao especificado como basico para um interface de comunicacao
	   restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response get(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response post(vector<string>& arguments,
		HttpServer::request const& request)
			\brief função que responde uma resquisição de POST seguindo o padrao
	   especificado como basico para uma interface de comunicacao restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response post(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response mdelete(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao DELETE
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o delete do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera deletada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	valor vazio visto que nao e necessario nenum parametro para a exucucao do
	delete
		\return a resposta http correspondente a requisicao delete, podendo ser
	este o estado da delecao que ocorreu, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response mdelete(vector<string> &, HttpServer::request const &);
}


//-----------------------------------------------------------------------------
/** \namespace StuffHighRoutes
	\brief namespace o qual pertence as funções lançadas para tratamento das
   requisições feita pelo High sever no que desrespeita à stuff(objetos do
   usuario)
*/
//-----------------------------------------------------------------------------
namespace StuffHighRoutes {
	/**	\fn HttpServer::response get(vector<string>& arguments,
		HttpServer::request const& request)
			\brief funcao que responde a uma requisicao GET seguindo
	   o padrao especificado como basico para um interface de comunicacao
	   restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response get(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response post(vector<string>& arguments,
		HttpServer::request const& request)
			\brief função que responde uma resquisição de POST seguindo o padrao
	   especificado como basico para uma interface de comunicacao restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response post(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response put(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao PUT	seguindo o padrao
	especificado como basico para um interface de comunicacao
	restfull.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o update do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera atualizada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	aquilo que sera o valor de atuzalizacao da entidate que sera atualiza no
	database
		\return a resposta http correspondente a requisicao put, podendo ser
	este um id da entidade que foi atualizada, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response put(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response mdelete(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao DELETE
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o delete do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera deletada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	valor vazio visto que nao e necessario nenum parametro para a exucucao do
	delete
		\return a resposta http correspondente a requisicao delete, podendo ser
	este o estado da delecao que ocorreu, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response mdelete(vector<string> &, HttpServer::request const &);
}

//-----------------------------------------------------------------------------
/** \namespace UserHighRoutes
	\brief namespace o qual pertence as funções lançadas para tratamento das
   requisições feita pelo High sever no que desrespeita à Usuario
*/
//-----------------------------------------------------------------------------
namespace UserHighRoutes {

	/** \fn void deleteUserOnLow(uint);
		\brief função responsavel por lançar a action para propagar um delete de
	   usuario nos atuadores correspondentes;
	   \param id identificador do usuario
	*/
	void deleteUserOnLow(uint);


	/**	\fn HttpServer::response get(vector<string>& arguments,
		HttpServer::request const& request)
			\brief funcao que responde a uma requisicao GET seguindo
	   o padrao especificado como basico para um interface de comunicacao
	   restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response get(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response post(vector<string>& arguments,
		HttpServer::request const& request)
			\brief função que responde uma resquisição de POST seguindo o padrao
	   especificado como basico para uma interface de comunicacao restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response post(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response put(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao PUT	seguindo o padrao
	especificado como basico para um interface de comunicacao
	restfull.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o update do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera atualizada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	aquilo que sera o valor de atuzalizacao da entidate que sera atualiza no
	database
		\return a resposta http correspondente a requisicao put, podendo ser
	este um id da entidade que foi atualizada, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response put(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response mdelete(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao DELETE
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o delete do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera deletada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	valor vazio visto que nao e necessario nenum parametro para a exucucao do
	delete
		\return a resposta http correspondente a requisicao delete, podendo ser
	este o estado da delecao que ocorreu, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response mdelete(vector<string> &, HttpServer::request const &);
}


//-----------------------------------------------------------------------------
/** \namespace RelationAccessPointUserHighRoutes
	\brief namespace o qual pertence as funções lançadas para tratamento das
   requisições feita pelo High sever no que desrespeita à relação entre usuario
   e Ponto de Acesso
*/
//-----------------------------------------------------------------------------
namespace RelationAccessPointUserHighRoutes {
	/** \fn void saveRelationAccessPointUserOnLow(uint);
		\brief função responsavel por lançar a action para propagar uma relação
	   entre o usuario e um ponto de acesso, para o respequitivo atuador;
	   \param id indentificador da relação
	*/
	void saveRelationAccessPointUserOnLow(uint);

	/** \fn void mdeleteRelationAccessPointUserOnLow(uint);
		\brief função responsavel por lançãr a action para porpagar o delete de
	   uma relação entre um usuario e um ponto de acesso
	   \param id identificador da relação
	*/
	void mdeleteRelationAccessPointUserOnLow(uint);

	/**	\fn HttpServer::response post(vector<string>& arguments,
		HttpServer::request const& request)
			\brief função que responde uma resquisição de POST seguindo o padrao
	   especificado como basico para uma interface de comunicacao restfull
			\param arguments sao os argumentos que estao presentes na rota http
	   que gerou esta requisicao, sendo para este caso paramentros que serao
	   utilizado ou para direcionar a query do sistema, neste caso sendo os ids
	   presentes na rota
			\param request parametro que define a requisicao que foi recebida
	   pelo server, o que por sua vez permite uma recuperacao do body da
	   requisicao, ou de outros parametros que possam vir agregados a mesma
			\return a resposta http correspondente a requisicao get, podendo ser
		este a resposta da query solicita, ou um erro http devido a ma
	   formatacao ou perquisas inexistentes, sendo que este erro seguira o
	   padrao http
		*/
	HttpServer::response post(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response put(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao PUT	seguindo o padrao
	especificado como basico para um interface de comunicacao
	restfull.
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o update do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera atualizada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	aquilo que sera o valor de atuzalizacao da entidate que sera atualiza no
	database
		\return a resposta http correspondente a requisicao put, podendo ser
	este um id da entidade que foi atualizada, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response put(vector<string> &, HttpServer::request const &);

	/**	\fn HttpServer::response mdelete(vector<string>& arguments,
	HttpServer::request const& request)
		\brief funcao que responde a uma requisicao DELETE
		\param arguments sao os argumentos que estao presentes na rota http que
	gerou esta requisicao, sendo para este caso paramentros que serao utilizado
	ou para direcionar o delete do sistema, neste caso sendo o id presentes na
	rota para definir a entidade que sera deletada
		\param request parametro que define a requisicao que foi recebida pelo
	server, o que por sua vez permite uma recuperacao do body da requisicao, ou
	de outros parametros que possam vir agregados a mesma, neste caso sendo o
	valor vazio visto que nao e necessario nenum parametro para a exucucao do
	delete
		\return a resposta http correspondente a requisicao delete, podendo ser
	este o estado da delecao que ocorreu, ou um erro http devido a ma
	formatacao ou perquisas inexistentes, sendo que este erro seguira o padrao
	http
	*/
	HttpServer::response mdelete(vector<string> &, HttpServer::request const &);
}

//###########################################################################
//#############################	LOW #########################################
//###########################################################################

namespace AccessLowRoutes {
	void saveAccessOnHigh(uint);
	HttpServer::response post(vector<string> &, HttpServer::request const &);
}

namespace DeniedAccessLowRoutes {
	void propagateOnHigh(std::string &);
	HttpServer::response post(vector<string> &, HttpServer::request const &);
}

namespace AccessPointLowRoutes {
	void saveAccessPointOnHigh(uint);
	HttpServer::response post(vector<string> &arguments,
							  HttpServer::request const &request);
}

namespace RegisterKitLowRoutes {
	void saveRegisterKitOnHigh(uint);
	HttpServer::response post(vector<string> &arguments,
							  HttpServer::request const &request);
}

#endif
