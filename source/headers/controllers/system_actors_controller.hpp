/** \file system_actors_controller.hpp
	\brief Arquivo responsavel por armazenar os atores do sistema.

	Arquivo a qual as entidades que executao acoes internas que sao geradas
durante algum processamento ou requisicao, estas entidade sendo as responsaveis
por proparagar os efeitos colatares das acoes que ocorrem durante o decorrer de
um sistema de controle de acesso
*/
#ifndef SYSTEM_ACTORS_CONTROLLER_H
#define SYSTEM_ACTORS_CONTROLLER_H

// includes
#include <limits.h>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>

#include <boost/network/protocol/http/client.hpp>
#include <boost/network/uri.hpp>

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../http/HttpEntities.hpp"
#include "../mananger/QueueMananger.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

#include "../models/base_models.hpp"
#include "../models/entities_models.hpp"

#include "./entities_controllers.hpp"
#include "./system_controller.hpp"

// defines

// namespace
using namespace std;
namespace http = boost::network::http;

typedef boost::network::http::basic_client<
	boost::network::http::tags::http_async_8bit_udp_resolve, 1, 1>
	client;

//------------------------------------------------------------------------------
/**	\namespace DataActor
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace DataActor {

	/** \fn void propagate(Action&);
		\brief funçãp que propaga uma data(cartao, celular, ...) para os pontos
	   de acesso que à pertencer.
	   \param action um objeto que contem o id da data, no atributo action.id_1
	   \sa Action();
	*/
	void propagate(Action &action);
	/**
	  \fn void mdelete(Action& action)
	  \brief funcao que propaga a acao de delete de uma data para o lowServer,
	  pesquisando todos os AccessPoint que pertecem a essa data, se for a
	  ultima data
	  do usuario essa funçao deleta o usuario dos accessPoint
	  \param action recbe de argurmento uma action onde contera o id da data a
	  ser deletada
	 */
	void mdelete(Action &action);
}

//------------------------------------------------------------------------------
/**	\namespace UserActor
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace UserActor {

	/** \fn void mdelete(Action&);
		\brief função que propaga a ação de delete de um user nos atuadores que
	   o pertence;
	   \param Action objeto para ser uasado a variavel id_1, onde tera o
	   indentificador de user
	   \sa Action();
	*/
	void mdelete(Action &action);
}

//------------------------------------------------------------------------------
/**	\namespace BaseActor
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace BaseActor {
	/**
	  \fn void post_curl(std::string local_route, std::string body)
	  \brief funcao responsavel realizar um POST de acordo com a rota.
	   \param Action objeto que devera conter a rota e o corpo da requisição
	  POST
	  \sa Action();
	 */
	void post_http(Action &);

	/** \fn void put_http(Action&);
		\brief realiza a requisiçãode PUT na rota indicada.
		\param Action objeto que contem a rota e o body (corpo) da requisição
		\sa Action();
	*/
	void put_http(Action &);

	/** \fn void mdelete_http(Action&);
		\brief realiza a requisição de DELETE na rota indicada
		\param Action objeto que  contem a roda da requisição
		\sa Action();
	*/
	void mdelete_http(Action &);

	/** \fn std::string get_http(Action::DEST, std::string);
		\brief realiza a requisição de PUT na rota indicada
		\param Action::DEST destido de qual servidor vai ser a requisição
		\param std::string rota da requisição
		\return Json que foi respondido pelo servidor,seguindo um padrao
	   restfull.
	   \sa Action();
	*/
	std::string get_http(Action::DEST, std::string);

	/**
	 * \tparam entidade que vai ser trabalhada no metodo
	 * \fn  void post(Action&, std::string local_route)
	 * \brief funcao responsavel resolver um acao de post basico no sistema, a
	 * 	qual apenas ocorre divergencia de tipo e rota
	 *  \param local_route rota em que o post ocorrera
	 *  \param action action que devera ser resolvida
	 */
	template <typename E = Entity>
	void post(Action &, std::string);

	/** \fn template <typename E = Entity> void put(Action &, std::string);
		\tparam entidade que vai ser trabalhada no metodo
		\brief função responsavel resolver uma ação de put basico do sistema
	   para o HIGH.
	   \param action ação a ser resolvida
	   \param route rota em que o put ocorrera
	*/
	template <typename E = Entity>
	void put(Action &, std::string);

	/** \fn void actionPost(Action::DEST dest, std::string route, std::string
	   body);
		\brief rotina para gerar uma action de POST
		\param dest servidor de destino
		\param route rota da requisição
		\param body corpo da requisição
		\sa Action();
	*/
	void actionPost(Action::DEST, std::string, std::string);

	/**	/fn void actionPut(Action::DEST dest, std::string route, std::string
	   body);
	   \brief rotina para gerar a action de PUT
	   \param dest servidor de destino
	   \param route rota da requisição
	   \param body corpo da requisição
	   \sa Action();
	*/
	void actionPut(Action::DEST, std::string, std::string);

	/** \fn void actionDelete(Action::DEST dest, std::string route);
		\brief rotina para gerar a action de DELETE
		\param dest servidor de destino
		\param route rota da requisição
		\sa Action();
	*/
	void actionDelete(Action::DEST, std::string);
}

//------------------------------------------------------------------------------
/**	\namespace AccessActor
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace AccessActor {
	/**
	 * \fn  void post(Action& action, std::string local_route)
	 * \brief funcao responsavel resolver um acao de post de acesso no sistema,
	 sendo estas
	 responsavel por repassar a mudanca desta entedidade para frente, resolvendo
	 assim os efietos colateriais
	 do acesso deta entidade
	 *  \param action action com o dados basicos da acao
	 */
	void post(Action &);

	/** \fn void get(std::string route);
		\brief rotina que busca todos os acessos que por algum motivo o low
	   server nao enviou e os salva no banco de dados
	   \param route rota para definir de qua atuador fazer essa busca, exemplo:
	   "http://10.190.60.52:1000/"
	*/
	void get(std::string);
};

//------------------------------------------------------------------------------
/**	\namespace AccessPointActor
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace AccessPointActor {
	/**
	 * \fn  void post(Action& action, std::string local_route)
	 * \brief funcao responsavel resolver um acao de post de ponto de acesso no
	 sistema, sendo estas
	 responsavel por repassar a mudanca desta entedidade para frente, resolvendo
	 assim os efietos colateriais
	 do acesso deta entidade
	 *  \param action action com o dados basicos da acao
	 */
	void post(Action &);

	/** \fn void put(Action& action);
		\brief rotina para alterar algum dado do ponto de acesso seja nome ou
	   tipo
	   \param action objeto que contera o id do ponto de acesso à ser alterado
	   \sa Action(); AccessPoint();
	*/
	void put(Action &);
};

namespace RegisterKitActor {
	/** \fn void put(Action& action);
		\brief rotina para alterar algum dado de um kit de cadastro
		\param action objeto que contera o id do kit à ser alterado
		\sa Action(); RegisterKit();
	*/
	void put(Action &);

	/** \fn void post(Action& action);
		\brief rotina para adicionar/registrar um novo kit de cadastro
		\param action objeto que contera o id do novo kit de cadastro
		\sa Action(); RegisterKit();
	*/
	void post(Action &);
}
//------------------------------------------------------------------------------
/**	\namespace RelationAccessPointUserActor
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace RelationAccessPointUserActor {

	void mdelete(Action &);
	/**
	 * \fn  void getIds(uint& userid, uint& apid, uint relation_id)
	 * \brief funcao que e respionavel por pegar o id de usuario e acesso pont
	 * em um deteminda relacao
	 * \param userid o id do usuario, por referencio, ou seja este sera
	 * prenchido
	 * \param apid o id do ponto de accesso, por referencio, ou seja este sera
	 * prenchido
	 * \param relation_id o id dad relacao que por sua vez sera a que
	 * relacionara os dois
	 */
	void getIds(uint &, uint &, uint);

	/**
	* \fn  void saveUser(uint _id, string ip)
	* \brief funcao que salva um usuiario em uma controladora
	* \param _id o id do usuario que sera salva no caontroladora
	* \param o ip da controladora onde o usuario sera salvo
	*/
	void saveUser(uint _id, AccessPoint &);

	/**
	* \fn  void getIds(uint& userid, uint& apid, uint relation_id)
	* \brief funcao que e respionavel por pegar o id de usuario e acesso pont em
	* um deteminda relacao
	* \param userid o id do usuario, por referencio, ou seja este sera prenchido
	* \param apid o id do ponto de accesso, por referencio, ou seja este sera
	* prenchido
	* \param relation_id dad relacao que por sua vez sera a que relacionara os
	* dois
	*/
	void saveDatas(uint user_id, AccessPoint &ap) throw(odb::exception);

	/**
	* \fn  string getIp(uint id)
	* \brief funcao resgata o ip de uma controladora
	* \param id o id da controladora que se deseja o ip
	* \return o ip da controladora
	*/
	AccessPoint getIp(uint id);

	/**
	 * \fn  void post(Action& action, std::string local_route)
	 * \brief funcao responsavel resolver um acao de post de uma relacao entre
	 ponto de acesso e usuaurio no sistema, sendo estas
	 responsavel por repassar a mudanca desta entedidade para frente, resolvendo
	 assim os efietos colateriais
	 do acesso deta entidade
	 *  \param action action com o dados basicos da acao
	 */
	void post(Action &);
};

//------------------------------------------------------------------------------
/**	\namespace OtherActor
	\brief namespace que guarda as funcoes basicas interface com sensores de, o
que por sua vez permite o controle dos mesmo via uma interface restfull,
permitindo que seja possivel fazer requisicoes pelos dados dos mesmo, ou ate
mesmo fazer alteracao no estado dos mesmos.
**/
//------------------------------------------------------------------------------
namespace OtherActor {
	/**
	 * \fn  void setOnline(Action &action)
	 * \brief funcao responsavel por informar ao serividor de alto nivel que
	 * o servido de comuunicalao mediana esta online
	 *  \param action action que devera ser resolvida
	 */
	void setOnline(Action &);

	/** \fn void loginHigh(Action& action);
		\brief rotina responsavel para logar no high, esta rorina é chamada
	   quando o cookie de acesso ao high é espirado ou é o primeiro login do
	   sistema. O username e password de acesso do mid ao high é configurado
	   atravez do json de config do sistema
	   \param action objeto para chamar essa ação seguindo o padrao
	   \sa Action();
	*/
	void loginHigh(Action &);

	/** \fn void syncHigh(Action& action);
		\brief rotina responsavel para tratar a sincroniazação do banco de dados
	   com o high
	   \param objeto para lançar a ação seguindo o padrão
	   \sa Action();
	*/
	void syncHigh(Action &);

	/** \fn void syncHigh(Action& action);
		\brief rotina responsavel para tratar a sincroniazação do banco de dados
	   com o low
	   \param objeto para lançar a ação seguindo o padrão
	   \sa Action();
	*/
	void syncLow(Action &);

	/** \fn void panic(Action& action);
		\brief rotina que ativa o estado de panico no sistema,abrindo todas as
	   portas;
	   \param obejto para lançar a ação e que contém se é para ativar ou
	   desativar o estado de panico
	   \sa Action();
	*/
	void panic(Action &);

	/** \fn void syncDiffHigh(Json::Value& json);
		\brief rotina que faz um diff com o high e os User que o high nao tiver
	   posta no high
	   \param json enviado pelo high no sync
	*/
	void syncDiffHigh(Json::Value &);

	/** \fn void syncDiffUserHigh(Json::Value& json);
		\brief rotina que faz o sync dos User com o high os campos de user
		\param json json de usuario enviado pelo high
	*/
	void syncDiffUserHigh(Json::Value &);

	/** \fn void syncDiffDataHigh(Json::Value& json);
		\brief rotina que faz o sync de Data
		\param json parte de data do json do high
	*/
	void syncDiffDataHigh(Json::Value &);

	/** \fn void syncDiffDataHigh(Json::Value& json);
		\brief rotina que faz o sync de stugg
		\param json parte de stuff do json do high
	*/
	void syncDiffStuffHigh(Json::Value &);

	/** \fn void syncDiffDataHigh(Json::Value& json);
		\brief rotina que faz o sync de ponto de acesso
		\param json parte de ponto de acesso do json do high
	*/
	void syncDiffRelationUserApHigh(Json::Value &);

	/** \fn Json::Value decodeJsonOfSync(std::string body);
		\brief função que transforma o body da requisição de sync respondida
	   pelo high em um json para ser trabalhado posteriomente
	   \param body resposta do high
	   \return json resultante
	*/
	Json::Value decodeJsonOfSync(std::string);
};

namespace DeniedAccessActor {
	/** \fn void post(Action& action);
		\brief rotina de ação quando acontece um acesso negado, isso é feito
	   para propagar esse acesso para o high
	   \param action obejto para lançar essa action seguindo o padrao, alem do
	   mais ai contera o acesso que foi negado
	   \sa Action();
   ))
	*/
	void post(Action &);
}

#endif
