/** \file UdpDriver.hpp
	\brief Arquivo responsavel pelas funcoes/modulos basico de criacao de um
 server udp.

	Arquivo que possui todas as funcoes basicas necessarias para a criacao de um
server udp em ambinetes linux, sendo estas funcoes de principalemente de se
lidar com conexao envio e recebimento de dados.
*/
#ifndef UDP_DRIVER_H
#define UDP_DRIVER_H

// includes
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

// namespace
//------------------------------------------------------------------------------
/**	\namespace UdpDriver
	\brief Este namespace e reponsavel por manter as funçoes gerais de
servidores Udp estas por sua vez são as funçoes que possuem a capacidade de
lidar com o SO para assim manters as capacidade basicas de um server que sao
enviar e receber dados e tambem sintetizar conexao, neste caso como Udp apenas
colocar um listener em um determinada porta.
**/
//------------------------------------------------------------------------------

namespace UdpDriver {
	/** \fn uint getConnection(uint port)
		\brief função responsavel pela criacao de um listener em uma determinda
	porta,
	permitindo assim que o sistema tenha a capacidade de receber menssagens
	nessta porta
	ou seja por meio desta funcao e possivel criar um link de das menssagens de
	uma porta
	com o processo que esta executando o servidor
	 \param port porta que dever ser associada ao processo, sendo a qual ira
	receber as menssagens udp
	 \return socket da conexao criada, que por sua vez e o resultado do link
	*/
	int getConnection(uint port);

	/**	\fn uint getMessage(char *message, uint maxsize, int connection,
	sockaddr_in &from)
		\brief função responsavel por receber uma  menssagem de um processo udp,
	esta por sua devera se usada de maneira blocante, vido que devera esta em
	operacao assim que a messagen for recebido, devido ao fato de ser o
	protocolo udp e se isso nao ocorrer a mesma sera perdida
		\param message a messagem a qual o funcao ira receber, devendo a mesma
	ser um ponterio previamente alocado
		\param maxsize tamanho maximo de caracteres que o vetor passado como
	argumento suporta, para a messagen, fazendo assim com que a messagen nao
	ultrapasse este valor
		\param connection o link com o SO, sendo o valor necessario para que
	possa se ouvir na porta daquele padrao
		\param from endereco de quem enviou a messagem necessario para a
	responder a messagem para o mesmo destinatario, o que por sua vez permitira
	a comunicao ponta a ponta
		\return o tamanho real da messagen, ou seja o numero de caracteres que
	foram lidos durante a troca de menssagens
	*/
	uint getMessage(char *message, uint maxsize, int connection,
					sockaddr_in &from);

	/**	\fn void sendMessage(char *message, uint size, int connection,
	sockaddr_in &to)
		\brief função responsavel por eviar uma menssagem udp, esta por sua vez
	envia a messagem de maneira nao blocante nao esperando assim um resposta do
	destinatario, o que por sua vez seque os principios da comunicao udp
		\param message a messagem que sera enviada ao destinatario
		\param size a quantidade de caracteres que a messagem possui
		\param connection o link com o SO, sendo o valor necessario para que
	possa se enviar uma menssagem por uma determinada porta
		\param to endereco a quem se enviara a messagem
	*/
	void sendMessage(char *message, uint size, int connection, sockaddr_in &to);
};

#endif
