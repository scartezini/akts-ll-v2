/** \file ServerDriver.hpp
	\brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
   servers.

	Arquivo que possui todas as funcoes basicas necessarias para o uso de um
server udp ou udp em ambinetes linux, sendo estas funcoes de principalemente
de se lidar com portas e sockets do SO.
*/
#ifndef SERVER_DRIVER_H
#define SERVER_DRIVER_H

// includes
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

// defines

// namespace

//------------------------------------------------------------------------------
/**
\namespace ServerDriver
\brief Este namespace e reponsavel por manter as funçoes gerais de servidor
estas por sua vez são as funçoes que possuem context geral de sitema
quando se envolve servidor me relação ao sistema ou ao contexto geral
de execução
*/
//------------------------------------------------------------------------------
namespace ServerDriver {

	/**	\fn void killProcessOnPort(uint port)
		\brief função responsavel por matar um processo em determida porta tcp o
	 que por sua vez e feito por meio do comando fuser -k "port"/tcp, que por
	 sua vez e um comando padrao do linux que mata processos ou threads
	 associadas a porta determinda este por sua vez deve ser execuado com os
	 privilegio de root, logo caso esta função seja executado o programa devera
	 ser lançado com tais privilegios
		\param port porta a que o processo que sera morto devera estar associado
	 */
	void killProcessOnPort(uint port);

	/**	\fn void killUdpProcessOnPort(uint port)
		\brief função responsavel por matar um processo em determida porta o que
	por sua vez e feito por meio do comando fuser -k "port"/udp, que por sua
	vez e um comando padrao do linux que mata processos ou threads associadas
	a porta determinda este por sua vez deve ser execuado com os privilegio
	de root, logo caso esta função seja executado o programa devera ser
	lançado com tais privilegios
		\param port porta a que o processo que sera morto devera estar associado
	 */
	void killUdpProcessOnPort(uint port);

	/**	\fn IpAddress getMyIp()
		\brief Funçao responsavel por verificar qual o ip da maquina a qual o
	quem chamou a função esta sendo execudo, porem esta tem uma necessidade
	por uma configuração da interface de rede a qual o se deseja pegar o ip
	sendo esta "wlan0", "eth0" ou outra, que por sua vez deve ser feita por
	meio do json de entrada que salvara o parametro na variavel
	server_interface
		\return ip o qual o server esta sendo executado
	*/
	IpAddress getMyIp();
};

#endif
