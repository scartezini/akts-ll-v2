/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef SENSOR_H
#define SENSOR_H

// includes

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"
#include "../models/entities_models.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

class Sensor {
  public:
	enum TYPE { NFC = 0 };

  private:
  public:
	TYPE type;

	static void startAll();
	static void add_sensor(TYPE, uint);
	static void endAll();

	virtual bool allOK() = 0;
	virtual void restart() = 0;
	virtual bool hasData() = 0;

	virtual void lock() = 0;
	virtual void unlock() = 0;

	virtual Data getData() = 0;
	virtual void waitRemove() = 0;
};

class SensorNfc : public Sensor {
  public:
	SensorNfc(uint);

	bool allOK();
	void restart();
	bool hasData();

	void lock();
	void unlock();
	void waitRemove();

	Data getData();
};

typedef vector<Sensor*> sensors_vec;

namespace io {
	extern sensors_vec sensors;
}

#endif
