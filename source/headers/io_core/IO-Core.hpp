/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef IO_SENSOR_H
#define IO_SENSOR_H

// includes

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "./Actuator.hpp"
#include "./Sensor.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

// defines

typedef vector<Actuator*> actuators_vec;

// namespace

//------------------------------------------------------------------------------
/**	\namespace io
\brief XXXXXXXXXXXXXX namespace que guarda as funcoes basicas de respostas http
relacoes de pontos de acesso com usuarios por meio de uma interface restfull a
qual faz dos metodos que sao permitidos para um acesso, o que spor sua vez se da
pelos sequinte metodos: POST, DELETE, PUT. Estes por sua vez sequem o padrao de
json espesificados em \ref relation_access_point_user_json.
**/
//------------------------------------------------------------------------------
namespace io {
	extern sensors_vec sensors;
	extern actuators_vec actuators;
}

#endif
