/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef ACTUATOR_H
#define ACTUATOR_H

// includes

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"

class Actuator {
  public:
	static void startAll();
};

#endif
