/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef BASETYPES_H
#define BASETYPES_H

#include <algorithm>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <odb/core.hxx>

#include <stdlib.h>
#include <time.h>

typedef unsigned int uint;

typedef unsigned char uint8_t;

#pragma db value
class Timestamp {
  private:
	friend class odb::access;

	uint month;
	uint day;
	uint year;
	uint hour;
	uint minutes;
	uint seconds;
	uint mseconds;

  public:
	Timestamp();
	// builders
	Timestamp(std::string);
	Timestamp(uint, uint, uint, uint, uint, uint, uint);

	void now() throw(std::invalid_argument);
	void setAll(std::string) throw(std::invalid_argument);
	void setAll(uint, uint, uint, uint, uint, uint,
				uint) throw(std::invalid_argument);

	// from database
	std::string stringFormat();
	std::string databaseFormat();

	double transformInSeconds();

	// operator
	bool operator==(const Timestamp &);
	bool operator!=(const Timestamp &);
	bool operator>(const Timestamp &);
	bool operator<(const Timestamp &);
	bool operator>=(const Timestamp &);
	bool operator<=(const Timestamp &);
};

#pragma db value
class Photo {
  private:
	friend class odb::access;

#pragma db type("BLOB")
	std::vector<char> value;
	uint size;

  public:
	Photo();
	~Photo();
	Photo(const Photo &photo);
	Photo(const Photo *photo);
	Photo(const std::vector<char>, uint);
	void setPhoto(const std::vector<char>, uint);

	std::vector<char> databaseFormat();
	uint getSize();

	// operator
	void operator=(const Photo &photo);
	bool operator==(const Photo &);
	bool operator!=(const Photo &);
};

#pragma db value
class IpAddress {
  private:
	friend class odb::access;

#pragma db type("BLOB")
	std::vector<char> ip;

  public:
	// builders
	IpAddress();
	IpAddress(std::string);

	IpAddress(char, char, char, char);

	// set
	void setIp(char, char, char, char);
	void setIp(std::string) throw(std::invalid_argument);

	// get
	std::string databaseFormat();

	// operator
	bool operator==(const IpAddress &);
	bool operator!=(const IpAddress &);
};

class Port {
  private:
	uint port;

  public:
	Port();
	Port(uint);
	Port(std::string);

	uint getPort();
	void setPort(uint) throw(std::invalid_argument);
	void setPort(std::string) throw(std::invalid_argument);

	// operator
	bool operator==(const Port &);
	bool operator!=(const Port &);
};

#pragma db value
class Blob {
  private:
	friend class odb::access;

#pragma db type("BLOB")
	std::vector<char> value;
	uint size;

  public:
	Blob();
	~Blob();

	Blob(const std::vector<char>, uint);
	void setValue(const std::vector<char>, uint);
	std::vector<char> databaseFormat();
	uint getSize();

	// operator
	bool operator==(const Blob &);
	bool operator!=(const Blob &);
	void operator=(const Blob &blob);
};

// TODO luziania
#pragma db value
class Document {
  private:
	friend class odb::access;

#pragma db type("BLOB")
	std::vector<char> value;
	uint size;

  public:
	Document();
	~Document();
	Document(const Document &document);
	Document(const Document *document);
	Document(const std::vector<char>, uint);
	void setDocument(const std::vector<char>, uint);

	std::vector<char> databaseFormat();
	uint getSize();

	// operator
	void operator=(const Document &document);
	bool operator==(const Document &);
	bool operator!=(const Document &);
};

/// inline
///////////////////////////////////////////////////////////////////////////////////////
// document
///////////////////////////////////////////////////////////////////////////////////////
inline Document::Document(const Document &document) {
	this->value = std::vector<char>();
	this->size = 0;
	setDocument(document.value, document.size);
}

inline Document::Document(const Document *document) {
	this->value = std::vector<char>();
	this->size = 0;
	setDocument(document->value, document->size);
}

inline Document::Document() {
	this->value = std::vector<char>();
	this->size = 0;
}

inline Document::Document(const std::vector<char> value, uint size) {
	this->value = std::vector<char>();
	this->size = 0;
	setDocument(value, size);
}

inline std::vector<char> Document::databaseFormat() {
	return value;
}

inline uint Document::getSize() {
	return size;
}

inline void Document::setDocument(const std::vector<char> value, uint size) {
	if (value.empty()) {
		if (this->value.empty()) {
			this->value.~vector();
		}
		this->value = value;
		this->size = size;
	} else {
		if (this->value.empty())
			this->value.~vector();
		this->value = std::vector<char>();
		this->size = 0;
	}
}

inline bool Document::operator==(const Document &document) {
	if (this->size != document.size)
		return false;
	if (this->value != document.value)
		return false;
	return true;
}

inline bool Document::operator!=(const Document &document) {
	return !(*this == document);
}

inline void Document::operator=(const Document &document) {
	setDocument(document.value, document.size);
}

inline Document::~Document() {
	if (value.empty()) {
		value.~vector();
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// photo
///////////////////////////////////////////////////////////////////////////////////////
inline Photo::Photo(const Photo &photo) {
	this->value = std::vector<char>();
	this->size = 0;
	setPhoto(photo.value, photo.size);
}

inline Photo::Photo(const Photo *photo) {
	this->value = std::vector<char>();
	this->size = 0;
	setPhoto(photo->value, photo->size);
}

inline Photo::Photo() {
	this->value = std::vector<char>();
	this->size = 0;
}

inline Photo::Photo(const std::vector<char> value, uint size) {
	this->value = std::vector<char>();
	this->size = 0;
	setPhoto(value, size);
}

inline std::vector<char> Photo::databaseFormat() {
	return value;
}

inline uint Photo::getSize() {
	return size;
}

inline void Photo::setPhoto(const std::vector<char> value, uint size) {
	if (value.empty()) {
		if (this->value.empty()) {
			this->value.~vector();
		}
		this->value = value;
		this->size = size;
	} else {
		if (this->value.empty())
			this->value.~vector();
		this->value = std::vector<char>();
		this->size = 0;
	}
}

inline bool Photo::operator==(const Photo &photo) {
	if (this->size != photo.size)
		return false;
	if (this->value != photo.value)
		return false;
	return true;
}

inline bool Photo::operator!=(const Photo &photo) {
	return !(*this == photo);
}

inline void Photo::operator=(const Photo &photo) {
	setPhoto(photo.value, photo.size);
}

inline Photo::~Photo() {
	if (value.empty()) {
		value.~vector();
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Timestamp
///////////////////////////////////////////////////////////////////////////////////////

inline Timestamp::Timestamp() {
	this->month = 0;
	this->day = 0;
	this->year = 0;
	this->hour = 0;
	this->minutes = 0;
	this->seconds = 0;
	this->mseconds = 0;
}

inline Timestamp::Timestamp(std::string isoFormat) {
	setAll(isoFormat);
}

inline void Timestamp::setAll(std::string iso_string) throw(
	std::invalid_argument) {
	size_t nofdash = std::count(iso_string.begin(), iso_string.end(), '-');
	size_t nofpoints = std::count(iso_string.begin(), iso_string.end(), ':');
	size_t nofpoint = std::count(iso_string.begin(), iso_string.end(), '.');
	size_t nofZ = std::count(iso_string.begin(), iso_string.end(), 'T');
	size_t nofT = std::count(iso_string.begin(), iso_string.end(), 'Z');

	if (nofdash != 2) {
		std::stringstream out;
		out << "Parametro de entrada Timestamp::setAll invalido, numero de "
			   "dashs invalido";
		throw std::invalid_argument(out.str().c_str());
	}

	if (nofpoints != 2) {
		std::stringstream out;
		out << "Parametro de entrada Timestamp::setAll invalido, numero de "
			   "dois pontos invalido";
		throw std::invalid_argument(out.str().c_str());
	}

	if (nofpoint != 1) {
		std::stringstream out;
		out << "Parametro de entrada Timestamp::setAll invalido, numero de "
			   "pontos invalido";
		throw std::invalid_argument(out.str().c_str());
	}

	if (nofZ != 1) {
		std::stringstream out;
		out << "Parametro de entrada Timestamp::setAll invalido, Fora do "
			   "padrão ISO";
		throw std::invalid_argument(out.str().c_str());
	}

	if (nofT != 1) {
		std::stringstream out;
		out << "Parametro de entrada Timestamp::setAll invalido, Fora do "
			   "padrão ISO";
		throw std::invalid_argument(out.str().c_str());
	}

	std::stringstream out;
	out << "Parametro de entrada Timestamp::setAll faltando valores";
	// ano
	std::size_t pos = iso_string.find("-");
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());

	std::string year = iso_string.substr(0, pos);
	// mes
	iso_string = iso_string.substr(pos + 1);
	pos = iso_string.find("-");
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	std::string month = iso_string.substr(0, pos);

	// dia
	iso_string = iso_string.substr(pos + 1);
	pos = iso_string.find("T");
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	std::string day = iso_string.substr(0, pos);

	// hora
	iso_string = iso_string.substr(pos + 1);
	pos = iso_string.find(":");
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	std::string hour = iso_string.substr(0, pos);

	// minutos
	iso_string = iso_string.substr(pos + 1);
	pos = iso_string.find(":");
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	std::string minutes = iso_string.substr(0, pos);

	// seconds
	iso_string = iso_string.substr(pos + 1);
	pos = iso_string.find(".");
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	std::string seconds = iso_string.substr(0, pos);
	// seconds

	iso_string = iso_string.substr(pos + 1);
	pos = iso_string.find("Z");
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	std::string mseconds = iso_string.substr(0, pos);

	try {
		/*
		if(year.size() != 4)
			throw std::invalid_argument("Year");
		if(month.size() != 2)
			throw std::invalid_argument("Month");
		if(day.size() != 2)
			throw std::invalid_argument("Day");

		if(hour.size() != 2)
			throw std::invalid_argument("Hour");
		if(minutes.size() != 2)
			throw std::invalid_argument("Minutes");
		if(seconds.size() != 2)
			throw std::invalid_argument("Seconds");
		if(mseconds.size() != 3)
			throw std::invalid_argument("Micro seconds");
		*/

		this->year = stoi(year, nullptr);
		this->month = stoi(month, nullptr);
		this->day = stoi(day, nullptr);
		this->hour = stoi(hour, nullptr);
		this->minutes = stoi(minutes, nullptr);
		this->seconds = stoi(seconds, nullptr);
		this->mseconds = stoi(mseconds, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Timestamp::setAll não numerico :"
			<< e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline std::string Timestamp::databaseFormat() {
}

inline std::string Timestamp::stringFormat() {
	std::stringstream timestamp;
	timestamp << std::setfill('0') << std::setw(4) << (uint)year << "-";
	timestamp << std::setfill('0') << std::setw(2) << (uint)month << "-";
	timestamp << std::setfill('0') << std::setw(2) << (uint)day << "T";
	timestamp << std::setfill('0') << std::setw(2) << (uint)hour << ":";
	timestamp << std::setfill('0') << std::setw(2) << (uint)minutes << ":";
	timestamp << std::setfill('0') << std::setw(2) << (uint)seconds << ".";
	timestamp << std::setfill('0') << std::setw(3) << (uint)mseconds << "Z";
	return timestamp.str();
}

inline Timestamp::Timestamp(uint month, uint day, uint year, uint hour,
							uint minutes, uint seconds, uint mseconds) {
	setAll(month, day, year, hour, minutes, seconds, mseconds);
}

inline void Timestamp::setAll(uint month, uint day, uint year, uint hour,
							  uint minutes, uint seconds,
							  uint mseconds) throw(std::invalid_argument) {
	this->month = month;
	this->day = day;
	this->year = year;
	this->hour = hour;
	this->minutes = minutes;
	this->seconds = seconds;
	this->mseconds = mseconds;
}

inline bool Timestamp::operator==(const Timestamp &timestamp) {
	if (this->month != timestamp.month)
		return false;
	if (this->day != timestamp.day)
		return false;
	if (this->year != timestamp.year)
		return false;
	if (this->hour != timestamp.hour)
		return false;
	if (this->minutes != timestamp.minutes)
		return false;
	if (this->seconds != timestamp.seconds)
		return false;
	if (this->mseconds != timestamp.mseconds)
		return false;
	return true;
}

inline bool Timestamp::operator!=(const Timestamp &timestamp) {
	return !(*this == timestamp);
}

inline bool Timestamp::operator>(const Timestamp &timestamp) {
	struct tm timeAtual;
	struct tm timeCompare;

	timeAtual.tm_mon = this->month - 1;
	timeAtual.tm_mday = this->day;
	timeAtual.tm_year = this->year - 1900;
	timeAtual.tm_hour = this->hour;
	timeAtual.tm_min = this->minutes;
	timeAtual.tm_sec = this->seconds;

	timeCompare.tm_mon = timestamp.month - 1;
	timeCompare.tm_mday = timestamp.day;
	timeCompare.tm_year = timestamp.year - 1900;
	timeCompare.tm_hour = timestamp.hour;
	timeCompare.tm_min = timestamp.minutes;
	timeCompare.tm_sec = timestamp.seconds;

	return (difftime(mktime(&timeAtual), mktime(&timeCompare)) > 0);
}

inline bool Timestamp::operator<(const Timestamp &timestamp) {
	struct tm timeAtual;
	struct tm timeCompare;

	timeAtual.tm_mon = this->month - 1;
	timeAtual.tm_mday = this->day;
	timeAtual.tm_year = this->year - 1900;
	timeAtual.tm_hour = this->hour;
	timeAtual.tm_min = this->minutes;
	timeAtual.tm_sec = this->seconds;

	timeCompare.tm_mon = timestamp.month - 1;
	timeCompare.tm_mday = timestamp.day;
	timeCompare.tm_year = timestamp.year - 1900;
	timeCompare.tm_hour = timestamp.hour;
	timeCompare.tm_min = timestamp.minutes;
	timeCompare.tm_sec = timestamp.seconds;

	return (difftime(mktime(&timeAtual), mktime(&timeCompare)) < 0);
}

inline bool Timestamp::operator>=(const Timestamp &timestamp) {
	return (*this > timestamp || *this == timestamp);
}

inline bool Timestamp::operator<=(const Timestamp &timestamp) {
	return (*this < timestamp || *this == timestamp);
}

inline void Timestamp::now() throw(std::invalid_argument) {
	time_t rawtime;
	struct tm *timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	this->month = timeinfo->tm_mon + 1;
	this->day = timeinfo->tm_mday;
	this->year = timeinfo->tm_year + 1900;
	this->hour = timeinfo->tm_hour;
	this->minutes = timeinfo->tm_min;
	this->seconds = timeinfo->tm_sec;
	this->mseconds = 0;
}

inline double Timestamp::transformInSeconds() {
	struct tm reference = {0};
	struct tm timeAtual = {0};

	reference.tm_hour = 0;
	reference.tm_min = 0;
	reference.tm_sec = 0;
	reference.tm_year = 100;
	reference.tm_mon = 0;
	reference.tm_mday = 1;

	timeAtual.tm_mon = this->month - 1;
	timeAtual.tm_mday = this->day;
	timeAtual.tm_year = this->year - 1900;
	timeAtual.tm_hour = this->hour;
	timeAtual.tm_min = this->minutes;
	timeAtual.tm_sec = this->seconds;

	return difftime(mktime(&timeAtual), mktime(&reference));
}

///////////////////////////////////////////////////////////////////////////////////////
// IpAddress
///////////////////////////////////////////////////////////////////////////////////////
inline IpAddress::IpAddress() {
	setIp(0, 0, 0, 0);
}
inline IpAddress::IpAddress(std::string local) {
	setIp(local);
}

inline IpAddress::IpAddress(char b1, char b2, char b3, char b4) {
	setIp(b1, b2, b3, b4);
}

// set
inline void IpAddress::setIp(char b1, char b2, char b3, char b4) {
	ip.resize(4);
	ip[0] = b1;
	ip[1] = b2;
	ip[2] = b3;
	ip[3] = b4;
}

inline void IpAddress::setIp(std::string str) throw(std::invalid_argument) {
	size_t nofdots = std::count(str.begin(), str.end(), '.');
	if (nofdots != 3) {
		std::stringstream out;
		out << "Parametro de entrada IpAddress::setIp invalido, numero de dots "
			   "invalido";
		throw std::invalid_argument(out.str().c_str());
	}

	std::string remaining;
	std::string ip0, ip1, ip2, ip3;
	std::stringstream out;
	out << "Parametro de entrada Timestamp::setAll faltando valores";

	std::size_t pos = str.find('.');
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	ip0 = str.substr(0, pos);

	remaining = str.substr(pos + 1);
	pos = remaining.find('.');
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	ip1 = remaining.substr(0, pos);

	remaining = remaining.substr(pos + 1);
	pos = remaining.find('.');
	if (pos == std::string::npos)
		throw std::invalid_argument(out.str().c_str());
	ip2 = remaining.substr(0, pos);

	remaining = remaining.substr(pos + 1);
	ip3 = remaining;

	try {
		// TODO validar se e menor que 255
		char b1 = (char)stoi(ip0, nullptr);
		char b2 = (char)stoi(ip1, nullptr);
		char b3 = (char)stoi(ip2, nullptr);
		char b4 = (char)stoi(ip3, nullptr);
		this->setIp(b1, b2, b3, b4);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada IpAddress::setIp não numerico :"
			<< e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

// get
inline std::string IpAddress::databaseFormat() {
	std::stringstream ip_stream;
	ip_stream << ((uint)((uint8_t)ip[0])) << "." << ((uint)(uint8_t)ip[1]);
	ip_stream << "." << ((uint)(uint8_t)ip[2]) << "." << ((uint)(uint8_t)ip[3]);
	return ip_stream.str();
}

inline bool IpAddress::operator==(const IpAddress &ip) {
	bool equal = true;
	for (uint i = 0; i < this->ip.size(); i++)
		equal &= this->ip[i] == ip.ip[i];
	return equal;
}

inline bool IpAddress::operator!=(const IpAddress &ip) {
	bool equal = true;
	for (uint i = 0; i < this->ip.size(); i++)
		equal &= this->ip[i] == ip.ip[i];
	return !equal;
}

///////////////////////////////////////////////////////////////////////////////////////
// Blob
///////////////////////////////////////////////////////////////////////////////////////
inline Blob::Blob() {
	this->value = std::vector<char>();
	this->size = 0;
}

inline Blob::Blob(const std::vector<char> value, uint size) {
	this->value = std::vector<char>();
	this->size = 0;
	setValue(value, size);
}

inline void Blob::setValue(const std::vector<char> value, uint size) {
	if (!value.empty()) {
		if (!this->value.empty())
			this->value.~vector();

		this->value = value;
		this->size = size;
	} else {
		if (!this->value.empty())
			this->value.~vector();

		this->value = std::vector<char>();
		this->size = 0;
	}
}

inline std::vector<char> Blob::databaseFormat() {
	return value;
}

inline uint Blob::getSize() {
	return size;
}

inline bool Blob::operator==(const Blob &blob) {
	if (this->size != blob.size)
		return false;
	if (this->value != blob.value)
		return false;
	return true;
}

inline bool Blob::operator!=(const Blob &blob) {
	return !(*this == blob);
}

inline void Blob::operator=(const Blob &blob) {
	setValue(blob.value, blob.size);
}

inline Blob::~Blob() {
	if (value.empty()) {
		value.~vector();
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Port
///////////////////////////////////////////////////////////////////////////////////////
inline Port::Port() {
	this->port = 0;
}

inline Port::Port(uint port) {
	setPort(port);
}

inline Port::Port(std::string port) {
	setPort(port);
}

inline void Port::setPort(uint port) throw(std::invalid_argument) {
	if (port > 65535) {
		std::stringstream out;
		out << "Parametro de entrada Port::setPort invalido,";
		out << "porta maior que o limite de rede, error:";
		throw std::invalid_argument(out.str().c_str());
	}
	this->port = port;
}

inline void Port::setPort(std::string value) throw(std::invalid_argument) {
	try {
		uint i_value = stoi(value, nullptr);
		setPort(i_value);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Port::setPort não numerico : ";
		out << value << " " << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline uint Port::getPort() {
	return port;
}

inline bool Port::operator==(const Port &port) {
	return port.port == this->port;
}

inline bool Port::operator!=(const Port &port) {
	return port.port != this->port;
}

inline std::string itoa(int i) {
	std::stringstream s;
	s << i;
	return s.str();
}

#endif
