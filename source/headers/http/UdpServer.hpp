/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef UDP_SERVER_H
#define UDP_SERVER_H

// includes
#include <unistd.h>
#include <condition_variable>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../drivers/ServerDriver.hpp"
#include "../drivers/UdpDriver.hpp"

#include "../BaseTypes.hpp"
#include "../Entities.hpp"
#include "HttpEntities.hpp"

// defines

// namespace
using namespace std;

class UdpServer {
	// static variables
	static const string module_name;

	// request varialbels
	Http request;
	Http response;
	char message_input[10000];
	uint size;

	// object varialbles
	thread udp_thread;
	int connection;

	// basic methods
	void run();
	void connect();
	static void runServer(UdpServer*);

	// response methos
	void createHttp(char*, uint);
	void buildResponse();
	void processHttp();
	char* transformHttp();

	// route decide
	enum REQUEST_DIR { ERROR_INVALID_FORMAT, DISCOVERY_PING };
	REQUEST_DIR operation;
	bool error;

	// route
	void pingResponse();

  public:
	UdpServer();
};

#endif
