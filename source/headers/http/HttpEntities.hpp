/** \file HttpEntities.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basicas
 *de interpretacao http.
 *
 *  Arquivo que possui as fun;oes responsaveis por converter uma menssagem
 *textoual em uma classe
 *http fazedno assim toda a interpretacao http da mesma dando um parse completo
 *da menssagem, o que
 * por sua vez torna mais simples para os outros modulo do sitema definr qual o
 *comportamento necessario
 *para o processamento da mesma
 */
#ifndef HTTP_ENTITIES
#define HTTP_ENTITIES

// includes
#include <condition_variable>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

// files
#include "../../libs/json_parser/json.h"
#include "../../libs/output/IOMananger.hpp"

#include "../BaseTypes.hpp"

// defines and typedefs
#define CRNL "\r\n"

// namespace
using namespace std;

/**
 * \class HttpHeader
 * \brief Classe que representa o header de um requisicao http
 */
class HttpHeader {
  public:
	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	enum METHOD { GET, PUT, POST, DELETE };

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	enum ORIGIN { HIGH, LOW, OTHER };

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	enum HTTP_RESPONSE {
		CREATED,
		SUCCESS,
		ACCEPTED,
		UNAUTHORIZED,
		METHOD_NOT_ALLOWED,
		PAGE_NOT_FOUND,
		INTERNAL_ERROR,
		INVALID_REQ,
		PROTOCOL_ERROR
	};
	HttpHeader::HTTP_RESPONSE decodeResponse(int response);

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	enum FILE_TYPE { JSON, HTML };

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::vector<std::string> paths;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::map<std::string, std::string> args;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	HTTP_RESPONSE response_status;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::string status_msg;

  private:
	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	static const std::string module_name;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::string path;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	METHOD method;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::string protocol;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	ORIGIN origin;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::string connection;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::string result;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::string host;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::string user_agent;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	std::string contentType;

	/**
	 * \fn HttpServer::response ping(vector<string>& arguments,
	 HttpServer::request const& requsicao)
	 * \brief funcao que responde a requisi;ao de vertifica;ao se o sistem esta
	 online
	 * \param arguments sao os argumentos que sao responsaveis por fazer o
	 processameto, sendo estes paramentros de query ou
	 *paramentros que represetnao o id daquilo que sra buscado
	 \param request paramtro que define a requisicao que foi recebida pelo
	 serve, o que por sua vez permite uma revuperacao do body
	 * e outros parametros presetnes na mesma
	 \return a resposata http correspondente a aquele requisicao
	 */
	uint contentLength;

	std::string Method2String(METHOD method);
	METHOD stringToMethod(std::string str);
	ORIGIN stringToOrigin(std::string str);

	std::string headerToJson(std::string header);

	void parseFirst(std::string);
	void parseFirstResponse(std::string);
	void parseRoute();

  public:
	HttpHeader();
	HttpHeader(char*, uint);

	void fromText(std::string);
	void fromText(char*, uint);
	void fromTextResponse(std::string);

	std::string toText();
	std::string responseText();

	// metodos basicos
	std::string getPath() const;
	METHOD getMethod() const;
	std::string getMethod_s() const;
	std::string getProtocol() const;

	std::string getContentType() const;
	uint getContentLength() const;

	std::string getConnection() const;
	ORIGIN getOrigin() const;
	std::string getResult() const;
	std::string getHost() const;
	std::string getContent() const;

	void setPath(std::string path);
	void setMethod(METHOD method);
	void setMethod(std::string method);
	void setProtocol(std::string protocol);
	void setOrigin(ORIGIN origin);

	void setContentType(std::string contentType);
	void setContentType(FILE_TYPE);

	void setContentLength(uint);
	void setContentLength(std::string);

	void setConnection(std::string connection);
	void setHost(std::string host);
	void setUserAgent(std::string user_agent);
	void setResult(std::string result);

	void setResult(int);

	static uint findEnd(char*, uint);
};

/*! \class UserModel
 *  \brief Esse e a classe Usuario que reprenta o user_model no contexto deste
 * sistema.
 */
class HttpContent {
  private:
	static const std::string module_name;

	uint size;
	char* content;

  public:
	HttpContent();
	HttpContent(char*, uint size);

	void set(char*, uint size);
	void set(std::string text);
	void freeCont();

	char* getContent();
	void copy(char*);
	uint getSize();
};

/*! \class UserModel
 *  \brief Esse e a classe Usuario que reprenta o user_model no contexto deste
 * sistema.
 */
class Http {
  private:
	static const std::string module_name;

	void setBaseError(uint code, uint size, std::string text);

	// OTHER
	uint size;
	bool response;

  public:
	HttpHeader httpHeader;
	HttpContent httpContent;

	Http();

	void makeFromText(char* valor, uint size);
	char* toText();

	uint getTextSize();
	void setAsResponse();

	// predef
	void error(uint);
};

//------------------------------------------------------------------------------
/**	\namespace HttpError
\brief XXXXXXXXXXXXXX namespace que guarda as funcoes basicas de respostas http
relacoes de pontos de acesso com usuarios por meio de uma interface restfull a
qual faz dos metodos que sao permitidos para um acesso, o que spor sua vez se da
pelos sequinte metodos: POST, DELETE, PUT. Estes por sua vez sequem o padrao de
json espesificados em \ref relation_access_point_user_json.
**/
//------------------------------------------------------------------------------
namespace HttpError {
	std::string textBadRequest();
	std::string textUnauthorized();
	std::string textNotFound();
	std::string textMethodNotAllowed();

	// INTERNAL ERRORS
	std::string textInternalError();
	std::string textNotImplemented();
	std::string textServiceUnavailable();
	std::string textHTTPVersionNotSupported();
}

//------------------------------------------------------------------------------
/**	\namespace HttpTemplateTexts
\brief XXXXXXXXXXXXXX namespace que guarda as funcoes basicas de respostas http
relacoes de pontos de acesso com usuarios por meio de uma interface restfull a
qual faz dos metodos que sao permitidos para um acesso, o que spor sua vez se da
pelos sequinte metodos: POST, DELETE, PUT. Estes por sua vez sequem o padrao de
json espesificados em \ref relation_access_point_user_json.
**/
//------------------------------------------------------------------------------
namespace HttpTemplateTexts {}

#endif
