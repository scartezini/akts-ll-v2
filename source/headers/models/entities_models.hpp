/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef ENTITIES_MODELS_H
#define ENTITIES_MODELS_H

#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <odb/core.hxx>
#include <odb/lazy-ptr.hxx>
#include <odb/tr1/memory.hxx>

#include "../../libs/json_parser/json.h"
#include "../BaseTypes.hpp"
#include "../Entities.hpp"


typedef unsigned int uint;

using std::tr1::shared_ptr;
using std::tr1::weak_ptr;

using namespace odb::core;

class AccessPoint;
class Stuff;
class Relation_Data_AccessPoint;
class Relation_User_AccessPoint;
class Access;
class Data;
class DataType;

typedef std::vector<odb::lazy_weak_ptr<Data>> vector_datas_type;
typedef std::vector<odb::lazy_weak_ptr<Stuff>> vector_stuffs_type;
typedef std::vector<odb::lazy_weak_ptr<Access>> vector_accesses_type;
typedef std::vector<odb::lazy_weak_ptr<Relation_User_AccessPoint>>
	vector_relations_u_ap_type;
typedef std::vector<odb::lazy_weak_ptr<Relation_Data_AccessPoint>>
	vector_relations_d_ap_type;

/**
\page user_json Json de Usuario
{
	id:"id"
}
**/
#pragma db object
class User : public Entity, public ShortEntity {
  private:
	friend class odb::access;

// database
#pragma db id auto
	uint id;

// data
#pragma db type("VARCHAR(256)")
	std::string name;

#pragma db type("VARCHAR(32)")
	std::string cpf;

#pragma db type("VARCHAR(32)")
	std::string rg;

#pragma db type("VARCHAR(128)")
	std::string destiny;

#pragma db type("VARCHAR(256)")
	std::string email;

#pragma db type("VARCHAR(256)")
	std::string password;

#pragma db type("VARCHAR(32)")
	std::string phone;


// realtions

#pragma db inverse(user)
	vector_stuffs_type stuffs;

#pragma db inverse(user)
	vector_datas_type datas;

#pragma db inverse(user)
	vector_relations_u_ap_type relations_user_accessPoint;

#pragma db inverse(user)
	vector_accesses_type accesses;


  public:
	bool activated = true;

	User(){};
	User(uint _id) : id(_id) {
	}
	User(uint _id, std::string _rg, std::string _cpf, std::string _name,
		 std::string _email, std::string _phone, std::string _destiny,
		 std::string _password)
	  : id(_id)
	  , rg(_rg)
	  , cpf(_cpf)
	  , name(_name)
	  , email(_email)
	  , phone(_phone)
	  , destiny(_destiny)
	  , password(_password) {
	}

	// set
	void setId(std::string) throw(std::invalid_argument);
	void setId(uint) throw(std::invalid_argument);

	void setRg(std::string) throw(std::invalid_argument);
	void setCpf(std::string) throw(std::invalid_argument);
	void setName(std::string) throw(std::invalid_argument);
	void setEmail(std::string) throw(std::invalid_argument);
	void setPhone(std::string) throw(std::invalid_argument);
	void setDestiny(std::string) throw(std::invalid_argument);
	void setPassword(std::string) throw(std::invalid_argument);

	// get
	uint getId() const;
	std::string getRg() const;
	std::string getCpf() const;
	std::string getName() const;
	std::string getEmail() const;
	std::string getPhone() const;
	std::string getDestiny() const;
	std::string getPassword() const;

	vector_datas_type& getDatas();
	vector_stuffs_type& getStuffs();
	vector_accesses_type& getAccesses();
	vector_relations_u_ap_type& getRelations_user_accessPoint();

	// basics
	std::string toJson();
	std::string toShortJson();
	void fromJson(std::string);

	// operator
	bool operator==(const User&);
	bool operator!=(const User&);
};

/**
\page access_point_json Json de Ponto de Accesso
{
	id:"id"
}
**/
#pragma db object
class AccessPoint : public Entity {
  public:
	enum ACCESS_POINT_TYPE {
		DOOR_FRONT = 0,
		DOOR_BACK = 1,
		DOOR_FRONT_BACK = 2,
		TICKET_GATE = 3
	};

	std::string highTypeConverter(ACCESS_POINT_TYPE);
	ACCESS_POINT_TYPE lowTypeConverter(std::string);

  private:
	friend class odb::access;

// database
#pragma db id auto
	uint id;

#pragma db type("VARCHAR(256)")
	std::string name;

#pragma db type("INT")
	ACCESS_POINT_TYPE type;

	IpAddress ipAddress;

  public:
#pragma db type("INT")
	uint port;

  private:
// ralations
#pragma db inverse(accessPoint)
	vector_relations_u_ap_type relations_user_accessPoint;

#pragma db inverse(accessPoint)
	vector_accesses_type accesses;

#pragma db inverse(accessPoint)
	vector_relations_d_ap_type relations_data_accessPoint;

  public:
	bool activated = true;

	AccessPoint();
	AccessPoint(uint _id) : id(_id) {
		port = 10000;
	}

	AccessPoint(uint _id, std::string _name, IpAddress _ipAddress,
				ACCESS_POINT_TYPE _type)
	  : id(_id), name(_name), ipAddress(_ipAddress), type(_type) {
	}

	// set
	void setId(uint) throw(std::invalid_argument);
	void setId(std::string) throw(std::invalid_argument);
	void setName(std::string) throw(std::invalid_argument);
	void setType(uint) throw(std::invalid_argument);
	void setType(std::string) throw(std::invalid_argument);
	void setType(ACCESS_POINT_TYPE) throw(std::invalid_argument);
	void setIpAddress(IpAddress) throw(std::invalid_argument);

	// get
	uint getId() const;
	std::string getName() const;
	ACCESS_POINT_TYPE getType() const;
	IpAddress* getIpAddress();

	vector_accesses_type& getAccesses();
	vector_relations_u_ap_type& getRelations_user_accessPoint();
	vector_relations_d_ap_type& getRelations_data_accessPoint();

	// basics
	std::string toJson();
	std::string toShortJson();
	void fromJson(std::string);

	// operator
	bool operator==(const AccessPoint&);
	bool operator!=(const AccessPoint&);
};

/**
\page datatype_json Json de DataType
{
	id:"id"
}
**/
#pragma db object
class DataType : public Entity {
  public:
	enum SENSOR_TYPE { NFC = 0, RFID = 1, BIOMETRIC = 2 };

  private:
	friend class odb::access;

// database
#pragma db id auto
	uint id;

#pragma db type("VARCHAR(128)")
	std::string name;

#pragma db type("INT")
	SENSOR_TYPE sensor;

#pragma db type("VARCHAR(256)")
	std::string model;

  private:
// ralations
#pragma db inverse(dataType)
	vector_datas_type datas;

	DataType::SENSOR_TYPE lowSensorTypeConverter(std::string sensor_type);

  public:
	bool activated = true;

	DataType() {
	}
	DataType(uint _id) : id(_id) {
	}

	DataType(uint _id, std::string _name, std::string _model,
			 SENSOR_TYPE _sensor)
	  : id(_id), name(_name), sensor(_sensor), model(_model) {
	}

	// set
	void setId(uint) throw(std::invalid_argument);
	void setId(std::string) throw(std::invalid_argument);
	void setName(std::string) throw(std::invalid_argument);
	void setModel(std::string) throw(std::invalid_argument);
	void setSensor(uint) throw(std::invalid_argument);
	void setSensor(std::string) throw(std::invalid_argument);
	void setSensor(SENSOR_TYPE) throw(std::invalid_argument);

	// get
	uint getId() const;
	std::string getName() const;
	SENSOR_TYPE getSensor() const;
	std::string getModel() const;

	vector_datas_type& getDatas();

	// basics
	std::string toJson();
	void fromJson(std::string);

	// operator
	bool operator==(const DataType&);
	bool operator!=(const DataType&);
};

/**
\page data_json Json de Data
{
	id:"id"
}
**/
#pragma db object
class Data : public Entity, public ShortEntity {
  private:
	friend class odb::access;
// database
#pragma db id auto
	uint id;
	bool visitor;

#pragma db type("BLOB")
	Blob value;

// ralations
#pragma db inverse(data)
	vector_relations_d_ap_type relations_data_accessPoint;

  private:
#pragma db not_null
	std::tr1::shared_ptr<User> user;
#pragma db not_null
	std::tr1::shared_ptr<DataType> dataType;

  public:
	bool activated = true;

	Data() {
	}
	Data(uint _id) : id(_id) {
	}

	Data(uint _id, Blob _value, std::tr1::shared_ptr<User> _user,
		 std::tr1::shared_ptr<DataType> _dataType)
	  : id(_id), value(_value), user(_user), dataType(_dataType) {
	}

	// set
	void setId(uint) throw(std::invalid_argument);
	void setId(std::string) throw(std::invalid_argument);

	void setValue(Blob&) throw(std::invalid_argument);
	void setValue(const std::vector<char>, int) throw(std::invalid_argument);

	void setUser(User&) throw(std::invalid_argument);
	void setDataType(DataType&) throw(std::invalid_argument);

	// get
	uint getId() const;
	Blob* getValue();

	User& getUser();
	DataType& getDataType();

	vector_relations_d_ap_type& getRelations_data_accessPoint();

	// basics
	std::string toJson();
	std::string toShortJson();
	void fromJson(std::string);

	// operators
	bool operator==(const Data&);
	bool operator!=(const Data&);
};

/**
\page stuff_json Json de Stuff
{
	id:"id"
}
**/
#pragma db object
class Stuff : public Entity {
  private:
	friend class odb::access;

// database
#pragma db id auto
	uint id;

#pragma db type("VARCHAR(64)")
	std::string name;

#pragma db type("VARCHAR(1024)")
	std::string description;

#pragma db type("VARCHAR(128)")
	std::string serial;

  private:
	// ralations
	std::tr1::shared_ptr<User> user;

#pragma db inverse(stuff)
	vector_accesses_type accesses;

  public:
	bool activated = true;

	Stuff() {
	}
	Stuff(uint _id) : id(_id) {
	}
	Stuff(uint _id, std::string _name, std::string _serial,
		  std::string _description, std::tr1::shared_ptr<User> _user)
	  : id(_id)
	  , name(_name)
	  , serial(_serial)
	  , description(_description)
	  , user(_user) {
	}

	// set
	void setId(uint) throw(std::invalid_argument);
	void setId(std::string) throw(std::invalid_argument);
	void setName(std::string) throw(std::invalid_argument);
	void setDescription(std::string) throw(std::invalid_argument);
	void setSerial(std::string) throw(std::invalid_argument);

	void setUser(User&) throw(std::invalid_argument);

	// get
	uint getId() const;
	std::string getName() const;
	std::string getDescription() const;
	std::string getSerial() const;

	User& getUser();

	vector_accesses_type& getAccesses();

	std::string toJson();
	void fromJson(std::string);

	// operator
	bool operator==(const Stuff&);
	bool operator!=(const Stuff&);
};

/**
\page access_json Json de Acesso
{
	id:"id"
}
**/
#pragma db object
class Access : public Entity {
  public:
	enum DIRECTION { INPUT = 0, OUTPUT = 1, TIMEOUT = 2, DENIED = 3, NONE = 4 };

  private:
	friend class odb::access;

// dados
// database
#pragma db id auto
	uint id;

	Timestamp hora;
	DIRECTION direction;
	bool denied = false;

  private:
	// ralations
	std::tr1::shared_ptr<User> user;
	std::tr1::shared_ptr<Data> data;
	std::tr1::shared_ptr<Stuff> stuff;
	std::tr1::shared_ptr<AccessPoint> accessPoint;

  public:
	bool activated = true;

	Access() {
	}
	Access(uint _id) : id(_id) {
	}

	Access(uint _id, Timestamp _hora, DIRECTION _direction,
		   std::tr1::shared_ptr<User> _user, std::tr1::shared_ptr<Data> _data,
		   std::tr1::shared_ptr<Stuff> _stuff,
		   std::tr1::shared_ptr<AccessPoint> _accessPoint)
	  : id(_id)
	  , hora(_hora)
	  , direction(_direction)
	  , user(_user)
	  , data(_data)
	  , stuff(_stuff)
	  , accessPoint(_accessPoint) {
	}

	// set
	void setId(uint) throw(std::invalid_argument);
	void setId(std::string) throw(std::invalid_argument);

	void setType(DIRECTION) throw(std::invalid_argument);
	void setType(uint) throw(std::invalid_argument);
	void setType(std::string) throw(std::invalid_argument);

	void setHora(Timestamp&) throw(std::invalid_argument);
	void setHora(std::string) throw(std::invalid_argument);

	void setData(Data&) throw(std::invalid_argument);
	void setUser(User&) throw(std::invalid_argument);
	void setStuff(Stuff&) throw(std::invalid_argument);
	void setAccessPoint(AccessPoint&) throw(std::invalid_argument);

	// get
	uint getId() const;
	DIRECTION getType() const;
	uint getTypeDbFormat() const;
	Timestamp* getHora();

	User& getUser();
	Data& getData();
	Stuff& getStuff();
	AccessPoint& getAccessPoint();

	// basicas
	std::string toJson();
	void fromJson(std::string);

	// operator
	bool operator==(const Access&);
	bool operator!=(const Access&);
};

/**
\page relation_access_point_data_json Json de Relation_Data_AccessPoint
{
	id:"id"
}
**/
#pragma db object
class Relation_Data_AccessPoint : public Entity {
  private:
	friend class odb::access;

#pragma db id auto
	uint id;
	uint translated_id;

  private:
	std::tr1::shared_ptr<Data> data;
	std::tr1::shared_ptr<AccessPoint> accessPoint;

  public:
	bool activated = true;

	Relation_Data_AccessPoint() {
	}
	Relation_Data_AccessPoint(uint _id) : id(_id) {
	}

	Relation_Data_AccessPoint(uint _id, uint _translated_id,
							  std::tr1::shared_ptr<Data> _data,
							  std::tr1::shared_ptr<AccessPoint> _accessPoint)
	  : id(_id)
	  , translated_id(_translated_id)
	  , data(_data)
	  , accessPoint(_accessPoint) {
	}

	// set
	void setId(uint) throw(std::invalid_argument);
	void setId(std::string) throw(std::invalid_argument);
	void setTranslatedId(uint) throw(std::invalid_argument);
	void setTranslatedId(std::string) throw(std::invalid_argument);

	void setData(Data&) throw(std::invalid_argument);
	void setAccessPoint(AccessPoint&) throw(std::invalid_argument);

	// get
	uint getId();
	uint getTranslated_id() const;

	Data& getData();
	AccessPoint& getAccessPoint();

	// basic
	std::string toJson();
	void fromJson(std::string);

	// operators
	bool operator==(const Relation_Data_AccessPoint&);
	bool operator!=(const Relation_Data_AccessPoint&);
};

/**
\page relation_access_point_user_json Json de Relacao de User com Ponto de
Acesso
{
	id:"id"
}
**/
#pragma db object
class Relation_User_AccessPoint : public Entity {
  private:
	friend class odb::access;

#pragma db id auto
	uint id;
	uint period;

	Timestamp start;

#pragma db type("BLOB")
	std::vector<char> sunday;
#pragma db type("BLOB")
	std::vector<char> monday;
#pragma db type("BLOB")
	std::vector<char> tuesday;
#pragma db type("BLOB")
	std::vector<char> wednesday;
#pragma db type("BLOB")
	std::vector<char> thursday;
#pragma db type("BLOB")
	std::vector<char> friday;
#pragma db type("BLOB")
	std::vector<char> saturday;

  private:
// relation
#pragma db not_null
	std::tr1::shared_ptr<User> user;

#pragma db not_null
	std::tr1::shared_ptr<AccessPoint> accessPoint;

  public:
	bool activated = true;
	Relation_User_AccessPoint() {
	}

	Relation_User_AccessPoint(uint _id, uint _period, Timestamp _start,
							  std::tr1::shared_ptr<User> _user,
							  std::tr1::shared_ptr<AccessPoint> _accessPoint)
	  : id(_id)
	  , period(_period)
	  , start(_start)
	  , user(_user)
	  , accessPoint(_accessPoint) {
	}

	Relation_User_AccessPoint(uint _id) : id(_id) {
	}

	// set
	void setId(uint) throw(std::invalid_argument);
	void setId(std::string) throw(std::invalid_argument);
	void setPeriod(uint) throw(std::invalid_argument);
	void setPeriod(std::string) throw(std::invalid_argument);

	void setStart(Timestamp&) throw(std::invalid_argument);
	void setStart(std::string) throw(std::invalid_argument);

	void setUser(User&) throw(std::invalid_argument);
	void setAccessPoint(AccessPoint&) throw(std::invalid_argument);

	void setSundey(const std::vector<char>&);
	void setMonday(const std::vector<char>&);
	void setTuesday(const std::vector<char>&);
	void setWednesday(const std::vector<char>&);
	void setThursday(const std::vector<char>&);
	void setFriday(const std::vector<char>&);
	void setSaturday(const std::vector<char>&);

	// get
	uint getId();
	uint getPeriod();
	Timestamp* getStart();

	User& getUser();
	AccessPoint& getAccessPoint();

	std::string toJson();
	void fromJson(std::string);

	bool operator==(const Relation_User_AccessPoint&);
	bool operator!=(const Relation_User_AccessPoint&);
};


/**
\page register_kit_json Json de kit de cadastro
{
	id:"id"
}
**/
#pragma db object
class RegisterKit : public Entity {
  private:
	friend class odb::access;
// database
#pragma db id auto
	uint id;

#pragma db type("VARCHAR(256)")
	std::string name;

	IpAddress ipAddress;

  public:
#pragma db type("INT")
	uint port;

	bool activated = true;

	RegisterKit();
	RegisterKit(uint _id) : id(_id) {
		port = 10000;
	}

	RegisterKit(uint _id, std::string _name, IpAddress _ipAddress)
	  : id(_id), name(_name), ipAddress(_ipAddress) {
	}

	// set
	void setId(uint) throw(std::invalid_argument);
	void setId(std::string) throw(std::invalid_argument);
	void setName(std::string) throw(std::invalid_argument);
	void setIpAddress(IpAddress) throw(std::invalid_argument);

	// get
	uint getId() const;
	std::string getName() const;
	IpAddress* getIpAddress();

	// basics
	std::string toJson();
	std::string toShortJson();
	void fromJson(std::string);

	// operator
	bool operator==(const RegisterKit&);
	bool operator!=(const RegisterKit&);
};


#include "./inline_entities_models.hpp"
#endif
