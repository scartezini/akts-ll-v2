/////////////////////////////////////////////////////////////
////User
////////////////////////////////////////////////////////////

/////////SET
inline void User::setId(std::string id) throw(std::invalid_argument) {
	std::string valor = id;
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void User::setId(uint id) throw(std::invalid_argument) {
	this->id = id;
}

inline void User::setRg(std::string rg) throw(std::invalid_argument) {
	this->rg = rg;
}

inline void User::setCpf(std::string cpf) throw(std::invalid_argument) {
	this->cpf = cpf;
}

inline void User::setName(std::string name) throw(std::invalid_argument) {
	this->name = name;
}

inline void User::setEmail(std::string email) throw(std::invalid_argument) {
	this->email = email;
}

inline void User::setPhone(std::string phone) throw(std::invalid_argument) {
	this->phone = phone;
}

inline void User::setDestiny(std::string destiny) throw(std::invalid_argument) {
	this->destiny = destiny;
}

inline void User::setPassword(std::string password) throw(
	std::invalid_argument) {
	this->password = password;
}

/// get

inline uint User::getId() const {
	return id;
}

inline std::string User::getRg() const {
	return rg;
}

inline std::string User::getCpf() const {
	return cpf;
}

inline std::string User::getName() const {
	return name;
}

inline std::string User::getEmail() const {
	return email;
}

inline std::string User::getPhone() const {
	return phone;
}

inline std::string User::getDestiny() const {
	return destiny;
}

inline std::string User::getPassword() const {
	return password;
}


///////////////////////////////////////////////////////////////
//// AccessPoint
////////////////////////////////////////////////////////////////

/////set
inline void AccessPoint::setId(std::string id) throw(std::invalid_argument) {
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro " << id
			<< " de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void AccessPoint::setId(uint id) throw(std::invalid_argument) {
	this->id = id;
}

inline void AccessPoint::setName(std::string name) throw(std::invalid_argument) {
	this->name = name;
}

inline void AccessPoint::setType(uint type) throw(std::invalid_argument) {
	this->type = static_cast<AccessPoint::ACCESS_POINT_TYPE>(type);
}

inline void AccessPoint::setType(std::string type) throw(std::invalid_argument) {
	int value = 0;
	try {
		value = stoi(type, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setType não numerico :"
			<< e.what();
		throw std::invalid_argument(out.str().c_str());
	}
	setType(value);
}

inline void AccessPoint::setType(ACCESS_POINT_TYPE type) throw(
	std::invalid_argument) {
	this->type = type;
}

inline void AccessPoint::setIpAddress(IpAddress ip) throw(
	std::invalid_argument) {
	this->ipAddress = ip;
}

// get
inline uint AccessPoint::getId() const {
	return id;
}

inline std::string AccessPoint::getName() const {
	return name;
}

inline AccessPoint::ACCESS_POINT_TYPE AccessPoint::getType() const {
	return type;
}

inline IpAddress* AccessPoint::getIpAddress() {
	return &ipAddress;
}

// relation
inline vector_accesses_type& AccessPoint::getAccesses() {
	return accesses;
}

inline vector_relations_u_ap_type& AccessPoint::getRelations_user_accessPoint() {
	return relations_user_accessPoint;
}

inline vector_relations_d_ap_type& AccessPoint::getRelations_data_accessPoint() {
	return relations_data_accessPoint;
}

/////////////////////////////////////////////////
// DataType
/////////////////////////////////////////////////
// set
inline void DataType::setId(std::string id) throw(std::invalid_argument) {
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro " << id
			<< " de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void DataType::setId(uint id) throw(std::invalid_argument) {
	this->id = id;
}

inline void DataType::setName(std::string name) throw(std::invalid_argument) {
	this->name = name;
}

inline void DataType::setModel(std::string model) throw(std::invalid_argument) {
	this->model = model;
}

inline void DataType::setSensor(uint sensor) throw(std::invalid_argument) {
	this->sensor = static_cast<DataType::SENSOR_TYPE>(sensor);
}

inline void DataType::setSensor(std::string sensor) throw(
	std::invalid_argument) {
	int value = 0;
	try {
		value = stoi(sensor, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setType não numerico :"
			<< e.what();
		throw std::invalid_argument(out.str().c_str());
	}
	setSensor(value);
}

inline void DataType::setSensor(SENSOR_TYPE sensor) throw(
	std::invalid_argument) {
	this->sensor = sensor;
}

// get
inline uint DataType::getId() const {
	return id;
}

inline std::string DataType::getName() const {
	return name;
}

inline DataType::SENSOR_TYPE DataType::getSensor() const {
	return sensor;
}

inline std::string DataType::getModel() const {
	return model;
}

// relation
inline vector_datas_type& DataType::getDatas() {
	return datas;
}

/////////////////////////////////////////////////////////////////
// Data
/////////////////////////////////////////////////////////////////
// set
inline void Data::setId(uint id) throw(std::invalid_argument) {
	this->id = id;
}

inline void Data::setId(std::string id) throw(std::invalid_argument) {
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void Data::setValue(Blob& value) throw(std::invalid_argument) {
	this->value = value;
}

inline void Data::setValue(const std::vector<char> value,
						   int size) throw(std::invalid_argument) {
	this->value.setValue(value, size);
}

inline void Data::setUser(User& user) throw(std::invalid_argument) {
	std::tr1::shared_ptr<User> user_pointer(new User(user));
	this->user = user_pointer;
}

inline void Data::setDataType(DataType& dataType) throw(std::invalid_argument) {
	std::tr1::shared_ptr<DataType> dataType_pointer(new DataType(dataType));
	this->dataType = dataType_pointer;
}

// get
inline uint Data::getId() const {
	return id;
}

inline Blob* Data::getValue() {
	return &value;
}

inline User& Data::getUser() {
	return *user;
}

inline DataType& Data::getDataType() {
	return *dataType;
}

// relation
inline vector_relations_d_ap_type& Data::getRelations_data_accessPoint() {
	return relations_data_accessPoint;
}

//////////////////////////////////////////////////////////////////
// Stuff
/////////////////////////////////////////////////////////////////

// set
inline void Stuff::setId(uint id) throw(std::invalid_argument) {
	this->id = id;
}

inline void Stuff::setId(std::string id) throw(std::invalid_argument) {
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro " << id
			<< " de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void Stuff::setName(std::string name) throw(std::invalid_argument) {
	this->name = name;
}

inline void Stuff::setDescription(std::string description) throw(
	std::invalid_argument) {
	this->description = description;
}

inline void Stuff::setUser(User& user) throw(std::invalid_argument) {
	std::tr1::shared_ptr<User> user_pointer(new User(user));
	this->user = user_pointer;
}

inline void Stuff::setSerial(std::string serial) throw(std::invalid_argument) {
	this->serial = serial;
}
// get
inline uint Stuff::getId() const {
	return id;
}

inline std::string Stuff::getName() const {
	return name;
}

inline std::string Stuff::getDescription() const {
	return description;
}

inline User& Stuff::getUser() {
	return *user;
}

inline vector_accesses_type& Stuff::getAccesses() {
	return accesses;
}

inline std::string Stuff::getSerial() const {
	return this->serial;
}

//////////////////////////////////////////////////////////////////////
// Access
/////////////////////////////////////////////////////////////////////

// set

inline void Access::setId(std::string id) throw(std::invalid_argument) {
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void Access::setId(uint id) throw(std::invalid_argument) {
	this->id = id;
}

inline void Access::setType(DIRECTION direction) throw(std::invalid_argument) {
	this->direction = direction;
}

inline void Access::setType(uint value) throw(std::invalid_argument) {
	switch (value) {
		case 0:
			direction = DIRECTION::INPUT;
			break;
		case 1:
			direction = DIRECTION::OUTPUT;
			break;
		case 2:
			direction = DIRECTION::TIMEOUT;
			break;
		case 3:
			direction = DIRECTION::DENIED;
			break;
		case 4:
			direction = DIRECTION::NONE;
			break;

		default:
			std::stringstream out;
			out << "Parametro de entrada Access::setType fora do limite "
				   "numerico";
			throw std::invalid_argument(out.str().c_str());
	}
}

inline void Access::setType(std::string value_s) throw(std::invalid_argument) {
	int value = 0;
	try {
		value = stoi(value_s, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setType não numerico :"
			<< e.what();
		throw std::invalid_argument(out.str().c_str());
	}
	setType(value);
}

inline void Access::setHora(Timestamp& hora) throw(std::invalid_argument) {
	this->hora = hora;
}

inline void Access::setHora(std::string data) throw(std::invalid_argument) {
	try {
		this->hora.setAll(data);
	} catch (std::invalid_argument e) {
		throw e;
	}
}

inline void Access::setData(Data& data) throw(std::invalid_argument) {
	std::tr1::shared_ptr<Data> data_pointer(new Data(data));
	this->data = data_pointer;
}

inline void Access::setUser(User& user) throw(std::invalid_argument) {
	std::tr1::shared_ptr<User> user_pointer(new User(user));
	this->user = user_pointer;
}

inline void Access::setStuff(Stuff& stuff) throw(std::invalid_argument) {
	std::tr1::shared_ptr<Stuff> stuff_pointer(new Stuff(stuff));
	this->stuff = stuff_pointer;
}

inline void Access::setAccessPoint(AccessPoint& accessPoint) throw(
	std::invalid_argument) {
	std::tr1::shared_ptr<AccessPoint> accessPoint_pointer(
		new AccessPoint(accessPoint));
	this->accessPoint = accessPoint_pointer;
}

// get
inline uint Access::getId() const {
	return id;
}

inline Access::DIRECTION Access::getType() const {
	return direction;
}

inline uint Access::getTypeDbFormat() const {
	if (direction == DIRECTION::INPUT)
		return 0;

	if (direction == DIRECTION::OUTPUT)
		return 1;

	if (direction == DIRECTION::TIMEOUT)
		return 2;

	if (direction == DIRECTION::DENIED)
		return 3;

	if (direction == DIRECTION::NONE)
		return 4;
}

inline Timestamp* Access::getHora() {
	return &hora;
}

inline User& Access::getUser() {
	return *user;
}

inline Data& Access::getData() {
	return *data;
}

inline Stuff& Access::getStuff() {
	return *stuff;
}

inline AccessPoint& Access::getAccessPoint() {
	return *accessPoint;
}

///////////////////////////////////////////////////////////////////
// Relation_Data_AccessPoint
//////////////////////////////////////////////////////////////////

// set
inline void Relation_Data_AccessPoint::setId(uint id) throw(
	std::invalid_argument) {
	this->id = id;
}

inline void Relation_Data_AccessPoint::setId(std::string id) throw(
	std::invalid_argument) {
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void Relation_Data_AccessPoint::setTranslatedId(
	uint translated_id) throw(std::invalid_argument) {
	this->translated_id = translated_id;
}

inline void Relation_Data_AccessPoint::setTranslatedId(
	std::string translated_id) throw(std::invalid_argument) {
	try {
		this->translated_id = stoi(translated_id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void Relation_Data_AccessPoint::setData(Data& data) throw(
	std::invalid_argument) {
	std::tr1::shared_ptr<Data> data_pointer(new Data(data));
	this->data = data_pointer;
}

inline void Relation_Data_AccessPoint::setAccessPoint(
	AccessPoint& accessPoint) throw(std::invalid_argument) {
	std::tr1::shared_ptr<AccessPoint> accessPoint_pointer(
		new AccessPoint(accessPoint));
	this->accessPoint = accessPoint_pointer;
}

// get
inline uint Relation_Data_AccessPoint::getId() {
	return id;
}

inline uint Relation_Data_AccessPoint::getTranslated_id() const {
	return translated_id;
}

inline Data& Relation_Data_AccessPoint::getData() {
	return *data;
}

inline AccessPoint& Relation_Data_AccessPoint::getAccessPoint() {
	return *accessPoint;
}
//////////////////////////////////////////////////////////////
// Relation_User_AccessPoint::
/////////////////////////////////////////////////////////////
// set
inline void Relation_User_AccessPoint::setId(uint id) throw(
	std::invalid_argument) {
	this->id = id;
}
inline void Relation_User_AccessPoint::setId(std::string id) throw(
	std::invalid_argument) {
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}
inline void Relation_User_AccessPoint::setPeriod(uint period) throw(
	std::invalid_argument) {
	this->period = period;
}
inline void Relation_User_AccessPoint::setPeriod(std::string period) throw(
	std::invalid_argument) {
	try {
		this->period = stoi(period, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}


inline void Relation_User_AccessPoint::setStart(Timestamp& start) throw(
	std::invalid_argument) {
	this->start = start;
}
inline void Relation_User_AccessPoint::setStart(std::string start) throw(
	std::invalid_argument) {
	try {
		this->start.setAll(start);
	} catch (std::invalid_argument e) {
		throw e;
	}
}

inline void Relation_User_AccessPoint::setUser(User& user) throw(
	std::invalid_argument) {
	std::tr1::shared_ptr<User> user_pointer(new User(user));
	this->user = user_pointer;
}
inline void Relation_User_AccessPoint::setAccessPoint(
	AccessPoint& accessPoint) throw(std::invalid_argument) {
	std::tr1::shared_ptr<AccessPoint> accessPoint_pointer(
		new AccessPoint(accessPoint));
	this->accessPoint = accessPoint_pointer;
}

inline void Relation_User_AccessPoint::setSundey(const std::vector<char>& v) {
	if (v.size() != 24)
		throw std::invalid_argument("[entities_models] setSaturday");

	this->sunday = v;
}
inline void Relation_User_AccessPoint::setMonday(const std::vector<char>& v) {
	if (v.size() != 24)
		throw std::invalid_argument("[entities_models] setMonday");

	this->monday = v;
}
inline void Relation_User_AccessPoint::setTuesday(const std::vector<char>& v) {
	if (v.size() != 24)
		throw std::invalid_argument("[entities_models] setTuesday");

	this->tuesday = v;
}
inline void Relation_User_AccessPoint::setWednesday(const std::vector<char>& v) {
	if (v.size() != 24)
		throw std::invalid_argument("[entities_models] setWednesday");

	this->wednesday = v;
}
inline void Relation_User_AccessPoint::setThursday(const std::vector<char>& v) {
	if (v.size() != 24)
		throw std::invalid_argument("[entities_models] setThursday");

	this->thursday = v;
}
inline void Relation_User_AccessPoint::setFriday(const std::vector<char>& v) {
	if (v.size() != 24)
		throw std::invalid_argument("[entities_models] setFriday");

	this->friday = v;
}
inline void Relation_User_AccessPoint::setSaturday(const std::vector<char>& v) {
	if (v.size() != 24)
		throw std::invalid_argument("[entities_models] setSaturday");

	this->saturday = v;
}

// get

inline uint Relation_User_AccessPoint::getId() {
	return id;
}
inline uint Relation_User_AccessPoint::getPeriod() {
	return period;
}

inline Timestamp* Relation_User_AccessPoint::getStart() {
	return &start;
}

inline User& Relation_User_AccessPoint::getUser() {
	return *user;
}
inline AccessPoint& Relation_User_AccessPoint::getAccessPoint() {
	return *accessPoint;
}


///////////////////////////////////////////////////////////////////////////
// RegisterKit ///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

/////set
inline void RegisterKit::setId(std::string id) throw(std::invalid_argument) {
	try {
		this->id = stoi(id, nullptr);
	} catch (std::invalid_argument e) {
		std::stringstream out;
		out << "Parametro " << id
			<< " de entrada Access::setId não numerico :" << e.what();
		throw std::invalid_argument(out.str().c_str());
	}
}

inline void RegisterKit::setId(uint id) throw(std::invalid_argument) {
	this->id = id;
}

inline void RegisterKit::setName(std::string name) throw(std::invalid_argument) {
	this->name = name;
}

inline void RegisterKit::setIpAddress(IpAddress ip) throw(
	std::invalid_argument) {
	this->ipAddress = ip;
}

// get
inline uint RegisterKit::getId() const {
	return id;
}

inline std::string RegisterKit::getName() const {
	return name;
}

inline IpAddress* RegisterKit::getIpAddress() {
	return &ipAddress;
}
