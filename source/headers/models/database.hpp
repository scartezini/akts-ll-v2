/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se lidar com portas e sockets do SO.
 */
#ifndef DATABASE_HXX
#define DATABASE_HXX

#include <string>
#include <memory>   // std::unique_ptr
#include <cstdlib>  // std::exit
#include <iostream>

#include "../../libs/output/IOMananger.hpp"

#include <odb/database.hxx>

#define DATABASE_SQLITE

#if defined(DATABASE_MYSQL)
    #include <odb/mysql/database.hxx>
#elif defined(DATABASE_SQLITE)
    #include <odb/connection.hxx>
    #include <odb/transaction.hxx>
    #include <odb/schema-catalog.hxx>
    #include <odb/sqlite/database.hxx>
#elif defined(DATABASE_PGSQL)
    #include <odb/pgsql/database.hxx>
#elif defined(DATABASE_ORACLE)
    #include <odb/oracle/database.hxx>
#elif defined(DATABASE_MSSQL)
    #include <odb/mssql/database.hxx>
#else
    #error unknown database; did you forget to define the DATABASE_* macros?
#endif

#include "../../../bin/source/models/entities_models-odb.hxx"
#include "../../../bin/source/models/system_models-odb.hxx"


namespace database_monkey{
    template <typename T> uint persist(T &var) throw (odb::exception);
    template <typename T> void update(T &var) throw (odb::exception);
    template <typename T> void remove(T &var) throw (odb::exception);
    std::unique_ptr<odb::database> open_database();
    void create_database();
}



///////////////////////////////////////////////////////////////
/// inline
///////////////////////////////////////////////////////////////

inline std::unique_ptr<odb::database>
database_monkey::open_database ()
{
    using namespace std;
    using namespace odb::core;

    #if defined(DATABASE_MYSQL)
        return std::unique_ptr<odb::database>(new odb::mysql::database (argc, argv));
    #elif defined(DATABASE_SQLITE)
        return std::unique_ptr<odb::database>(new odb::sqlite::database(io::configs.database_name, SQLITE_OPEN_READWRITE));
    #elif defined(DATABASE_PGSQL)
        return new odb::pgsql::database (argc, argv);
    #elif defined(DATABASE_ORACLE)
        return new odb::oracle::database (argc, argv);
    #elif defined(DATABASE_MSSQL)
        return new odb::mssql::database (argc, argv);
    #endif
}


inline void
database_monkey::create_database ()
{
  using namespace std;
  using namespace odb::core;


#if defined(DATABASE_MYSQL)
  unique_ptr<database> db (new odb::mysql::database (argc, argv));
#elif defined(DATABASE_SQLITE)
    ifstream obj(io::configs.database_name);

    if(!obj){

        unique_ptr<database> db (
        new odb::sqlite::database (
           io::configs.database_name, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE));



        // Create the database schema. Due to bugs in SQLite foreign key
        // support for DDL statements, we need to temporarily disable
        // foreign keys.
        //
        {
            connection_ptr c (db->connection ());

            c->execute ("PRAGMA foreign_keys=OFF");

            transaction t (c->begin ());
            schema_catalog::create_schema (*db);
            t.commit ();

            c->execute ("PRAGMA foreign_keys=ON");
        }


		{
			transaction t (db->begin());

            DataType dt;

            dt.setName("NFC");
    		dt.setSensor("0");
    		dt.setModel("Proprietario");
			db->persist(dt);

            dt.setName("RFID");
    		dt.setSensor("1");
    		dt.setModel("Proprietario");
			db->persist(dt);

            dt.setName("BIOMETRIC");
            dt.setSensor("2");
            dt.setModel("Proprietario");
            db->persist(dt);

			t.commit();
		}
    }

    obj.close();
#elif defined(DATABASE_PGSQL)
  unique_ptr<database> db (new odb::pgsql::database (argc, argv));
#elif defined(DATABASE_ORACLE)
  unique_ptr<database> db (new odb::oracle::database (argc, argv));
#elif defined(DATABASE_MSSQL)
  unique_ptr<database> db (new odb::mssql::database (argc, argv));
#endif

}


template<typename T>
inline uint database_monkey::persist(T &var) throw (odb::exception){
    unique_ptr<odb::database> db  = database_monkey::open_database();
    odb::transaction t (db->begin());
    uint id = db->persist(var);
    t.commit();
    db.release();
    return id;
}

template<typename T>
inline void database_monkey::update(T &var) throw (odb::exception){
    unique_ptr<odb::database> db  = database_monkey::open_database();
    odb::transaction t (db->begin());
    db->update(var);
    t.commit();
    db.release();
}

template<typename T>
inline void database_monkey::remove(T &var) throw (odb::exception){
    unique_ptr<odb::database> db  = database_monkey::open_database();
    odb::transaction t (db->begin());
    db->erase(var);
    t.commit();
    db.release();
}


#endif // DATABASE_HXX
