/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef BASE_MODEL_H
#define BASE_MODEL_H

// includes
#include <sqlite3.h>
#include <climits>
#include <cstring>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <cstdlib>  // std::exit
#include <iostream>
#include <memory>  // std::unique_ptr
#include <string>

#include "../../libs/output/ContextVariables.hpp"
#include "../../libs/output/IOMananger.hpp"
#include "../ConfigComp.hpp"

#include <odb/database.hxx>

#define USE_DATABASE_SQLITE

#if defined(DATABASE_MYSQL)
#include <odb/mysql/database.hxx>
#elif defined(USE_DATABASE_SQLITE)
#include <odb/connection.hxx>
#include <odb/schema-catalog.hxx>
#include <odb/sqlite/database.hxx>
#include <odb/transaction.hxx>
#elif defined(DATABASE_PGSQL)
#include <odb/pgsql/database.hxx>
#elif defined(DATABASE_ORACLE)
#include <odb/oracle/database.hxx>
#elif defined(DATABASE_MSSQL)
#include <odb/mssql/database.hxx>
#else
#error unknown database; did you forget to define the DATABASE_* macros?
#endif

#include "entities_models-odb.hxx"
#include "system_models-odb.hxx"

// files
#include "../../libs/json_parser/json.h"

#include "../../libs/output/IOMananger.hpp"
// defines

// namespace
using namespace std;

// program
class BaseModel : public Variables {
	Config config;

  public:
	void saveConfigs();
	string createConfigFile();
	void deleteConfigFile();
};

//------------------------------------------------------------------------------
/**	\namespace database_monkey
\brief XXXXXXXXXXXXXX namespace que guarda as funcoes basicas de respostas http
relacoes de pontos de acesso com usuarios por meio de uma interface restfull a
qual faz dos metodos que sao permitidos para um acesso, o que spor sua vez se da
pelos sequinte metodos: POST, DELETE, PUT. Estes por sua vez sequem o padrao de
json espesificados em \ref relation_access_point_user_json.
**/
//------------------------------------------------------------------------------
namespace database_monkey {
	template <typename T>
	uint persist(T &var) throw(odb::exception);
	template <typename T>
	void update(T &var) throw(odb::exception);
	template <typename T>
	void remove(T &var) throw(odb::exception);
	std::unique_ptr<odb::database> open_database();
	void create_database();
}

///////////////////////////////////////////////////////////////
/// inline
///////////////////////////////////////////////////////////////

inline std::unique_ptr<odb::database> database_monkey::open_database() {
	using namespace std;
	using namespace odb::core;

#if defined(DATABASE_MYSQL)
	return std::unique_ptr<odb::database>(new odb::mysql::database(argc, argv));
#elif defined(DATABASE_SQLITE)
	return std::unique_ptr<odb::database>(new odb::sqlite::database(
		io::configs.database_name, SQLITE_OPEN_READWRITE));
#elif defined(DATABASE_PGSQL)
	return new odb::pgsql::database(argc, argv);
#elif defined(DATABASE_ORACLE)
	return new odb::oracle::database(argc, argv);
#elif defined(DATABASE_MSSQL)
	return new odb::mssql::database(argc, argv);
#endif
}

inline void database_monkey::create_database() {
	io::configs.database_name = DATABASE_NAME;
	using namespace std;
	using namespace odb::core;

#if defined(DATABASE_MYSQL)
	unique_ptr<database> db(new odb::mysql::database(argc, argv));
#elif defined(DATABASE_SQLITE)
	ifstream obj(io::configs.database_name);

	if (!obj) {
		unique_ptr<database> db(new odb::sqlite::database(
			io::configs.database_name,
			SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE));

		{
			connection_ptr c(db->connection());

			c->execute("PRAGMA foreign_keys=OFF");

			transaction t(c->begin());
			schema_catalog::create_schema(*db);
			t.commit();

			c->execute("PRAGMA foreign_keys=ON");
		}

		{
			transaction t(db->begin());

			DataType dt;

			dt.setName("NFC");
			dt.setSensor("0");
			dt.setModel("Proprietario");
			db->persist(dt);

			dt.setName("RFID");
			dt.setSensor("1");
			dt.setModel("Proprietario");
			db->persist(dt);

			dt.setName("BIOMETRIC");
			dt.setSensor("2");
			dt.setModel("Proprietario");
			db->persist(dt);

			t.commit();
		}
	}

	obj.close();
#elif defined(DATABASE_PGSQL)
	unique_ptr<database> db(new odb::pgsql::database(argc, argv));
#elif defined(DATABASE_ORACLE)
	unique_ptr<database> db(new odb::oracle::database(argc, argv));
#elif defined(DATABASE_MSSQL)
	unique_ptr<database> db(new odb::mssql::database(argc, argv));
#endif
}

template <typename T = Entity>
inline uint database_monkey::persist(T &var) throw(odb::exception) {
	var.activated = true;

	unique_ptr<odb::database> db = database_monkey::open_database();
	odb::transaction t(db->begin());

	uint id = db->persist(var);
	t.commit();
	db.release();
	return id;
}

template <typename T = Entity>
inline void database_monkey::update(T &var) throw(odb::exception) {
	unique_ptr<odb::database> db = database_monkey::open_database();
	odb::transaction t(db->begin());

	db->update(var);
	t.commit();
	db.release();
}

template <typename T = Entity>
inline void database_monkey::remove(T &var) throw(odb::exception) {
	var.activated = false;

	unique_ptr<odb::database> db = database_monkey::open_database();
	odb::transaction t(db->begin());
	db->update(var);
	t.commit();
	db.release();
}

#endif
