/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef ACTION_MODELS_H
#define ACTION_MODELS_H

#include <climits>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <odb/core.hxx>
#include <odb/database.hxx>
#include <odb/lazy-ptr.hxx>
#include <odb/transaction.hxx>

#include "../../libs/json_parser/json.h"
#include "../BaseTypes.hpp"
#include "../Entities.hpp"

typedef unsigned int uint;

using namespace odb::core;
using odb::database;
using odb::transaction;

#pragma db object
class Action {
  public:
	Action();

	enum TYPE { POST = 0, PUT = 1, DELETE = 2, GET = 3, SYNC = 4, LOGIN = 5 };
	enum TABLE {
		USER = 0,
		STUFF = 1,
		ACCESS = 2,
		ACCESS_POINT = 3,
		DATA = 4,
		DATA_TYPE = 5,
		RELATION_ACCESSPOINT_USER = 6,
		RELATION_ACCESSPOINT_DATA = 7,
		ROUTES = 8,
		REGISTER_KIT = 9,
		OTHER = 10
	};

	enum DEST { HIGH = 0, LOW = 1, LOCAL = 2 };

#pragma db id auto
	uint id;

#pragma db type("INT")
	TYPE type;

#pragma db type("INT")
	TABLE table;

#pragma db type("INT")
	DEST dest;

	std::string route;
	std::string body;

	int id_1;
	int id_2;

#pragma db type("INT")
	double exec_time;

	uint exec;

	bool success;
	bool running;

	void setSuccess();
	void setExecuted();
};

#pragma db object
class Config {
  public:
	Config();
	Config(std::string);

	bool activated;
#pragma db id auto
	uint id;
#pragma db type("VARCHAR(2000)")
	std::string json;
};


class Log {
	std::string line;
	std::vector<std::string> args;

  public:
	Log(std::string);
	std::string parse();
	std::string decodeLog(std::string);
};

#endif
