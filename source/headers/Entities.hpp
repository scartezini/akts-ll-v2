/** \file ServerDriver.hpp
 *  \brief Arquivo responsavel poelo armazenamento das funcoes/modulos basico do
 * servers.
 *
 *  Arquivo que possui todas as funcoes basicas necessarias para o uso de um
 * server
 *  udp ou udp em ambinetes linux, sendo estas funcoes de principalemente de se
 * lidar com portas e sockets do SO.
 */
#ifndef ENTITIES_H
#define ENTITIES_H

#include <climits>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#include <exception>

#include "../libs/json_parser/json.h"
#include "./BaseTypes.hpp"
#include "./ConfigComp.hpp"

typedef unsigned int uint;

class Entity {
  public:
	virtual std::string toJson() = 0;
	virtual void fromJson(std::string) = 0;
};

class ShortEntity {
  public:
	virtual std::string toShortJson() = 0;
};

namespace EntityFunc {
	////////////////////////////////////
	// Entity
	////////////////////////////////////

	template <typename E = Entity>
	std::string vector2json(std::vector<E> entities) {
		std::string json = "{entities=[";

		for (typename std::vector<E>::iterator i = entities.begin();
			 i != entities.end(); i++) {
			json += i->toJson();
			json += ",";
		}
		json += "]}";

		return json;
	}

	template <typename E = Entity>
	void vectorFromJson(std::string key, std::string body,
						std::vector<E>& entities) {
		Json::Value parsed;
		Json::Reader reader;

		if (reader.parse(body, parsed)) {
			if (parsed[key] != Json::nullValue) {
				const Json::Value values = parsed[key];

				for (uint i = 0; i < values.size(); ++i) {
					E entiny;
					entiny.fromJson(values[i].asString());
					entities.push_back(entiny);
				}
			}
		}
	}

	template <typename SE = ShortEntity>
	std::string vector2shortJson(std::vector<SE> entities) {
		std::string json = "{entities=[";

		for (typename std::vector<SE>::iterator i = entities.begin();
			 i != entities.end(); i++) {
			json += i->toShortJson();
			json += ",";
		}
		json += "]}";

		return json;
	}
}

#endif
