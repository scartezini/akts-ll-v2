#ifndef VARIABLES_H
#define VARIABLES_H

#include <string>

class Variables {
  public:
	// cookie
	std::string cookie;

	// user password
	std::string login;
	std::string password;

	// start configs
	unsigned int num_actions_routers;
	unsigned int num_http_routers;

	// server
	std::string server_interface;

	// udp server
	uint udp_port;
	uint num_try_udp_get_connection;
	bool force_udp_connection;
	bool udp_online;

	// tcp
	uint tcp_port;
	uint tcp_connect_trys;

	std::string high_ip;


	// database
	std::string database_name;

	// sensors
	uint kit_sensor_id;
	uint kit_max_trys;
	uint kit_led_id;  // TODO

	std::vector<int> sensors_types;  // TODO
	std::vector<int> sensors_ports;  // TODO

	uint sensor_start_max_trys;
};

#endif
