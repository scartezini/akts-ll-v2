#include "IOMananger.hpp"

// Start screen
void Output::makeMap() {
	Screen *main_messages = (Screen *)new MessageBox(
		"main_messages", 0, 0, 35, 80, io::configs.windows_lines);
	screen.insert(std::pair<string, Screen *>("main_messages", main_messages));
	Screen *progress_bar = (Screen *)new ProgressBar(
		"progress_bar", 0, 80, 5, 60, ProgressBar::HORIZONTAL, 0, 100,
		io::configs.windows_lines);
	screen.insert(std::pair<string, Screen *>("progress_bar", progress_bar));
}

void Configs::decodeMap() {
	// start configs
	if (json["login"] != Json::nullValue)
		this->login = json["login"].asString();
	else
		this->login = std::string("admin");

	if (json["password"] != Json::nullValue)
		this->password = json["password"].asString();
	else
		this->password = std::string("admin");

	if (json["num_actions_routers"] != Json::nullValue)
		this->num_actions_routers = json["num_actions_routers"].asInt();
	else
		this->num_actions_routers = 10;

	if (json["num_http_routers"] != Json::nullValue)
		this->num_http_routers = json["num_http_routers"].asInt();
	else
		this->num_http_routers = 10;

	// server
	if (json["server_interface"] != Json::nullValue)
		this->server_interface = json["server_interface"].asString();
	else
		this->server_interface = "wlp8s0";

	// udp server
	if (json["udp_online"] != Json::nullValue)
		this->udp_online = json["udp_online"].asBool();
	else
		this->udp_online = true;

	if (json["udp_port"] != Json::nullValue)
		this->udp_port = json["udp_port"].asInt();
	else
		this->udp_port = 6000;

	if (json["num_try_udp_get_connection"] != Json::nullValue)
		this->num_try_udp_get_connection =
			json["num_try_udp_get_connection"].asInt();
	else
		this->num_try_udp_get_connection = 5000;

	if (json["force_udp_connection"] != Json::nullValue)
		this->force_udp_connection = json["force_udp_connection"].asBool();
	else
		this->force_udp_connection = true;

	// tcp server
	if (json["tcp_port"] != Json::nullValue)
		this->tcp_port = json["tcp_port"].asInt();
	else
		this->tcp_port = 10000;

	if (json["tcp_connect_trys"] != Json::nullValue)
		this->tcp_connect_trys = json["tcp_connect_trys"].asInt();
	else
		this->tcp_connect_trys = 5000;

	// tcp server
	if (json["high_ip"] != Json::nullValue)
		this->high_ip = json["high_ip"].asString();
	else
		this->high_ip = "10.190.60.25";

	this->database_name = "./resources/monkey.db";

	// kit sensors
	if (json["kit_sensor_id"] != Json::nullValue)
		this->kit_sensor_id = json["kit_sensor_id"].asInt();
	else
		this->kit_sensor_id = 0;

	if (json["kit_led_id"] != Json::nullValue)
		this->kit_led_id = json["kit_led_id"].asInt();
	else
		this->kit_led_id = 0;

	if (json["kit_max_trys"] != Json::nullValue)
		this->kit_max_trys = json["kit_max_trys"].asInt();
	else
		this->kit_max_trys = 10000;

	if (json["sensor_start_max_trys"] != Json::nullValue)
		this->sensor_start_max_trys = json["sensor_start_max_trys"].asInt();
	else
		this->sensor_start_max_trys = 10000;

	if (json["sensors_types"] != Json::nullValue) {
		const Json::Value sensors_types = json["sensors_types"];
		for (int index = 0; index < sensors_types.size();
			 ++index)  // Iterates over the sequence elements.
			this->sensors_types.push_back(sensors_types[index].asInt());
	}

	if (json["sensors_ports"] != Json::nullValue) {
		const Json::Value sensors_ports = json["sensors_ports"];
		for (int index = 0; index < sensors_ports.size();
			 ++index)  // Iterates over the sequence elements.
			this->sensors_ports.push_back(sensors_ports[index].asInt());
	}
}


// Logger make Map
void Logger::makeMap() {
}
