// includes
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

// files
#include "./libs/json_parser/json.h"
#include "./libs/output/IOMananger.hpp"

#include "./headers/http/UdpServer.hpp"
#include "./headers/mananger/TaskMananger.hpp"
#include "./headers/models/base_models.hpp"

#include "./headers/io_core/Actuator.hpp"
#include "./headers/io_core/IO-Core.hpp"
#include "./headers/io_core/Sensor.hpp"

#include "./headers/ConfigComp.hpp"

// defines

// namespace
using namespace std;

// main
int main(int argc, char *argv[]) {
	database_monkey::create_database();

	BaseModel model;
	// verify input
	if (argc < 2) {
		string file = model.createConfigFile();
		io::configs.runFile(file.c_str());
	} else {
		model.createConfigFile();
		io::configs.openFile(argv[1]);
	}

	io::configs.setConfigs();
	thread io_th(run_io);

	model.saveConfigs();

	// start sensors
	Sensor::startAll();
	Actuator::startAll();

	// start queue
	queues::actions.start();

	// start the upd
	UdpServer udp_server;

	// start the router mananger
	TaskMananger task_mananger;
	// começando as threads de recebimento
	task_mananger.start();

#ifdef COMPL_SERVER
	task_mananger.feed_server();
#endif

#ifdef CASE
	task_mananger.feed_case();
#endif
}
