############################################################################################################
#Makefile
#TODO mudar os paths dos headers e dos src para parametros
#
#
#
#
#
############################################################################################################
#normal flags
############################################################################################################

CC = g++
HEADER_PATH = source/header

SRC_PATH = source
BIN_PATH = bin
REL_PATH = release
DEP_PATH = dep
EXEC_NAME = monkey_exec
SINT_FILES = $(SRC_PATH)/auto_files

#main
_OBJ = main.o

#context
_OBJ += ./src/Entities.o

_OBJ += ./src/mananger/TaskMananger.o
_OBJ += ./src/mananger/RouteMananger.o
_OBJ += ./src/mananger/QueueMananger.o
_OBJ += ./src/mananger/Routes.o

_OBJ += ./src/io_core/IO-Core.o
_OBJ += ./src/io_core/Actuator.o
_OBJ += ./src/io_core/Sensor.o

_OBJ += ./src/controllers/system_controllers.o
_OBJ += ./src/controllers/entities_controllers.o
_OBJ += ./src/controllers/sensor_controllers.o
_OBJ += ./src/controllers/system_actors_controller.o

_OBJ += ./src/http/UdpServer.o

_OBJ += ./src/drivers/UdpDriver.o
_OBJ += ./src/drivers/ServerDriver.o

_OBJ += ./src/http/HttpEntities.o
_OBJ += ./src/http/HttpErrors.o

_OBJ += ./src/models/base_models.o
_OBJ += ./src/models/entities_models.o
_OBJ += ./src/models/system_models.o


#libs
_LIBS_O += ./libs/json_parser/jsoncpp.o
_LIBS_O += ./libs/output/IOMananger.o
_LIBS_O += ./libs/output/ContextBuild.o
_LIBS_O += ./libs/output/BaseTypes.o
_LIBS_O += ./libs/output/Entities.o
_OBJ += $(_LIBS_O)

OBJ = $(patsubst %,$(BIN_PATH)/%,$(_OBJ))


#files
_FILES = ./headers/models/entities_models.cxx
_FILES += ./headers/models/system_models.cxx
_FILES += ./headers/BaseTypes.cxx

FILES = $(patsubst %,$(SINT_FILES)/%-odb.cxx, $(basename $(_FILES)))
OBJ += $(patsubst %,$(BIN_PATH)/%.oxx, $(basename $(FILES)))
OBD_DIRS = $(patsubst %,-I $(SINT_FILES)/%, $(dir $(_FILES)))

#script

#directives

DATABASE = -DDATABASE_SQLITE
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEP_PATH)/$*.Td $(DATABASE)
#MEM_FLAGS = --param ggc-min-expand=0 --param ggc-min-heapsize=8192

OPENCV = `pkg-config --libs opencv` -O4
PTHREAD = -pthread
CPPNETLIB = -lboost_system -lboost_thread -lcppnetlib-server-parsers -lcppnetlib-client-connections -lcppnetlib-uri

ODB_PATH =  -I /usr/lib/odb/x86_64-linux-gnu/include -I source/headers/models -I source/headers/ -I ./bin/models/
ODB = -L/usr/local/lib -lodb-sqlite -L/usr/lib/odb -lodb

LIBS = $(PTHREAD) $(CPPNETLIB) $(ODB_PATH) $(ODB)

ODB_CC = odb
ODB_FLAGS = --generate-query --generate-schema --std c++11 --database sqlite
ODB_DIR = -I $(SRC_PATH)/$(dir  $*<) -I $(SINT_FILES)
TEST_LIBS = -lcppunit -ftest-coverage -fprofile-arcs

DIRECTIVES = -c -std=c++11 $(MEM_FLAGS) -I $(HEADER_PATH) -I $(SINT_FILES) $(OBD_DIRS) $(ODB_PATH)

#script final
FINAL_EXEC = $(REL_PATH)/$(EXEC_NAME)
all: post-build

post-build: pre-build
	@$(MAKE) --silent execs

pre-build:
	@$(MAKE) --silent files

files: $(FILES)
execs: $(FINAL_EXEC)

$(FINAL_EXEC): $(OBJ)
	@echo linking $@
	@mkdir -p $(REL_PATH)
	@$(CC) -o $@ -std=c++11  $^ $(LIBS)
	@echo AllDone

$(BIN_PATH)/%.o: $(SRC_PATH)/%.cpp
	@echo compiling $@
	@mkdir -p $(DEP_PATH)/$(dir $*)  $(BIN_PATH)/$(dir $*)
	@$(CC) $(DEPFLAGS) -c $(DIRECTIVES) -o $@ $<
	@mv -f $(DEP_PATH)/$*.Td $(DEP_PATH)/$*.d

$(SINT_FILES)/%-odb.cxx: $(SRC_PATH)/%.hpp
	@echo gen $@
	@mkdir -p $(SINT_FILES)/$(dir $*)
	@$(ODB_CC) $(ODB_FLAGS) -o $(dir $@) $<


$(BIN_PATH)/%.oxx:  %.cxx
	@echo compiling $@
	@mkdir -p $(DEP_PATH)/$(dir $*)  $(BIN_PATH)/$(dir $*)
	@$(CC) $(DEPFLAGS) -c $(DIRECTIVES) -o $@ $<
	@mv -f $(DEP_PATH)/$*.Td $(DEP_PATH)/$*.d

debug: DIRECTIVES += -ggdb
debug: all

.PHONY: clean
clean:
	-@rm -r -f $(BIN_PATH)
	-@rm -r -f $(REL_PATH)
	-@rm -r -f $(SINT_FILES)
	-@rm -r -f $(LOGS)

$(DEP_PATH)/%.d: ;
.PRECIOUS: $(DEP_PATH)/%.d

-include $(patsubst %,$(DEP_PATH)/%.d,$(basename $(_OBJ)))


############################################################################################################
#Make the documentation
############################################################################################################
SOURCE_ROOT = ./doc_gen
DOXY_STYLE_PLUS_PATH = ./doc_code/template
documentation: ./doc_code/doxygen.cfg
	doxygen ./doc_code/doxygen.cfg
	cp ./doc_code/doxygen-bootstrapped/doxy-boot.js ./doc_gen/html/

##############################################################################################################
#Make gcov
##############################################################################################################
gcov:
	gcov source/srs/models/*.cpp
